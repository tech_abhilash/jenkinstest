"---------*Simplotel Booking Engine Path*------------"
webSite_url = "http://www.habitathotels.com/"
book_now_button_path = "/html/body/div[1]/div[1]/div[1]/div/div/form/div[10]/div/input"
check_availability_button_path = "/html/body/section/div/div[2]/div/div[1]/div/div[2]/div/div/div[1]/div/div[3]/div/div/button" 
pay_price_path = "/html/body/section/div/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[4]/div/div/div[2]/p"	
modify_button_path = "/html/body/section/div/div[2]/div/div[2]/div/div[1]/div[2]/div/div[2]/button"
proceed_button_path = "/html/body/section/div/div[2]/div/div[2]/div/div[3]/div/div/div/div[2]/div/button[1]"

"---------*MMT WedSite Path*------------------"
city_name = "XCO"
hotel_id = "200701131408174293"
check_in = "10112015" #MMDDYYYY
check_out = "10122015" #MMDDYYYY
hotel_name_path = "/html/body/div[2]/div[7]/div[4]/div[1]/div[1]/div[1]/div/div[1]/p[1]/a"
select_room_path = "/html/body/div[2]/div[7]/div[4]/div[1]/div[2]/div/a"
mmt_pay_price_path = "/html/body/div[3]/div[7]/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div[7]/div[3]/p[1]"
