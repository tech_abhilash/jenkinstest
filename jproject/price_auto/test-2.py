# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
from selenium.common.exceptions import TimeoutException
from timeit import Timer
import unittest, time, re
import single_hotel_data


class Untitled2(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://www.eagleridgeindia.com/" 
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True
    
    
    def test_single_hotel_publish(self):
        driver = self.driver
        with open("hotel_price_data.txt",'a') as f:
            f.write("\n\n   Our Price \n    Room Name | Search Price | Pay Price \n")
            f.close() 
        i = 1
        while 1:
            driver.get(self.base_url + "")
            driver.maximize_window()
            time.sleep(10)
            driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[1]/div/div/form/div[6]/div/input").click()
            path = "/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/div[5]/div/div/div/div[1]/div["+str(i)+"]/table[1]/tbody/tr/td[2]/h2"
            x = self.check_exists_by_xpath(path)
            if (x == True):
                room_name = driver.find_element_by_xpath("/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/div[5]/div/div/div/div[1]/div["+str(i)+"]/table[1]/tbody/tr/td[2]/h2").text
                driver.find_element_by_xpath("/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/div[5]/div/div/div/div[1]/div["+str(i)+"]/table[1]/tbody/tr/td[2]/div[3]/table/tbody/tr/td[1]/table/tbody/tr/td[3]/img").click()
                time.sleep(10)                
                search_price =  driver.find_element_by_xpath("/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/div[5]/div/div/div/div[1]/div["+str(i)+"]/table[2]/tbody/tr/td[6]/span[2]").text
                pay_price =  driver.find_element_by_xpath("/html/body/table/tbody/tr[2]/td/table/tbody/tr/td/form/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr[6]/td/div/table/tbody/tr[1]/td[3]/span").text
                with open("hotel_price_data.txt",'a') as f:
                    f.write("     " + room_name + " | " + search_price + " | " + pay_price + "\n")
                    f.close()
                i = i + 1 
            else:
                break
        print done
        time.sleep(10)
       # hotel_name = driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[4]/div[1]/div[1]/div[1]/div/div[1]/p[1]/a").text
        # with open("hotel_price_data.txt",'w') as f:
        #     f.write(hotel_name + "\n    MMT Price \n    Room Name | Search Price | Pay Price \n")
        #     f.close()   
        # i = 1
        # path = "/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[4]/div/div[3]/div[1]/div/div/div[1]/div[1]/div[1]/span/h2[2]"
        # x = self.check_exists_by_xpath(path)
        # if (x == True):
        #     while 1:
        #         driver.get(self.base_url + "")
        #         driver.maximize_window()
        #         time.sleep(10)
        #         driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[4]/div[1]/div[2]/div/a").click()
        #         time.sleep(10)
        #         path = "/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[4]/div/div[3]/div[1]/div/div/div["+str(i)+"]/div[1]/div[1]/span/h2[2]"
        #         x = self.check_exists_by_xpath(path)
        #         if (x == True):
        #             room_name = driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[4]/div/div[3]/div[1]/div/div/div[1]/div[1]/div[1]/span/h2[2]").text
        #             search_price = driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[4]/div/div[3]/div[1]/div/div/div[1]/div[3]/div/div[1]/p[1]").text
        #             driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[4]/div/div[3]/div[1]/div/div/div[1]/div[4]/span/span[1]/a").click()
        #             time.sleep(10)
        #             pay_price = driver.find_element_by_xpath("/html/body/div[3]/div[7]/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div[7]/div[3]/p[1]").text
        #             with open("hotel_price_data.txt",'a') as f:
        #                 f.write("     " + room_name + " | " + search_price + " | " + pay_price + "\n")
        #                 f.close()
        #             i = i + 1
        #         else:
        #             break
        # else:

        #     while 1:
        #         driver.get(self.base_url + "")
        #         driver.maximize_window()
        #         time.sleep(10)
        #         driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[4]/div[1]/div[2]/div/a").click()
        #         time.sleep(10)
        #         path = "/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[5]/div/div[3]/div[1]/div["+str(i)+"]/div[1]/h2[2]"
        #         x = self.check_exists_by_xpath(path)
        #         if (x == True):
        #             room_name = driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[5]/div/div[3]/div[1]/div["+str(i)+"]/div[1]/h2[2]").text
        #             search_price = driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[5]/div/div[3]/div[1]/div["+str(i)+"]/div[2]/div[1]/div/div/div[2]/div/div/div[3]/div/div[1]/p[1]").text
        #             driver.find_element_by_xpath("/html/body/div[2]/div[7]/div[6]/div[2]/div[1]/div[5]/div/div[3]/div[1]/div["+str(i)+"]/div[2]/div[1]/div/div/div[2]/div/div/div[4]/span[2]/a").click()
        #             time.sleep(10)
        #             pay_price = driver.find_element_by_xpath("/html/body/div[3]/div[7]/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div[7]/div[3]/p[1]").text
        #             with open("hotel_price_data.txt",'a') as f:
        #                 f.write("     " + room_name + " | " + search_price + " | " + pay_price + "\n")
        #                 f.close()
        #             i = i + 1
        #         else:
        #             break


    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

