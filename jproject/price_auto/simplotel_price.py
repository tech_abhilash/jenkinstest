# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
from selenium.common.exceptions import TimeoutException
from timeit import Timer
import unittest, time, re
import data


class Untitled2(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = data.webSite_url 
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True
    
    
    def test_single_hotel_publish(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.maximize_window()
        time.sleep(10)
        with open("hotel_price_data.txt",'a') as f:
            f.write("\n\n   Our Price \n    Room Name | Search Price | Pay Price \n")
            f.close() 
        i = 1                           
        driver.find_element_by_xpath(data.book_now_button_path).click()
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        driver.maximize_window()
        time.sleep(10)
        driver.find_element_by_xpath(data.check_availability_button_path).click()
        time.sleep(5)    
        while 1:
            path = "/html/body/section/div/div[2]/div/div[2]/div/div[2]/div/div/div["+str(i)+"]/div[1]/div/div/div[1]/div/div/p[1]"
            x = self.check_exists_by_xpath(path)
            if (x == True):                               
                room_name = driver.find_element_by_xpath("/html/body/section/div/div[2]/div/div[2]/div/div[2]/div/div/div["+str(i)+"]/div[1]/div/div/div[1]/div/div/p[1]").text
                driver.find_element_by_xpath("/html/body/section/div/div[2]/div/div[2]/div/div[2]/div/div/div["+str(i)+"]/div[2]/div[2]/div/div/div/span/i[1]/span").click()
                time.sleep(3)               
                search_price =  driver.find_element_by_xpath("/html/body/section/div/div[2]/div/div[2]/div/div[2]/div/div/div["+str(i)+"]/div[2]/div[1]/div/div/div/div[1]/div/div/div[2]/div/div[1]/div[1]/div[2]/span").text
                driver.find_element_by_xpath("/html/body/section/div/div[2]/div/div[2]/div/div[2]/div/div/div["+str(i)+"]/div[2]/div[1]/div/div/div/div[1]/div/div/div[2]/div/div[2]/button").click()
                time.sleep(3)
                pay_price = driver.find_element_by_xpath(data.pay_price_path).text
                driver.find_element_by_xpath(data.modify_button_path).click()
                time.sleep(2)
                driver.find_element_by_xpath(data.proceed_button_path).click()
                with open("hotel_price_data.txt",'a') as f:
                    f.write("     " + room_name + " | " + search_price + " | " + pay_price + "\n")
                    f.close()
                i = i + 1
                time.sleep(5) 
            else:
                break
        print "done"
        time.sleep(10)


        
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()



        # driver.find_element_by_id("from_city_data_box").clear()
        # driver.find_element_by_id("from_city_data_box").send_keys("The Infantry Hotel, IN")
        # time.sleep(3)
        # driver.find_element_by_xpath("//div[@id='nightAnchor']/div/p[2]").click()
        # driver.find_element_by_link_text("1").click()
        # time.sleep(3)
        # driver.find_element_by_id("hotels_submit").click()
        # driver.find_element_by_name("userid").clear()
        # driver.find_element_by_name("userid").send_keys(single_hotel_data.User_Name)
        # driver.find_element_by_name("password").clear()
        # driver.find_element_by_name("password").send_keys(single_hotel_data.Password)
        # driver.find_element_by_xpath("//button[@type='submit']").click()
        # #-------------Creating New Promotions------------------------------------------
        # driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        # driver.find_element_by_link_text(single_hotel_data.hotel_names).click()
        # driver.find_element_by_link_text("Website Content").click()
        # driver.find_element_by_link_text("  Design").click()
        # driver.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[3]/a").click()
        # print driver.find_element_by_xpath("/html/body").get_attribute("class")
        # driver.find_element_by_xpath("/html/body/div[1]/aside[2]/div[2]/div/div[2]/button").click()
        # name = driver.find_element_by_xpath("/html/body").get_attribute("class")
        # t0 = 0
        # path = "/html/body/div[7]/p"
        # while 1:
        #     t0 = time.clock()
        #     x = self.check_exists_by_xpath(path)
        #     print x
        #     if (x == True):
        #         print driver.find_element_by_xpath("/html/body/div[7]/p").text
        #     if (x == False):
        #         print "not"
        #     print t0
        #     if (name != driver.find_element_by_xpath("/html/body").get_attribute("class")):
        #         break
        #     elif (t0 >= 25):
        #         print t0
        #         print "error"
#         #         break
# http://hotelz.makemytrip.com/makemytrip/site/hotels/detail?session_cId=1443533617157&city=BLR&country=IN&checkin=10012015&checkout=10032015&area=&roomStayQualifier=1e0e&hotelId=201407021329227972&showtime=false&hrnMobEnabled=false&hrnHtlAvailable=false&clearFilters=false&cityName=Bangalore&countryName=India&showMTR=true&mtrConversionFactor=3&isNewListing=true
# # http://hotelz.makemytrip.com/makemytrip/site/hotels/detail?session_cId=1443533686276&city=BLR&country=IN&checkin=10012015&checkout=10022015&area=&roomStayQualifier=1e0e&hotelId=201407021329227972
# #         # print "done"
#         time.sleep(10)

    
#     def is_element_present(self, how, what):
#         try: self.driver.find_element(by=how, value=what)
#         except NoSuchElementException, e: return False
#         return True
    
#     def is_alert_present(self):
#         try: self.driver.switch_to_alert()
#         except NoAlertPresentException, e: return False
#         return True
    
#     def close_alert_and_get_its_text(self):
#         try:
#             alert = self.driver.switch_to_alert()
#             alert_text = alert.text
#             if self.accept_next_alert:
#                 alert.accept()
#             else:
#                 alert.dismiss()
#             return alert_text
#         finally: self.accept_next_alert = True
    
#     def tearDown(self):
#         self.driver.quit()
#         self.assertEqual([], self.verificationErrors)

# if __name__ == "__main__":
#     unittest.main()

