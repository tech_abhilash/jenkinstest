# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import single_hotel_data


class Untitled2(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = single_hotel_data.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_single_hotel_publish(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys(single_hotel_data.User_Name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(single_hotel_data.Password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        #-------------Creating New Promotions------------------------------------------
        name = single_hotel_data.hotel_names.split(';')
        n = len(name)
        i = 0
        for i in range(0, n):
            driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
            driver.find_element_by_link_text(name[i]).click()
            driver.find_element_by_link_text("Website").click()
            time.sleep(2)
            driver.find_element_by_link_text("Publish").click()
            driver.find_element_by_xpath(single_hotel_data.pubilsh_input_path).clear()
            driver.find_element_by_xpath(single_hotel_data.pubilsh_input_path).send_keys(single_hotel_data.publish_Comments)
            # driver.find_element_by_id("publish-site").click()
            time.sleep(150)
            print name[i] + " - published"



    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
