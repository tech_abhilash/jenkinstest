import MySQLdb
import datetime
from time import strftime
from decimal import Decimal


def connecting_to_data_base(database_host, database_user_name, database_password, data_base):
    db = MySQLdb.connect(host=database_host, user=database_user_name,
                         passwd=database_password, db=data_base)
    return db

def fetching_hotel_name(cur, property_id):
    cur.execute(
        "SELECT hotel_name FROM sm_hotel_info where hotel_id = %s", [property_id])
    data = cur.fetchall()
    return data[0][0]

def fetching_hotel_website(cur, property_id):
    cur.execute("SELECT attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id = '11'", [
                property_id])
    data = cur.fetchall()
    return data[0][0]

def fetching_room_name(cur, room_id):
    cur.execute("SELECT room_type_name from sm_hotel_room where room_type_id = %s", [room_id])
    data = cur.fetchall()
    return data[0][0]

def fetching_website_rooms(cur, room_id):
    cur.execute("SELECT website_rooms from sm_hotel_room where room_type_id = %s", [room_id])
    data = cur.fetchall()
    return data[0][0]

def fetching_rate_plan_id(cur, property_id):
    cur.execute("SELECT id from sm_rate_plan where hotel_id = %s and enabled = 1",[property_id])
    data = cur.fetchall()
    result = [int(each[0]) for each in data]
    return result

def fetching_rate_plan_status(cur, rate_plan_id):
    cur.execute("SELECT activate from sm_rate_plan where id = %s",[rate_plan_id])
    data = cur.fetchall()
    if data[0][0] == 1:
        return "Active"
    return "In-Active"

def fetching_room_id_mapped_with_rate_id(cur, rate_plan_id):
    cur.execute("SELECT room_type_id from sm_rate_plan_room_type_mapping where rate_plan_id = %s",[rate_plan_id])
    data = cur.fetchall()
    result = [int(each[0]) for each in data]
    return result

def fetching_rate_plan_id_mapped_with_room_id(cur, room_id):
    cur.execute("SELECT id from sm_rate_plan where enabled = 1 and baseplan IS NULL and id in (SELECT rate_plan_id from sm_rate_plan_room_type_mapping where room_type_id = %s and enabled = 1)", [room_id])
    data = cur.fetchall()
    result = [int(each[0]) for each in data]
    return result

def fetching_rate_plan_name(cur, rate_plan_id):
    cur.execute("SELECT value from sm_translation where translation_id =(SELECT name from sm_rate_plan where id = %s)", [rate_plan_id])
    data = cur.fetchall()
    return data[0][0]

def fetching_rate_plan_short_description(cur, rate_plan_id):
    cur.execute("SELECT value from sm_translation where translation_id =(SELECT short_description from sm_rate_plan where id = %s)", [rate_plan_id])
    data = cur.fetchall()
    return data[0][0]

def fetching_tax_amount_mapped_with_rate_plan(cur, rate_plan_id):
    cur.execute("SELECT unit_id,value from sm_hotel_tax where id in (select tax_plan_id from sm_rate_plan_tax_type_mapping where rate_plan_id = %s and enabled = 1)",[rate_plan_id])   
    data = cur.fetchall()
    result = {}
    for tax_object in data:
        if tax_object[0] not in result.keys():
            result[tax_object[0]] = {'value': int(tax_object[1])}
        else:
            result[tax_object[0]]['value'] += int(tax_object[1])
    return result

def fetching_season_period_mapped_with_rate_plan(cur, rate_plan_id):
    cur.execute("SELECT start,end from sm_season_period where season_id = (select season_id from sm_rate_plan where id = %s)", [rate_plan_id])
    data = cur.fetchall()
    result = {}
    result['start_date'] = data[0][0]
    result['end_date'] = data[0][1]
    return result

def fetching_room_price_mapped_with_rate_plan(cur, rate_plan_id, room_id, start_date, end_date):
    cur.execute("SELECT no_of_adults,rate from sm_overridden_rate where date between %s and %s and rate_plan_room_type_mapping_id = (select id from sm_rate_plan_room_type_mapping where rate_plan_id = %s and room_type_id = %s)",[start_date, end_date, rate_plan_id, room_id])
    data = cur.fetchall()
    result = {}
    for price in data:
        if price[0] == 0: key = 'extra_child'
        elif price[0] == 1: key = 'single'
        elif price[0] == 2: key = 'double'
        elif price[0] == 3: key = 'triple'
        elif price[0] == 4: key = 'quad'
        result[key] = int(price[1])
    return result

def fetching_room_price_mapped_with_relative_plan_price(cur, rate_plan_id, room_id, start_date, end_date):
    cur.execute("SELECT baseplan,relative_value,relative_unit_id from sm_rate_plan where id = %s",[rate_plan_id])
    data = cur.fetchall()
    base_plan_id = data[0][0]
    relative_value = data[0][1]
    relative_unit_id = data[0][2]
    price_dictionary = fetching_room_price_mapped_with_rate_plan(cur, base_plan_id, room_id, start_date, end_date)
    for price in price_dictionary:
        if relative_unit_id == 'PERCENTAGE':
            relative_price = (float(price_dictionary[price]) * float(relative_value)) / 100
            price_dictionary[price] = float(price_dictionary[price]) + float(relative_price) 
        if relative_unit_id == 'FIXEDAMOUNT':
            price_dictionary[price] = float(price_dictionary[price]) + float(relative_value) 
    return price_dictionary

def fetching_hotel_email(cur, property_id):
    cur.execute(
        "SELECT hotel_email from sm_hotel_email where hotel_id = %s", [property_id])
    data = cur.fetchall()
    return data

def fetching_room_ids(cur, property_id):
    cur.execute("SELECT room_type_id from sm_hotel_room where hotel_id = %s",[property_id])
    data = cur.fetchall()
    result = [int(each[0]) for each in data]
    return result


def fetching_all_room_inventory(cur, room_type_ids, start_date, end_date):
    cur.execute("SELECT room_type_id,date,available from sm_room_inventory where room_type_id in %s and date BETWEEN %s and %s",[room_type_ids, start_date, end_date])
    data = cur.fetchall()
    result = {}
    for room_inv_data in data:
        if room_inv_data[0] not in result.keys():
            result[room_inv_data[0]] = {room_inv_data[1]: int(room_inv_data[2])}
        else:
            result[room_inv_data[0]][room_inv_data[1]] = int(room_inv_data[2])
    return result

def fetching_room_inventory(cur, room_type_id, start_date, end_date):
    cur.execute("SELECT available from sm_room_inventory where room_type_id = %s and date BETWEEN %s and %s",[room_type_id, start_date, end_date])
    data = cur.fetchall()
    return data

def fetching_room_data(cur, property_id):
    cur.execute("SELECT room_type_id,room_type_name,long_description,max_adults,max_children,max_guests,size,room_size_attribute_name from sm_hotel_room where hotel_id = %s and enabled = 1", [
                property_id])
    data = cur.fetchall()
    return data

def fetching_phone_number(cur, property_id):
    cur.execute("SELECT hotel_phone,additional_attribute from sm_hotel_phone where hotel_id = %s", [
                property_id])
    data = cur.fetchall()
    return data

def fetching_room_amenities(cur, room_id):
    cur.execute(
        "SELECT attribute_name from sm_hotel_room_attributes where hotel_room_attribute_id in (SELECT attribute_id from sm_room_attributes where room_type_id = %s)", [room_id])
    data = cur.fetchall()
    return data

def feching_hotel_amenities(cur, property_id):
    cur.execute(
        "SELECT value from sm_hotel_amenities where hotel_id = %s", [property_id])
    data = cur.fetchall()
    return data


def feching_logo_name_and_alt_text(cur, property_id):
    cur.execute("SELECT img_src,alt_text from sm_picture_info where picture_id = (select parent_picture_id from sm_cropped_picture_info where picture_id = (select logo_id from sm_hotel_logo_info where hotel_id = %s))", [
                property_id])
    data = cur.fetchall()
    return data


def feching_favicon_name(cur, property_id):
    cur.execute("SELECT img_src from sm_picture_info where picture_id = (select parent_picture_id from sm_cropped_picture_info where picture_id = (select fav_icon_id from sm_hotel_logo_info where hotel_id = %s))", [
                property_id])
    data = cur.fetchall()
    return data

def feching_guest_details_in_database(cur, booking_id):
    cur.execute("SELECT name,email,phone_number,country_code,div_code,address,pincode,country,state,city FROM sm_booking_guest_detail where booking_id = %s",[booking_id])
    data = cur.fetchall()
    return data

def feching_booking_detail_in_database(cur, booking_id):
    cur.execute("SELECT check_in_date,check_out_date,created_date,total_amount,amount_paid,special_request,booking_status_id FROM sm_booking where id = %s",[booking_id])
    data = cur.fetchall()
    return data
    
def feching_hotel_address(cur, property_id):
    cur.execute("SELECT attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id = '17'", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def feching_minimum_and_maximum_child_age(cur, property_id):
    cur.execute("SELECT attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id in (32,33);", [
                property_id])
    data = cur.fetchall()
    return data


def feching_hotel_currency(cur, property_id):
    cur.execute("SELECT currency_code from sm_default_currency_list where currency_id = (select attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id = 9)", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def feching_booking_terms_message(cur, property_id):
    cur.execute("SELECT value from sm_translation where translation_id = (select value from sm_message_value where hotel_id = %s and message_type_id = 'BOOKING_TERMS')", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def feching_booking_reservation_policy_message(cur, property_id):
    cur.execute("SELECT value from sm_translation where translation_id = (select value from sm_message_value where hotel_id = %s and message_type_id = 'BOOKING_RESERVATION_POLICY')", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def feching_booking_message(cur, property_id):
    cur.execute("SELECT value from sm_translation where translation_id = (select value from sm_message_value where hotel_id = %s and message_type_id = 'BOOKING_MESSAGE')", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def feching_booking_confirmation_message(cur, property_id):
    cur.execute("SELECT value from sm_translation where translation_id = (select value from sm_message_value where hotel_id = %s and message_type_id = 'BOOKING_CONFIRMATION_MESSAGE')", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def feching_advertisement_message_status(cur, property_id):
    cur.execute("SELECT toggle_value from sm_feature_toggle where hotel_id = %s and toggle_name = 'be_advertisement_message';", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def feching_email_slider_status(cur, property_id):
    cur.execute("SELECT toggle_value from sm_feature_toggle where hotel_id = %s and toggle_name = 'email_slider';", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def fetching_full_payment_choice_status(cur, property_id):
    cur.execute("SELECT toggle_value from sm_feature_toggle where hotel_id = %s and toggle_name ='choice_to_guest_for_pay_now'", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def booking_engine_payment_gateway_data(cur, property_id):
    cur.execute("SELECT attribute_name,attribute_value from sm_payment_gateway_attributes where hotel_payment_gateway_id=(select id from sm_payment_gateways where hotel_id= %s) order by attribute_name asc", [
                property_id])
    data = cur.fetchall()
    # converting the response of database to dictionary format(for easy
    # retrival of the value)
    ret = dict((a, b) for a, b in data)
    return ret


def fetching_paymentgateway_fee(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("paymentGatewayFee")


def fetching_simplotel_fee(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("simplotelPercentage")


def fetching_google_add_percentage(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("googleAdsPercentage")


def fetching_service_tax_percentage(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("serviceTaxPercentage")


def fetching_is_before_tax(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("beforeTax")


def min_coll_amt_for_pg_fee(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("minCollectAmountForPGFee")


def payment_gateway(cur, property_id):
    cur.execute("select payment_gateway_type,payment_gateway from sm_payment_gateways where hotel_id=%s", [
        property_id])
    data = cur.fetchall()
    return data


def paymentgateway_type(cur, property_id):
    data = payment_gateway(cur, property_id)
    return data[0][0]


def payment_gateway_name(cur, property_id):
    data = payment_gateway(cur, property_id)
    return data[0][1]


def booking_engine_common_data(cur, property_id):
    cur.execute("select attribute_name,attribute_value from sm_booking_engine_attributes where hotel = %s  and booking_engine=100 order by attribute_name asc", [
                property_id])
    data = cur.fetchall()
# converting the response of database to dictionary format(for easy
# retrival of the value)
    val = dict((x, y) for x, y in data)
    return val

def feching_otp_status(cur, property_id):
    cur.execute("SELECT toggle_value from sm_feature_toggle where hotel_id = %s and toggle_name = 'otp'",[property_id])
    data =  cur.fetchall()
    return data[0][0]

def booking_url(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("bookingURL")


def booking_engine_type(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("bookingEngineType")


def button_name(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("resBtnName")


def open_new_window(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("openInNewWindow")


def checkin_date(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("checkInDate")


def chain_id(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("chainId")


def show_promocode(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("showPromocode")


def collect_address(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("collectAddress")


def length_of_stay(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("lengthOfStay")


def language_url_parameter(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("languageUrlParameter")


def default_adult(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("adults")


def default_children(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("children")


def max_adult_children(cur, property_id):
    cur.execute("select max(max_adults),max(max_children) from sm_hotel_room where hotel_id=%s ", [
                property_id])
    data = cur.fetchall()
    return data


def max_adult(cur, property_id):
    data = max_adult_children(cur, property_id)
    return data[0][0]


def max_children(cur, property_id):
    data = max_adult_children(cur, property_id)
    return data[0][1]


def property_Id(cur, property_id):
    data = booking_engine_common_data(cur, property_id)
    return data.get("propertyId")


def is_stop_payout(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("stopPayouts")


def is_zero_pg_account(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("zeroPGAccount")


def fetching_chain_id(cur, property_id):
    cur.execute("SELECT parent_id from sm_chain_hotel_mapping where child_id = %s", [
                property_id])
    data = cur.fetchall()
    if not data:
        return "None"
    else:
        return data[0][0]


def fetching_country_name(cur, property_id):
    cur.execute("SELECT country_name from sm_default_country_list where country_id = (select attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id = '7')", [
                property_id])
    data = cur.fetchall()
    return data[0][0]


def fechinh_before_tax(cur, property_id):
    cur.execute("SELECT attribute_name,attribute_value from sm_payment_gateway_attributes where hotel_payment_gateway_id = (select id from sm_payment_gateways where hotel_id = %s)", [
                property_id])
    data = cur.fetchall()
    for i in range(len(data)):
        if (data[i][0] == "beforeTax"):
            return data[i][1]


def fechinh_advance_persentage(cur, property_id):
    cur.execute("SELECT id,payment_policy_type,enabled from sm_hotel_payment_policies where hotel_id = %s", [
                property_id])
    data = cur.fetchall()
    if (len(data) == 0):
        return 0

    elif (len(data) == 1 and data[0][1] == 'defaults' and data[0][2] == 1):
        cur.execute(
            "SELECT attribute_value from sm_hotel_payment_policy_attributes where policy_id = %s", [data[0][0]])
        advance_percentage = cur.fetchall()
        return advance_percentage[0][0]

    elif (len(data) > 1):
        for i in range(len(data)):
            if (data[i][1] == 'lastMinuteOverrides' and data[i][2] == 1):
                cur.execute(
                    "SELECT attribute_name,attribute_value from sm_hotel_payment_policy_attributes where policy_id = %s", [data[i][0]])
                last_minute_advance_percentage = cur.fetchall()
                for i in range(len(last_minute_advance_percentage)):
                    if (last_minute_advance_percentage[i][0] == "advancePercentage"):
                        last_minute_ad_percentage = last_minute_advance_percentage[
                            i][1]
                    if (last_minute_advance_percentage[i][0] == "hoursBeforeCheckIn"):
                        hours_before_checkIn = last_minute_advance_percentage[
                            i][1]
                    if (last_minute_advance_percentage[i][0] == "inventory"):
                        last_minute_inventory = last_minute_advance_percentage[
                            i][1]
                    if (last_minute_advance_percentage[i][0] == "inventoryEnabled"):
                        last_minute_inventory_enabled = last_minute_advance_percentage[
                            i][1]
                    if (last_minute_advance_percentage[i][0] == "operator"):
                        last_minute_operator = last_minute_advance_percentage[
                            i][1]
                current_time = strftime("%H")
                diff_time = 24 - int(current_time)
                if (last_minute_inventory_enabled == "True"):
                    cur.execute("SELECT available from sm_room_inventory where hotel_id = %s and date = '" + str(
                        strftime("%Y-%m-%d")) + "'", [property_id])
                    inventory = cur.fetchall()
                    total_inventory = 0
                    status = False
                    for i in range(len(inventory)):
                        total_inventory = total_inventory + \
                            int(inventory[i][0])
                    if (str(last_minute_operator) == 'GREATERTHAN'):
                        if(total_inventory >= int(last_minute_inventory)):
                            status = True
                        else:
                            status = False
                    if (last_minute_operator == 'LESSTHAN'):
                        if(total_inventory <= int(last_minute_inventory)):
                            status = True
                        else:
                            status = False
                    if (int(hours_before_checkIn) >= diff_time and status == True):
                        return last_minute_ad_percentage
                elif (int(hours_before_checkIn) >= diff_time):
                    return last_minute_ad_percentage

        for i in range(len(data)):
            if (data[i][1] == 'overrides' and data[i][2] == 1):
                cur.execute(
                    "SELECT attribute_name,attribute_value from sm_hotel_payment_policy_attributes where policy_id = %s", [data[i][0]])
                overrides_advance_percentage = cur.fetchall()
                for i in range(len(overrides_advance_percentage)):
                    if (overrides_advance_percentage[i][0] == "advancePercentage"):
                        overrides_ad_percentage = overrides_advance_percentage[
                            i][1]
                    if (overrides_advance_percentage[i][0] == "applicableDay1"):
                        monday = overrides_advance_percentage[i][1]
                    if (overrides_advance_percentage[i][0] == "applicableDay2"):
                        tuesday = overrides_advance_percentage[i][1]
                    if (overrides_advance_percentage[i][0] == "applicableDay3"):
                        wednesday = overrides_advance_percentage[i][1]
                    if (overrides_advance_percentage[i][0] == "applicableDay4"):
                        thursday = overrides_advance_percentage[i][1]
                    if (overrides_advance_percentage[i][0] == "applicableDay5"):
                        friday = overrides_advance_percentage[i][1]
                    if (overrides_advance_percentage[i][0] == "applicableDay6"):
                        saturday = overrides_advance_percentage[i][1]
                    if (overrides_advance_percentage[i][0] == "applicableDay7"):
                        sunday = overrides_advance_percentage[i][1]
                    if (overrides_advance_percentage[i][0] == "inventory"):
                        overrides_inventory = overrides_advance_percentage[
                            i][1]
                    if (overrides_advance_percentage[i][0] == "inventoryEnabled"):
                        overrides_inventory_enabled = overrides_advance_percentage[
                            i][1]
                    if (overrides_advance_percentage[i][0] == "operator"):
                        overrides_operator = overrides_advance_percentage[i][1]
                days = [monday, tuesday, wednesday,
                        thursday, friday, saturday, sunday]
                if (days[datetime.datetime.today().weekday()] == "True"):
                    if (overrides_inventory_enabled == "True"):
                        cur.execute("SELECT available from sm_room_inventory where hotel_id = %s and date = '" + str(
                            strftime("%Y-%m-%d")) + "'", [property_id])
                        inventory = cur.fetchall()
                        total_inventory = 0
                        status = False
                        for i in range(len(inventory)):
                            total_inventory = total_inventory + \
                                int(inventory[i][0])
                        if (str(overrides_operator) == 'GREATERTHAN'):
                            if(total_inventory >= int(overrides_inventory)):
                                status = True
                            else:
                                status = False
                        if (str(overrides_operator) == 'LESSTHAN'):
                            if(total_inventory <= int(overrides_inventory)):
                                status = True
                            else:
                                status = False
                        print status
                        if (status == True):
                            return overrides_ad_percentage
                    else:
                        return overrides_ad_percentage
        for i in range(len(data)):
            if (data[i][1] == 'defaults' and data[i][2] == 1):
                cur.execute(
                    "SELECT attribute_value from sm_hotel_payment_policy_attributes where policy_id = %s", [data[i][0]])
                defaults_advance_percentage = cur.fetchall()
                return defaults_advance_percentage[0][0]

    cur.execute("SELECT toggle_value from sm_feature_toggle where hotel_id = %s and toggle_name = 'google_hotel_ads'", [
                property_id])
    google_hotel_ads_status = cur.fetchall()
    cur.execute("SELECT attribute_name,attribute_value from sm_payment_gateway_attributes where hotel_payment_gateway_id = (select id from sm_payment_gateways where hotel_id = %s)", [
                property_id])
    payment_gateway_attributes = cur.fetchall()
    for i in range(len(payment_gateway_attributes)):
        if (payment_gateway_attributes[i][0] == "simplotelPercentage"):
            simplotel_percentage = payment_gateway_attributes[i][1]
        if (payment_gateway_attributes[i][0] == "googleAdsPercentage"):
            google_percentage = payment_gateway_attributes[i][1]
        if (payment_gateway_attributes[i][0] == "serviceTaxPercentage"):
            service_tax_percentage = payment_gateway_attributes[i][1]
        if (payment_gateway_attributes[i][0] == "minCollectAmountForPGFee"):
            min_collect_amount = payment_gateway_attributes[i][1]
        if (payment_gateway_attributes[i][0] == "paymentGatewayFee"):
            payment_gateway_fee = payment_gateway_attributes[i][1]
    if (google_hotel_ads_status[0][0] == "enabled" and float(google_percentage) > float(simplotel_percentage)):
        simplotel_percentage = google_percentage
    advance_percentage = Decimal(
        simplotel_percentage) * (Decimal(100) + Decimal(service_tax_percentage)) / Decimal(100)
    if advance_percentage > float(min_collect_amount) and float(payment_gateway_fee) > 0:
        payment_gateway_fee = Decimal(
            payment_gateway_fee) * (Decimal(100) + Decimal(service_tax_percentage)) / Decimal(100)
        advance_percentage = (advance_percentage * 100) / \
            (100 - payment_gateway_fee)
    return Decimal(round(advance_percentage, 2)).quantize(Decimal('.01'))
