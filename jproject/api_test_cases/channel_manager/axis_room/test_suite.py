import unittest

print "------------------------------------------------------------------------"
print "Axis Room Room Info Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("room_info")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Axis Room Rate Plan Info Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("rate_plan_info")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Axis Room Inventory Update Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("inventory_update")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Axis Room Rate Update Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("price_update")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
