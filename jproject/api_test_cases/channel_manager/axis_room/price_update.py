import requests
import json
import unittest
import sys, os
import time
import datetime
sys.path.append(os.path.join(os.path.dirname("axis_room"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("axis_room"), '../..', "utils"))
import data_base_utils
from random import randint


class axis_room_price_update(unittest.TestCase):
	def setUp(self):
		#Data Base Connection
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password, config.data_base)
 			
	def calling_rate_plan_update_api(self, url, room_id, rate_plan_id, property_id, password, price_detail):
		payload = {"data": {"roomType": room_id,
          			"rate": [{"startDate": price_detail['start_date'], "endDate": price_detail['end_date'], "Single": price_detail['single'], "Double": price_detail['double'], "Triple": price_detail['triple'], "Quad":price_detail['quad'], "Extra Child": price_detail['extra_child']}],
          			"rateplanId": rate_plan_id,
          			"propertyId": property_id},
					"auth": {"key": password}}
		return requests.post(url, data=json.dumps(payload), headers=config.headers)

	def checking_bad_api_response(self, response,message):
		print response.status_code
		self.assertEqual(response.status_code, 400)
		print response.reason
		self.assertEqual(response.reason, "BAD REQUEST")
		resp_dict = json.loads(response.text)
		print resp_dict['status']
		self.assertEqual(resp_dict['status'], "failure")
		print resp_dict['message']
		self.assertEqual(resp_dict['message'], message)

	def checking_good_api_response(self, response, rate_plan_id, room_id, price_detail):
		print response.status_code
		self.assertEqual(response.status_code, 200)
		print response.reason
		self.assertEqual(response.reason, "OK")
		resp_dict = json.loads(response.text)
		print resp_dict['status']
		self.assertEqual(resp_dict['status'], "success")
		print resp_dict['message']
		self.assertEqual(resp_dict['message'], "Updated Rate Info")

		self.db.close()
		self.setUp()
		cur = self.db.cursor()

		db_rates = data_base_utils.fetching_room_price_mapped_with_rate_plan(cur, rate_plan_id, room_id, price_detail['start_date'], price_detail['end_date'])
		
		for db_rate in db_rates:
			self.assertEqual(db_rates[db_rate], price_detail[db_rate])

	def test_price_update(self):
		url = config.channel_manager_url + "/axisrooms/rateUpdate"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()

		#Getting room Ids
		db_rooms_data = data_base_utils.fetching_room_data(cur, config.property_id)
		price_detail = {'start_date':str(datetime.date.today()), 'end_date':str(datetime.date.today() + datetime.timedelta(days=3)), 'current_date':str(datetime.date.today()), 'single': randint(100, 1000), 'double': randint(100, 1000), 'triple':randint(100, 1000), 'quad':randint(100, 1000),'extra_child': randint(100, 1000)}

		#With wrong property id and password
		self.checking_bad_api_response(self.calling_rate_plan_update_api(url, "007", "007", "007", "xxx", price_detail), "Access Denied")

		#With blank room_id, property id and password
		self.checking_bad_api_response(self.calling_rate_plan_update_api(url, " ", " ", " ", "  ", price_detail), "Access Denied")

		#Testing Api
		for db_room_data in db_rooms_data:
			cur = self.db.cursor()
			db_rate_plan_ids = data_base_utils.fetching_rate_plan_id_mapped_with_room_id(cur, db_room_data[0])
			
			for db_rate_plan_id in db_rate_plan_ids:
				price_detail = {'start_date':str(datetime.date.today()), 'end_date':str(datetime.date.today() + datetime.timedelta(days=3)), 'current_date':str(datetime.date.today()), 'single': randint(100, 1000), 'double': randint(100, 1000), 'triple':randint(100, 1000), 'quad':randint(100, 1000),'extra_child': randint(100, 1000)}
				self.checking_good_api_response(self.calling_rate_plan_update_api(url, db_room_data[0], db_rate_plan_id, config.property_id, config.axis_room_password, price_detail), db_rate_plan_id, db_room_data[0], price_detail)
		
		#With wrong date
		price_detail = {'start_date':str(datetime.date.today()), 'end_date':str(datetime.date.today() + datetime.timedelta(days=-1)), 'current_date':str(datetime.date.today()), 'single': 1000, 'double': 2000, 'triple':3000, 'quad':4000,'extra_child': 500}
		self.checking_bad_api_response(self.calling_rate_plan_update_api(url, db_room_data[0], db_rate_plan_id, config.property_id, config.axis_room_password, price_detail), "The request type is wrong, startDate > endDate")
		self.db.close()

if __name__ == "__main__":
    unittest.main()