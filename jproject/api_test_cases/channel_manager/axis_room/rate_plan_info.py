import requests
import json
import unittest
import sys, os
sys.path.append(os.path.join(os.path.dirname("axis_room"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("axis_room"), '../..', "utils"))
import data_base_utils


class axis_room_rate_plan_info(unittest.TestCase):
	def setUp(self):
		#--------Data Base Connection-------------------
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
 			
	def calling_rate_plan_info(self, url, room_id, property_id, password):
		payload = {"roomId": room_id,"propertyId":property_id,"auth": {"key": password}}
		return requests.post(url, data=json.dumps(payload), headers=config.headers)

	def checking_bad_api_response(self, response):
		print response.status_code
		self.assertEqual(response.status_code, 400)
		print response.reason
		self.assertEqual(response.reason, "BAD REQUEST")
		resp_dict = json.loads(response.text)
		self.assertEqual(resp_dict['status'], "failure")
		self.assertEqual(resp_dict['message'], "Access Denied")

	def checking_good_api_response(self, cur, response, room_id, property_id):
		try:
			print response.status_code
			self.assertEqual(response.status_code, 200)
			print response.reason
			self.assertEqual(response.reason, "OK")

			resp_dict = json.loads(response.text)
			db_rate_plan_ids = data_base_utils.fetching_rate_plan_id_mapped_with_room_id(cur, room_id)
			self.assertEqual(resp_dict['status'], "success")
			self.assertEqual(resp_dict['message'], "Rateplan info received successfully")
			api_rate_plans_info = resp_dict['data']

			self.assertEqual(len(api_rate_plans_info), len(db_rate_plan_ids))

			for api_rate_plan_info in api_rate_plans_info:
				if api_rate_plan_info['rateplanId'] in db_rate_plan_ids:
					tax_amount = 0
					tax_percentage = 0
					
					db_rate_plan_name = data_base_utils.fetching_rate_plan_name(cur, api_rate_plan_info['rateplanId'])
					db_tax_objects = data_base_utils.fetching_tax_amount_mapped_with_rate_plan(cur, api_rate_plan_info['rateplanId'])
					db_season_period = data_base_utils.fetching_season_period_mapped_with_rate_plan(cur, api_rate_plan_info['rateplanId'])

					for db_tax_object in db_tax_objects:
						if db_tax_object == 'FIXEDAMOUNT':
							tax_amount = tax_amount + db_tax_objects[db_tax_object]['value']
						elif db_tax_object == 'PERCENTAGE':
							tax_percentage = tax_percentage + db_tax_objects[db_tax_object]['value']

					self.assertEqual(db_rate_plan_name,api_rate_plan_info['ratePlanName'])
					self.assertEqual(tax_amount, api_rate_plan_info['taxAmount'])
					self.assertEqual(tax_percentage, api_rate_plan_info['taxPerc'])
					self.assertEqual(str(db_season_period['start_date']), api_rate_plan_info['validity'][0]['startDate'])
					self.assertEqual(str(db_season_period['end_date']), api_rate_plan_info['validity'][0]['endDate'])
					self.assertEqual(0, float(api_rate_plan_info['commissionPerc']))
				else:
					raise ValueError('Rate Plan Not Matched')
		except Exception as error:
			print ('Error: ' + repr(error))



	def test_rate_plan_info(self):
		url = config.channel_manager_url + "/axisrooms/ratePlanInfo"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()
		
		#Testing With wrong property id and password
		self.checking_bad_api_response(self.calling_rate_plan_info(url, "007", "007", "xxx"))

		#Testing With blank room_id, property id and password
		self.checking_bad_api_response(self.calling_rate_plan_info(url, " ", " ", " "))

		#Getting Room Ids
		database_rooms = data_base_utils.fetching_room_data(cur, config.property_id)
	 	
	 	#Testing Api
		for room in database_rooms:
			self.checking_good_api_response(cur, self.calling_rate_plan_info(url, room[0], config.property_id, config.axis_room_password), room[0], config.property_id)

		cur.close()

	def tearDown(self):
		self.db.close()



if __name__ == "__main__":
    unittest.main()