import requests
import json
import unittest
import sys, os
import time
import datetime
sys.path.append(os.path.join(os.path.dirname("axis_room"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("axis_room"), '../..', "utils"))
import data_base_utils
from random import randint

class axis_room_inventory_update(unittest.TestCase):
	def setUp(self):
		#Data Base Connection
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
 			
	def calling_inventory_update_api(self, url, room_id, property_id, password, inventory_detail):
		payload = {"data": {"roomType":room_id,
					"inventory":[{"startDate": inventory_detail['start_date'], "endDate": inventory_detail['end_date'], "free": inventory_detail['inventory']}],
					"propertyId":property_id},
					"auth": {"key": password}}
		return requests.post(url, data=json.dumps(payload), headers=config.headers)

	def checking_bad_api_response(self, response, message):
		print response.status_code
		self.assertEqual(response.status_code, 400)
		print response.reason
		self.assertEqual(response.reason, "BAD REQUEST")
		resp_dict = json.loads(response.text)
		print resp_dict['status']
		self.assertEqual(resp_dict['status'], "failure")
		print resp_dict['message']
		self.assertEqual(resp_dict['message'], message)

	def checking_good_api_response(self, cur, response, room_id, inventory_detail):
		print response.status_code
		self.assertEqual(response.status_code, 200)
		print response.reason
		self.assertEqual(response.reason, "OK")
		resp_dict = json.loads(response.text)
		print resp_dict['status']
		self.assertEqual(resp_dict['status'], "success")
		print resp_dict['message']
		self.assertEqual(resp_dict['message'], "Updated Inventory")

		#db connection
		self.db.close()
		self.setUp()
		cur = self.db.cursor()

		db_inventory_array = data_base_utils.fetching_room_inventory(cur, room_id, inventory_detail['start_date'], inventory_detail['end_date'])
		
		for db_inventory in db_inventory_array:
			self.assertEqual(int(db_inventory[0]),int(inventory_detail['inventory']))

	def test_inventory_update(self):
		url = config.channel_manager_url + "/axisrooms/inventoryUpdate"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()

		#Getting room Ids
		database_rooms = data_base_utils.fetching_room_data(cur, config.property_id)

		inventory_detail = {'start_date':str(datetime.date.today()), 'end_date':str(datetime.date.today() + datetime.timedelta(days=3)), 'current_date':str(datetime.date.today()), 'inventory': randint(10, 100)}

		#With wrong property id and password
		self.checking_bad_api_response(self.calling_inventory_update_api(url, "007", "007", "xxx", inventory_detail), "Access Denied")

		#With blank room_id, property id and password
		self.checking_bad_api_response(self.calling_inventory_update_api(url, " ", " ", " ", inventory_detail), "Access Denied")

	 	#Testing Api
		for room in database_rooms:
			inventory_detail = {'start_date':str(datetime.date.today()), 'end_date':str(datetime.date.today() + datetime.timedelta(days=3)), 'current_date':str(datetime.date.today()), 'inventory': randint(10, 100)}
			self.checking_good_api_response(cur, self.calling_inventory_update_api(url, room[0], config.property_id, config.axis_room_password, inventory_detail), room[0], inventory_detail)
		
		#With wrong date
		inventory_detail = {'start_date':str(datetime.date.today()), 'end_date':str(datetime.date.today() + datetime.timedelta(days=-1)), 'current_date':str(datetime.date.today()), 'inventory': randint(10, 100)}
		self.checking_bad_api_response(self.calling_inventory_update_api(url, room[0], config.property_id, config.axis_room_password, inventory_detail), "The request type is wrong, startDate > endDate")

		self.db.close()
			
if __name__ == "__main__":
    unittest.main()