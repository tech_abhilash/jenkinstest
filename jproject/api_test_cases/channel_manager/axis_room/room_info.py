import requests
import json
import unittest
import sys, os
sys.path.append(os.path.join(os.path.dirname("axis_room"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("axis_room"), '../..', "utils"))
import data_base_utils


class axis_room_room_info(unittest.TestCase):
	def setUp(self):
		#Data Base Connection
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
 			
	def calling_product_info_api(self, url, property_id, password):
		payload = {"propertyId":property_id,"auth": {"key": password}}
		return requests.post(url, data=json.dumps(payload), headers=config.headers)

	def checking_bad_api_response(self, response):
		print response.status_code
		self.assertEqual(response.status_code, 400)
		print response.reason
		self.assertEqual(response.reason, "BAD REQUEST")
		resp_dict = json.loads(response.text)
		self.assertEqual(resp_dict['status'], "failure")
		self.assertEqual(resp_dict['message'], "Access Denied")

	def checking_good_api_response(self, cur, response):
		print response.status_code
		self.assertEqual(response.status_code, 200)
		print response.reason
		self.assertEqual(response.reason, "OK")
		resp_dict = json.loads(response.text)
		self.assertEqual(resp_dict['status'], "success")
		self.assertEqual(resp_dict['message'], "Product info received successfully")
		api_rooms_info = resp_dict['data']

		api_names = []
		api_ids = []

		db_rooms_id_and_rates = data_base_utils.fetching_room_data(cur, config.property_id)

		self.assertEqual(len(api_rooms_info),len(db_rooms_id_and_rates))
		
		for api_room_info in api_rooms_info:
			api_names.append(api_room_info['name'])
			api_ids.append(api_room_info['id'])

		for db_room_id_and_rates in db_rooms_id_and_rates:
			self.assertEqual(True, (True if db_room_id_and_rates[0] in api_ids and db_room_id_and_rates[1] in api_names else False))


	def test_room_info(self):
		url = config.channel_manager_url + "/axisrooms/productInfo"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()

		#With wrong property id
		self.checking_bad_api_response(self.calling_product_info_api(url, "007", config.axis_room_password))
		
		#With wrong password
		self.checking_bad_api_response(self.calling_product_info_api(url, config.property_id, "xxx"))

		#With wrong property id and password
		self.checking_bad_api_response(self.calling_product_info_api(url, "007", "xxx"))

		#With blank property id and password
		self.checking_bad_api_response(self.calling_product_info_api(url, " ", " "))

 		#Testing Api
		self.checking_good_api_response(cur, self.calling_product_info_api(url, config.property_id, config.axis_room_password))
			
if __name__ == "__main__":
    unittest.main()