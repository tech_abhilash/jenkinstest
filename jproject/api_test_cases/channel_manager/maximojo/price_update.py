import requests
import json
import unittest
import sys, os
import time
import datetime
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "utils"))
import data_base_utils
from random import randint

class maximojo_price_update(unittest.TestCase):
	def setUp(self):
		#Data Base Connection
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,
		 config.data_base)
 			
	def calling_inventory_update_api(self, url=None, room_id=None, rate_plan_id=None, property_id=config.property_id, price_detail=None):
		
		payload = """<OTA_HotelRateAmountNotifRQ xmlns="http://www.opentravel.org/OTA/2003/05" TimeStamp="2012-08-01T00:34:58.9894175+10:00" Version="1.0">
                        <RateAmountMessages HotelCode='"""+str(property_id)+"""'>
                            <RateAmountMessage>
                            <StatusApplicationControl InvTypeCode='"""+str(room_id)+"""' RatePlanCode='"""+str(rate_plan_id)+"""' Start='"""+str(price_detail['start_date'])+"""' End='"""+str(price_detail['end_date'])+"""' />
                                 <Rates>
                                    <Rate>
                                        <BaseByGuestAmts>
                                            <BaseByGuestAmt NumberOfGuests="1" AmountBeforeTax='"""+str(price_detail['single'])+"""' />
                                            <BaseByGuestAmt NumberOfGuests="2" AmountBeforeTax='"""+str(price_detail['double'])+"""' />
                                            <BaseByGuestAmt NumberOfGuests="3" AmountBeforeTax='"""+str(price_detail['triple'])+"""' />
                                            <BaseByGuestAmt NumberOfGuests="4" AmountBeforeTax='"""+str(price_detail['quad'])+"""' />
                                        </BaseByGuestAmts>
                                        <AdditionalGuestAmounts>
                                            <AdditionalGuestAmount AgeQualifyingCode="8" Amount='"""+str(price_detail['extra_child'])+"""' />
                                        </AdditionalGuestAmounts>
                                    </Rate>
                                </Rates>
                            </RateAmountMessage>
                        </RateAmountMessages>
                     </OTA_HotelRateAmountNotifRQ>"""
		headers = {'Content-Type': 'application/xml'}
		return requests.post(url, data=payload, headers=headers)

	def checking_bad_api_response(self, response, message):
		print response.status_code
		self.assertEqual(response.status_code, 403)
		print response.reason
		print response.reason == "FORBIDDEN"
		self.assertEqual(response.reason, "FORBIDDEN")

	def checking_good_api_response(self, response, rate_plan_id, room_id, price_detail):
		print response.status_code
		self.assertEqual(response.status_code, 200)
		self.db.close()
		self.setUp()
		cur = self.db.cursor()

		db_rates = data_base_utils.fetching_room_price_mapped_with_rate_plan(cur, rate_plan_id, room_id, price_detail['start_date'], price_detail['end_date'])
		
		for db_rate in db_rates:
			self.assertEqual(db_rates[db_rate], price_detail[db_rate])

	

	def test_inventory_update(self):
		url = config.channel_manager_url + "/rates_and_inventory"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()

		#Getting room Ids
		db_rooms = data_base_utils.fetching_room_data(cur, config.property_id)
		price_detail = {'start_date':str(datetime.date.today()), 'end_date':str(datetime.date.today() + datetime.timedelta(days=3)), 'current_date':str(datetime.date.today()), 'single': randint(100, 1000), 'double': randint(100, 1000), 'triple':randint(100, 1000), 'quad':randint(100, 1000),'extra_child': randint(100, 1000)}
		
		for db_room in db_rooms:
			cur = self.db.cursor()
			db_rate_plan_ids = data_base_utils.fetching_rate_plan_id_mapped_with_room_id(cur, db_room[0])
			
			for db_rate_plan_id in db_rate_plan_ids:
				self.checking_good_api_response(self.calling_inventory_update_api(url, db_room[0], db_rate_plan_id, price_detail=price_detail), db_rate_plan_id, db_room[0], price_detail)
				self.checking_bad_api_response(self.calling_inventory_update_api(url, db_room[0], db_rate_plan_id, 9999, price_detail=price_detail), "FORBIDDEN")
		self.db.close()
			
if __name__ == "__main__":
    unittest.main()