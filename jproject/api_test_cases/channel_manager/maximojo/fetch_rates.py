import requests
import json
import unittest
import sys, os
import xmltodict
import datetime
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "utils"))
import data_base_utils


class maximojo_fetch_rate(unittest.TestCase):
	def setUp(self):
		#Data Base Connection
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
 			
	def calling_fetch_rate_api(self, url, property_id, start_date, end_date):
		payload = """<OTA_HotelRatePlanRQ xmlns="http://www.opentravel.org/OTA/2003/05" Version="1.000" EchoToken="sdf54-sdf65-sdf98652" SummaryOnly="false" TimeStamp="2017-09-26:41:55">
   						<RatePlans>
      						<RatePlan>
         						<DateRange Start='"""+str(start_date)+"""' End='"""+str(end_date)+"""' />
         						<HotelRef HotelCode='"""+str(property_id)+"""' />
      						</RatePlan>
   						</RatePlans>
					</OTA_HotelRatePlanRQ>"""
		return requests.post(url, data=payload, headers=config.maximojo_headers)

	def checking_bad_api_response(self, response):
		print response.status_code
		self.assertEqual(response.status_code, 400)
		print response.reason
		self.assertEqual(response.reason, "BAD REQUEST")
		print response.text
		self.assertEqual(response.text, "invalid literal for int() with base 10: ''")

	def checking_good_api_response(self, cur, property_id, response, start_date, end_date):
		try:
			print response.status_code
			self.assertEqual(response.status_code, 200)
			print response.reason
			self.assertEqual(response.reason, "OK")
			resp_dict = xmltodict.parse(response.text)

			api_rate_plans_info = resp_dict['OTA_HotelRatePlanRS']['RatePlans']['RatePlan']
			db_hotel_currency = data_base_utils.feching_hotel_currency(cur, property_id)
			db_rate_plan_ids = data_base_utils.fetching_rate_plan_id(cur, property_id)

			self.assertEqual(int(property_id),int(resp_dict['OTA_HotelRatePlanRS']['RatePlans']['@HotelCode']))

			for api_rate_plan_info in api_rate_plans_info:
				if int(api_rate_plan_info['@RatePlanCode']) in db_rate_plan_ids:
					db_rooms_ids = data_base_utils.fetching_room_id_mapped_with_rate_id(cur, api_rate_plan_info['@RatePlanCode'])
					api_rooms_info = api_rate_plan_info['Rates']['Rate']

					#checking hotel currency
					self.assertEqual(db_hotel_currency, api_rate_plan_info['@CurrencyCode'])
					
					#Checking Hotel Status
					self.assertEqual(api_rate_plan_info['@RatePlanStatusType'], data_base_utils.fetching_rate_plan_status(cur, api_rate_plan_info['@RatePlanCode']))
					
					#Checking Dates
					self.assertEqual(start_date, api_rate_plan_info['@Start'])
					self.assertEqual(end_date, api_rate_plan_info['@End'])
					self.assertEqual(len(api_rooms_info),len(db_rooms_ids))

					for api_room_info in api_rooms_info:
						if int(api_room_info['@InvTypeCode']) in db_rooms_ids:
							self.assertEqual(start_date, api_room_info['@Start'])
							self.assertEqual(end_date, api_room_info['@End'])
							
							if '@BaseRatePlanCode' in api_rate_plan_info:
								db_rates = data_base_utils.fetching_room_price_mapped_with_relative_plan_price(cur, api_rate_plan_info['@RatePlanCode'], api_room_info['@InvTypeCode'], start_date, end_date)
							else:
								db_rates = data_base_utils.fetching_room_price_mapped_with_rate_plan(cur, api_rate_plan_info['@RatePlanCode'], api_room_info['@InvTypeCode'], start_date, end_date)
							
							for api_room_rates in api_room_info['BaseByGuestAmts']['BaseByGuestAmt']:
								key = ('single' if api_room_rates['@NumberOfGuests'] =='1' else 'double' if api_room_rates['@NumberOfGuests'] =='2' else 'triple' if api_room_rates['@NumberOfGuests'] == '3' else 'quad' if api_room_rates['@NumberOfGuests'] == '4'  else 'extra_child')
								self.assertEqual(float(db_rates[key]), float(api_room_rates['@AmountBeforeTax']))
								self.assertEqual(float(db_rates['extra_child']), float(api_room_info['AdditionalGuestAmounts']['AdditionalGuestAmount']['@Amount']))										
						else:
							raise ValueError('Room Id Missing')
				else:
					raise ValueError('Rate Plan Id Missing')
		except Exception as error:
			print ('Error: ' + repr(error))

	def test_fetch_rate(self):
		url = config.channel_manager_url + "/maximojo/get_rateplan_detail"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()
		
		#Date
		start_date = str(datetime.date.today()) 
		end_date = str(datetime.date.today()  + datetime.timedelta(days=2))

		# Checking with wrong data
		self.checking_bad_api_response(self.calling_fetch_rate_api(url, " ", start_date, end_date))

		#Checking with Good data
		self.checking_good_api_response(cur, config.property_id, self.calling_fetch_rate_api(url, config.property_id, start_date, end_date), start_date, end_date)
			
if __name__ == "__main__":
    unittest.main()