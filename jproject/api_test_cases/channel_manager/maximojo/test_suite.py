import unittest

print "------------------------------------------------------------------------"
print "Maximojo Room and Rate Plan Info Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("fetch_room_and_rateplan_info")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Maximojo Inventory Update Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("inventory_update")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Maximojo Rate Update Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("price_update")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Maximojo Fetch Rates Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("fetch_rates")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Maximojo Fetch Inventory Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("fetch_inventory")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

