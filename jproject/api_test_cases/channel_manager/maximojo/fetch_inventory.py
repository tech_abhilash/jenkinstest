import requests
import json
import unittest
import sys, os
import xmltodict
import datetime
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "utils"))
import data_base_utils


class maximojo_fetch_inventory(unittest.TestCase):
	def setUp(self):
		#Data Base Connection
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,config.data_base)
 			
	def calling_pfetch_inventory_api(self, url, property_id, start_date, end_date):
		payload = """<OTA_HotelAvailGetRQ xmlns="http://www.opentravel.org/OTA/2003/05" Version="1.100" EchoToken="a373f6ee-d91a-40bb-8ce9-82bcca253a6e" TimeStamp="2017-09-21T10:50:08.9014485Z" TransactionIdentifier="552a37f4-d1e0-4d01-a169-07146af239ac">
	   					<HotelAvailRequests>
	      					<HotelAvailRequest>
	         					<DateRange Start='"""+str(start_date)+"""' End='"""+str(end_date)+"""' />
	         					<HotelRef HotelCode='"""+str(property_id)+"""' />
	      					</HotelAvailRequest>
	   					</HotelAvailRequests>
					</OTA_HotelAvailGetRQ>"""

		return requests.post(url, data=payload, headers=config.maximojo_headers)

	def checking_bad_api_response(self, response):
		print response.status_code
		self.assertEqual(response.status_code, 400)
		print response.reason
		self.assertEqual(response.reason, "BAD REQUEST")
		print response.text
		self.assertEqual(response.text, "invalid literal for int() with base 10: ''")

	def checking_good_api_response(self, cur, property_id, response, start_date, end_date):
		print response.status_code
		self.assertEqual(response.status_code, 200)
		print response.reason
		self.assertEqual(response.reason, "OK")
		resp_dict = xmltodict.parse(response.text)
		
		db_room_ids = data_base_utils.fetching_room_ids(cur, property_id)
		db_inventory = data_base_utils.fetching_all_room_inventory(cur, db_room_ids, start_date, end_date)

		self.assertEqual(int(property_id),int(resp_dict['OTA_HotelAvailGetRS']['AvailStatusMessages']['@HotelCode']))
		
		api_inventory = {}
		for api_room_inv_data in resp_dict['OTA_HotelAvailGetRS']['AvailStatusMessages']['AvailStatusMessage']:
			start = datetime.datetime.strptime(api_room_inv_data['StatusApplicationControl']['@Start'], '%Y-%m-%d')
			end = datetime.datetime.strptime(api_room_inv_data['StatusApplicationControl']['@End'], '%Y-%m-%d')
			
			start = start.date()
			end = end.date()
			
			while(start<=end):
				if int(api_room_inv_data['StatusApplicationControl']['@InvTypeCode']) not in api_inventory.keys():
					api_inventory[int(api_room_inv_data['StatusApplicationControl']['@InvTypeCode'])] = {start: int(api_room_inv_data['@BookingLimit'])}
				else:
					api_inventory[int(api_room_inv_data['StatusApplicationControl']['@InvTypeCode'])][start] = int(api_room_inv_data['@BookingLimit'])
				start += datetime.timedelta(days=1)

		print api_inventory

		self.assertEqual(db_inventory.__cmp__(api_inventory),0)

	def test_fetch_inventory(self):
		url = config.channel_manager_url + "/maximojo/get_rateplan_detail"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()
		
		#Date
		start_date = datetime.date.today() 
		end_date = datetime.date.today() + datetime.timedelta(days=10)

		#Checking with Good data
		self.checking_good_api_response(cur, config.property_id, self.calling_pfetch_inventory_api(url, config.property_id, start_date, end_date), start_date, end_date)
			
if __name__ == "__main__":
    unittest.main()