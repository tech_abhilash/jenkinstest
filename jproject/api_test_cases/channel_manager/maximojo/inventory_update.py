import requests
import json
import unittest
import sys, os
import time
import datetime
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "utils"))
import data_base_utils
from random import randint

class maximojo_inventory_update(unittest.TestCase):
	def setUp(self):
		#Data Base Connection
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,
		 config.data_base)
 			
	def calling_inventory_update_api(self, url=None, room_id=None, property_id=config.property_id, inventory_detail=None):
		
		payload = """<OTA_HotelAvailNotifRQ xmlns="http://www.opentravel.org/OTA/2003/05" Version="1.0" 
		TimeStamp="2017-09-10T09:30:47-05:00">
                        <AvailStatusMessages HotelCode='"""+str(property_id)+"""'>
                            <AvailStatusMessage BookingLimit='"""+str(inventory_detail['inventory'])+"""'>
                            <StatusApplicationControl Start='"""+str(inventory_detail['start_date'])+"""' End='"""+str(inventory_detail['end_date'])+"""' InvTypeCode='"""+str(room_id)+"""' 
                            Mon="1" Tue="1" Weds="1" Thur="1" Fri="1" Sat="1" Sun="1" />
                            <RestrictionStatus Status="Open" />
                            </AvailStatusMessage>
                        </AvailStatusMessages>
                    </OTA_HotelAvailNotifRQ> """
		headers = {'Content-Type': 'application/xml'}
		return requests.post(url, data=payload, headers=headers)

	def checking_bad_api_response(self, response, message):
		print response.status_code
		self.assertEqual(response.status_code, 403)
		print response.reason
		print response.reason == "FORBIDDEN"
		self.assertEqual(response.reason, "FORBIDDEN")

	def checking_good_api_response(self, cur, response, room_id, inventory_detail):
		print response.status_code
		self.assertEqual(response.status_code, 200)
		print response.reason
		self.db.close()
		self.setUp()
		cur = self.db.cursor()
		
		db_inventory_array = data_base_utils.fetching_room_inventory(cur, room_id, inventory_detail['start_date'], inventory_detail['end_date'])
		for db_inventory in db_inventory_array:
			self.assertEqual(int(db_inventory[0]),int(inventory_detail['inventory']))


	def test_inventory_update(self):
		url = config.channel_manager_url + "/rates_and_inventory"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()

		#Getting room Ids
		inventory_detail = {'start_date':str(datetime.date.today()), 'end_date':str(datetime.date.today() + datetime.timedelta(days=3)),
		 'current_date':str(datetime.date.today()),'inventory': randint(10, 100)}
		db_rooms = data_base_utils.fetching_room_data(cur, config.property_id)
		
		for db_room in db_rooms:
			room_id = db_room[0]

			self.checking_good_api_response(cur, self.calling_inventory_update_api(url,room_id, inventory_detail =inventory_detail), room_id, inventory_detail)
			self.checking_bad_api_response(self.calling_inventory_update_api(url,room_id,9999,inventory_detail),"FORBIDDEN")
		
			
if __name__ == "__main__":
    unittest.main()