import requests
import json
import unittest
import sys, os
import xmltodict
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("maximojo"), '../..', "utils"))
import data_base_utils


class maximojo_fetch_room_and_rateplan_info(unittest.TestCase):
	def setUp(self):
		#Data Base Connection
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
 			
	def calling_fetch_room_and_rateplan_info(self, url, property_id):
		payload = """<OTA_HotelRatePlanRQ xmlns="http://www.opentravel.org/OTA/2003/05" Version="1.000" EchoToken="sdf54-sdf65-sdf98652" SummaryOnly="true" TimeStamp="2017-09-26:41:55">
						<RatePlans>
							<RatePlan>
								<HotelRef HotelCode='"""+str(property_id)+"""' />
							</RatePlan>
						</RatePlans>
					</OTA_HotelRatePlanRQ>"""
		return requests.post(url, data=payload, headers=config.maximojo_headers)

	def checking_bad_api_response(self, response):
		print response.status_code
		self.assertEqual(response.status_code, 400)
		print response.reason
		self.assertEqual(response.reason, "BAD REQUEST")
		print response.text
		self.assertEqual(response.text, "invalid literal for int() with base 10: ''")

	def checking_good_api_response(self, cur, property_id, response):
		try:
			print response.status_code
			self.assertEqual(response.status_code, 200)
			print response.reason
			self.assertEqual(response.reason, "OK")
			resp_dict = xmltodict.parse(response.text)
			hotel_currency = data_base_utils.feching_hotel_currency(cur, property_id)		
			db_rate_plan_ids = data_base_utils.fetching_rate_plan_id(cur, property_id)
			api_rate_plans_info = resp_dict['OTA_HotelRatePlanRS']['RatePlans']['RatePlan']
			
			self.assertEqual(int(property_id),int(resp_dict['OTA_HotelRatePlanRS']['RatePlans']['@HotelCode']))

			for api_rate_plan_info in api_rate_plans_info:
				if int(api_rate_plan_info['@RatePlanCode']) in db_rate_plan_ids:
					db_room_ids = data_base_utils.fetching_room_id_mapped_with_rate_id(cur, api_rate_plan_info['@RatePlanCode'])
					api_rooms_info = api_rate_plan_info['Rates']['Rate']

					self.assertEqual(hotel_currency, api_rate_plan_info['@CurrencyCode'])				
					self.assertEqual(api_rate_plan_info['@RatePlanStatusType'], data_base_utils.fetching_rate_plan_status(cur, api_rate_plan_info['@RatePlanCode']))
					self.assertEqual(api_rate_plan_info['Description']['@Name'], data_base_utils.fetching_rate_plan_name(cur, api_rate_plan_info['@RatePlanCode']))

					for api_room_info in api_rooms_info:
						if int(api_room_info['@InvTypeCode']) in db_room_ids:
							self.assertEqual(api_room_info['@InvCode'], data_base_utils.fetching_room_name(cur, api_room_info['@InvTypeCode']))
							self.assertEqual(int(api_room_info['@NumberOfUnits']), int(data_base_utils.fetching_website_rooms(cur, api_room_info['@InvTypeCode'])))
						else:
							raise ValueError('Room Id Missing')
					
					# self.assertEqual(api_rate_plan_info['Description']['Text']['#text'], data_base_utils.fetching_rate_plan_short_description(cur, api_rate_plan_info['@RatePlanCode']))
				else:
					raise ValueError('Rate Plan Id Missing')
		except Exception as error:
			print ('Error: ' + repr(error))

	def test_fetch_room_and_rateplan_info(self):
		url = config.channel_manager_url + "/maximojo/get_rateplan_detail"
		print url
		
		#Data Base Connection
		cur = self.db.cursor()
		
		#Checking with wrong data
		self.checking_bad_api_response(self.calling_fetch_room_and_rateplan_info(url, " "))

		#Checking with Good data
		self.checking_good_api_response(cur, config.property_id, self.calling_fetch_room_and_rateplan_info(url, config.property_id))
			
if __name__ == "__main__":
    unittest.main()