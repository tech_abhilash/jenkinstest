import requests
import json
import MySQLdb
import unittest
import sys, os
import datetime
sys.path.append(os.path.join(os.path.dirname("index_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("index_step"), '../..', "utils"))
import data_base_utils

class Advance_percentage_api_test(unittest.TestCase):
	def setUp(self):
		#--------Data Base Connection-------------------
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
	
	def test_advance_percentage_api(self):
		url = config.admin_url+"/settings/advancePercentage?propertyId="+str(config.property_id)+"&checkIn="+str(datetime.date.today())+"&checkOut="+str(datetime.date.today() + datetime.timedelta(days=1))
		print url
		r = requests.get(url) 
		print r.status_code
		self.assertEqual(r.status_code, 200)
		print r.reason
		self.assertEqual(r.reason, "OK")

		#------- python dictionary-------------------------
		resp_dict = json.loads(r.text)

		#--------Data Base Connection----------------------
		cur = self.db.cursor()

		#--------Checking full_payment_choice_status----------
		data_base_full_payment_choice_status = data_base_utils.fetching_full_payment_choice_status(cur, config.property_id)
		if(data_base_full_payment_choice_status == 'enabled'):
			status = True
		else:
			status = False
		print status
		print resp_dict['fullPaymentChoice']
		self.assertEqual(status,resp_dict['fullPaymentChoice'])

		#-------Checking advance percentage -------------------------
		print float(resp_dict['advancePercentage'])
		data_base_advance_percentage =  data_base_utils.fechinh_advance_persentage(cur, config.property_id)
		self.assertEqual(float(data_base_advance_percentage),float(resp_dict['advancePercentage']))

		#------Checking before tax----------------------------------
		print resp_dict['beforeTax']
		data_base_before_tax = data_base_utils.fechinh_before_tax(cur, config.property_id)
		print data_base_before_tax
		self.assertEqual(data_base_before_tax.title(),str(resp_dict['beforeTax']))
		
		cur.close()

	def tearDown(self):
		self.db.close()

if __name__ == "__main__":
    unittest.main()