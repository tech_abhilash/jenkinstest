import requests
import json, ast
import unittest
import sys
import os
sys.path.append(os.path.join(os.path.dirname("index_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("index_step"), '../..', "utils"))
#import data_base_utils
import locales

class Locale_api_test(unittest.TestCase):
    def test_locales_api(self):
        url = config.admin_url+"/static/app/locales/en-US.json"
        print url
        r = requests.get(url) 
        print r.status_code
        self.assertEqual(r.status_code, 200)
        print r.reason
        self.assertEqual(r.reason, "OK")
        resp_dict = json.loads(r.text)
        #----ast for removing unicode---------------------
        resp_dict = ast.literal_eval(json.dumps(resp_dict))
        print resp_dict.__cmp__(locales.arr)
        self.assertEqual(resp_dict.__cmp__(locales.arr),0)
        
if __name__ == "__main__":
    unittest.main()
