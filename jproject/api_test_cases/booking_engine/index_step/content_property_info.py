import requests
import json
import MySQLdb
import unittest
import sys, os
import pycountry
import phonenumbers
from phonenumbers.phonenumberutil import region_code_for_country_code
sys.path.append(os.path.join(os.path.dirname("."), "config"))
import config
sys.path.append(os.path.join(os.path.dirname("."), "utils"))
import data_base_utils

class Hotel_info_api_test(unittest.TestCase):
	def setUp(self):
#--------Data Base Connection-------------------
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
		
	def check_hotel_email(self, cur, resp_dict):
		database_hotel_emails = data_base_utils.fetching_hotel_email(cur, config.property_id)
		for email in database_hotel_emails:
			print email[0]
			if email[0] not in resp_dict[''+str(config.property_id)+'']['en-US']['emails']:
				return False
		return True 	

	def check_room_details(self, cur, resp_dict):
		database_rooms = data_base_utils.fetching_room_data(cur, config.property_id)
		i =  0
		for room in database_rooms:
			print "Room Id :- %s " %room[0]
			print "Room Name :- %s " %room[1]
			print "Room Detailed Description :- %s " %room[2]
			print "Room Max Adults :- %s " %room[3]
			print "Room Max Children :- %s " %room[4]
			print "Room Max Guest :- %s " %room[5]
			print "Room Max Size :- %s " %room[6]
			print "Room Max Unit :- %s " %room[7]

			self.assertEqual(room[0], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['id'])
			self.assertEqual(room[1], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['name'])
			self.assertEqual(room[2], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['description'])
			self.assertEqual(room[3], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['adults'])
			self.assertEqual(room[4], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['children'])
			self.assertEqual(room[5], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['guests'])
			self.assertEqual(room[6], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['size']['area'])
			self.assertEqual(room[7], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['size']['unit'])

			database_room_aminities = data_base_utils.fetching_room_amenities(cur, room[0])
			for aminities in database_room_aminities:
				print aminities[0] 
				if aminities[0] not in resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['amenities']:
		 			return False
			i = i + 1
		return True 	

	def check_phone_number(self, cur, resp_dict):
		database_hotel_phone_numbers = data_base_utils.fetching_phone_number(cur, config.property_id)
		i = 0
		for phone_number in database_hotel_phone_numbers:
			print phone_number[0]
			print phone_number[1]
			self.assertEqual(phone_number[0], resp_dict[''+str(config.property_id)+'']['en-US']['phone_numbers'][i]['number'])
			self.assertEqual(phone_number[1], resp_dict[''+str(config.property_id)+'']['en-US']['phone_numbers'][i]['label'])
			i = i + 1

	def check_hotel_aminities(self, cur, resp_dict):
		data_base_hotel_amenities = data_base_utils.feching_hotel_amenities(cur, config.property_id)
		hotel_aminities_array = []
		for hotel_amenitie in data_base_hotel_amenities:
			hotel_amenities_dict = json.loads(hotel_amenitie[0])
			hotel_aminities_array.append(hotel_amenities_dict['name'])
		for hotel_amenitie in range(len(data_base_hotel_amenities)):
			print resp_dict[''+str(config.property_id)+'']['en-US']['amenities'][hotel_amenitie]['name']
			if resp_dict[''+str(config.property_id)+'']['en-US']['amenities'][hotel_amenitie]['name'] not in hotel_aminities_array:
				return False
		return True 

	def check_logo_name(self, cur, resp_dict):
		wordList = resp_dict[''+str(config.property_id)+'']['en-US']['logo']['src'].split("/")
		Image_Name = wordList[len(wordList)-1]
		print Image_Name
		data_base_logo_name = data_base_utils.feching_logo_name_and_alt_text(cur,config.property_id)
		wordList_1 = data_base_logo_name[0][0].split("/")
		Image_Name_1 = wordList_1[len(wordList_1)-1]
		Image_Name_1 = Image_Name_1.split(".")
		print Image_Name_1[0]
		self.assertEqual(Image_Name_1[0], Image_Name)
		print resp_dict[''+str(config.property_id)+'']['en-US']['logo']['alt_text']
		# self.assertEqual(data_base_logo_name[0][1], resp_dict[''+str(config.property_id)+'']['en-US']['logo']['alt_text'])

	def check_favicon_name(self, cur, resp_dict):
		wordList = resp_dict[''+str(config.property_id)+'']['en-US']['favicon']['src'].split("/")
		Image_Name = wordList[len(wordList)-1]
		Image_Name = Image_Name.split(".")
		print Image_Name[0]
		data_base_favicon_name = data_base_utils.feching_favicon_name(cur,config.property_id)
		wordList_1 = data_base_favicon_name[0][0].split("/")
		Image_Name_1 = wordList_1[len(wordList_1)-1]
		Image_Name_1 = Image_Name_1.split(".")
		print Image_Name_1[0]
		self.assertEqual(Image_Name_1[0], Image_Name[0])
		# print resp_dict[''+str(config.property_id)+'']['en-US']['logo']['alt_text']

	def check_advertisement_toggle_status(self, cur, resp_dict):
		data_base_advertisement_toggle_status = data_base_utils.feching_advertisement_message_status(cur, config.property_id)
		if (data_base_advertisement_toggle_status == "enabled"):
			self.assertEqual(True, resp_dict[''+str(config.property_id)+'']['en-US']['advertisement_message_status'])		
		else:
			self.assertEqual(False, resp_dict[''+str(config.property_id)+'']['en-US']['advertisement_message_status'])		
		print resp_dict[''+str(config.property_id)+'']['en-US']['advertisement_message_status']
		print resp_dict[''+str(config.property_id)+'']['en-US']['advertisement_message']

	def check_email_slider_status(self, cur, resp_dict):
		data_base_email_slider_status = data_base_utils.feching_email_slider_status(cur, config.property_id)
		if (data_base_email_slider_status == "enabled"):
			self.assertEqual(True, resp_dict[''+str(config.property_id)+'']['en-US']['email_slider_status'])		
		else:
			self.assertEqual(False, resp_dict[''+str(config.property_id)+'']['en-US']['email_slider_status'])		
		print resp_dict[''+str(config.property_id)+'']['en-US']['email_slider_status']

	def check_div_code_and_country_phone_code(self, cur, resp_dict):
		data_base_country_name = data_base_utils.fetching_country_name(cur, config.property_id)
		mapping = {country.name: country.alpha_2 for country in pycountry.countries}
		print data_base_country_name
		print mapping.get(data_base_country_name)
		country_div_code = mapping.get(data_base_country_name)
		print phonenumbers.country_code_for_region(country_div_code)
		country_phone_code = "+" + str(phonenumbers.country_code_for_region(country_div_code))
		print resp_dict[''+str(config.property_id)+'']['en-US']['div_code']
		self.assertEqual(country_div_code, (resp_dict[''+str(config.property_id)+'']['en-US']['div_code']).upper())
		print resp_dict[''+str(config.property_id)+'']['en-US']['country_phone_code']
		self.assertEqual(country_phone_code, resp_dict[''+str(config.property_id)+'']['en-US']['country_phone_code'])

	def test_hotel_info_api(self):
		url = config.admin_url+"/content/property/"+str(config.property_id)+"/info"
		r = requests.get(url) 
		print r.status_code
		self.assertEqual(r.status_code, 200)
		print r.reason
		self.assertEqual(r.reason, "OK")

#------- python dictionary---------------------
		resp_dict = json.loads(r.text)

#--------Data Base Connection----------------------

		cur = self.db.cursor()
#------- Checking Hotel Name ------------------
		database_hotel_name = data_base_utils.fetching_hotel_name(cur, config.property_id)
		print database_hotel_name
		self.assertEqual(database_hotel_name, resp_dict[''+str(config.property_id)+'']['en-US']['name'])

#------- Checking Hotel Website Name ------------------		
		database_hotel_website_name = data_base_utils.fetching_hotel_website(cur, config.property_id)
		print database_hotel_website_name
		self.assertEqual(database_hotel_website_name, resp_dict[''+str(config.property_id)+'']['en-US']['website'])

#------- Checking Hotel Email ------------------		
		self.assertEqual(self.check_hotel_email(cur, resp_dict), True)
		
#------- Checking Room Details ------------------
		self.assertEqual(self.check_room_details(cur, resp_dict), True)

#------- Checking hotel Phone Number ------------------
		self.check_phone_number(cur, resp_dict)
		
#--------Checking Hotel Aminities ------------------------
		self.assertEqual(self.check_hotel_aminities(cur, resp_dict), True)

#-------- Checking Porperty Id --------------------------
		print resp_dict[''+str(config.property_id)+'']['en-US']['propertyId']
		self.assertEqual(resp_dict[''+str(config.property_id)+'']['en-US']['propertyId'], str(config.property_id))


#-------Checking Logo ------------------------------------
		self.check_logo_name(cur, resp_dict)


#--------Checking Favicon --------------------------------
		self.check_favicon_name(cur, resp_dict)
			
#-------checking Address --------------------------------
		data_base_hotel_address = data_base_utils.feching_hotel_address(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['address']
		self.assertEqual(data_base_hotel_address, resp_dict[''+str(config.property_id)+'']['en-US']['address'])

#-------chicking Minimum and Maximum Child--------------------
		data_base_min_max_child_age = data_base_utils.feching_minimum_and_maximum_child_age(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['min_child_age']
		self.assertEqual(data_base_min_max_child_age[0][0], resp_dict[''+str(config.property_id)+'']['en-US']['min_child_age'])
		print resp_dict[''+str(config.property_id)+'']['en-US']['max_child_age']
		self.assertEqual(data_base_min_max_child_age[1][0], resp_dict[''+str(config.property_id)+'']['en-US']['max_child_age'])

#-------Checking Currency -------------------------------------
		data_base_hotel_currency =  data_base_utils.feching_hotel_currency(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['currency']
		self.assertEqual(data_base_hotel_currency, resp_dict[''+str(config.property_id)+'']['en-US']['currency'])

#-------checking booking terms message----------------------------------
		data_base_booking_terms_message = data_base_utils.feching_booking_terms_message(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_TERMS']
		self.assertEqual(data_base_booking_terms_message, resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_TERMS'])

#-------checking booking reservation policy message----------------------------------
		data_base_booking_reservation_policy_message = data_base_utils.feching_booking_reservation_policy_message(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_RESERVATION_POLICY']
		self.assertEqual(data_base_booking_reservation_policy_message, resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_RESERVATION_POLICY'])

#-------checking booking message----------------------------------
		data_base_booking_message = data_base_utils.feching_booking_message(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_MESSAGE']
		self.assertEqual(data_base_booking_message, resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_MESSAGE'])

#-------checking booking confirmation message----------------------------------
		data_base_booking_confirmation_message = data_base_utils.feching_booking_confirmation_message(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_CONFIRMATION_MESSAGE']
		self.assertEqual(data_base_booking_confirmation_message, resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_CONFIRMATION_MESSAGE'])

#-------chicking advertisement toggle status-----------------------------------
		self.check_advertisement_toggle_status(cur, resp_dict)
		
#-------chicking email slider status-----------------------------------
		self.check_email_slider_status(cur, resp_dict)

#-------Checking div_code and country_phone_code ---------------------------
		self.check_div_code_and_country_phone_code(cur, resp_dict)
		
#--------Checking Child Id------------------------------------------------------
		print resp_dict[''+str(config.property_id)+'']['en-US']['chainId']
		data_base_chainId = data_base_utils.fetching_chain_id(cur, config.property_id)
		self.assertEqual(str(data_base_chainId), str(resp_dict[''+str(config.property_id)+'']['en-US']['chainId']))

		cur.close()

	def tearDown(self):
		self.db.close()

if __name__ == "__main__":
    unittest.main()
 