import unittest

print "------------------------------------------------------------------------"
print "Hotel Info Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("content_property_info")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Advance Percentage Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("settings_advance_percentage")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Locale Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("locale_api")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Booking Setting Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("booking_setting")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)