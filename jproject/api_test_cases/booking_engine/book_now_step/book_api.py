import requests
import json
import MySQLdb
import unittest
import sys, os, time
import datetime
import random
sys.path.append(os.path.join(os.path.dirname("book_now_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("book_now_step"), '../..', "utils"))
import data_base_utils
import booking_steps_constants

class Book_api_test(unittest.TestCase):
	def checking_api_response(self, url, booking_detail, check_availability_data, guest_details):
		payload = {"propertyId":config.property_id,"checkIn":booking_detail['check_in'],"name":"","email":"","phone":"",
		"checkOut":booking_detail['check_out'],"adults":1,"children":0,"promocode":"","step":"None","source":"Booking Engine",
		"roomId":"None","ratePlanId":"None","page":"None","skyscnr_clid":"None","currentDate":booking_detail['current_date'],
		"isSterling":False,"defaultNoOfAdults":"1","defaultNoOfChildren":"0","showPromocode":False,"max_child_age":"6",
		"min_child_age":"2","lineItems":[{"room":{"availability":101,"name":check_availability_data['room_name'],
		"room_type":check_availability_data['room_type_id'],"scarcity":False,"selected":True,
		"rate_plan":{"secondary_period_start":"null","rate_plan_id":check_availability_data['rate_plan_id'],
		"name":check_availability_data['rate_plan_name'],"no_of_children":1,"average_price":check_availability_data['average_price']
		,"secondary_rate_plan_id":"null","penalty":{"name":check_availability_data['penalty_name'],"rules":check_availability_data['penalty_rules'],"enabled":True,
		"id":check_availability_data['penalty_id'],"tax_included":False,"description":check_availability_data['penalty_id_description']},
		"prices":[{"total_base_price":check_availability_data['total_base_price'],"total_price":check_availability_data['total_price'],
		"rate_plan_id":check_availability_data['rate_plan_id'],"rack_price":check_availability_data['rack_price'],"extra_child_price":"0.0000",
		"taxes":check_availability_data['tax'],"extra_child_base_price":"0.0000","total_tax_amount":check_availability_data['total_tax_amount'],
		"date":booking_detail['check_in'],"price":check_availability_data['price'],"base_price":check_availability_data['base_price'],"pricePerDay":900}],
		"inclusions":[],"combined_plan":False,"short_description":"","addons":[],"average_base_price":check_availability_data['average_base_price'],
		"secondary_season_id":"null","description":"","penaltyMsg":"Free cancellation!","zeroPenaltyFlag":True,
		"selected":True,"totalPrice":"900.00"}},"id":1,"adults":1,"children":0}],"grandTotal":check_availability_data['grand_total'],
		"totalTax":"100.00","subTotal":"900.00","advanceAmount":booking_detail['advance_collect_amount'],
		"payment_gateway":"INSTAMOJO","advancePercentage":check_availability_data['advance_percentage'],"collectPayment":False,
		"advPercent":0,"specialRequest":booking_detail['special_request'],"guestDetails":{"name":guest_details['guest_name'],"email":guest_details['guest_email'],"phone":guest_details['guest_phone_no'],
		"countryCode":guest_details['guest_country_code'],"divCode":guest_details['guest_div_code'],"address":guest_details['guest_address'],"pincode":guest_details['guest_pincode'],
		"country":guest_details['guest_country'],"state":guest_details['guest_state'],"city":guest_details['guest_city']},"fullPayment":False}
		r = requests.post(url, data=json.dumps(payload))
		print r.status_code
		self.assertEqual(r.status_code, 200)
		print r.reason
		self.assertEqual(r.reason, "OK")
		return r.text


	def test_book_api(self):
		booking_steps_constants.init()
		url = config.admin_url+"/book"
		print url
		#--------Checking with Wrong Phone number---------------------
		booking_steps_constants.guest_details['guest_phone_no'] = '+91223'
		resp_dict = json.loads(self.checking_api_response(url, booking_steps_constants.booking_detail, booking_steps_constants.check_availability_data, booking_steps_constants.guest_details))
		print resp_dict['error']['message']
		self.assertEqual(resp_dict['error']['message'], "The phone number you entered is incorrect please re-enter phone number Custom Message for Search Failure")
		print resp_dict['error']['code']
		self.assertEqual(resp_dict['error']['code'], "2020")
		print resp_dict['error']['tracking_message']
		self.assertEqual(resp_dict['error']['tracking_message'], "Phone no check failed, check log file for more details")

		#-------Checking with Wrong Room_Type-----------------------------------
		booking_steps_constants.init()
		booking_steps_constants.check_availability_data['room_type_id'] = 0
		resp_dict = json.loads(self.checking_api_response(url, booking_steps_constants.booking_detail, booking_steps_constants.check_availability_data, booking_steps_constants.guest_details))

		print resp_dict['error']['message']
		self.assertEqual(resp_dict['error']['message'], "We are sorry, Unexpected error occured. Please try again later. Custom Message for Search Failure")
		print resp_dict['error']['code']
		self.assertEqual(resp_dict['error']['code'], "2019")
		print resp_dict['error']['tracking_message']
		self.assertEqual(resp_dict['error']['tracking_message'], "Book api raised Exception, check log file for more details")


		#------Checking withWrong booking_steps_constants.rate_plan_id-----------------------------------
		booking_steps_constants.init()
		booking_steps_constants.check_availability_data['rate_plan_id'] = 0
		resp_dict = json.loads(self.checking_api_response(url, booking_steps_constants.booking_detail, booking_steps_constants.check_availability_data, booking_steps_constants.guest_details))
		
		print resp_dict['error']['message']
		self.assertEqual(resp_dict['error']['message'], "We are sorry, Unexpected error occured. Please try again later. Custom Message for Search Failure")
		print resp_dict['error']['code']
		self.assertEqual(resp_dict['error']['code'], "2019")
		print resp_dict['error']['tracking_message']
		self.assertEqual(resp_dict['error']['tracking_message'], "Book api raised Exception, check log file for more details")


		#------Checking with Wrong Penalty id-----------------------------------
		booking_steps_constants.init()
		print booking_steps_constants.check_availability_data['penalty_id']
		booking_steps_constants.check_availability_data['penalty_id'] = 0
		resp_dict = json.loads(self.checking_api_response(url, booking_steps_constants.booking_detail, booking_steps_constants.check_availability_data, booking_steps_constants.guest_details))
		
		print resp_dict['error']['message']
		self.assertEqual(resp_dict['error']['message'], "We are sorry, Unexpected error occured. Please try again later. Custom Message for Search Failure")
		print resp_dict['error']['code']
		self.assertEqual(resp_dict['error']['code'], "2019")
		print resp_dict['error']['tracking_message']
		self.assertEqual(resp_dict['error']['tracking_message'], "Book api raised Exception, check log file for more details")
 

		#------Checking with Wrong Tax id-----------------------------------
		booking_steps_constants.init()
		booking_steps_constants.check_availability_data['tax'][0]['id'] = 0
		resp_dict = json.loads(self.checking_api_response(url, booking_steps_constants.booking_detail, booking_steps_constants.check_availability_data, booking_steps_constants.guest_details))
		print resp_dict['error']['message']
		self.assertEqual(resp_dict['error']['message'], "We are sorry, Unexpected error occured. Please try again later. Custom Message for Search Failure")
		print resp_dict['error']['code']
		self.assertEqual(resp_dict['error']['code'], "2019")
		print resp_dict['error']['tracking_message']
		self.assertEqual(resp_dict['error']['tracking_message'], "Book api raised Exception, check log file for more details")
 
		#-----Checking with Wrong advancePercentage-----------------------------------
		booking_steps_constants.init()
		booking_steps_constants.check_availability_data['advance_percentage'] = 0
		resp_dict = json.loads(self.checking_api_response(url, booking_steps_constants.booking_detail, booking_steps_constants.check_availability_data, booking_steps_constants.guest_details))
		print resp_dict['error']['message']
		self.assertEqual(resp_dict['error']['message'], "We're sorry, the payment policy for this hotel has changed. Please click <a href=None>here</a> to begin your search again. Custom Message for Search Failure")
		print resp_dict['error']['code']
		self.assertEqual(resp_dict['error']['code'], "2016")
		print resp_dict['error']['tracking_message']
		self.assertEqual(resp_dict['error']['tracking_message'], "Advance percentage has changed")

 		#------Testing Api-----------------------------------
		booking_steps_constants.init()
		resp_dict = json.loads(self.checking_api_response(url, booking_steps_constants.booking_detail, booking_steps_constants.check_availability_data, booking_steps_constants.guest_details))
		#------- python dictionary-------------------------
		print resp_dict

		print resp_dict['guestDetails']['name']
		self.assertEqual(resp_dict['guestDetails']['name'], booking_steps_constants.guest_details['guest_name'])
		print resp_dict['guestDetails']['email']
		self.assertEqual(resp_dict['guestDetails']['email'], booking_steps_constants.guest_details['guest_email'])
		print resp_dict['guestDetails']['phone']
		self.assertEqual(resp_dict['guestDetails']['phone'], booking_steps_constants.guest_details['guest_phone_no'])
		print resp_dict['guestDetails']['countryCode']
		self.assertEqual(resp_dict['guestDetails']['countryCode'], booking_steps_constants.guest_details['guest_country_code'])
		print resp_dict['guestDetails']['divCode']
		self.assertEqual(resp_dict['guestDetails']['divCode'], booking_steps_constants.guest_details['guest_div_code'])
		print resp_dict['guestDetails']['address']
		self.assertEqual(resp_dict['guestDetails']['address'], booking_steps_constants.guest_details['guest_address'])
		print resp_dict['guestDetails']['pincode']
		self.assertEqual(resp_dict['guestDetails']['pincode'], booking_steps_constants.guest_details['guest_pincode'])
		print resp_dict['guestDetails']['country']
		self.assertEqual(resp_dict['guestDetails']['country'], booking_steps_constants.guest_details['guest_country'])
		print resp_dict['guestDetails']['state']
		self.assertEqual(resp_dict['guestDetails']['state'], booking_steps_constants.guest_details['guest_state'])
		print resp_dict['guestDetails']['city']
		self.assertEqual(resp_dict['guestDetails']['city'], booking_steps_constants.guest_details['guest_city'])
		print resp_dict['specialRequest']
		self.assertEqual(resp_dict['specialRequest'], booking_steps_constants.booking_detail['special_request'])

		advance_amount = resp_dict['advanceAmount']
		booking_id = resp_dict['booking_id'] 
		print booking_id

		#---- Saving Booking Id--------------------------------------
		with open("../output/check_availability_data.json",'r') as f:
			check_availability_data = json.loads(f.read())
			f.close()

		check_availability_data['booking_id'] = resp_dict['booking_id']		
		
		with open('../output/check_availability_data.json', 'w') as f:
			json.dump(check_availability_data, f)
			f.close()

		
		
# checkIn
# propertyId

if __name__ == "__main__":
    unittest.main()