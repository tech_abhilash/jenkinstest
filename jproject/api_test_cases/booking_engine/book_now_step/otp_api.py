import requests
import json
import MySQLdb
import unittest
import sys, os, time
import datetime
import random
sys.path.append(os.path.join(os.path.dirname("book_now_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("book_now_step"), '../..', "utils"))
import data_base_utils
import booking_steps_constants

class Otp_api_test(unittest.TestCase):
	def setUp(self):
		#--------Data Base Connection-------------------
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password, config.data_base)
	
	def test_otp_api(self):
		booking_steps_constants.init()
		#--------Data Base Connection----------------------
		cur = self.db.cursor()
		url = config.admin_url+"/services/message/"+str(booking_steps_constants.check_availability_data['booking_id'])+"/send_otp"
		r = requests.get(url) 
		print r.status_code
		print r.reason
		resp_dict = json.loads(r.text)
		otp_status = resp_dict['otp']
		print otp_status
		if (otp_status == ""):
			otp_status = 'disabled'
		elif (otp_status == True):
			otp_status = 'enabled'

		self.assertEqual(data_base_utils.feching_otp_status(cur, config.property_id), otp_status)

		if (otp_status == True):
			url = config.admin_url+"/services/message/"+str(booking_steps_constants.check_availability_data['booking_id'])+"/check_otp"
			payload = {"otp":str(otp_status)}
			r = requests.post(url, data=payload)
			print r.status_code
			print r.reason
			print r.text

		cur.close()


	def tearDown(self):
		self.db.close()


if __name__ == "__main__":
    unittest.main()