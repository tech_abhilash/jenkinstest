import requests
import json
import unittest
import sys, os, time
import datetime
import random
sys.path.append(os.path.join(os.path.dirname("book_now_step"), '../..', "config"))
import config
import booking_steps_constants

class Be_book_now_log_api_test(unittest.TestCase):
	def checking_api_response(self, url, booking_detail, check_availability_data, guest_details):
		payload = {"propertyId":config.property_id,"check_in":booking_detail['check_in'],"check_out":booking_detail['check_out'],
		"source":"Booking Engine","search_step":"booking_attempt","bookingInfo":{"propertyId":config.property_id,
		"checkIn":booking_detail['check_in'],"name":"","email":"","phone":"","checkOut":booking_detail['check_out'],
		"adults":1,"children":0,"promocode":"","step":"None","source":"Booking Engine","roomId":"None","ratePlanId":"None",
		"page":"None","skyscnr_clid":"None","currentDate":booking_detail['current_date'],"isSterling":False,
		"defaultNoOfAdults":"1","defaultNoOfChildren":"0","showPromocode":True,"max_child_age":"0","min_child_age":"0",
		"lineItems":[{"room":{"availability":2,"name":check_availability_data['room_name'],"room_type":check_availability_data['room_type_id'],
		"scarcity":True,"selected":True,"rate_plan":{"secondary_period_start":"null","rate_plan_id":check_availability_data['rate_plan_id'],
		"name":check_availability_data['rate_plan_name'],"no_of_children":1,"average_price":check_availability_data['average_price'],
		"secondary_rate_plan_id":"null","penalty":{"name":check_availability_data['penalty_name'],"rules":check_availability_data['penalty_rules'],
		"enabled":True,"id":check_availability_data['penalty_id'],"tax_included":False,"description":check_availability_data['penalty_id_description']},
		"prices":[{"total_base_price":check_availability_data['total_base_price'],"total_price":check_availability_data['total_price'],
		"rate_plan_id":check_availability_data['rate_plan_id'],"rack_price":"0.0000","extra_child_price":"0.0000","taxes":check_availability_data['tax'],
		"extra_child_base_price":"0.0000","total_tax_amount":check_availability_data['total_tax_amount'],"date":booking_detail['check_in'],
		"price":check_availability_data['price'],"base_price":check_availability_data['base_price'],"pricePerDay":800}],"inclusions":[],
		"combined_plan":False,"short_description":"Short Description of Rate","average_base_price":check_availability_data['average_base_price'],
		"secondary_season_id":"null","description":"Detail Description of Rate","selected":True,"totalPrice":"800.00"}},"id":1,"adults":1,"children":0}],
		"grandTotal":check_availability_data['grand_total'],"totalTax":"90.00","subTotal":"800.00","advanceAmount":booking_detail['advance_collect_amount'],
		"payment_gateway":"INSTAMOJO","advancePercentage":check_availability_data['advance_percentage'],"collectPayment":False,"advPercent":0,
		"specialRequest":booking_detail['special_request'],"guestDetails":{"name":guest_details['guest_name'],"email":guest_details['guest_email'],"phone":guest_details['guest_phone_no'],
		"countryCode":guest_details['guest_country_code'],"divCode":guest_details['guest_div_code'],"address":guest_details['guest_address'],
		"pincode":guest_details['guest_pincode'],"country":guest_details['guest_country'],"state":guest_details['guest_state'],"city":guest_details['guest_city']}}}

		r = requests.post(url, data=json.dumps(payload))
		print r.status_code
		self.assertEqual(r.status_code, 200)
		print r.reason
		self.assertEqual(r.reason, "OK")
		return r.text


	def test_be_book_now_log_api(self):
		booking_steps_constants.init()
		url = config.admin_url+"/be_book_now_log"
		print url
		
 		#------Testing Api-----------------------------------
		resp_dict = json.loads(self.checking_api_response(url, booking_steps_constants.booking_detail, booking_steps_constants.check_availability_data, booking_steps_constants.guest_details))

		#------- python dictionary-------------------------
		print resp_dict['status']
		self.assertEqual(resp_dict['status'], "success")
		
if __name__ == "__main__":
    unittest.main()