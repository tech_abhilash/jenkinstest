import unittest

print "------------------------------------------------------------------------"
print "BE Book Now Log Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("be_book_now_log_api")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Book Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("book_api")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "OTP Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("otp_api")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Initiate Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("initiate_api")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Static App Locales Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("static_app_locales_api")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
