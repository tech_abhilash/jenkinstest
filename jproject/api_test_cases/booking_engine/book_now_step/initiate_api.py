import requests
import json
import MySQLdb
import unittest
import sys, os, time
import datetime
import random
sys.path.append(os.path.join(os.path.dirname("book_now_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("book_now_step"), '../..', "utils"))
import data_base_utils
import booking_steps_constants

class Initiate_test(unittest.TestCase):
	def setUp(self):
		#--------Data Base Connection-------------------
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password, config.data_base)
	
	def test_initiate_api(self):
		booking_steps_constants.init()
		#--------With Wrong Booking Id---------
		url = config.admin_url+"/payment/initiate"
		payload = {"booking_id":str(0000),"fullPayment":False}
		r = requests.post(url, data=payload)
		print r.status_code
		self.assertEqual(r.status_code, 500)
		print r.reason
		self.assertEqual(r.reason, "INTERNAL SERVER ERROR")
		
		url = config.admin_url+"/payment/initiate"
		payload = {"booking_id":str(booking_steps_constants.check_availability_data['booking_id']),"fullPayment":False}
		r = requests.post(url, data=payload)
		print r.status_code
		self.assertEqual(r.status_code, 200)
		print r.reason
		self.assertEqual(r.reason, "OK")
		# if(float(advance_amount) != 0):
		# 	res_pay = r.text.strip()
		# 	res_pay = res_pay.split(" ")
		# 	n = len(res_pay)
		# 	for i in range(n):
		# 		if res_pay[i].startswith('action='):
		# 			payment_request_id = res_pay[i].split("/")
		# 			k = len(payment_request_id)
		# 			payment_request_id = payment_request_id[k -1].replace('"','') 
		# 			print payment_request_id

		# 	url = config.admin_url+"/payment/instamojo-webhook"
		# 	print advance_amount
		# 	payload = {"payment_request_id":str(payment_request_id),"payment_id":"MOJO7818005A79722197","fees":str(7),"amount":str(advance_amount)}
		# 	r = requests.post(url, data=payload)
		# 	print r.status_code
		# 	print r.reason
		# 	print r.text

		time.sleep(10)
		#--------Data Base Connection----------------------
		cur = self.db.cursor()
		data = data_base_utils.feching_guest_details_in_database(cur, booking_steps_constants.check_availability_data['booking_id'])
		self.assertEqual(str(data[0][0]), booking_steps_constants.guest_details['guest_name'])
		self.assertEqual(str(data[0][1]), booking_steps_constants.guest_details['guest_email'])
		self.assertEqual(str(data[0][2]), booking_steps_constants.guest_details['guest_phone_no'])
		self.assertEqual(str(data[0][3]), booking_steps_constants.guest_details['guest_country_code'])
		self.assertEqual(str(data[0][4]), booking_steps_constants.guest_details['guest_div_code'])
		self.assertEqual(str(data[0][5]), booking_steps_constants.guest_details['guest_address'])
		self.assertEqual(str(data[0][6]), booking_steps_constants.guest_details['guest_pincode'])
		self.assertEqual(str(data[0][7]), booking_steps_constants.guest_details['guest_country'])
		self.assertEqual(str(data[0][8]), booking_steps_constants.guest_details['guest_state'])
		self.assertEqual(str(data[0][9]), booking_steps_constants.guest_details['guest_city'])

		data = data_base_utils.feching_booking_detail_in_database(cur, booking_steps_constants.check_availability_data['booking_id'])
		self.assertEqual(str(data[0][0]), booking_steps_constants.booking_detail['check_in'])
		self.assertEqual(str(data[0][1]), booking_steps_constants.booking_detail['check_out'])
		self.assertEqual(str(data[0][2]), booking_steps_constants.booking_detail['current_date'])
		self.assertEqual(float(data[0][3]), float(booking_steps_constants.check_availability_data['grand_total']))
		self.assertEqual(str(data[0][4]), booking_steps_constants.booking_detail['advance_collect_amount'])
		self.assertEqual(str(data[0][5]), booking_steps_constants.booking_detail['special_request'])
		self.assertEqual(str(data[0][6]), booking_steps_constants.booking_detail['booking_status'])

		cur.close()


	def tearDown(self):
		self.db.close()

if __name__ == "__main__":
    unittest.main()