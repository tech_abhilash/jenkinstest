import sys, os, time
import datetime
import json

def init():
	global check_availability_data
	with open("../output/check_availability_data.json",'r') as f:
		check_availability_data = json.loads(f.read())
		f.close()
	global booking_detail
	booking_detail = {'check_in':str(datetime.date.today()), 'check_out':str(datetime.date.today() + datetime.timedelta(days=1)), 'current_date':str(datetime.date.today()), 'advance_collect_amount':'0.0000', 'special_request':'Special Request', 'booking_status':'CONFIRMED'}
	global guest_details
	guest_details = {'guest_name':'Test Name', 'guest_email':'qa@simplotel.com', 'guest_phone_no':'+911234567890', 'guest_country_code':'+91', 'guest_div_code':'in', 'guest_address':'simplotel', 'guest_pincode':'577117', 'guest_country':'India', 'guest_state':'Karnataka', 'guest_city':'Bangalore'}
