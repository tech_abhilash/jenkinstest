import requests
import json
import MySQLdb
import unittest
import sys, os
import datetime
import random
sys.path.append(os.path.join(os.path.dirname("select_room_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("select_room_step"), '../..', "utils"))
import data_base_utils

class Be_book_now_log_api_test(unittest.TestCase):
	def setUp(self):
		#--------Data Base Connection-------------------
		self.db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password, config.data_base)
	
	def test_be_book_now_log_api(self):
		#--------Data Base Connection----------------------
		cur = self.db.cursor()
		cur.execute("SELECT id from sm_rate_plan where hotel_id = %s and enabled = '1'",[config.property_id])
		data_base_rate_plan_ids = cur.fetchall() 
		rate_plan_ids = []
		for i in range(len(data_base_rate_plan_ids)):
			rate_plan_ids.append(data_base_rate_plan_ids[i][0])
		rate_plan_id = random.choice(rate_plan_ids)
		cur.execute("SELECT room_type_id from sm_rate_plan_room_type_mapping where rate_plan_id = %s",[rate_plan_id])
		data_base_room_ids = cur.fetchall() 
		room_ids = []
		for i in range(len(data_base_room_ids)):
			room_ids.append(data_base_room_ids[i][0])
		room_id = random.choice(room_ids)
		url = config.admin_url+"/be_book_now_log"
		payload = {"propertyId":config.property_id,"check_in":str(datetime.date.today()),"check_out":str(datetime.date.today() + datetime.timedelta(days=1)),"source":"None","search_step":"book_now","selection":{"roomId":room_id,"ratePlanId":rate_plan_id}}
		# payload = {"propertyId":config.property_id,"check_in":str(datetime.date.today()),"check_out":str(datetime.date.today() + datetime.timedelta(days=1)),"source":"None","search_step":"book_now","selection":{"roomId":0,"ratePlanId":0}}

		r = requests.post(url, data=json.dumps(payload))

		print url
		print r.status_code
		self.assertEqual(r.status_code, 200)
		print r.reason
		self.assertEqual(r.reason, "OK")

		#------- python dictionary-------------------------
		resp_dict = json.loads(r.text)


		#--------Checking full_payment_choice_status----------
		print resp_dict['status']
		self.assertEqual("success",resp_dict['status'])
		
		cur.close()

	def tearDown(self):
		self.db.close()

if __name__ == "__main__":
    unittest.main()