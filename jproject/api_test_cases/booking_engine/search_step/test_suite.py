import unittest

print "------------------------------------------------------------------------"
print "Check Availability"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("check_availability")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)

print "------------------------------------------------------------------------"
print "Advance Percentage Api Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("settings_advance_percentage")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
