import requests
import json
import MySQLdb
import unittest
import datetime
import sys
import os
sys.path.append(os.path.join(os.path.dirname("search_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("search_step"), '../..', "utils"))
import data_base_utils
from dateutil.relativedelta import relativedelta
import calendar
import info_data


class Check_availability(unittest.TestCase):
    # ====================Starting of the TEST CASES============================
    # SAVING THE REPONSE(current date check-in,next day check-out)
    def availability_save_response(self):
        
        #Getting the reposnce
        jsonformat = self.availability_with_constraints()
        
        #Saving the response to a file
        with open('../output/test_availability.json', 'w') as outfile:
            json.dump(jsonformat, outfile)
    
    #REUSABLE METHOD
    def availability_with_constraints(self, check_in_date=None, check_out_date=None, promocode="", max_adult=1, max_children=0):
        
        if (not check_in_date):
            check_in_date = datetime.datetime.strftime(info_data.today,'%Y-%m-%d')
            check_out_date = datetime.datetime.strftime(info_data.today + datetime.timedelta(1),'%Y-%m-%d')
        #data for POST Request
        data = {"source":"None","propertyId":str(config.property_id),"checkIn":str(check_in_date),"checkOut":str(check_out_date),
                "rooms":[{"id":1,"adults":max_adult,"children":max_children}],"isSterling":"false","promocode":str(promocode),"name":"None",
                "email":"None","phone":"None","step":"None","roomId":"None","ratePlanId":"None"}   
        #POST Request
        url_response = requests.post(
            config.admin_url+"/availability", data=json.dumps(data), headers=config.headers)
        jformat = url_response.json()
        return jformat
    
    # COMPARING THE RESPONSE(random date as check-in and the next date as check-out)        
    def test_availablity_compare_response(self):

        self.availability_save_response()
        
        check_in_date = datetime.datetime.strftime(info_data.today + datetime.timedelta(1),'%Y-%m-%d')
        print check_in_date
        check_out_date = datetime.datetime.strftime(info_data.today + datetime.timedelta(2),'%Y-%m-%d')
        print check_out_date
        jnformat_for_comapre = self.availability_with_constraints(check_in_date,check_out_date)
        
        #Reading json file(Previous date)
        with open('../output/test_availability.json') as json_data:
            jdata_to_compare = json.load(json_data)
        d1=json.dumps(jnformat_for_comapre)
        jdata_for_compare=json.loads(d1)
    
        response= jdata_for_compare['results'][0]['rooms']
        for rate_plan in response:
            for price in rate_plan['rate_plans']:
                for date_to_delete in price['prices']:
                    del date_to_delete['date']
        
        response_d= jdata_to_compare['results'][0]['rooms']
        for rate_plan in response_d:
            for price in rate_plan['rate_plans']:
                for date_to_delete in price['prices']:
                    del date_to_delete['date']
        #print json.dumps(jdata_for_compare) == json.dumps(jdata_to_compare)
        
        #comparing both the response
        print "RESPONSE Comparision :-" + str(jdata_for_compare == jdata_to_compare)

    #THE RATE PLANS CORRESPONDING TO THE CURRENT SEASON SHOULD BE SHOWING
    def test_availablity_current_season_check(self):
        
        #expected result
        expected_output = ('DEFAULT_RATE_PLAN_2','DEFAULT_RATE_PLAN_1','DEFAULT_RATE_PLAN_2','DEFAULT_RATE_PLAN_1')
        get_response = self.availability_with_constraints()
        response= get_response['results'][0]['rooms']
        #print response
        test_list = []
        for rate_plan in response:
            for rateplan_name in rate_plan['rate_plans']:
                #print rateplan_name['name']
                test_list.append(str(rateplan_name['name']))
        
        actual_output = tuple(test_list) 
        # print expected_output
        # print actual_output
        
        print "test_availablity_current_season_check : -" + str(expected_output == actual_output)

    #THE RATE PLANS CORRESPONDING TO THE NEXT SEASON SHOULD BE SHOWING(IN HERE NEXT MONTH SEASON RATEPLANS)
    def test_availablity_next_season_check(self):
        check_in_date = datetime.datetime.strftime(info_data.today + relativedelta(months=+1),'%Y-%m-%d')
        print check_in_date
        check_date = info_data.today + datetime.timedelta(1)
        check_out_date = datetime.datetime.strftime(check_date + relativedelta(months=+1),'%Y-%m-%d')
        print check_out_date     
        #expected result
        expected_output = ('NEXT_SEASON_RATE_PLAN_2','NEXT_SEASON_RATE_PLAN_1','NEXT_SEASON_RATE_PLAN_2','NEXT_SEASON_RATE_PLAN_1')
        get_response = self.availability_with_constraints(check_in_date,check_out_date)
        #print get_response
        response= get_response['results'][0]['rooms']
        #print response
        test_list = []
        for rate_plan in response:
            for rateplan_name in rate_plan['rate_plans']:
                #print rateplan_name['name']
                test_list.append(str(rateplan_name['name']))
        actual_output = tuple(test_list) 
        print "test_availablity_next_season_check : -" + str(expected_output == actual_output)
    
    #CAPTURING THE ERROR MESSAGE THAT IS GENERATED WHEN THE CHECK-IN AND CHECK-OUT DATES ARE OUT OF ANY AVAILABLE SEASON        
    def test_availability_out_of_season_check(self):
        
        check_in_date = datetime.datetime.strftime(info_data.today + relativedelta(years=+3),'%Y-%m-%d')
        print check_in_date
        check_date = info_data.today + datetime.timedelta(1)
        check_out_date = datetime.datetime.strftime(check_date + relativedelta(years=+3),'%Y-%m-%d')
        print check_out_date
        
        #expected result
        expected_output = {"error":
         {"message": "We're sorry, no valid rates are available for your stay dates. Custom Message for Search Failure","code": "1008",
         "tracking_message": "No valid Season"}}
        get_error_message = self.availability_with_constraints(check_in_date,check_out_date)
        print "Out of Season Error Check Status : -" + str(expected_output == get_error_message)

    #CAPTURING THE RATEPLANS THAT DOES NOT COME WITHIN LENGTH OF STAY(CURRENT SEASON)PS:DURATION 5 DAYS
    def test_availablity_length_of_stay_check(self):
        
        #Random Check-In date
        check_in_date = datetime.datetime.strftime(info_data.today + datetime.timedelta(1),'%Y-%m-%d')
        #Random Check-Out date
        check_out_date = datetime.datetime.strftime(info_data.today + datetime.timedelta(5),'%Y-%m-%d')
        
        #expected result
        expected_output = ('DEFAULT_RATE_PLAN_2','DEFAULT_RATE_PLAN_2')
        
        get_response = self.availability_with_constraints(check_in_date,check_out_date)
        response= get_response['results'][0]['rooms']
        #print response
        test_list = []
        for rate_plan in response:
            for rateplan_name in rate_plan['rate_plans']:
                #print rateplan_name['name']
                test_list.append(str(rateplan_name['name']))
        actual_output = tuple(test_list)
        print "test_availablity_length_of_stay_check : -" + str(actual_output == expected_output)

    #CAPTURING RATEPLANS AND RELATED TAXES WITH RESPECT TO CURRENT SEASON
    def test_availablity_tax_check(self):
        
        #expected result
        expected_output = ('DEFAULT_RATE_PLAN_2', 'tax_1', 'tax_2', 'DEFAULT_RATE_PLAN_1', 'tax_1', 'DEFAULT_RATE_PLAN_2', 'tax_1', 'tax_2', 'DEFAULT_RATE_PLAN_1', 'tax_1')
        
        # Getting the Response
        get_response = self.availability_with_constraints()
        response= get_response['results'][0]['rooms']
        #print response
        test_list1 = []
        for rate_plan in response:
            for rateplan_name in rate_plan['rate_plans']:
                #print rateplan_name['name']
                test_list1.append(str(rateplan_name['name']))
                for price in rateplan_name['prices']:
                    for tax_name in price['taxes']:
                        #print tax_name['tax_name']
                        test_list1.append(str(tax_name['tax_name']))
        actual_output = tuple(test_list1)
        print "test_availablity_tax_check : -" + str(expected_output == actual_output)
    
    #CAPTURING RATEPLANS AND RELATED CANCELLATION POLICY WITH RESPECT TO CURRENT SEASON
    def test_availablity_cancellation_policy_check(self):
        
        #expexted data
        expected_output = ('DEFAULT_RATE_PLAN_2', 'Penalty', 'DEFAULT_RATE_PLAN_1', 'Penalty', 'DEFAULT_RATE_PLAN_2', 'Penalty', 'DEFAULT_RATE_PLAN_1', 'Penalty')
        
        #Getting The Response
        get_response = self.availability_with_constraints()
        response= get_response['results'][0]['rooms']
        #print response
        test_list1 = []
        for rate_plan in response:
            for rateplan_name in rate_plan['rate_plans']:
                #print rateplan_name['name']
                test_list1.append(str(rateplan_name['name']))
                #print rateplan_name['penalty']['name']
                test_list1.append(str(rateplan_name['penalty']['name']))
                # for p in rateplan_name['penalty']:
                #     print p
                #     test_list1.append(str(q['tax_name']))                
        actual_output = tuple(test_list1)
        print "test_availablity_cancellation_policy_check : -" + str(expected_output == actual_output)

    #CHECKING THE PROMO CODE RELATED RATE PLAN AND OTHER DEFAULT RATE PLANS WITH RESPECT TO CURRENT SEASON
    def test_availablity_promo_code_check(self):
        #expected result
        expected_output = ('DEFAULT_RATE_PLAN_2', 'PROMO_CODE_CHECK_RATE_PLAN', 'DEFAULT_RATE_PLAN_1', 'DEFAULT_RATE_PLAN_2', 'PROMO_CODE_CHECK_RATE_PLAN', 'DEFAULT_RATE_PLAN_1')
        check_in_date = datetime.datetime.strftime(info_data.today,'%Y-%m-%d')
        check_out_date = datetime.datetime.strftime(info_data.today + datetime.timedelta(1),'%Y-%m-%d')
        promocode = info_data.promocode
        get_response = self.availability_with_constraints(check_in_date, check_out_date, promocode)
        response= get_response['results'][0]['rooms']
        
        #print response
        test_list1 = []
        for rate_plan in response:
            for rateplan_name in rate_plan['rate_plans']:
                #print rateplan_name['name']
                test_list1.append(str(rateplan_name['name']))                
        
        actual_output = tuple(test_list1)
        print "test_availablity_promo_code_check : -" + str(actual_output == expected_output)
    
    #CAPTURING THE RATE PLAN WHEN CHECK-IN AND CHECK-OUT COMES UNDER TWO SEASON
    def test_availablity_cross_season_check(self):
        month_end_date =  info_data.today.replace(day = calendar.monthrange(info_data.today.year, info_data.today.month)[1])
        
        #check-in date
        check_in_date = datetime.datetime.strftime(month_end_date,'%Y-%m-%d')
        
        #print check_in_date
        second_date = info_data.today.replace(day = 1)
        check_out = second_date + relativedelta(months=+1)
        
        #check-out date
        check_out_date = datetime.datetime.strftime(check_out+ datetime.timedelta(1),'%Y-%m-%d')
        # print check_out_date
       
        #expected data
        expected_output = ('DEFAULT_RATE_PLAN_2', 'DEFAULT_RATE_PLAN_2')
        get_response = self.availability_with_constraints(check_in_date,check_out_date)
        response= get_response['results'][0]['rooms']
        #print response
        test_list1 = []
        for rate_plan in response:
            for rateplan_name in rate_plan['rate_plans']:
                #print rateplan_name['name']
                test_list1.append(str(rateplan_name['name']))                
        
        actual_output = tuple(test_list1)
        print "test_availablity_cross_season_check : -" + str(expected_output == actual_output)

    #CAPTURING THE RATE PLAN AND CORRESPONDING RATE PLAN INCLUSIONS WITH RESPECT TO CURRENT SEASON
    def test_availablity_rateplan_inclusions_check(self):
        #expected data
        expected_output = ('DEFAULT_RATE_PLAN_2', 'Free Wi-Fi', 'DEFAULT_RATE_PLAN_1', 'Breakfast included', 'Breakfast and lunch included',
         'Breakfast and dinner included', 'DEFAULT_RATE_PLAN_2', 'Free Wi-Fi', 'DEFAULT_RATE_PLAN_1', 'Breakfast included', 
         'Breakfast and lunch included', 'Breakfast and dinner included')
        #Getting The Response
        get_response = self.availability_with_constraints()
        response= get_response['results'][0]['rooms']
        #print response
        test_list1 = []
        for rate_plan in response:
            for rateplan_name in rate_plan['rate_plans']:
                #print rateplan_name['name']
                test_list1.append(str(rateplan_name['name']))
                for inclusions in rateplan_name['inclusions']:
                    #print inclusions['name']
                    test_list1.append(str(inclusions['name']))
        actual_output = tuple(test_list1)
        print "test_availablity_rateplan_insclusions_check : -" + str(actual_output == expected_output)

    #CAPTURING THE AVAILABLE ROOMS WITH RESPECT TO CURRENT SEASON
    def test_availablity_room_name_check(self):
        #expected result
        expected_output = ('Test Room_1', 'Test Room_2')
        get_response = self.availability_with_constraints()
        response= get_response['results'][0]['rooms']
        #print response
        test_list1 = []
        for rooms in response:
            print rooms['name']
            test_list1.append(str(rooms['name']))
        actual_output = tuple(test_list1)
        print "test_availablity_room_name_check : -" + str(actual_output == expected_output)

    #EXCEPTION VERIFICATION WITH RESPECT TO MAX ADULT WITH RESPECT TO CURRENT SEASON
    def test_availablity_max_adult_error_check(self):
        expected_output = {"error": 
                    {"message":
                     "We're sorry, available rooms at the hotel cannot accommodate 7 adults. Please add more rooms to your search to accommodate 7 adults. Custom Message for Search Failure",
                     "code": "1003","tracking_message": "No rooms that can accommodate number of adults"}}
        max_adult = info_data.max_adult 
        get_response = self.availability_with_constraints(max_adult=max_adult)
        #get_response = url_reposnse5.json()
        print "Max Adult Out Of Range Exception Status :-" + str(expected_output == get_response)

    #EXCEPTION VERIFICATION WITH RESPECT TO MAX CHILDREN
    def test_availablity_max_children_error_check(self):
        expected_output = {"error":
                    {"message": "We're sorry, available rooms at the hotel cannot accommodate 7 children. Custom Message for Search Failure",
                     "code": "1004","tracking_message": "No rooms that can accommodate number of children"}}        
        max_children = info_data.max_children
        get_response = self.availability_with_constraints(max_children=max_children)
        print "Max Children Out Of Range Exception Status :-" + str(expected_output == get_response)

    #EXCEPTION VERIFICATION WITH RESPECT NO VALID RATES WITHIN CURRENT SEASON(SET RATES AS 0 AND SET THE DATES IN TESTCASE MANUALLY)
    def test_availablity_no_valid_rates_available_error_check(self):
        next_month = datetime.datetime.now().replace(day=1) + relativedelta(years=3)
        nextmonth = str(next_month).split("-")
        days_in_month = calendar.monthrange(int(nextmonth[0]),int(nextmonth[1]))
        check_in_date = str(nextmonth[0])+"-"+str(nextmonth[1])+"-01"
        print check_in_date
        check_out_date = str(nextmonth[0])+"-"+str(nextmonth[1])+"-02"
        print check_out_date

        # second_date = info_data.today.replace(day = 29)
        # check_in_date = datetime.datetime.strftime(second_date,'%Y-%m-%d')
        # print check_in_date
        # check_out_date = datetime.datetime.strftime(second_date+ datetime.timedelta(1),'%Y-%m-%d')
        # print check_out_date
        #expected results
        expected_output = {"error": 
                    {"message": "We're sorry, no valid rates are available for your stay dates. Custom Message for Search Failure",
                     "code": "1008","tracking_message": "No valid Season"}}

        # expected_output = {"error": 
        #             {"message": "We're sorry, no valid rates are available for your stay dates. Custom Message for Search Failure",
        #              "code": "1005","tracking_message": "No valid rates available"}}
        # #post data(give checkin date and cheeckout date such that the rate should be 0 for those date)
        get_response = self.availability_with_constraints(check_in_date,check_out_date)
        print "No Valid Rates Exception Status :-" + str(expected_output == get_response)

    #EXCEPTION VERIFICATION WITH RESPECT TO NO RATES RELATED GIVEN LOS IN CURRENT SEASON(GIVEN MORE THAN 6 DAYS TO GET THE ERROR)
    def test_availablity_no_rates_for_Given_LOS_error_check(self):
        check_in_date = datetime.datetime.strftime(info_data.today,'%Y-%m-%d')
        check_out_date = datetime.datetime.strftime(info_data.today + datetime.timedelta(8),'%Y-%m-%d')
        #expected result
        expected_output = {"error": 
                    {"message":
                     "The hotel requires you to book a maximum of (6,) nights stay. Please change the checkin or checkout dates. Custom Message for Search Failure",
                     "code": "1006","tracking_message": "No rates for given LOS"}}
        get_response = self.availability_with_constraints(check_in_date,check_out_date) 
        print "No Valid Rates For Given LOS Status :-" + str(expected_output == get_response)        
    
    #EXCEPTION VERIFICATION WITH RESPECT TO PROMOCODE MISMATCH
    def test_availablity_promo_code_doesnot_match_error_check(self):
        next_month = datetime.datetime.now().replace(day=1) + relativedelta(months=1)
        print next_month
        next_year = str(datetime.datetime(year=next_month.year+1, month=2, day=1)).split("-")
        days_in_month = calendar.monthrange(int(next_year[0]),int(next_year[1]))
        check_in_date = str(next_year[0])+"-"+str(next_year[1])+"-01"
        check_out_date = str(next_year[0])+"-"+str(next_year[1])+"-02"

        # reference_date = info_data.today.replace(day = 1)
        # check_in = reference_date + relativedelta(months=+5)
        # #check-out date
        # check_in_date = datetime.datetime.strftime(check_in+ datetime.timedelta(1),'%Y-%m-%d')
        print check_in_date
        # check_out_date = datetime.datetime.strftime(check_in+ datetime.timedelta(2),'%Y-%m-%d')
        print check_out_date
        # expected result
        expected_output = {"error": 
                    {"message": "We're sorry, no valid rates are available for your stay dates. Custom Message for Search Failure",
                     "code": "1009","tracking_message": "Promocode does not match"}}
        get_response = self.availability_with_constraints(check_in_date, check_out_date)
        print "Promo Code Does Not Match Status :-" + str(expected_output == get_response)

    #EXCEPTION VERIFICATION WITH RESPECT ADVANCE PURCHASE ERROR
    def test_availablity_no_rates_for_Given_advance_purchase_window_error_check(self):
        next_month = datetime.datetime.now().replace(day=1) + relativedelta(months=1)
        next_year = str(datetime.datetime(year=next_month.year+1, month=1, day=1)).split("-")
        days_in_month = calendar.monthrange(int(next_year[0]),int(next_year[1]))
        check_in_date = str(next_year[0])+"-"+str(next_year[1])+"-01"
        check_out_date = str(next_year[0])+"-"+str(next_year[1])+"-02"

        # reference_date = info_data.today.replace(day = 1)
        # check_in = reference_date + relativedelta(months=+4)
        # #check-out date
        # check_in_date = datetime.datetime.strftime(check_in+ datetime.timedelta(1),'%Y-%m-%d')
        print check_in_date
        # check_out_date = datetime.datetime.strftime(check_in+ datetime.timedelta(2),'%Y-%m-%d')
        print check_out_date
        # #expected result
        expected_output = {"error": 
                    {"message": "We're sorry, no valid rates are available for your stay dates. Custom Message for Search Failure",
                     "code": "1007","tracking_message": "No rates for given advance purchase window"}}
        get_response = self.availability_with_constraints(check_in_date,check_out_date)
        print "Advance Purchase Window Check Error Status :-" + str(expected_output == get_response)

    #EXCEPTION VERIFICATION WITH RESPECT TO GUEST ACCOMODATION ERROR
    def test_availablity_no_rooms_can_accomodate_no_guest_check(self):
        expected_output = {"error": 
                    {"message":
                     "We're sorry, available rooms at the hotel cannot accommodate 5 guests. Please add more rooms to your search to accommodate 5 guests. Custom Message for Search Failure",
                     "code": "1010","tracking_message": "No rooms that can accommodate number of guests"}}
        adult = info_data.adult
        child = info_data.child
        get_response = self.availability_with_constraints(max_adult = adult, max_children = child)
        print "No Rooms Can Accomodate No Of Guest Error Status :-" + str(expected_output == get_response)

    #DATA TO BE PASSED
    def test_data_required_for_next_stage(self):
        self.availability_save_response()
        with open('../output/test_availability.json') as json_data:
            data = json.load(json_data)

        response= data['results'][0]['rooms']
        dict_save = {}
        dict_save['room_type_id'] = response[0]['room_type']
        dict_save['rate_plan_id'] = response[0]['rate_plans'][0]['rate_plan_id']
        dict_save['rate_plan_name'] = response[0]['rate_plans'][0]['name']
        dict_save['average_price'] = response[0]['rate_plans'][0]['average_price']
        dict_save['penalty_id'] = response[0]['rate_plans'][0]['penalty']['id']
        dict_save['penalty_rules'] = response[0]['rate_plans'][0]['penalty']['rules']
        dict_save['penalty_id_description'] = response[0]['rate_plans'][0]['penalty']['description']
        dict_save['penalty_name'] = response[0]['rate_plans'][0]['penalty']['name']
        dict_save['total_base_price'] = response[0]['rate_plans'][0]['prices'][0]['total_base_price']
        dict_save['total_price'] = response[0]['rate_plans'][0]['prices'][0]['total_price']
        dict_save['rack_price'] = response[0]['rate_plans'][0]['prices'][0]['rack_price']
        dict_save['total_tax_amount'] = response[0]['rate_plans'][0]['prices'][0]['total_tax_amount']
        dict_save['price'] = response[0]['rate_plans'][0]['prices'][0]['price']
        dict_save['base_price'] = response[0]['rate_plans'][0]['prices'][0]['base_price']
        dict_save['grand_total'] = float(response[0]['rate_plans'][0]['prices'][0]['total_price']) + float(response[0]['rate_plans'][0]['prices'][0]['total_tax_amount'])
        dict_save['average_base_price'] = response[0]['rate_plans'][0]['average_base_price']
        dict_save['availability'] = response[0]['availability']
        dict_save['room_name'] = response[0]['name']
        dict_save['tax'] = response[0]['rate_plans'][0]['prices'][0]['taxes']
        dict_save['advance_percentage'] = ""
        dict_save['booking_id'] = ""
        print "\n\n"
        print dict_save
        with open('../output/check_availability_data.json', 'w') as outfile:
            json.dump(dict_save, outfile)   

if __name__ == "__main__":
    unittest.main()
