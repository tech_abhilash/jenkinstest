import smtplib, os
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.utils import COMMASPACE, formatdate
from email.MIMEBase import MIMEBase
from email import Encoders
from os.path import basename
from email.mime.application import MIMEApplication
import email_data
import glob

class Sendingmail():
	def mailsending(self,name,email,email_subject):
		gmail_user = email_data.user_nane
		gmail_pwd = email_data.password
		TO = email
		cc_addresses = "renu@simplotel.com"
		bcc_addresses = "ashwin@simplotel.com,sales@simplotel.com"
		SUBJECT = email_subject
		hotel_name_data = email_subject
		stopwords = ['monthly','Monthly','Metrics','metrics','-','Report','report']
		querywords = hotel_name_data.split()
		resultwords  = [word for word in querywords if word.lower() not in stopwords]
		hotel_name = ' '.join(resultwords)
		TEXT = "Hi,\n\nAttached is the monthly report for " + hotel_name + "\n\nDo let us know your questions\n\nRegards,\nSimplotel"
		server = smtplib.SMTP('smtp.gmail.com', 587)
		msg = MIMEMultipart()
		msg['From'] = gmail_user
		msg['To'] = TO
		msg['Subject'] = SUBJECT
		msg['Date'] = formatdate(localtime = True)
		msg['Cc'] = cc_addresses
		msg['Bcc'] = bcc_addresses
		msg.attach( MIMEText(TEXT) )

		os.chdir('/home/akshata/Desktop/ga_montly_report/pdf')
		f = glob.glob('Analytics*.pdf')
		for file in f:
			with open(file, "rb") as fil:
				msg.attach(MIMEApplication(fil.read(),Content_Disposition='attachment; filename="%s"' % basename(file),Name=basename(file)))

		toaddrs = [TO] + [cc_addresses] + [bcc_addresses]
		server.ehlo()
		server.starttls()
		server.login(gmail_user, gmail_pwd)
		server.sendmail(gmail_user, toaddrs, msg.as_string())
		print ('email sent')

# x = Sendingmail()
# x.mailsending()