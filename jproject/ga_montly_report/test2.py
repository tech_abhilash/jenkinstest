# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
import data
import rename
import email_send
import remove_all_files

class Untitled2(unittest.TestCase):
    def setUp(self):
        firefoxProfile = webdriver.FirefoxProfile()
        firefoxProfile.set_preference('browser.download.folderList',2)
        firefoxProfile.set_preference("browser.download.manager.showWhenStarting",False)
        firefoxProfile.set_preference("browser.download.dir","/home/akshata/Desktop/ga_montly_report/pdf")
        firefoxProfile.set_preference("browser.helperApps.alwaysAsk.force", False)
        firefoxProfile.set_preference("browser.helperApps.neverAsk.saveToDisk",'application/pdf,application/x-pdf')
        firefoxProfile.set_preference("plugin.disable_full_page_plugin_for_types", "application/pdf")
        firefoxProfile.set_preference("pdfjs.disabled", True)
        self.driver = webdriver.Firefox(firefox_profile = firefoxProfile)
        self.driver.implicitly_wait(30)
        self.base_url = data.base_url
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def check_monthly_report_exists(self, xpath):
        report_name = self.driver.find_element_by_xpath(xpath).text 
        report_name = report_name.split(" ")
        if (report_name[0] == "Monthly"):
            return True
        else:
            return False

    def check_private_dashboard_exists(self, xpath):
        dashboard_name = self.driver.find_element_by_xpath(xpath).text 
        if (dashboard_name == "Private"):
            return True
        else:
            return False

    def test_untitled2(self):
        driver = self.driver
        # driver.get(self.base_url + "/ServiceLogin?service=analytics&continue=https://www.google.com/analytics/web/provision?et%3D%26authuser%3D0%23provision%2FSignUp%2F&followup=https://www.google.com/analytics/web/provision?et%3D%26authuser%3D0%23provision%2FSignUp%2F")
        # driver.maximize_window()
        # driver.find_element_by_id("Email").clear()
        # driver.find_element_by_id("Email").send_keys(data.email)
        # driver.find_element_by_id("next").click()
        # driver.find_element_by_id("Passwd").clear()
        # driver.find_element_by_id("Passwd").send_keys(data.password)
        # driver.find_element_by_id("signIn").click()
        # time.sleep(5)
        # remove_pdf = remove_all_files.Remove()
        # remove_pdf.remove_all_pdf()

        with open("hotel_name.txt",'r') as f:
            name_and_email = f.read().split('\n')
            f.close()
        n = len(name_and_email)
        i = 0
        for i in range(0,n):
            print name_and_email[i]
        driver.find_element_by_xpath(data.drop_down_path).click()
        time.sleep(3)
        driver.find_element_by_xpath(data.hotel_name_input_path).clear()
        driver.find_element_by_xpath(data.hotel_name_input_path).send_keys("ample inn")
        time.sleep(3)
        driver.find_element_by_xpath(data.hotel_name_select_path).click()
        driver.find_element_by_xpath(data.Reporting_tab_path).click()
        time.sleep(5)
        
        for i in range(0, n):
            parts = name_and_email[i].split(':')
            name = parts[0]
            email = parts[1]
            driver.find_element_by_xpath(data.drop_down_path).click()
            time.sleep(3)
            driver.find_element_by_xpath(data.hotel_name_input_path).clear()
            driver.find_element_by_xpath(data.hotel_name_input_path).send_keys(name)
            time.sleep(3)
            print name
            time.sleep(3)
            x = self.check_exists_by_xpath(data.hotel_name_select_path)
            if x == True:
                driver.find_element_by_xpath(data.hotel_name_select_path).click()
                time.sleep(3)
                if i == 0:
                    driver.find_element_by_xpath(data.Reporting_tab_path).click()
                    driver.find_element_by_xpath(data.dash_board_path).click()
                    j = 1
                    while 1:
                        path = "/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/div"
                        y = self.check_exists_by_xpath(path)
                        if y == True:
                            path = "/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/div"
                            z = self.check_private_dashboard_exists(path)
                            if (z == True):
                                driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/div").click()
                                k = 1
                                while 1:
                                    path = "/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/ul/li["+str(k)+"]/a/div"
                                    x = self.check_exists_by_xpath(path)
                                    if x == True:
                                        path = "/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/ul/li["+str(k)+"]/a/div"
                                        z = self.check_monthly_report_exists(path)
                                        if (z == True):
                                            driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/ul/li["+str(k)+"]/a/div").click()
                                            time.sleep(3)
                                            email_subject = driver.find_element_by_xpath(data.email_subject_path).text
                                            driver.find_element_by_xpath(data.date_selecter_path).click()
                                            time.sleep(3)
                                            driver.find_element_by_xpath(data.start_date_path).clear()
                                            driver.find_element_by_xpath(data.start_date_path).send_keys(data.start_date)
                                            driver.find_element_by_xpath(data.end_date_path).clear()
                                            driver.find_element_by_xpath(data.end_date_path).send_keys(data.end_date)
                                            driver.find_element_by_xpath(data.date_submit_button_path).click()
                                            time.sleep(5)   
                                            driver.find_element_by_xpath(data.export_tab_part).click()
                                            driver.find_element_by_xpath(data.pdf_tab_path).click()
                                            # time.sleep(20)
                                            # mailing = email_send.Sendingmail()
                                            # mailing.mailsending(name,email,email_subject)
                                            # renameing_pdf = rename.Rename()
                                            # renameing_pdf.renameing(name)
                                            j = j + 1
                                            break
                                        else:
                                            k = k + 1
                                            
                                        
                                    else:
                                        j = j + 1
                                        print "Hotel " + name + " not have Reports"
                                        break
                                            
                            else:
                                j = j + 1
                        else:
                            break
                else:
                    j = 1
                    while 1:
                        path = "/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/div"
                        y = self.check_exists_by_xpath(path)
                        if y == True:
                            path = "/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/div"
                            z = self.check_private_dashboard_exists(path)
                            if (z == True):
                                # driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/div").click()
                                k = 1
                                while 1:
                                    path = "/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/ul/li["+str(k)+"]/a/div"
                                    x = self.check_exists_by_xpath(path)
                                    if x == True:
                                        path = "/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/ul/li["+str(k)+"]/a/div"
                                        z = self.check_monthly_report_exists(path)
                                        if (z == True):
                                            driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/ul/li["+str(j)+"]/ul/li["+str(k)+"]/a/div").click()
                                            time.sleep(3)
                                            email_subject = driver.find_element_by_xpath(data.email_subject_path).text
                                            driver.find_element_by_xpath(data.export_tab_part).click()
                                            driver.find_element_by_xpath(data.pdf_tab_path).click()
                                            # time.sleep(20)
                                            # mailing = email_send.Sendingmail()
                                            # mailing.mailsending(name,email,email_subject)
                                            # renameing_pdf = rename.Rename()
                                            # renameing_pdf.renameing(name)
                                            j = j + 1
                                            break
                                        else:
                                            k = k + 1
                                            
                                        
                                    else:
                                        j = j + 1
                                        print "Hotel" + name + "not have Reports"
                                        break
                                            
                            else:
                                j = j + 1
                        else:
                            break
            else:
                print name + "not Found"
            time.sleep(10)

        # renameing_pdf = rename.Rename()
        # renameing_pdf.renameing(name[i])



    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
