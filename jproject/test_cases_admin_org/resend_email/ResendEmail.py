from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from ResendEmailCall import *
import datetime
import sys
import os
sys.path.append(os.path.join(os.path.dirname("resend_email"), '..', "config"))
import hotel_info
import datetime
sys.path.append(os.path.join(os.path.dirname("resend_email"), '..', "room"))
import room_page_info_data as room
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select

def resendingEmail():
	driver = webdriver.Firefox()
	driver.maximize_window()
	#driver.get("http://qaadmin.simplotel.com/simp/")
	driver.get(hotel_info.Admin_Url)
	driver.find_element_by_name("userid").send_keys(hotel_info.User_Name)
	driver.find_element_by_name("password").send_keys(hotel_info.Password)
	driver.find_element_by_xpath("//button[@type='submit']").click()
	print "Clicked on LogIn"
	time.sleep(6)
	#driver.find_element_by_xpath("//button[contains(text(),'Wonderla')]").click()
	# driver.find_element_by_xpath("//html/body/div[1]/aside[1]/section/ul/div/div/div[1]/button[@type='button']").click()
	# time.sleep(3)
	# driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
	# time.sleep(6)
	# driver.find_element_by_xpath("//a[text()='Hotel Grand Palace Precidency']").click()
	# driver.implicitly_wait(10)
	print "Clicked on our desired Hotel"
	#Generate and Launch the Desktop Version
	time.sleep(5)
	driver.find_element_by_xpath("//span[contains(text(),'Booking Engine')]").click()
	time.sleep(2)
	driver.find_element_by_xpath("//span[contains(text(),'Rates')]").click()
	time.sleep(20)
	time.sleep(10)
	rates=driver.find_elements_by_xpath("//div[button[@class='btn btn-success']/i]/ul/li/a")
	print rates
	rate=rates[0].text
	print rate
	time.sleep(5)
	driver.find_element_by_xpath("//a[@class='dropdown-toggle']").click()
	time.sleep(5)
	driver.find_element_by_id("idGenerateSite").click()
	print "Generating Site"
	time.sleep(8)
	driver.find_element_by_xpath("//a[@class='dropdown-toggle']").click()
	time.sleep(5)
	driver.find_element_by_id("desktop-preview").click()
	print "Getting the Desktop Preview"
	time.sleep(5)
	window_after = driver.window_handles[1]
	driver.switch_to_window(window_after)
	time.sleep(5)
	print "Switching the driver control and maximizing the window"
	driver.maximize_window()
	#In the website trying to book a room
	#driver.find_element_by_xpath("//div[@class='form-group check-btn-holder']/input[@value='Book Now']").click()
	driver.find_element_by_xpath("//form[div[label[contains(text(),'Check In')]]]//div/div/input[@type='submit']").click()
	time.sleep(5)
	mydate = datetime.date.today()
	month=mydate.strftime("%B")
	day=str(mydate).split("-")[2]
	date12=day.lstrip('0')
	print month
	print day
	print "getting todays date : "+ str(mydate)
	print "Started booking a room"
	try:
		driver.find_element_by_xpath("//div[@id='checkInWrapper']//span[@id='checkInCalendar']").click()
	except NoSuchElementException:
		window_booking_engine = driver.window_handles[2]
		driver.switch_to_window(window_booking_engine)	
		driver.maximize_window()
		driver.find_element_by_xpath("//div[@id='checkInWrapper']//span[@id='checkInCalendar']").click()
	print "Clicked on the calender"
	#driver.find_element_by_xpath("//div[div[div[contains(text(),'"+month1+"')]]]//button[text()='"+todayd+"']").click()
	driver.find_element_by_xpath("//div[div[div[contains(text(),'"+month+"')]]]//button[text()='"+date12+"']").click()
	print "selected the desired date"
	time.sleep(5)
	#selectedRoom='Maharaja Suite'
	driver.find_element_by_xpath("//span[contains(text(),'Check Availability')]").click()
	time.sleep(5)
	#driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'"+selectedRoom+"')]]]]]]]]//span[@class='view-hide-text js-view-rate-text']").click()
	#driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'"+room_page_info_data.Room_Name+"')]]]]]]]]//span[@class='view-hide-text js-view-rate-text']").click()
	driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'"+room.Room_Name+"')]]]]]]]]//span[@class='view-hide-text js-view-rate-text']").click()
	time.sleep(4)
	#driver.find_element_by_xpath("//button[@data-room-type='31977']").click()
	#driver.find_element_by_xpath("//div[div[div[contains(text(),'"+rate+"')]]]/div[2]/div/div[2]/button").click()
	#driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'test1 room')]]]]]]]]//div[div[div[contains(text(),'"+rate+"')]]]/div[2]/div/div[2]/button").click()
	driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'"+room.Room_Name+"')]]]]]]]]//div[div[div[contains(text(),'"+rate+"')]]]/div[2]/div/div[2]/button").click()
	time.sleep(5)
	driver.find_element_by_xpath("//input[@placeholder='Name']").send_keys("Sandeep Mohanty")
	time.sleep(4)
	driver.find_element_by_xpath("//input[@placeholder='Email Address']").send_keys("tech.abhilash01@gmail.com")
	time.sleep(2)
	driver.find_element_by_xpath("//input[@placeholder='Phone Number']").send_keys("8050896077")
	time.sleep(2)
	driver.find_element_by_xpath("//textarea[@placeholder=' Special Requests']").send_keys("Not Of Need")
	time.sleep(2)
#	driver.find_element_by_xpath("//input[@value='Continue to Payment']").click()
	time.sleep(10)
	try:
		print "0"
		driver.execute_script("window.scrollTo(0,400);")
		time.sleep(7)
		#driver.find_element_by_xpath("//input[@value='Book Now & Pay at Hotel']").click()
		driver.find_element_by_xpath("//*[@class='guest-info-form']/div[8]/div[2]/div[1]/input").click()
		time.sleep(10)
		reservationId=driver.find_element_by_xpath("//div[div[label[contains(text(),'Reservation Id')]]]//p[@class='details-input']").text
	except NoSuchElementException:
		print "1"
		#driver.find_element_by_xpath("//input[@value='Continue to Payment']").click()
		time.sleep(10)
		print "2"
		time.sleep(3)
		window_after = driver.window_handles[2]
		driver.switch_to_window(window_after)
		driver.maximize_window()
		time.sleep(6)
		time.sleep(4)
		driver.find_element_by_xpath("//ul[@class='resp-tabs-list span3']/li[3]/div[1]/span[2]").click()
		time.sleep(3)
		select=Select(driver.find_element_by_id("netBankingBank"))
		select.select_by_visible_text("AvenuesTest")
		time.sleep(3)
		driver.find_element_by_xpath("//div[div[div[select[@id='netBankingBank']]]]//a[@id='SubmitBillShip']//span[contains(text(),'Make Payment')]").click()
		time.sleep(5)
		driver.find_element_by_xpath("//input[@value='Return To the Merchant Site']").click()
		try:
		    # alert = driver.switch_to_alert()
		    # alert.accept()
		    driver.switch_to.alert.accept()
		    print "alert accepted"
		except:
		    print "no alert"    
		#RESERVATION_ID=driver.find_element_by_xpath("//div[label[contains(text(),'Invoice Id')]]/div/p").text
		#RESERVATION_ID=driver.find_element_by_xpath("//div[div[label[contains(text(),'Reservation Id')]]]//p[@class='details-input']").text
		print "trying to get the RESERVATION_ID"
		time.sleep(5)
		# driver.find_element_by_id("creditCardNumber").click()
		# driver.find_element_by_id("creditCardNumber").send_keys("5105")
		# driver.find_element_by_id("creditCardNumber").send_keys("1051")
		# driver.find_element_by_id("creditCardNumber").send_keys("0510")
		# driver.find_element_by_id("creditCardNumber").send_keys("5100")
		# #driver.find_element_by_id("creditCardNumber").send_keys("288888888188")
		# time.sleep(4)
		# print "3"
		# month2 = Select(driver.find_element_by_id("creditCardExpiryMonth"))
		# print "4"
		# month2.select_by_visible_text("04")
		# print "5"
		# time.sleep(2)
		# year=Select(driver.find_element_by_id("creditCardExpiryYear"))
		# print "6"
		# year.select_by_visible_text("2017")
		# print "7"
		# time.sleep(2)
		# driver.find_element_by_id("creditCardCVV").send_keys("477")
		# print "8"
		# time.sleep(2)
		# driver.find_element_by_id("creditCardHolderName").send_keys("ABHILASH MOHANTY")
		# print "9"
		# time.sleep(2)
		# driver.find_element_by_xpath("//form[@id='creditCardDetails']//button[@class='pay']").click()
		# time.sleep(4)
		# try:
		#     alert = driver.switch_to_alert()
		#     alert.accept()
		#     print "alert accepted"
		# except:
		#     print "no alert"
		# print "10" 	
		print "Trying to get the reservation Id"
		time.sleep(6)
		reservationId=driver.find_element_by_xpath("//div[div[label[contains(text(),'Reservation Id')]]]//p[@class='details-input']").text
	print "ReservationId is : "+reservationId
	time.sleep(5)
	driver.quit()
#verify with reservationId
	time.sleep(5)
	logInAgain(reservationId)
	verifyGmail()
	
	
if __name__ == '__main__':
		resendingEmail()	