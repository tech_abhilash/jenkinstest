# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys, os
sys.path.append(os.path.join(os.path.dirname("rounding_rule"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("rounding_rule"), '..', "booking_engine_common_function"))
import rounding_rule_common_function
 
class Nearest999(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True
    
    def test_nearest_999(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        r = rounding_rule_common_function
        r.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        #---selecting tax in Percentage-- 
        option = "2"
        r.selecting_tax(self, hotel_info.Admin_Url, option)
        option = "Nearest 999"
        r.selecting_rounding_rule(self, option)
        one_adult = "450"
        two_adult = "1250"
        extra_child = "1750"
        r.adding_rates(self, one_adult, two_adult, extra_child)
        one_adult_after_rounding = "999.00"
        two_adult_after_rounding = "999.00"
        extra_child_after_rounding = "1,999.00"
        one_adult_after_rounding_tax = "99.90"
        two_adult_after_rounding_tax = "99.90"
        extra_child_after_rounding_tax = "199.90"
        r.checking_booking_engine_preview(self, hotel_info.Preview_Url, one_adult_after_rounding, one_adult_after_rounding_tax, 1)
        r.checking_booking_engine_preview(self, hotel_info.Preview_Url, two_adult_after_rounding, two_adult_after_rounding_tax, 2)
        booking_id = r.checking_booking_engine_preview(self, hotel_info.Preview_Url, extra_child_after_rounding, extra_child_after_rounding_tax, 3)
        r.checking_reservations_tab(self, hotel_info.Admin_Url, booking_id, extra_child_after_rounding, extra_child_after_rounding_tax)
        time.sleep(10)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
