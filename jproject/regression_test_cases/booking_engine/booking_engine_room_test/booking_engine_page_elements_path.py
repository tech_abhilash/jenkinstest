#-----------------Add New seasons Path----------------------
add_seasons_path = "/html/body/div[2]/div/section/div/div[1]/button"
seasons_name_path = "/html/body/div[2]/div/section/div[2]/div/div[2]/div/div[2]/form/div/div[1]/input"
add_period_path = "/html/body/div[2]/div/section/div/div[2]/div/div[3]/div/button"
seasons_save_path = "/html/body/div[2]/div/section/div/div[2]/div/div[4]/button[2]"


#-----------------Add New Penalty Path-----------------------
add_penalty_path = "/html/body/div[2]/div/section/div/div[1]/button"
penalty_name_path = "/html/body/div[2]/div/section/div[2]/div/div[2]/div/div[2]/form/div/div[1]/input"
penalty_save_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/button[2]"
#-----------------Add New Rate Path-------------------------
add_rate_plans_path = "/html/body/div[2]/div/section/div/div[1]/button"
rate_plans_name_path = "/html/body/div[2]/div/section/div[2]/div/div[2]/div/div[2]/form/div/div[1]/input"
active_rate_plans_path = "/html/body/div[2]/div/section/div/div[2]/div/div[3]/div/div/label"
one_adult_value_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[2]/div[1]/div/div[2]/div[2]/div[1]/input"
two_adult_value_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[2]/div[1]/div/div[2]/div[2]/div[2]/input"
extra_child_value_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[2]/div[1]/div/div[2]/div[2]/div[3]/input"
rate_short_description_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[8]/textarea"
rate_detail_Description_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[9]/textarea"
rate_plans_save_path = "/html/body/div[2]/div/section/div/div[2]/div/div[6]/button[2]"
second_room_one_adult_value_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[2]/div[2]/div/div[2]/div[2]/div[1]/input"
second_room_two_adult_value_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[2]/div[2]/div/div[2]/div[2]/div[2]/input"
second_room_extra_child_value_path = "/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[2]/div[2]/div/div[2]/div[2]/div[3]/input"
#--------------------Add New Tax ------------------------------
add_tax_path = "/html/body/div[2]/div/section/div/div[1]/button"
tax_name_path = "/html/body/div[2]/div/section/div[2]/div/div[2]/div/div[2]/form/div/div[1]/input"
tax_description_path = "/html/body/div[2]/div/section/div/div[2]/div/form/div[2]/div/textarea"
tax_save_path = "/html/body/div[2]/div/section/div/div[2]/div/div[3]/button[2]"
#--------------------Messaging-------------------------------------
booking_message_iframe_path = "/html/body/div[2]/div/section/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/iframe"
confirmation_message_iframe_path = "/html/body/div[2]/div/section/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/iframe"
confirmation_message_iframe_path = "/html/body/div[2]/div/section/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/iframe"
reservation_policy_iframe_path = "/html/body/div[2]/div/section/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/iframe"
terms_conditions_iframe_path = "/html/body/div[2]/div/section/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/iframe"
message_save_button = "/html/body/div[2]/div/section/div/div[2]/div/div[4]/button[2]"







#--------------Preview Path-------------------
website_booknow_more_info = "/html/body/div[1]/div[1]/div[1]/div/div/div[1]/div/a/span/i[2]"

								 
web_site_book_now_button_path = "/html/body/div[1]/div[1]/div[1]/div/div/form/div[10]/div/input"
adult_plus_button_path = "/html/body/section/div[1]/div[2]/div/div[1]/div/div[2]/div/div/div[1]/div/div[2]/div[1]/div[2]/div/div[1]/div/input[3]"
child_plus_button_path = "/html/body/section/div[1]/div[2]/div/div[1]/div/div[2]/div/div/div[1]/div/div[2]/div[1]/div[2]/div/div[2]/div/input[3]"
check_availability_path = "/html/body/section/div[1]/div[2]/div/div[1]/div/div[2]/div/div/div[1]/div/div[3]/div/div/div[3]/div/button"
promotion_code_path = "/html/body/section/div[1]/div[2]/div/div[1]/div/div[2]/div/div/div[1]/div/div[3]/div/div/div[1]/div/div/input"



select_room_title_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/span[2]"
room_name_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[1]/div/div/div[1]/div/div/p[1]"
max_adult_max_child_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[1]/div/div/div[1]/div/div/p[2]/span[2]"
price_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[1]/div/div/div[3]/div[1]/div/span"
second_room_name_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div/p[1]"
second_room_max_adult_max_child_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div/p[2]/span[2]"
second_room_price_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div/div/div[3]/div[1]/div/span"
modify_button_path = "/html/body/section/div[1]/div[2]/div/div[1]/div/div[1]/div[2]/div/div[2]/button"
modify_proceed_button_path = "/html/body/section/div[1]/div[2]/div/div[1]/div/div[3]/div/div/div/div[2]/div/button[1]"
error_msg_path = "/html/body/section/div[1]/div[2]/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div"
view_rate_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/span/i[1]/span"
second_room_view_rate_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div/div/span/i[1]/span"
preview_rate_plans_name_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[1]/div/div/div[1]/div[1]"
second_room_preview_rate_plans_name_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div/div[1]/div[1]"

preview_rate_plans_short_description = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[1]/div/div/div[1]/div[2]"
preview_view_detail_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[1]/div/div/div[1]/div[3]/a"
preview_rate_plans_detail_description = "/html/body/section/div[2]/div/div/div/div[1]/div[2]/div/div/div[1]/div/div"
#*
preview_view_detail_close_path = "/html/body/section/div[2]/div/div/div/div[2]/button"
detail_tab_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/ul/li[2]/a"
detail_room_detail_description = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[2]/div[1]/p"
photos_tab_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/ul/li[3]/a"
photo_tab_image_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[3]/div/div[1]/div[1]/"
photos_tab_view_rate_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[3]/div/div[2]/button"					


select_room_tab_book_now_path = "/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[1]/div/div/div[2]/div/div[2]/button"
guest_information_tab_title_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[1]/div/span[2]"
guest_information_tab_add_name_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[1]/div/div/input"
guest_information_tab_add_email_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[2]/div/div[1]/div/input"
guest_information_tab_add_phone_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[2]/div/div[2]/div/span[2]/input"
guest_information_tab_add_address_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[3]/div/div/textarea"
guest_information_tab_add_address_all_data_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[3]/div/div/input"
guest_information_tab_add_city_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[4]/div/div[1]/div/input"
guest_information_tab_add_state_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[4]/div/div[2]/div/input"
guest_information_tab_add_pincode_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[5]/div/div[1]/div/input"
guest_information_tab_add_country_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[5]/div/div[2]/div/input"
guest_information_tab_add_requests_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[6]/div/div/textarea"
guest_information_tab_check_in_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/div[1]/div[2]/div/span"
guest_information_tab_check_out_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/div[2]/div[2]/div/span"
guest_information_tab_no_of_night_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[1]/div[2]/div[1]/div[2]/p"
guest_information_tab_room_name_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[1]/div[2]/p"
guest_information_tab_adult_child_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[2]/p"
guest_information_tab_room_rate_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/p"
guest_information_tab_sub_total_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[2]/div[2]/p"
guest_information_tab_tax_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[3]/div[2]/p"
guest_information_tab_grand_total_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[4]/div/div/div[2]/p"
guest_information_tab_pay_now_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[5]/div[2]/p"
guest_information_tab_balance_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[6]/div[2]/p"
guest_information_book_now_and_pay_button_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[5]/div[2]/div/input"
guest_information_book_now_and_pay_button_with_all_data_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[8]/div[2]/div/input"
guest_information_book_now_and_pay_button_error_msg_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[4]/div/div/div/div"
guest_information_book_now_and_pay_button_error_msg_all_data_path = "/html/body/section/div[1]/div[2]/div/div[3]/div[1]/div[2]/div/div/div[2]/div/form/div[7]/div/div/div/div"


thank_you_for_your_reservation_tab_path = "/html/body/section/div/div/div/div/div/div/div/div[1]/p"
thank_you_for_your_reservation_tab_name_path = "/html/body/section/div/div/div/div/div/div/div/div[2]/div/div[1]/div/div/p"
thank_you_for_your_reservation_tab_check_in_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[1]/div[1]/div/div[1]/div[2]/div/span"
thank_you_for_your_reservation_tab_check_out_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[1]/div[1]/div/div[2]/div[2]/div/span"
thank_you_for_your_reservation_tab_no_of_night_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[1]/div[2]/div[1]/div[2]"
thank_you_for_your_reservation_tab_room_name_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[1]/div[2]/div[2]/div[1]/div[2]/p"
thank_you_for_your_reservation_tab_adult_child_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[1]/div[2]/div[2]/div[2]/div[2]/p"
thank_you_for_your_reservation_tab_room_rate_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[2]/div/div[1]/div/div[2]/p"
thank_you_for_your_reservation_tab_sub_total_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[2]/div/div[2]/div[2]/p"
thank_you_for_your_reservation_tab_tax_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[2]/div/div[3]/div[2]/p"
thank_you_for_your_reservation_grand_total_path = "/html/body/section/div/div/div/div/div/div/div/div[4]/div/div[2]/div/div[4]/div/div/div[2]/p"

payment_gateway_page_heading_path = "/html/body/div/form/div[1]/div/span"
payment_gateway_page_price_path = "/html/body/div/form/div[2]/div[3]/div[2]/div[4]/div/div/div[1]/div/div[7]/span/strong"
