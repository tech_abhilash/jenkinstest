# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
import info_data
import booking_engine_page_elements_path
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("booking_engine_room_test"), '..', 'room'))
import room_page_info_data
import room_page_elements_path
sys.path.append(os.path.join(os.path.dirname("booking_engine_room_test"), '..', ''))
import hotel_info

class Untitled2(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_untitled2(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys(hotel_info.user_name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(hotel_info.password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Rooms").click()

        #-------------Creating New Room------------------------------------------

        driver.find_element_by_link_text("Add New Room").click()
        driver.find_element_by_id("pagename").send_keys(room_page_info_data.second_room_name)
        driver.find_element_by_xpath(room_page_elements_path.Submit_Button_Path).click()
        time.sleep(10)

        #-------------Adding Hero Images-----------------------------------------

        driver.find_element_by_xpath(room_page_elements_path.Add_Hero_Image_Path).click()
        driver.find_element_by_xpath("/html/body/div[7]/div[2]/div/div[1]/div/div/div[1]/ul/li[1]/a").click()
        time.sleep(3)
        driver.find_element_by_xpath(room_page_elements_path.Image_Path + "li[1]/div").click()
        driver.find_element_by_xpath(room_page_elements_path.Image_Path + "li[2]/div").click()
        driver.find_element_by_xpath(room_page_elements_path.Image_Path + "li[3]/div").click()
        time.sleep(3)
        driver.find_element_by_id("doneBtn").click()
        time.sleep(5)
        driver.find_element_by_xpath(room_page_elements_path.Submit_Button_Path).click()
        time.sleep(10)
        #------------Adding Text to Short and Detailed Description of the Room-----------------
        
        driver.switch_to_frame(driver.find_element_by_xpath(room_page_elements_path.Short_Description_Iframe_Path))
        driver.find_element_by_xpath("/html/body").send_keys(room_page_info_data.Room_Short_Description)
        driver.switch_to_default_content()


        driver.switch_to_frame(driver.find_element_by_xpath(room_page_elements_path.Detailed_Description_Iframe_Path))
        driver.find_element_by_xpath("/html/body").send_keys(room_page_info_data.Room_Detailed_Description)
        driver.switch_to_default_content()
         #------------Adding User Content-----------------------
        #------------Adding H1 and H2-------------------------------
        driver.find_element_by_xpath(room_page_elements_path.Add_Content_Path).click()
        driver.find_element_by_link_text("Headers").click()
        driver.find_element_by_xpath(room_page_elements_path.H1_Path).clear()
        driver.find_element_by_xpath(room_page_elements_path.H1_Path).send_keys(room_page_info_data.second_room_h1)
        driver.find_element_by_xpath(room_page_elements_path.H1_Done_Button_Path).click()


        #-------------Save------------------------------
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)
        
        #-------------------Room Information & Amenities-------------------------------
        driver.find_element_by_link_text("Room Information & Amenities").click()
        driver.find_element_by_id("roomnumbers-2").clear()
        driver.find_element_by_id("roomnumbers-2").send_keys("5")
        driver.find_element_by_id("websitenumbers-2").clear()
        driver.find_element_by_id("websitenumbers-2").send_keys("3")
        driver.find_element_by_id("size-2").clear()
        driver.find_element_by_id("size-2").send_keys("150")
        driver.find_element_by_id("maxAdults-2").clear()
        driver.find_element_by_id("maxAdults-2").send_keys("2")
        driver.find_element_by_id("maxChild-2").clear()
        driver.find_element_by_id("maxChild-2").send_keys("1")
        driver.find_element_by_id("maxGuests-2").clear()
        driver.find_element_by_id("maxGuests-2").send_keys("2")

        
        #-------------Save------------------------------s
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)


        #--------Adding Rates------------------------------------------    
        driver.find_element_by_link_text("Booking Engine").click()
        driver.find_element_by_link_text("Rates").click()
        time.sleep(2)
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[5]/div[2]/div[2]/input").click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.second_room_one_adult_value_path).clear()
        driver.find_element_by_xpath(booking_engine_page_elements_path.second_room_one_adult_value_path).send_keys(info_data.second_room_one_adult_price)
        driver.find_element_by_xpath(booking_engine_page_elements_path.second_room_two_adult_value_path).clear()
        driver.find_element_by_xpath(booking_engine_page_elements_path.second_room_two_adult_value_path).send_keys(info_data.second_room_two_adult_price)
        driver.find_element_by_xpath(booking_engine_page_elements_path.second_room_extra_child_value_path).clear()
        driver.find_element_by_xpath(booking_engine_page_elements_path.second_room_extra_child_value_path).send_keys(info_data.second_room_extra_child_price)
        driver.find_element_by_xpath(booking_engine_page_elements_path.rate_plans_save_path).click()
        time.sleep(2)
        


        time.sleep(10)

        #-------------Save and Generation------------------------------
        driver.find_element_by_link_text("Preview").click()
        driver.find_element_by_id("idGenerateSite").click()
        time.sleep(20)


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
