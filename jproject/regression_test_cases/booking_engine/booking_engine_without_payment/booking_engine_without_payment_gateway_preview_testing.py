# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import info_data
import booking_engine_page_elements_path
import sys
import os
import MySQLdb
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'simpadmin/room'))
import room_page_info_data
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '..', "booking_engine_common_function"))
import testing_booking_engine_data_common_function
import creating_booking_engine_data_common_function

class TestingBEwithoutPayment(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True
    
    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).click()
        except NoSuchElementException:
            return False
        return True
    
    def test_booking_engine(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        test_be = testing_booking_engine_data_common_function
        creating_booking_engine_data_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        hotel_id = test_be.getting_hotel_id(self)
        driver.get(hotel_info.Preview_Url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        db = MySQLdb.connect(host='simplotel-db.c2zzwgclqpx6.ap-southeast-1.rds.amazonaws.com', user='simploteladmin', passwd='simplotel123',  db='qa_db')
        cur = db.cursor() 
        test_be.moving_to_new_tab(self)                           
        check_in = driver.find_element_by_xpath(".//*[@id='checkInWrapper']/div/span[1]").text
        print check_in
        check_out = driver.find_element_by_xpath(".//*[@id='checkOutWrapper']/div/span[1]").text
        print check_out
        guest_name = "Test Name"
        guest_phone_no = "8971412668"
        guest_address = "Simplotel"
        guest_email_id = "test.simplotel@gmail.com"
        email_password = "120tc002"
        guset_data = [guest_name, guest_email_id, guest_phone_no, guest_address]  
        # --------------*** No Need To test Max Adult ***-------------------#
        test_be.testing_max_adult(self, room_page_info_data.Room_Name)
        test_be.testing_max_child(self, room_page_info_data.Room_Name)
        test_be.testing_for_one_adult(self, room_page_info_data.Room_Name)
        test_be.testing_for_two_adult(self, room_page_info_data.Room_Name)
        test_be.testing_for_three_adult(self, room_page_info_data.Room_Name)
        test_be.testing_for_child_included_in_the_rate(self, room_page_info_data.Room_Name)
        test_be.testing_for_extra_2child(self, room_page_info_data.Room_Name)
        test_be.testing_for_extra_child(self, room_page_info_data.Room_Name)
         #----------------------------------------Getting Image Name----------------------------------
        with open("../../simpadmin/room/image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        test_be.testing_rate_plane_and_photos(self, room_page_info_data.Room_Detailed_Description, wordList)
        test_be.testing_guest_information(self, check_in, check_out, room_page_info_data.Room_Name, guset_data)
        Booking_id = test_be.testing_booking_conformation_page_with_out_payment(self, check_in, check_out, room_page_info_data.Room_Name, guest_name)
        # Booking_id = 'TBAI3J'
        guset_data = [guest_name, guest_email_id, email_password, guest_phone_no, guest_address]
        booking_data = [Booking_id, check_in, check_out]
        test_be.gmail_verification_of_confirmation_mail(self, hotel_info.hotel_name, guset_data, booking_data)
        test_be.checking_sm_booking_table(self, cur, Booking_id, hotel_id)
            
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
