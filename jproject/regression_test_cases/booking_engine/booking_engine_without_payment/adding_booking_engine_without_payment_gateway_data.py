# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
import info_data
import booking_engine_page_elements_path
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function

class BEwithoutpayment(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
        
    def test_bookingengine(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        admin = creating_booking_engine_data_common_function
        #-- login --
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        #------------- Enable Simplotel BE------------------------------------------
        admin.enable_simplotel_be(self)
        # --------Selecting Simplotel BE------------------------------------------
        admin.selecting_simplotel_be(self)
        #--------Adding Seasons------------------------------------------    
        season_name = "Seasons"
        next_month = True      
        admin.adding_seasons(self, season_name, next_month)
        # --------Adding Cancellation Penalties------------------------------------------ 
        penalty_name = "Penalty" 
        admin.adding_cancellation(self, penalty_name)
        #--------Adding Rates------------------------------------------   
        rate_plane_name = "Rate Plan"
        admin.adding_rates(self, rate_plane_name)
        #--------Adding Taxes and Fees------------------------------------------    
        tax_name = "Tax"
        admin.adding_taxes_and_fees(self, tax_name)
        
        driver.find_element_by_link_text("Preview").click()
        driver.find_element_by_id("idGenerateSite").click()
        time.sleep(20)                          
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
