# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import booking_engine_page_elements_path
import sys
import os
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'simpadmin/room'))
import room_page_info_data
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '..', "booking_engine_common_function"))
import testing_booking_engine_data_common_function


class TestingAirpay(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True
    
    def test_airpay(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        testing_booking_engine_data_common_function.moving_to_new_tab(self)                           
        check_in = driver.find_element_by_xpath(".//*[@id='checkInWrapper']/div/span[1]").text
        print check_in
        check_out = driver.find_element_by_xpath(".//*[@id='checkOutWrapper']/div/span[1]").text
        print check_out  
        testing_booking_engine_data_common_function.testing_max_adult(self, room_page_info_data.Room_Name)
        testing_booking_engine_data_common_function.testing_max_child(self, room_page_info_data.Room_Name)
        testing_booking_engine_data_common_function.testing_for_one_adult(self, room_page_info_data.Room_Name)
        testing_booking_engine_data_common_function.testing_for_two_adult(self, room_page_info_data.Room_Name)
        testing_booking_engine_data_common_function.testing_for_three_adult(self, room_page_info_data.Room_Name)
        testing_booking_engine_data_common_function.testing_for_child_included_in_the_rate(self, room_page_info_data.Room_Name)
        testing_booking_engine_data_common_function.testing_for_extra_2child(self, room_page_info_data.Room_Name)
        testing_booking_engine_data_common_function.testing_for_extra_child(self, room_page_info_data.Room_Name)
         #----------------------------------------Getting Image Name----------------------------------
        with open("../../simpadmin/room/image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        testing_booking_engine_data_common_function.testing_rate_plane_and_photos(self, room_page_info_data.Room_Detailed_Description, wordList)
        testing_booking_engine_data_common_function.testing_guest_information(self, check_in, check_out, room_page_info_data.Room_Name)

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
