# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidElementStateException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("testing_rate_plans"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("testing_rate_plans"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function
import rate_plan_common_function
import testing_booking_engine_data_common_function
import common_data

class Lengthofstay(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def check_visible_by_xpath(self, xpath, days):
        try:
            self.driver.find_element_by_xpath(xpath).clear()
            self.driver.find_element_by_xpath(xpath).send_keys(days)
        except InvalidElementStateException:
            return False
        return True
    
    def test_length_of_stay(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        # -------- login ------------------------------------------
        creating_booking_engine_data_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        # --------Adding length of stay------------------------------------------   
        rate_plan_common_function.removing_maximum_advance_purchase(self, 2)
        rate_plan_common_function.adding_minimum_length_of_stay(self, 3)

        driver.get(hotel_info.Preview_Url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        testing_booking_engine_data_common_function.moving_to_new_tab(self)
        rate_plan_common_function.testing_minimum_length_of_stay(self)
        
        driver.get(self.base_url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        rate_plan_common_function.removing_minimum_advance_purchase(self, 3)
        rate_plan_common_function.adding_maximum_length_of_stay(self, 2)

        driver.get(hotel_info.Preview_Url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        testing_booking_engine_data_common_function.moving_to_new_tab(self)
        rate_plan_common_function.testing_maximum_length_of_stay(self)
       
        driver.get(self.base_url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        rate_plan_common_function.adding_minimum_length_of_stay(self, 3)

        driver.get(hotel_info.Preview_Url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        testing_booking_engine_data_common_function.moving_to_new_tab(self)
        rate_plan_common_function.testing_minimum_length_of_stay(self)
        d.refresh()
        d.implicitly_wait(30)
        rate_plan_common_function.testing_maximum_length_of_stay(self)


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
