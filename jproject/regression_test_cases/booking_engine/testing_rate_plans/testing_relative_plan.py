# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
# import booking_engine_page_elements_path
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'simpadmin/room'))
import room_page_info_data
sys.path.append(os.path.join(os.path.dirname("testing_rate_plans"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("testing_rate_plans"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function
import rate_plan_common_function
import testing_booking_engine_data_common_function
import common_data

class Relativerateplan(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True
    
    def test_relativerateplan(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        # #-------- login ------------------------------------------
        # creating_booking_engine_data_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        # #--------Adding Rates------------------------------------------   
        # creating_booking_engine_data_common_function.adding_rates(self, common_data.secound_rate_plane_name)
        # rate_plan_common_function.adding_relative_rates(self, common_data.secound_rate_plane_name, common_data.secound_rate_plane_amount, common_data.secound_rate_plane_unit)
        # creating_booking_engine_data_common_function.adding_rates(self, common_data.third_rate_plane_name)
        # rate_plan_common_function.adding_relative_rates(self, common_data.third_rate_plane_name, common_data.third_rate_plane_amount, common_data.third_rate_plane_unit)
        # time.sleep(15)
        # #-----------------testing Preview-----------------------
        driver.get(hotel_info.Preview_Url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        time.sleep(10)
        testing_booking_engine_data_common_function.moving_to_new_tab(self) 
        
        rate_plan_common_function.testing_one_adult_with_relative_rate(self, common_data.secound_rate_plane_amount)
        rate_plane = [common_data.rate_plane_name,common_data.secound_rate_plane_name,common_data.third_rate_plane_name]
        rate_plan_common_function.testing_rate_plan_and_rates(self, rate_plane, common_data.one_adult_price, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 1)

        rate_plan_common_function.testing_two_adult_with_relative_rate(self, common_data.secound_rate_plane_amount)
        rate_plan_common_function.testing_rate_plan_and_rates(self, rate_plane, common_data.two_adult_price, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 1)
        
        rate_plan_common_function.testing_three_adult_with_relative_rate(self, common_data.secound_rate_plane_amount)
        rate_plan_common_function.testing_rate_plan_and_rates(self, rate_plane, common_data.three_adult_price, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 1)

        rate_plan_common_function.testing_for_child_included_in_the_rate_with_relative_rate(self, common_data.secound_rate_plane_amount)
        rate_plan_common_function.testing_rate_plan_and_rates(self, rate_plane, common_data.one_adult_price, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 1)

        rate_plan_common_function.testing_for_extra_2child_relative_rate(self, common_data.secound_rate_plane_amount)
        total = float(common_data.one_adult_price)+float(common_data.extra_child_price)+float(common_data.extra_child_price)
        rate_plan_common_function.testing_rate_plan_and_rates(self, rate_plane, total, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 3)
        print "extra child"
        rate_plan_common_function.testing_for_extra_child_relative_rate(self, common_data.secound_rate_plane_amount)
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        rate_plan_common_function.testing_rate_plan_and_rates(self, rate_plane, total, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 2)
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        rate_plan_common_function.testing_guest_information(self, total, common_data.secound_rate_plane_amount)


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
