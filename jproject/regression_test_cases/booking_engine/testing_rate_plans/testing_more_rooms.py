# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
# import booking_engine_page_elements_path
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("testing_rate_plans"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("testing_rate_plans"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function
import more_rooms_common_function
import testing_booking_engine_data_common_function
import common_data

class Morerooms(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True
    
    def test_morerooms(self):
        driver = self.driver
        driver.get(hotel_info.Preview_Url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        time.sleep(10)
        testing_booking_engine_data_common_function.moving_to_new_tab(self)
        no_of_adults = 2 
        no_of_child = 0
        more_rooms_common_function.adding_new_room(self, 2, no_of_adults, no_of_child)
        no_of_adults = 3 
        no_of_child = 0
        more_rooms_common_function.adding_new_room(self, 3, no_of_adults, no_of_child)
        no_of_adults = 1 
        no_of_child = 1
        more_rooms_common_function.adding_new_room(self, 4, no_of_adults, no_of_child)
        no_of_adults = 1 
        no_of_child = 3
        more_rooms_common_function.adding_new_room(self, 5, no_of_adults, no_of_child)
        no_of_adults = 2 
        no_of_child = 2
        more_rooms_common_function.adding_new_room(self, 6, no_of_adults, no_of_child)
        more_rooms_common_function.check_availability(self)
        rate_plane = [common_data.rate_plane_name,common_data.secound_rate_plane_name,common_data.third_rate_plane_name]
        room_1_price = more_rooms_common_function.testing_rate_plan_and_rates(self, rate_plane, common_data.one_adult_price, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 1, 1)
        room_2_price = more_rooms_common_function.testing_rate_plan_and_rates(self, rate_plane, common_data.two_adult_price, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 1, 2)
        room_3_price = more_rooms_common_function.testing_rate_plan_and_rates(self, rate_plane, common_data.three_adult_price, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 1, 3)
        room_4_price = more_rooms_common_function.testing_rate_plan_and_rates(self, rate_plane, common_data.one_adult_price, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 1, 4)
        total = float(common_data.one_adult_price)+float(common_data.extra_child_price)+float(common_data.extra_child_price)
        room_5_price = more_rooms_common_function.testing_rate_plan_and_rates(self, rate_plane, total, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 3, 5)
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        room_6_price = more_rooms_common_function.testing_rate_plan_and_rates(self, rate_plane, total, common_data.secound_rate_plane_amount, common_data.third_rate_plane_amount, 2, 6)
        room_price = [room_1_price, room_2_price, room_3_price, room_4_price, room_5_price, room_6_price]
        more_rooms_common_function.testing_guest_information(self, room_price)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
