instapay_create_invoice_button=".//*[@class='content-heading']/h1/button"
instapay_title_input="//div[div[label[contains(text(),'Title')]]]/div[2]/input"
instapay_description_input="//div[div[label[contains(text(),'Description')]]]/div[2]/textarea"
instapay_customer_name_input="//div[div[label[contains(text(),'Customer Name')]]]/div[2]/input"
instapay_email_input="//div[div[label[contains(text(),'Email')]]]/div[2]/input"
instapay_phone_number_input="//div[div[label[contains(text(),'Phone Number')]]]/div[2]/input"
instapay_amount_input="//div[div[label[contains(text(),'Amount')]]]/div[2]/input"
instapay_save_button="//button[contains(text(),'Save')]"

invoice_status="//table/tbody/tr[1]/td[1]"
genereted_id="//table/tbody/tr[1]/td[4]"
gmail_url="http://www.gmail.com"
invoice_generetion_verification="//table/tbody/tr[1]/td[6]"
click_preview="//a[@class='dropdown-toggle']"
generate_desktop="idGenerateSite"

#verify_gmail
#-----------------
# click_on_invoice="//div[span[b[contains(text(),'"+data.TITLE+"')]]]/span[contains(text(),'Customer Support')]"
gmail_price_verify="//tr[td[contains(text(),'GRAND TOTAL')]]/td[2]"
complete_payment="//a[contains(text(),'Complete Payment')]"

#Avenue Test
#-----------------
click_netbanking="//ul[@class='resp-tabs-list span3']/li[3]/div[1]/span[2]"
select_class="netBankingBank"
visible_text="AvenuesTest"
make_payment="//div[div[div[select[@id='netBankingBank']]]]//a[@id='SubmitBillShip']//span[contains(text(),'Make Payment')]"
return_to_marchant_site="//input[@value='Return To the Merchant Site']"

#payment_verifivation
verify_invoice_id="//table/tbody/tr[1]/td[4]"
verify_invoice_status="//table/tbody/tr[1]/td[1]"


