from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re
import datetime
from time import gmtime, strftime
import os,sys,inspect
sys.path.append(os.path.join(os.path.dirname("instapay"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("instapay"), '..', "booking_engine_common_function"))
import instapay_and_webpay_common_function
import creating_booking_engine_data_common_function


class Webpay(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_web_pay(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        instapay_and_webpay_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        driver.find_element_by_link_text("Ops").click()
        url = driver.current_url
        url = url.split("/")
        property_id = url[4]
        instapay_and_webpay_common_function.enable_simplotel_instapay(self)
        # --------Selecting Payment Gateway-----------------------
        option = "CC Avenue"
        creating_booking_engine_data_common_function.adding_payment_gateway(self, option)
        # --------Selecting Payment policy------------------------
        creating_booking_engine_data_common_function.adding_payment_policy(self)
        # property_id = 875
        t = strftime("%H:%M", gmtime())
        description = "Invoice No test-"+ str(t) +""
        customer_name = "Test Name"
        phone_number = "8050896077"
        email_id = "test.simplotel@gmail.com"
        email_password = "120tc002"
        amount = 5
        invoice_data = [customer_name, email_id, phone_number, amount, description]
        instapay_and_webpay_common_function.creating_webpay_invoice(self, invoice_data, property_id)
        confirm_invoice_id = instapay_and_webpay_common_function.testing_ccavenue_payment_gateway_for_wedpay(self, amount, customer_name)
        print "-----"
        print confirm_invoice_id
        # ------ login ------------------------------------------
        driver.get(self.base_url + "/")
        driver.maximize_window()
        # instapay_and_webpay_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        status = "COMPLETED"
        invoice_data = [customer_name, email_id, description, amount]
        invoice_id = instapay_and_webpay_common_function.testing_invoice(self, invoice_data, status)
        self.assertEqual(confirm_invoice_id, invoice_id)

        # status = "INITIATED"
        # invoice_data = [title, customer_name, email_id, phone_number, amount, description]
        # invoice_id = instapay_and_webpay_common_function.testing_invoice(self, invoice_data, status)
        # email_subject = "Invoice from " + hotel_info.Hotel_Name + " for " + title
        # invoice_data = [title, email_id, email_password, amount, description]
        # instapay_and_webpay_common_function.gmail_verification(self, hotel_info.Hotel_Name, email_subject, invoice_data)
        # instapay_and_webpay_common_function.testing_ccavenue_payment_gateway(self, invoice_id, amount, customer_name)
        # invoice_data = [title, customer_name, email_id, email_password, amount, description]
        # instapay_and_webpay_common_function.gmail_verification_of_confirmation_mail(self, hotel_info.Hotel_Name, invoice_data, invoice_id)
        # driver.get(self.base_url + "/")
        time.sleep(30)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()