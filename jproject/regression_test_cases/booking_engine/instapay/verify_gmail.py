from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import datetime
import sys
import inventory_xpaths as xpath
import sys
import os
import os,sys,inspect
sys.path.append(os.path.join(os.path.dirname("instapay"), '../..', 'config'))
import hotel_info
# import hotel_info
import datetime
import instapay_info_data as data
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException

def gmail_verification(ID_GENERETED):
	driver=webdriver.Firefox()
	driver.maximize_window()
	driver.get(xpath.gmail_url)
	driver.find_element_by_id("Email").send_keys(data.gmail_id)
	driver.find_element_by_id("next").click()
	time.sleep(4)
	driver.find_element_by_id("Passwd").send_keys(data.gmail_password)
	driver.find_element_by_id("signIn").click()
	time.sleep(15)
	#driver.refresh()
	time.sleep(10)
	print hotel_info.Hotel_Name
	hotel_name=hotel_info.Hotel_Name
	try:
		#driver.find_element_by_xpath("//div[span[b[contains(text(),'Invoice from "+hotel_info.Hotel_Name+" for test invoice')]]]/span[contains(text(),'Customer Support')]").click()
		#driver.find_element_by_xpath("//div[span[b[contains(text(),'new1')]]]/span[contains(text(),'Customer Support')]").click()
		driver.find_element_by_xpath(xpath.click_on_invoice).click()
		#driver.find_element_by_xpath("//div[span[b[contains(text(),'test invoice7')]]]/span[contains(text(),'Customer Support')]").click()
		time.sleep(8)
		var=driver.find_element_by_xpath(xpath.gmail_price_verify).text
		price=float(var[4:])
		print data.AMOUNT
		print price
		if (float(data.AMOUNT)==price):
			print "Invoice genereted SUCCESSFULLY"
		else:
			print "NOT THE SAME INVOICE"						
		time.sleep(10)
		driver.find_element_by_xpath(xpath.complete_payment).click()
		time.sleep(3)
		window_after = driver.window_handles[1]
		driver.switch_to_window(window_after)
		driver.maximize_window()
		time.sleep(6)
		time.sleep(4)
		driver.find_element_by_xpath(xpath.click_netbanking).click()
		time.sleep(3)
		select=Select(driver.find_element_by_id(xpath.select_class))
		select.select_by_visible_text(xpath.visible_text)
		time.sleep(3)
		driver.find_element_by_xpath(xpath.make_payment).click()
		time.sleep(5)
		driver.find_element_by_xpath(xpath.return_to_marchant_site).click()
		try:
		    alert = driver.switch_to_alert()
		    alert.accept()
		    print "alert accepted"
		except:
		    print "no alert"    
		#RESERVATION_ID=driver.find_element_by_xpath("//div[label[contains(text(),'Invoice Id')]]/div/p").text
		#RESERVATION_ID=driver.find_element_by_xpath("//div[div[label[contains(text(),'Reservation Id')]]]//p[@class='details-input']").text
		print "trying to get the RESERVATION_ID"
		# time.sleep(12)
		# RESERVATION_ID=driver.find_element_by_xpath("//div[div[div[div[p[contains(text(),'sudip')]]]]]/div[2]/div/div/p[@class='details-input']").text
		# print RESERVATION_ID
	except NoSuchElementException:
		print "The generated Invoice is not delivered yet,need to increase the timeout"
	driver.quit()
	payment_verification(ID_GENERETED)
	verify_confirmation(hotel_name)
	#verifying the payment

def payment_verification(ID_GENERETED):
	driver1=webdriver.Firefox()
	driver1.maximize_window()
	driver1.get(hotel_info.Admin_Url)
	#driver1.find_element_by_name("userid").send_keys(hotel_info.Admin_Username)
	driver1.find_element_by_name("userid").send_keys(data.ADMIN_USERNAME)
	#driver1.find_element_by_name("password").send_keys(hotel_info.Admin_Password)
	driver1.find_element_by_name("password").send_keys(data.ADMIN_PASSWORD)
	driver1.find_element_by_xpath(xpath.submit).click()
	print "Clicked on LogIn"
	hotel_name=hotel_info.Hotel_Name
	time.sleep(6)
	print "trying to get our desired hotel"
	driver1.find_element_by_xpath(xpath.select_hotel_button).click()
	time.sleep(3)
	driver1.execute_script("window.scrollTo(0, document.body.scrollHeight);")
	time.sleep(6)
	driver1.find_element_by_xpath("//a[text()='"+hotel_name+"']").click()
	print "Clicked on our desired hotel"
	driver1.find_element_by_xpath(xpath.click_on_bookingengine).click()
	time.sleep(8)
	driver1.find_element_by_xpath(xpath.invoice).click()
	time.sleep(12)
	new_id=driver1.find_element_by_xpath(xpath.verify_invoice_id).text
	print new_id
	status=driver1.find_element_by_xpath(xpath.verify_invoice_status).text
	print status
	if (ID_GENERETED==new_id):
		if (status=="COMPLETED"):
			print "SUCCESSFULLY UPDATED THE DATABASE"
		else:
			print "VALUE NOT REFLECTED"	
	else:
		print "ID is not same,Please try again"
	driver1.quit()	

def verify_confirmation(hotel_name):
	driver2=webdriver.Firefox()
	driver2.maximize_window()
	driver2.get(xpath.gmail_url)
	driver2.find_element_by_id("Email").send_keys(data.gmail_id)
	driver2.find_element_by_id("next").click()
	time.sleep(4)
	driver2.find_element_by_id("Passwd").send_keys(data.gmail_password)
	driver2.find_element_by_id("signIn").click()
	time.sleep(15)
	try:
		#driver2.find_element_by_xpath("//div[span[b[contains(text(),'Invoice Payment Confirmation')]]]//span[contains(text(),'test hotel 29th mar')]").click()
		driver2.find_element_by_xpath("//div[span[b[contains(text(),'Invoice Payment Confirmation')]]]//span[contains(text(),'"+hotel_name+"')]").click()
		print "Entered in to the try block"
		#driver2.find_element_by_xpath("//div[span[contains(text(),'Invoice Payment Confirmation')]]//span[contains(text(),'test hotel 29th mar')]").click()
		time.sleep(6)
		text1=driver2.find_element_by_xpath("//p[contains(text(),'Invoice ID')]").text
		print text1
		values=text1.split(":")
		#print values
		confirmation_id=values[1]
		#print confirmation_id
	except NoSuchElementException:
		print "Confirmation mail is not delivered yet,need to increase the timeout"
	#driver.find_element_by_xpath("//div[span[b[contains(text(),'Invoice from test hotel 29th mar for test invoice')]]]/span[contains(text(),'Customer Support')]").click()
if __name__ == '__main__': 
	gmail_verification()
	payment_verification(ID_GENERETED)