from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest,time
import datetime
import sys
import os
sys.path.append(os.path.join(os.path.dirname("resend_email"), '../..', 'config'))
import hotel_info
from VerifyGmail import gmailVerification

def inventoryMissing():
	driver = webdriver.Firefox()	
	driver.maximize_window()
	#driver.get("http://staging.simplotel.com")
	#driver.get("http://qaadmin.simplotel.com/simp/")
	driver.get(hotel_info.Admin_Url)
	#driver.find_element_by_name("userid").send_keys("simpadmin")
	driver.find_element_by_name("userid").send_keys(hotel_info.user_name)
	#driver.find_element_by_name("password").send_keys("simplehoteladmin!@#")
	driver.find_element_by_name("password").send_keys(hotel_info.password)
	driver.find_element_by_xpath("//button[@type='submit']").click()
	print "Clicked on login"
	day= datetime.date.today()
	#day=datetime.datetime.strftime(datetime.datetime.now()+datetime.timedelta(1),'%Y-%m-%d')
	print day
	today=str(day)
	print "getting todays date : "+ today
	mydate = datetime.date.today()
	month=mydate.strftime("%B")
	day=str(mydate).split("-")[2]
	print month
	date12=day.lstrip('0')
	time.sleep(10)
	#now we are in login

	#driver.find_element_by_xpath("//button[contains(text(),'Wonderla')]").click()
	print "getting our desired hotel"
	time.sleep(5)
	#driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
	#time.sleep(3)
	#driver.find_element_by_xpath("//a[text()='test hotel 29th mar']").click()
	#driver.find_element_by_xpath("//a[text()='Hotel Grand Palace Precidency']").click()
	driver.implicitly_wait(10)
	driver.find_element_by_xpath("//span[contains(text(),'Booking Engine')]").click()
	print "Clicked on Booking Engine"
	driver.find_element_by_xpath("//span[contains(text(),'Inventory')]").click()
	print "Clicked on Inventory"
	time.sleep(8)
	driver.find_element_by_xpath("//a[contains(text(),'All Rooms')]").click()
	print "clicked on Allrooms"
	time.sleep(8)
#	driver.find_element_by_xpath("//div[div[@class='month-heading'][i[@class='fa fa-chevron-right next']]]//div[@data-date='2016-04-04']").click()
	driver.find_element_by_xpath("//div[div[@class='month-heading'][i[@class='fa fa-chevron-left previous']]]//div[@data-date='"+today+"']").click()
#	driver.find_element_by_xpath("//div[div[div[contains(text(),'March')]]]//td[@data-day='10']").click
	time.sleep(5)
	driver.find_element_by_xpath("//button[@class='btn btn-default']").click()
	time.sleep(5)
	driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()
	print "Saving the changes"
	#prev_window=driver.window_handles(0)
	#print prev_window
	time.sleep(10)
	driver.find_element_by_xpath("//a[@class='dropdown-toggle']").click()
	time.sleep(5)
	driver.find_element_by_id("idGenerateSite").click()
	print "Generating the sites"
	time.sleep(8)
	driver.find_element_by_xpath("//a[@class='dropdown-toggle']").click()
	driver.find_element_by_id("desktop-preview").click()
	print "Launching desktop version"
	time.sleep(5)
	window_after = driver.window_handles[1]
	driver.switch_to_window(window_after)
	time.sleep(6)
	driver.maximize_window()
	#In the website trying to book a room
	#driver.find_element_by_xpath("//div[@class='form-group check-btn-holder']/input[@value='Book Now']").click()
	#driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[1]/div/div/form/div[10]/div/input").click()
	driver.find_element_by_xpath("//form[div[label[contains(text(),'Check In')]]]//div/div/input[@type='submit']").click()
	driver.implicitly_wait(30)
	window_after1 = driver.window_handles[2]
	driver.switch_to_window(window_after1)
	driver.maximize_window()
	driver.find_element_by_xpath("//div[@id='checkInWrapper']//span[@id='checkInCalendar']").click()
	print "Clicked on the calender"
	time.sleep(3)
	driver.find_element_by_xpath("//div[div[div[contains(text(),'"+month+"')]]]//button[text()='"+date12+"']").click()
	print "selected the desired date"
	time.sleep(5)
	driver.find_element_by_xpath("//span[contains(text(),'Check Availability')]").click()
	#tracking the Error Message
	output=driver.find_element_by_xpath("//div[@role='alert']/div[@class='error-content']").is_displayed()
	textop=driver.find_element_by_xpath("//div[@role='alert']/div[@class='error-content']").text
	print textop
	time.sleep(5)
	print output
	time.sleep(5)
#	Error_Message=driver.find_element_by_xpath("//div[@role='alert']/div[@class='error-content']").text()
#	print Error_Message
	driver.quit()
	time.sleep(4)
	#now login to gmail to see the alert message
	gmailVerification()

if __name__ == '__main__':
	inventoryMissing()