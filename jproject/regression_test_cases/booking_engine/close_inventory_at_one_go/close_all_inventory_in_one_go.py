from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re
import datetime
import inventory_xpaths as xpath
import os, sys, inspect
sys.path.append(os.path.join(os.path.dirname("close_inventory_at_one_go"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("close_inventory_at_one_go"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function

class CloseInventoryVerification(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_closeInventoryVerification(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        #-- login --
        creating_booking_engine_data_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        print "Clicked on login"
        driver.implicitly_wait(10)
        driver.find_element_by_link_text("Rates & Inventory").click()
        driver.find_element_by_link_text("Inventory").click()
        time.sleep(8)
        driver.find_element_by_link_text("All Rooms").click()
        time.sleep(6)
        beforeClick = driver.find_element_by_xpath(xpath.check_availability).text
        print "Before closing all Inventory the status is : " + beforeClick 
        driver.find_element_by_xpath(xpath.click_date).click()
        driver.find_element_by_xpath(xpath.click_on_close_all).click()
        print "Close all Button is clicked"
        time.sleep(5)
        driver.find_element_by_xpath(xpath.click_on_save).click()
        print "Save Button is clicked"
        time.sleep(10)
        afterClick = driver.find_element_by_xpath(xpath.check_availability).text
        print "After closing all the Inventory the status is : " + afterClick
        index = '2'
        value6 = True
        while (value6 == True):
            try:
                t = int(index)
                driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[1]/ul/li["+ index +"]/a").click()
                roomName = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[1]/ul/li["+ index +"]/a").text
                time.sleep(5)
                value = driver.find_element_by_xpath(xpath.check_availability).text
                val = int(value)
                result = val is 0
                if result:
                    print "For "+roomName+" case PASSED with showing value : " + value
                else:
                    print "For "+roomName+" case  FAILED with showing value : " + value    
                t = t + 1
                index=str(t)

            except NoSuchElementException:
				value6=False
            print "End of while loop"                    
            
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()