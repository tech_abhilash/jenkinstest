# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
import MySQLdb
sys.path.append(os.path.join(os.path.dirname("rounding_rule"), '..', "config"))
import hotel_info
import info_data
import booking_engine_page_elements_path
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("rounding_rule"), '..', "simp_common"))
import crm_common_function
 
class Crm(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_crm(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        crm_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        crm_common_function.unchecking_payment_policy(self, hotel_info.Admin_Url)
        inventry_number = 10
        crm_common_function.edit_inventry(self, hotel_info.Admin_Url,inventry_number)
        booking_data = ["Test Name","email6@simplotel.com","1231234111","simplotel","Test city","test State","577117","Test Country","Test Requests"]
        booking_id = crm_common_function.booking_room(self, hotel_info.Preview_Url, booking_data)
        print booking_id
        time.sleep(20)
        db = MySQLdb.connect(host='simplotel-db.c2zzwgclqpx6.ap-southeast-1.rds.amazonaws.com', user='simploteladmin', passwd='simplotel123',  db='qa_db')
        cur = db.cursor() 
        customer_id = crm_common_function.checking_sm_crm_transaction_details_table(self, cur, booking_id)
        crm_common_function.checking_sm_customer_details_table(self, cur, customer_id, booking_data)
        crm_common_function.checking_sm_customer_address_table(self, cur, customer_id, booking_data)
        
        booking_data_new_phone = ["Test Name","email6@simplotel.com","1231234112","simplotel","Test city","test State","577117","Test Country","Test Requests"]
        booking_id_new = crm_common_function.booking_room(self, hotel_info.Preview_Url, booking_data_new_phone)
        customer_id_new = crm_common_function.checking_sm_crm_transaction_details_table(self, cur, booking_id_new)
        self.assertEqual(customer_id,customer_id_new)
        crm_common_function.checking_sm_customer_phone_table(self, cur, customer_id_new, booking_data_new_phone)
        
        booking_data_new_email = ["Test Name","email7@simplotel.com","1231234112","simplotel","Test city","test State","577117","Test Country","Test Requests"]        
        booking_id_new = crm_common_function.booking_room(self, hotel_info.Preview_Url, booking_data_new_email)
        customer_id_new = crm_common_function.checking_sm_crm_transaction_details_table(self, cur, booking_id_new)
        self.assertEqual(customer_id,customer_id_new)
        crm_common_function.checking_sm_customer_email_table(self, cur, customer_id_new, booking_data_new_email)

        cur.close()
        db.close()


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
