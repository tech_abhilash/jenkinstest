from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
import unittest, time, re
import datetime 
import os, sys, inspect
sys.path.append(os.path.join(os.path.dirname("advance_taxation_rule"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("advance_taxation_rule"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function
import rounding_rule_common_function
import testing_booking_engine_data_common_function

class Advance_Tax(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    def test_advance_tax(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        time.sleep(5)
        creating_booking_engine_data_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        time.sleep(6)
        one_adult = "50"
        two_adult = "150"
        extra_child = "250"
        rounding_rule_common_function.adding_rates(self, one_adult, two_adult, extra_child)
        
        #---- Deleting Old Rate Plan ----
        while 1:
            driver.refresh()
            try:
                driver.find_element_by_xpath("//div[button[@class='btn btn-success']]/ul/li[2]/a").click()
                driver.find_element_by_xpath(".//*[@class='delete-icon']/i").click()
                time.sleep(2)
                driver.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
            except NoSuchElementException, e:
                break
        
        #-----Uncheking tax from rate plan----    
        move_to_tax = driver.find_element_by_xpath("//label[contains(text(),'Show Tax Slab')]")
        self.driver.execute_script("return arguments[0].scrollIntoView();", move_to_tax)
        x = driver.find_element_by_xpath("//div[div[table[thead[tr[th[contains(text(),'TAX NAME')]]]]]]/input").is_selected()
        print x
        time.sleep(5)
        n = 1
        while  1:
            time.sleep(3)
            try:
                present = driver.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr["+str(n)+"]/td[2]/input").is_selected()
                if(present == True):
                    driver.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr["+str(n)+"]/td[2]/input").click()
                    n = n + 1
                else:
                    n = n + 1
            except NoSuchElementException, e:
                driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()
                time.sleep(8)
                driver.execute_script("window.scrollTo(0,0);")
                break

        #--- Deleting Tax ---
        driver.find_element_by_link_text("Rates & Inventory").click()
        driver.find_element_by_link_text("Taxes and Fees").click()
        time.sleep(2)
        while 1:
        	try:
        		driver.find_element_by_xpath("//div[button[@class='btn btn-success']]/ul/li[1]/a").click()
        		time.sleep(3)
        		driver.find_element_by_xpath("//i[@class='fa fa-trash fa-lg']").click()
        		time.sleep(2)
        		driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        		time.sleep(4)
        	except NoSuchElementException, e:
        		break

        # #---- Creating New Tax with out Override ----      
        driver.find_element_by_xpath("//button[@class='btn btn-success']").click()
        tax_name = ['newtax1', 'newtax2', 'newtax3']
        min_rate = ['0', '101', '201']
        max_rate = ['100', '200', '300']
        value_percentage = ['15', '20', '30']
        time.sleep(2)
        driver.find_element_by_xpath("//div[label[contains(text(),'Name of the Tax')]]/input").send_keys(tax_name[0])
        driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()
        time.sleep(5)
        driver.find_element_by_xpath("//textarea[@class='ember-view ember-text-area']").send_keys("First Tax")
        driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()
        time.sleep(5)
        
        #---- Creating New Tax with Override ----              
        driver.find_element_by_xpath("//button[@class='btn btn-success']").click()
        time.sleep(2)
        driver.find_element_by_xpath("//div[label[contains(text(),'Name of the Tax')]]/input").send_keys(tax_name[1])
        driver.find_element_by_xpath("//button[@class='btn btn-primary']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//input[@placeholder='Value']").clear()
        driver.find_element_by_xpath("//input[@placeholder='Value']").send_keys("10")
        driver.find_element_by_xpath("//textarea[@class='ember-view ember-text-area']").send_keys("Second Tax")
        driver.find_element_by_xpath("//div[label[contains(text(),'Override Taxes')]]/div/input[@type='checkbox']").click()
        driver.find_element_by_xpath("//div[h4[contains(text(),'Tax Slab')]]/div[2]/button/i").click()
        time.sleep(2)
        i = 1
        for i in range (1,4):
            driver.find_element_by_xpath("//table[@class='tax-period-list']/tr["+str(i)+"]/div[1]/div[1]/div/input").clear()
            driver.find_element_by_xpath("//table[@class='tax-period-list']/tr["+str(i)+"]/div[1]/div[1]/div/input").send_keys(min_rate[i-1])
            driver.find_element_by_xpath("//table[@class='tax-period-list']/tr["+str(i)+"]/div[1]/div[2]/div/input").clear()
            driver.find_element_by_xpath("//table[@class='tax-period-list']/tr["+str(i)+"]/div[1]/div[2]/div/input").send_keys(max_rate[i-1])
            driver.find_element_by_xpath("//table[@class='tax-period-list']/tr["+str(i)+"]/div[1]/div[3]/div/input").clear()
            driver.find_element_by_xpath("//table[@class='tax-period-list']/tr["+str(i)+"]/div[1]/div[3]/div/input").send_keys(value_percentage[i-1])
            i = i + 1
            if (i < 4):
                driver.find_element_by_xpath("//div[@class='tax-content']//i[@class='fa fa-plus']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()
        time.sleep(5)

        #--- Selecting 2nd Tax ----
        driver.find_element_by_link_text("Rates & Inventory").click()
        driver.find_element_by_link_text("Rates").click()
        time.sleep(5)
        move_to_tax1 = driver.find_element_by_xpath("//label[contains(text(),'Show Tax Slab')]")
        self.driver.execute_script("return arguments[0].scrollIntoView();", move_to_tax1)
        time.sleep(2)
        driver.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr[2]/td[2]/input").click()
        time.sleep(3)
        driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()                 
        time.sleep(5)

        # --- testing Booking Engine with 2nd tax-- 
        driver.get(hotel_info.Preview_Url + "/")
        time.sleep(20)       
        # driver.find_element_by_xpath("//div[p[contains(text(),'Reservations')]]/form/div[11]/div/input[@type='submit']").click()
        testing_booking_engine_data_common_function.moving_to_new_tab(self) 
        time.sleep(5)
        num_adult = 1
        num_child = 0
        tax = (int(one_adult)*int(value_percentage[0]))/100
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, one_adult)
        num_adult = 2
        num_child = 0
        tax = (int(two_adult)*int(value_percentage[1]))/100
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, two_adult)
        num_adult = 1
        num_child = 1
        price = int(one_adult)+int(extra_child)
        tax = (int(price)*int(value_percentage[2]))/100
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, price)
        num_adult = 2
        num_child = 2
        price = int(two_adult)+int(extra_child)+int(extra_child)
        tax = (int(price)*10)/100
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, price)          
        time.sleep(3)

        #--closing new window 
        testing_booking_engine_data_common_function.close_new_tab(self)
        #--- Selecting 1st Tax ----
        driver.get(hotel_info.Admin_Url + "/")       
        driver.find_element_by_link_text("Rates & Inventory").click()
        driver.find_element_by_link_text("Rates").click()
        time.sleep(5)
        move_to_tax1 = driver.find_element_by_xpath("//label[contains(text(),'Show Tax Slab')]")
        self.driver.execute_script("return arguments[0].scrollIntoView();", move_to_tax1)
        time.sleep(2)
        driver.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr[1]/td[2]/input").click()
        time.sleep(3)
        driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()                 
        time.sleep(5)

        #--- testing Booking Engine with 1st and 2nd tax-- 
        driver.get(hotel_info.Preview_Url + "/")    
        time.sleep(20)   
        driver.find_element_by_xpath("//div[p[contains(text(),'Reservations')]]/form/div[11]/div/input[@type='submit']").click()
        testing_booking_engine_data_common_function.moving_to_new_tab(self) 
        time.sleep(5)
        num_adult = 1
        num_child = 0
        tax = ((int(one_adult)*int(value_percentage[0]))/100)+((int(one_adult)*int(10))/100)
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, one_adult)
        num_adult = 2
        num_child = 0
        tax = ((int(two_adult)*int(value_percentage[1]))/100)+((int(two_adult)*int(10))/100)
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, two_adult)
        num_adult = 1
        num_child = 1
        price = int(one_adult)+int(extra_child)
        tax = ((int(price)*int(value_percentage[2]))/100)+((int(price)*int(10))/100)
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, price)
        num_adult = 2
        num_child = 2
        price = int(two_adult)+int(extra_child)+int(extra_child)
        tax = ((int(price)*10)/100)+((int(price)*int(10))/100)
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, price)          
        time.sleep(3)

        #--closing new window 
        testing_booking_engine_data_common_function.close_new_tab(self)

        #--- Unchecking  1st Tax and 2nd Tax ----
        driver.get(hotel_info.Admin_Url + "/")       
        driver.find_element_by_link_text("Rates & Inventory").click()
        driver.find_element_by_link_text("Rates").click()
        time.sleep(5)
        move_to_tax1 = driver.find_element_by_xpath("//label[contains(text(),'Show Tax Slab')]")
        self.driver.execute_script("return arguments[0].scrollIntoView();", move_to_tax1)
        time.sleep(2)
        driver.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr[1]/td[2]/input").click()
        driver.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr[2]/td[2]/input").click()
        time.sleep(3)
        driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()                 
        time.sleep(5)

        #--- testing Booking Engine with Out 1st and 2nd tax-- 
        driver.get(hotel_info.Preview_Url + "/")    
        time.sleep(20)   
        driver.find_element_by_xpath("//div[p[contains(text(),'Reservations')]]/form/div[11]/div/input[@type='submit']").click()
        testing_booking_engine_data_common_function.moving_to_new_tab(self) 
        time.sleep(5)
        num_adult = 1
        num_child = 0
        tax = 0
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, one_adult)
        num_adult = 2
        num_child = 0
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, two_adult)
        num_adult = 1
        num_child = 1
        price = int(one_adult)+int(extra_child)
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, price)
        num_adult = 2
        num_child = 2
        price = int(two_adult)+int(extra_child)+int(extra_child)
        testing_booking_engine_data_common_function.testing_override_taxes(self, num_adult, num_child, tax, price)          
        time.sleep(3)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
            
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
            
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

