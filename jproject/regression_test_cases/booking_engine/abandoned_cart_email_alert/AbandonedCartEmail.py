from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from EmailVerification import verification
from TestData import testUrl
import datetime
import sys
import os
sys.path.append(os.path.join(os.path.dirname("home"), '..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("resend_email"), '..', "room"))
import room_page_info_data
import pyscreenshot as ImageGrab
from selenium.common.exceptions import NoSuchElementException

def abandoned_cart():
# 	driver = webdriver.Firefox()	
# 	driver.maximize_window()
# 	driver.get(TestData.testUrl)
# 	driver.get("http://qaadmin.simplotel.com/simp/")
# 	driver.find_element_by_name("userid").send_keys("simpadmin")
# 	print "entered usename"
# 	driver.find_element_by_name("password").send_keys("simplehoteladmin!@#")
# 	print "entered password"
#	driver.find_element_by_xpath("//button[@type='submit']").click()
	driver = webdriver.Firefox()	
	driver.maximize_window()
	driver.get(hotel_info.Admin_Url)
	driver.find_element_by_name("userid").send_keys(hotel_info.user_name)
	driver.find_element_by_name("password").send_keys(hotel_info.password)
	driver.find_element_by_xpath("//button[@type='submit']").click()
	print "clicked on logIn"
	mydate = datetime.date.today()
	month=mydate.strftime("%B")
	day=str(mydate).split("-")[2]
	date12=day.lstrip('0')
	print "printing current month"
	print month
	print "printing current month"
	print day
	driver.implicitly_wait(10)
	#time.sleep(10)
	#now we are in login
	#driver.find_element_by_xpath("//button[contains(text(),'Wonderla')]").click()
	#driver.find_element_by_xpath("//html/body/div[1]/aside[1]/section/ul/div/div/div[1]/button[@type='button']").click()
	time.sleep(3)
	print "Clicking on our desired hotel"
	# hotel_name=hotel_info.user_name
	# driver.find_element_by_xpath("//a[text()='"+hotel_name+"']").click()
	# driver.implicitly_wait(10)
	#driver.find_element_by_xpath("//input[@id='contact-email-1']").get_attribute("value")
	#Generate and Launch the Desktop Version
	driver.find_element_by_xpath("//a[@class='dropdown-toggle']").click()
	time.sleep(5)
	print "Generating Site"
	driver.find_element_by_id("idGenerateSite").click()
	time.sleep(8)
	driver.find_element_by_xpath("//a[@class='dropdown-toggle']").click()
	print "Genereting the desktop version"
	driver.find_element_by_id("desktop-preview").click()
	#hotelName='Maharaja Suite'
	time.sleep(10)
	window_after = driver.window_handles[1]
	driver.switch_to_window(window_after)
	time.sleep(5)
	print "Maximizing the window"
	driver.maximize_window()
	time.sleep(5)
	#In the website trying to book a room
	#driver.find_element_by_xpath("//div[@class='form-group check-btn-holder']/input[@value='Book Now']").click()
	driver.find_element_by_xpath("//form[div[label[contains(text(),'Check In')]]]//div/div/input[@type='submit']").click()
	print "clicked on Book Now Button"
	time.sleep(5)
	try:
		driver.find_element_by_xpath("//div[@id='checkInWrapper']//span[@id='checkInCalendar']").click()
	except NoSuchElementException:
		window_booking_engine = driver.window_handles[2]
		driver.switch_to_window(window_booking_engine)	
		driver.maximize_window()
		driver.find_element_by_xpath("//div[@id='checkInWrapper']//span[@id='checkInCalendar']").click()
	print "Clicked on Calendar"
	time.sleep(5)
	driver.find_element_by_xpath("//div[div[div[contains(text(),'"+month+"')]]]//button[text()='"+date12+"']").click()
	print "selected the desired date"
	time.sleep(10)
	driver.find_element_by_xpath("//span[contains(text(),'Check Availability')]").click()
	print "clicked on Check Availability"
	time.sleep(5)
	hotel_room=room_page_info_data.Room_Name
	driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'"+hotel_room+"')]]]]]]]]//span[@class='view-hide-text js-view-rate-text']").click()
	time.sleep(4)
	#driver.find_element_by_xpath("//button[@data-room-type='31977']").click()
	driver.find_element_by_xpath("//div[div[div[contains(text(),'Rate Plan')]]]/div[2]/div/div[2]/button").click()
	time.sleep(5)
	driver.find_element_by_xpath("//input[@placeholder='Name']").send_keys("Asutosh Mohanty")
	time.sleep(2)
	driver.find_element_by_xpath("//input[@placeholder='Email Address']").send_keys("tech.abhilash1@gmail.com")
	time.sleep(2)
	driver.find_element_by_xpath("//input[@placeholder='Phone Number']").send_keys("8050896077")
	time.sleep(2)
	driver.find_element_by_xpath("//textarea[@placeholder=' Special Requests']").send_keys("Not Of Need")
	time.sleep(2)
	print "Clicking on Continue to Payment"
	driver.find_element_by_xpath("//input[@value='Continue to Payment']").click()
	"""
	window_after1 = driver.window_handles[2]
	driver.switch_to_window(window_after1)
	time.sleep(5)
	driver.maximize_window()
	"""
	time.sleep(15)
	print "Clicking on back button"
	driver.back()
	time.sleep(5)
	"""
	print "Trying to get the retained values"
	userName=driver.find_element_by_xpath("//input[@placeholder='Name']").text
	emailId=driver.find_element_by_xpath("//input[@placeholder='Email Address']").text
	phoneNumber=driver.find_element_by_xpath("//input[@placeholder='Phone Number']").text
	print "Values Retained : [UserName :"+userName+"],[Email ID : "+emailId+"][Phone Number :"+phoneNumber+"]"
	"""
	print "Taking ScreenShots"
	img=ImageGrab.grab()
	img.show()
	ImageGrab.grab_to_file('img.png')
	time.sleep(5)
	driver.find_element_by_xpath("//input[@value='Continue to Payment']").click()
	time.sleep(8)
	driver.find_element_by_id("creditCardHolderName").send_keys("ABHILASH MOHANTY")
	time.sleep(15)
	driver.quit()
	print "Waiting for 3 min. as abandoned cart mail might not been delivered"
	time.sleep(240)
	for i in xrange(240,0,-1):
	    time.sleep(1)
	    sys.stdout.write(str(i)+' ')
	    sys.stdout.flush()
	verification()



if __name__ == '__main__':

	abandoned_cart()