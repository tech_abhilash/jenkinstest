from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
import unittest, time, re
import datetime
#import inventory_xpaths as xpath
import instamojo_xpath as xpath
import test_data as data   
import os, sys, inspect
sys.path.append(os.path.join(os.path.dirname("offline_crs"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("offline_crs"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function
import common_data

class InstaMojo(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        #self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    def test_closeInventoryVerification(self):
        driver = self.driver
        #driver.get(self.base_url + "/")
        driver.get("http://staging.simplotel.com/")
        driver.maximize_window()
        time.sleep(5)
        driver.get(hotel_info.Admin_Url)    
        driver.find_element_by_name("userid").send_keys(hotel_info.user_name)
        driver.find_element_by_name("password").send_keys(hotel_info.password)
        #driver.find_element_by_xpath("//button[@type='submit']").click()
#        driver.find_element_by_name("userid").send_keys("simpadmin")
#        driver.find_element_by_name("password").send_keys("simplehoteladmin!@#")
        time.sleep(3)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(6)
        day= datetime.date.today()
        print day
        today=str(day)
        print "getting todays date : "+ today
        mydate = datetime.date.today()
        month=mydate.strftime("%B")
        day=str(mydate).split("-")[2]
        print month
        date12=day.lstrip('0')
        driver.find_element_by_xpath("//span[contains(text(),'Booking Engine')]").click()
        time.sleep(4)
        #driver.find_element_by_xpath("//html/body/header/nav/div/div[2]/ul[1]/li[4]/ul/li[1]/a/i").click()
        driver.find_element_by_xpath("//li[a[span[contains(text(),'Booking Engine')]]]/ul/li//span[contains(text(),'Settings')]").click()
        time.sleep(5)
        driver.find_element_by_xpath("//span[text()='Payment Gateway']").click()
        time.sleep(7)
        select=Select(driver.find_element_by_xpath("//div[label[contains(text(),'Select the PG')]]/div/div/select"))
        select.select_by_visible_text("Instamojo")
        time.sleep(5)
        driver.find_element_by_xpath("//div[label[contains(text(),'Enter the Simplotel Fee Amount')]]/div/input").clear()
        driver.find_element_by_xpath("//div[label[contains(text(),'Enter the Simplotel Fee Amount')]]/div/input").send_keys(data.simplotel_fee_amount)
        time.sleep(5)
        driver.find_element_by_xpath("//div[label[contains(text(),'Enter the Service Tax Amount')]]/div/input").clear()
        driver.find_element_by_xpath("//div[label[contains(text(),'Enter the Service Tax Amount')]]/div/input").send_keys(data.service_tax_amount)
        time.sleep(5)
        driver.find_element_by_xpath("//div[label[contains(text(),'Enter the Minimum Collection Amount For Payment')]]/div/input").clear()
        driver.find_element_by_xpath("//div[label[contains(text(),'Enter the Minimum Collection Amount For Payment')]]/div/input").send_keys(data.collection_amount_for_payment)
        time.sleep(5)
        driver.find_element_by_xpath("//div[label[contains(text(),'Enter the Payment Gateway Fee Amount ')]]/div/input").clear()
        driver.find_element_by_xpath("//div[label[contains(text(),'Enter the Payment Gateway Fee Amount ')]]/div/input").send_keys(data.payment_gateway_fee_amount)
        time.sleep(5)
        driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()
        time.sleep(8)
        driver.find_element_by_xpath("//div[div[div[label[b[contains(text(),'Instamojo settings')]]]]]/div[2]/div[3]/div[6]/button").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[label[contains(text(),'Username')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'Password')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'Phone Number')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'Email')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'Account Number')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'IFSC Code')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'Account Holder Name')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'First Name')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'Last Name')]]/div/input").clear()
        time.sleep(1)
        driver.find_element_by_xpath("//div[label[contains(text(),'Location')]]/div/input").clear()
        time.sleep(1)
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # #time.sleep(3)
        # statement = driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        # time.sleep(1)
        # print "The Error shown for Username not given :" + statement
        # time.sleep(5)
        driver.find_element_by_xpath("//div[label[contains(text(),'Username')]]/div/input").send_keys(data.username)
        time.sleep(3)
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # #time.sleep(1)
        # statement1 = driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        # time.sleep(2)
        # print "The Error shown for Password not given :" + statement1
        # time.sleep(5)
        driver.find_element_by_xpath("//div[label[contains(text(),'Password')]]/div/input").send_keys(data.password)
        time.sleep(3)
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # phone_err=driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        # time.sleep(3)
        # print "The Error shown for Phone Number not given :" + phone_err
        # time.sleep(5)
        driver.find_element_by_xpath("//div[label[contains(text(),'Phone Number')]]/div/input").send_keys("4357783612")
        time.sleep(3)
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # email_err=driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        # time.sleep(5)
        # print "The Error shown for Email not given :" + email_err
        # showing_err=driver.find_element_by_xpath("//div[@class='modal-content']/div[2]/div[5]/div[2]").text
        # print "Error showing for email ot given is :" + showing_err
        time.sleep(4)
        #put an wrong Email-ID
        # driver.find_element_by_xpath("//div[label[contains(text(),'Email')]]/div/input").send_keys("alf")
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # email_err1=driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        # time.sleep(3)
        # print "The Error shown for  Wrong Email-Id given :" + email_err1
        # time.sleep(4)
        # showing_err1=driver.find_element_by_xpath("//div[@class='modal-content']/div[2]/div[5]/div[2]").text
        # print "Error showing for Wrong email given is :" + showing_err1
        #put correct  Email-ID
        # driver.find_element_by_xpath("//div[label[contains(text(),'Email')]]/div/input").clear()
        driver.find_element_by_xpath("//div[label[contains(text(),'Email')]]/div/input").send_keys("abhitester1712@gmail.com")
        time.sleep(5)
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # accunt_err=driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        # print "Error shown for Account Number not given :" + accunt_err
        #account number validation can not be done noww,add it later
        time.sleep(3)
        driver.find_element_by_xpath("//div[label[contains(text(),'Account Number')]]/div/input").send_keys("34113381242")
        # time.sleep(5)
        # #IFCS code validation
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # ifsc_err1=driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        time.sleep(3)
        # print "Error shown for IFSC Code not given :" + ifsc_err1
        # time.sleep(5)
        # #Wrong data given for IFSC
        # driver.find_element_by_xpath("//div[label[contains(text(),'IFSC Code')]]/div/input").send_keys("abc")
        # time.sleep(3)
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # ifsc_err2=driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        # print "Error shown for IFSC Code Given Wrong:" + ifsc_err2
        # time.sleep(5)
        # driver.find_element_by_xpath("//div[label[contains(text(),'IFSC Code')]]/div/input").clear()
        driver.find_element_by_xpath("//div[label[contains(text(),'IFSC Code')]]/div/input").send_keys("OORT6755593")
        time.sleep(1)
        # driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        # acc_hlder_nm_err=driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        # print "Error shown for Account Holder Name not given :" + acc_hlder_nm_err
        time.sleep(6)
        driver.find_element_by_xpath("//div[label[contains(text(),'Account Holder Name')]]/div/input").send_keys("NEW TEST6")
        time.sleep(3)
        driver.find_element_by_xpath("//div[label[contains(text(),'First Name')]]/div/input").send_keys("NEW6")
        time.sleep(3)
        driver.find_element_by_xpath("//div[label[contains(text(),'Last Name')]]/div/input").send_keys("TEST6")
        time.sleep(3)
        driver.find_element_by_xpath("//div[label[contains(text(),'Location')]]/div/input").send_keys("BANGALORE6")
        time.sleep(3)
        driver.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
        time.sleep(1)
        success_msg=driver.find_element_by_xpath("html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]").text
        print "Message showing after  clicking  OK Button" + success_msg
        time.sleep(10)
        driver.find_element_by_xpath("//button[contains(text(),'Save')]").click()
        print "Saving the changes"
        time.sleep(10)
        driver.find_element_by_xpath("//a[@class='dropdown-toggle']").click()
        time.sleep(5)
        driver.find_element_by_id("idGenerateSite").click()
        print "Generating the sites"
        time.sleep(8)
        driver.find_element_by_xpath("//a[@class='dropdown-toggle']").click()
        driver.find_element_by_id("desktop-preview").click()
        print "Launching desktop version"
        time.sleep(5)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        time.sleep(6)
        driver.maximize_window()
        time.sleep(10)
        #In the website trying to book a room
        #driver.find_element_by_xpath("//div[@class='form-group check-btn-holder']/input[@value='Book Now']").click()
        #driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[1]/div/div/form/div[10]/div/input").click()
        #driver.find_element_by_xpath("//form[div[label[contains(text(),'Check In')]]]//div/div/input[@type='submit']").click()
        #driver.find_element_by_xpath("//div[p[contains(text(),'Reservations')]]/form[1]/div[10]/div/input").click()
        #driver.find_element_by_xpath("//div[p[contains(text(),'Reservations')]]/form/div[10]/div/input[@type='submit']").click()
        #driver.find_element_by_xpath("//form[div[label[contains(text(),'Check In')]]]//div/div/input[@type='submit']").click()
        print "1"
        driver.find_element_by_xpath("html/body/div[1]/div[1]/div/div/form/div[10]/div/input").click()
        driver.implicitly_wait(30)
        print "2"
        time.sleep(5)
        print "3"
        window_after1 = driver.window_handles[2]
        driver.switch_to_window(window_after1)
        driver.maximize_window()
        time.sleep(6)
        driver.find_element_by_xpath("//div[@id='checkInWrapper']//span[@id='checkInCalendar']").click()
        print "Clicked on the calender"
        time.sleep(3)
        driver.find_element_by_xpath("//div[div[div[contains(text(),'"+month+"')]]]//button[text()='"+date12+"']").click()
        print "selected the desired date"
        time.sleep(5)
        room_name = "Test Room"
        driver.find_element_by_xpath("//span[contains(text(),'Check Availability')]").click()
        time.sleep(5)
        #driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'Deluxe Room')]]]]]]]]/div[2]/div[2]/div[1]/div[1]/div[1]/span/i[1]/span[contains(text(),'View Rates')]").click()
        time.sleep(6)
        driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'"+room_name+"')]]]]]]]]/div[2]/div[2]/div[1]/div[1]/div[1]/span/i[1]/span[contains(text(),'View Rates')]").click()
        #driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'Deluxe Room')]]]]]]]]/div[2]/div[1]/div[1]/div[1]/div/div[1]/div[1]/div/div[2]/div/div[2]/button/span[contains(text(),'Book Now')]").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[div[div[div[div[div[div[p[contains(text(),'"+room_name+"')]]]]]]]]/div[2]/div[1]/div[1]/div[1]/div/div[1]/div[1]/div/div[2]/div/div[2]/button/span[contains(text(),'Book Now')]").click()
        time.sleep(3)
        driver.find_element_by_xpath("//input[@placeholder='Name']").send_keys("Prakash")
        time.sleep(1)
        driver.find_element_by_xpath("//input[@placeholder='Email Address']").send_keys("abhilash.mohanty06@gmail.com")
        time.sleep(1)
        driver.find_element_by_xpath("//input[@placeholder='Code']").send_keys("+91")
        time.sleep(1)
        driver.find_element_by_xpath("//input[@placeholder='Phone Number']").send_keys("8050896077")
        time.sleep(1)
        driver.find_element_by_xpath("//textarea[@placeholder=' Special Requests']").send_keys("no need")
        time.sleep(2)
        driver.find_element_by_xpath("//input[@value='Continue to Payment']").click()
        time.sleep(15)
        driver.find_element_by_xpath("//a[contains(text(),'Debit Card')]").click()
        time.sleep(3)
        #driver.switch_to_frame(driver.find_element_by_xpath("html/body/div[1]/div/div/div/div[1]/div/div[2]/div/div/div/div/div/div[2]/div/div/div/iframe"))
        #driver.switch_to_frame(driver.find_element_by_xpath("//div[div[span[contains(text(),'Debit Card')]]]/div[2]/div/div/div/iframe"))
        driver.switch_to_frame(driver.find_element_by_xpath("//div[div[div[span[contains(text(),'Debit Card')]]]]/div[2]/div/div/div/iframe"))
        time.sleep(3)
        # driver.find_element_by_xpath("html/body/div[1]/div[1]/div/div[2]/form/div[2]/div[2]/div/input[1]").send_keys("4242 4242 4242 4242")
        # #driver.switch_to_default_content()
        # time.sleep(4)
        driver.find_element_by_xpath("//input[@placeholder='Card Number']").send_keys("4242 4242 4242 4242")
        #driver.find_element_by_xpath("//input[@name='cardNumberMask']").send_keys("4242 4242 4242 4242")
        time.sleep(3)
        driver.find_element_by_xpath("//input[@placeholder='Name']").send_keys("PRAKASH")
        time.sleep(3)
        driver.find_element_by_xpath("//input[@placeholder='MM']").send_keys("11")
        time.sleep(2)
        driver.find_element_by_xpath("//input[@placeholder='YY']").send_keys("22")
        time.sleep(2)
        driver.find_element_by_xpath("//input[@placeholder='CVV']").send_keys("111")
        time.sleep(3)
        driver.find_element_by_xpath("//button[@id='common_pay_btn']").click()
        time.sleep(2)
        #window_after2 = driver.window_handles[3]
        #driver.switch_to_window(window_after2)
        #driver.maximize_window()
        driver.find_element_by_xpath("//input[@id='txtPassword']").send_keys("1221")
        time.sleep(2)
        driver.find_element_by_xpath("//input[@id='cmdSubmit']").click()
        #driver.switch_to_window(window_after1)
        time.sleep(10)
        text1 = driver.find_element_by_xpath("//div[label[contains(text(),'Reservation Confirmation')]]/div/p").text
        print "Reservation confirmation for :" + text1
        text2 = driver.find_element_by_xpath("//div[label[contains(text(),'Reservation Id')]]/div/p").text
        print "Reservation Id :" + text2
        time.sleep(10)
        time.sleep(10)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
            
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
            
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
            
    def tearDown(self):
        self.driver.quit()
        #self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

