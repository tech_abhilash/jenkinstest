# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'simpadmin/room'))
import room_page_info_data
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '..', "booking_engine_common_function"))
import testing_booking_engine_data_common_function


class Testingwithccavenuepayment(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).click()
        except NoSuchElementException:
            return False
        return True
    
    def test_bookingengine(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        test_be = testing_booking_engine_data_common_function
        test_be.moving_to_new_tab(self)                           
        check_in = driver.find_element_by_xpath(".//*[@id='checkInWrapper']/div/span[1]").text
        print check_in
        check_out = driver.find_element_by_xpath(".//*[@id='checkOutWrapper']/div/span[1]").text
        print check_out
        guest_name = "Test Name"
        guest_phone_no = "8971412668"
        guest_address = "Simplotel"
        guest_email_id = "test.simplotel@gmail.com"
        email_password = "120tc002"
        guset_data = [guest_name, guest_email_id, guest_phone_no, guest_address]
        test_be.testing_max_adult(self, room_page_info_data.Room_Name)
        test_be.testing_max_child(self, room_page_info_data.Room_Name)
        test_be.testing_for_one_adult(self, room_page_info_data.Room_Name)
        test_be.testing_for_two_adult(self, room_page_info_data.Room_Name)
        test_be.testing_for_three_adult(self, room_page_info_data.Room_Name)
        test_be.testing_for_child_included_in_the_rate(self, room_page_info_data.Room_Name)
        test_be.testing_for_extra_2child(self, room_page_info_data.Room_Name)
        test_be.testing_for_extra_child(self, room_page_info_data.Room_Name)
          #----------------------------------------Getting Image Name----------------------------------
        with open("../../simpadmin/room/image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        test_be.testing_rate_plane_and_photos(self, room_page_info_data.Room_Detailed_Description, wordList)
        pay_now = test_be.testing_guest_information(self, check_in, check_out, room_page_info_data.Room_Name, guset_data)
        Booking_id = test_be.testing_cc_avenue_payment_page(self, pay_now)
        test_be.testing_booking_conformation_page(self, check_in, check_out, room_page_info_data.Room_Name, Booking_id, guest_name)
        guset_data = [guest_name, guest_email_id, email_password, guest_phone_no, guest_address]
        booking_data = [Booking_id, check_in, check_out]
        test_be.gmail_verification_of_confirmation_mail(self, hotel_info.hotel_name, guset_data, booking_data)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
