# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'simpadmin/room'))
import room_page_info_data
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '..', "booking_engine_common_function"))
import chain_booking_engine_common_function


class Testing_chain_BE_preview(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.chain_Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True
    
    def testing_chain_preview(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        booking_buttton_xpath = ".//*[@id='booking-form']/div[10]/div/input"
        chain_booking_engine_common_function.checking_booking_form(self, hotel_info.group_name_one, hotel_info.chain_child_hotel_name_one, booking_buttton_xpath)
        driver.get(self.base_url + "/")    
        driver.maximize_window()
        self.driver.implicitly_wait(30)

        booking_buttton_xpath = "html/body/div[1]/div[1]/div[1]/div/div/form[2]/div[10]/div/input"
        chain_booking_engine_common_function.checking_booking_form(self, hotel_info.group_name_two, hotel_info.chain_child_hotel_name_two, booking_buttton_xpath)
        driver.get(self.base_url + "/")    
        driver.maximize_window()
        self.driver.implicitly_wait(30)

        booking_buttton_xpath = "html/body/div[1]/div[1]/div[1]/div/div/form[4]/div[10]/div/input"
        chain_booking_engine_common_function.checking_booking_form(self, hotel_info.group_name_three, hotel_info.chain_child_hotel_name_three, booking_buttton_xpath)
        driver.get(self.base_url + "/")    
        driver.maximize_window()
        self.driver.implicitly_wait(30)

        booking_buttton_xpath = "html/body/div[1]/div[1]/div[1]/div/div/form[3]/div[10]/div/input"
        chain_booking_engine_common_function.checking_booking_form(self, hotel_info.group_name_three, hotel_info.chain_child_hotel_name_fourth, booking_buttton_xpath)
        
        #-----------child without payment gateway------------------------------------
        driver.get(self.base_url + "/")    
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        booking_buttton_xpath = ".//*[@id='booking-form']/div[10]/div/input"
        chain_booking_engine_common_function.moving_to_child_hotels(self, hotel_info.group_name_one, hotel_info.chain_child_hotel_name_one, booking_buttton_xpath)
        check_in = driver.find_element_by_xpath(".//*[@id='checkInWrapper']/div/span[1]").text
        print check_in
        check_out = driver.find_element_by_xpath(".//*[@id='checkOutWrapper']/div/span[1]").text
        print check_out  
        chain_booking_engine_common_function.testing_max_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_max_child(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_one_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_two_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_three_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_child_included_in_the_rate(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_extra_2child(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_extra_child(self, room_page_info_data.Room_Name)
         #----------------------------------------Getting Image Name----------------------------------
        with open("../../simpadmin/room/image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        chain_booking_engine_common_function.testing_rate_plane_and_photos(self, room_page_info_data.Room_Detailed_Description, wordList)
        chain_booking_engine_common_function.testing_guest_information(self, check_in, check_out, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_booking_conformation_page(self, check_in, check_out, room_page_info_data.Room_Name)
        
        #-----------child with CC Avenue------------------------------------
        driver.get(self.base_url + "/")    
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        booking_buttton_xpath = "html/body/div[1]/div[1]/div[1]/div/div/form[2]/div[10]/div/input"
        chain_booking_engine_common_function.moving_to_child_hotels(self, hotel_info.group_name_two, hotel_info.chain_child_hotel_name_two, booking_buttton_xpath)
        check_in = driver.find_element_by_xpath(".//*[@id='checkInWrapper']/div/span[1]").text
        print check_in
        check_out = driver.find_element_by_xpath(".//*[@id='checkOutWrapper']/div/span[1]").text
        print check_out  
        chain_booking_engine_common_function.testing_max_adult(self)
        chain_booking_engine_common_function.testing_max_child(self)
        chain_booking_engine_common_function.testing_for_one_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_two_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_three_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_child_included_in_the_rate(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_extra_2child(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_extra_child(self, room_page_info_data.Room_Name)
          #----------------------------------------Getting Image Name----------------------------------
        with open("../../simpadmin/room/image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        chain_booking_engine_common_function.testing_rate_plane_and_photos(self, room_page_info_data.Room_Detailed_Description, wordList)
        chain_booking_engine_common_function.testing_guest_information(self, check_in, check_out, room_page_info_data.Room_Name)
        # chain_booking_engine_common_function.testing_cc_avenue_payment_page(self)
        chain_booking_engine_common_function.testing_booking_conformation_page(self, check_in, check_out, room_page_info_data.Room_Name)

        #-----------child with Citrus------------------------------------
        driver.get(self.base_url + "/")    
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        booking_buttton_xpath = "html/body/div[1]/div[1]/div[1]/div/div/form[4]/div[10]/div/input"
        chain_booking_engine_common_function.moving_to_child_hotels(self, hotel_info.group_name_three, hotel_info.chain_child_hotel_name_three, booking_buttton_xpath)
        check_in = driver.find_element_by_xpath(".//*[@id='checkInWrapper']/div/span[1]").text
        print check_in
        check_out = driver.find_element_by_xpath(".//*[@id='checkOutWrapper']/div/span[1]").text
        print check_out  
        chain_booking_engine_common_function.testing_max_adult(self)
        chain_booking_engine_common_function.testing_max_child(self)
        chain_booking_engine_common_function.testing_for_one_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_two_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_three_adult(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_child_included_in_the_rate(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_extra_2child(self, room_page_info_data.Room_Name)
        chain_booking_engine_common_function.testing_for_extra_child(self, room_page_info_data.Room_Name)
         #----------------------------------------Getting Image Name----------------------------------
        with open("../../simpadmin/room/image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        chain_booking_engine_common_function.testing_rate_plane_and_photos(self, room_page_info_data.Room_Detailed_Description, wordList)
        chain_booking_engine_common_function.testing_guest_information(self, check_in, check_out, room_page_info_data.Room_Name)

        driver.get(self.base_url + "/")    
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        booking_buttton_xpath = "html/body/div[1]/div[1]/div[1]/div/div/form[3]/div[10]/div/input"
        chain_booking_engine_common_function.moving_to_child_hotels(self, hotel_info.group_name_three, hotel_info.chain_child_hotel_name_fourth, booking_buttton_xpath)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
