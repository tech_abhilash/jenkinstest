# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys, os
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', "simpadmin/simpadmin_common_function"))
import create_data_common_function
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function

class Adding_BE_with_CC_Avenue_to_child(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_adding_boking_engine(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function 
        booking_engine = creating_booking_engine_data_common_function        
        
        #-------Login to admin------
        admin.login_to_admin(self, hotel_info.admin_user_name, hotel_info.admin_password)

        d.find_element_by_xpath("(//button[@type='button'])[2]").click()
        d.find_element_by_link_text(hotel_info.chain_child_hotel_name_two).click()
        
        #-------------Create Room-------------------------------------------------
        booking_engine.adding_room(self)

        #------------- Enable Simplotel BE------------------------------------------
        booking_engine.enable_simplotel_be(self)
        
        # --------Selecting Simplotel BE------------------------------------------
        booking_engine.selecting_simplotel_be(self)
        
        # --------Selecting Payment Gateway------------------------------------------
        option = "CC Avenue"
        booking_engine.adding_payment_gateway(self, option)
        
        # --------Selecting Payment policy------------------------------------------
        booking_engine.adding_payment_policy(self)
        
        #--------Adding Seasons------------------------------------------    
        season_name = "Seasons"
        next_month = True      
        booking_engine.adding_seasons(self, season_name, next_month)
        
        #--------Adding Cancellation Penalties------------------------------------------ 
        penalty_name = "Penalty" 
        booking_engine.adding_cancellation(self, penalty_name)
        
        #--------Adding Taxes and Fees------------------------------------------    
        tax_name = "Tax"
        booking_engine.adding_taxes_and_fees(self, tax_name)

        #--------Adding Rates------------------------------------------   
        rate_plane_name = "Rate Plan"
        booking_engine.adding_rates(self, rate_plane_name)
                
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
