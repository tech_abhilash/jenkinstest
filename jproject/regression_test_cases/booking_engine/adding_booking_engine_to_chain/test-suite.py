from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Adding BE to 1st child without Payment Gateway"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_booking_engine_to_1st_child_without_payment_gateway")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Adding BE to 2nd child with CC Avenue Payment Gateway"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_booking_engine_to_2nd_child_with_cc_avenue_payment_gateway")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Adding BE to 3rd child with Citrus Payment Gateway"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_booking_engine_to_3rd_child_with_citrus_payment_gateway")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Adding BE to 4th child with Paypal Payment Gateway"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_booking_engine_to_4th_child_with_paypal_payment_gateway")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing Preview"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("chain_booking_engine_preview_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)