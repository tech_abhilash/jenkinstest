from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
from selenium.webdriver.support.ui import Select
import sys
import os
import be_common_elements_path
import common_data
sys.path.append(os.path.join(os.path.dirname("booking_engine_common_function"), '../..', 'simpadmin/room'))
import room_page_info_data
import room_page_elements_path


path = be_common_elements_path

def login_to_admin(self, username, password):
    d = self.driver
    d.find_element_by_name("userid").clear()
    d.find_element_by_name("userid").send_keys(username)
    d.find_element_by_name("password").clear()
    d.find_element_by_name("password").send_keys(password)
    d.find_element_by_xpath("//button[@type='submit']").click()

def enable_simplotel_be(self):
    d = self.driver
    d.find_element_by_link_text("Ops").click()
    element = d.find_element_by_link_text("Toggle Features")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_link_text("Toggle Features").click()
    d.find_element_by_link_text("Simplotel BE").click()
    d.find_element_by_xpath(path.enable_simplotel_be_path).click()
    d.find_element_by_xpath(path.enable_simplotel_be_save_path).click()
    time.sleep(5)

def enable_simplotel_be_email_slider(self):
    d = self.driver
    d.find_element_by_link_text("Ops").click()
    element = d.find_element_by_link_text("Toggle Features")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_link_text("Toggle Features").click()
    d.find_element_by_link_text("Email Slider in Booking Engine").click()
    d.find_element_by_xpath(path.enable_simplotel_be_path).click()
    d.find_element_by_xpath(path.enable_simplotel_be_save_path).click()
    time.sleep(5)

def enable_simplotel_pay_now(self):
    d = self.driver
    d.find_element_by_link_text("Ops").click()
    element = d.find_element_by_link_text("Toggle Features")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_link_text("Toggle Features").click()
    d.find_element_by_link_text("Toggle Choice for Pay Now").click()
    d.find_element_by_xpath(path.enable_simplotel_be_path).click()
    d.find_element_by_xpath(path.enable_simplotel_be_save_path).click()
    time.sleep(5)

def enable_otp_for_pay_at_hotel(self):
    d = self.driver
    d.find_element_by_link_text("Ops").click()
    element = d.find_element_by_link_text("Toggle Features")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_link_text("Toggle Features").click()
    d.find_element_by_link_text("OTP for pay at hotel").click()
    d.find_element_by_xpath(path.enable_simplotel_be_path).click()
    d.find_element_by_xpath(path.enable_simplotel_be_save_path).click()
    time.sleep(5)

def enable_simplotel_be_advertisement(self):
    d = self.driver
    d.find_element_by_link_text("Ops").click()
    element = d.find_element_by_link_text("Toggle Features")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_link_text("Toggle Features").click()
    d.find_element_by_link_text("BE advertisement message").click()
    d.find_element_by_xpath(path.enable_simplotel_be_path).click()
    d.find_element_by_xpath(path.enable_simplotel_be_save_path).click()
    time.sleep(5)

def save_payment_gateway(self):
    d = self.driver
    d.find_element_by_link_text("Booking Engine").click()
    d.find_element_by_link_text("Settings").click()
    time.sleep(2)
    element = d.find_element_by_link_text("Payment Gateway")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_link_text("Payment Gateway").click()
    time.sleep(2)
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(2)

def save_booking_engine_messages(self):
    d = self.driver
    d.find_element_by_link_text("Booking Engine").click()
    d.find_element_by_link_text("Messaging").click()
    time.sleep(5)
    element = d.find_element_by_link_text("Confirmation Message")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")

    d.find_element_by_link_text("Confirmation Message").click()
    d.switch_to_frame(d.find_element_by_xpath(".//*[@id='cke_1_contents']/iframe"))
    d.find_element_by_xpath("/html/body").clear()
    d.find_element_by_xpath("/html/body").send_keys("Confirmation Message")
    d.switch_to_default_content()
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(2)

    d.find_element_by_link_text("Booking Message").click()
    d.switch_to_frame(d.find_element_by_xpath(".//*[@id='cke_1_contents']/iframe"))
    d.find_element_by_xpath("/html/body").clear()
    d.find_element_by_xpath("/html/body").send_keys("Booking Message")
    d.switch_to_default_content()
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(2)

    d.find_element_by_link_text("Reservation Policy").click()
    d.switch_to_frame(d.find_element_by_xpath(".//*[@id='cke_1_contents']/iframe"))
    d.find_element_by_xpath("/html/body").clear()
    d.find_element_by_xpath("/html/body").send_keys("Reservation Policy")
    d.switch_to_default_content()
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(2)

    d.find_element_by_link_text("Terms & Conditions").click()
    d.switch_to_frame(d.find_element_by_xpath(".//*[@id='cke_1_contents']/iframe"))
    d.find_element_by_xpath("/html/body").clear()
    d.find_element_by_xpath("/html/body").send_keys("Terms & Conditions")
    d.switch_to_default_content()
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(2)

    d.find_element_by_link_text("Custom Message for Search Failure").click()
    d.switch_to_frame(d.find_element_by_xpath(".//*[@id='cke_1_contents']/iframe"))
    d.find_element_by_xpath("/html/body").clear()
    d.find_element_by_xpath("/html/body").send_keys("Custom Message for Search Failure")
    d.switch_to_default_content()
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(2)

def selecting_simplotel_be(self):
    d = self.driver
    d.find_element_by_link_text("Booking Engine").click()
    d.find_element_by_link_text("Settings").click()
    time.sleep(2)
    d.find_element_by_id("bookingEngineListing").click()
    # d.find_element_by_id("s2id_bookingList").click()
    # time.sleep(2)  
    # d.find_element_by_xpath(path.select_simplotel_be_path).click()
    time.sleep(2)
    if (d.find_element_by_xpath("//div[label[contains(text(),'Show Promocode')]]/input").is_selected()):
        print "Already Selected"
    else:
        d.find_element_by_xpath("//div[label[contains(text(),'Show Promocode')]]/input").click()
    if (d.find_element_by_xpath("//div[label[contains(text(),'Collect Address')]]/input").is_selected()):
        print "Already Selected"
    else:
        d.find_element_by_xpath("//div[label[contains(text(),'Collect Address')]]/input").click()
    element = d.find_element_by_id("openInNewWindow")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_xpath("//input[@value='bookingForm']").click()
    if (d.find_element_by_id("openInNewWindow").is_selected()):
        print "Already Selected"
    else:
        d.find_element_by_id("openInNewWindow").click()
    d.find_element_by_xpath(path.be_setting_save_path).click()
    time.sleep(5)

def adding_payment_gateway(self, option):
    d = self.driver
    d.find_element_by_link_text("Booking Engine").click()
    d.find_element_by_link_text("Settings").click()
    time.sleep(2)
    element = d.find_element_by_link_text("Payment Gateway")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_link_text("Payment Gateway").click()
    time.sleep(2)
    d.find_element_by_xpath(path.use_simplotel_gateway_path).click()
    select = Select( d.find_element_by_xpath(".//*[@class='bottom-margin-15']/select"))
    select.select_by_visible_text(option)
    time.sleep(5)
    # d.find_element_by_xpath(path.payment_gateway_simplotel_fee_path).clear()
    # d.find_element_by_xpath(path.payment_gateway_simplotel_fee_path).send_keys("10")
    # d.find_element_by_xpath(path.payment_gateway_service_tax_path).clear()
    # d.find_element_by_xpath(path.payment_gateway_service_tax_path).send_keys("1")
    # d.find_element_by_xpath(path.payment_gateway_simplotels_fee_before_tax).click()
    # if (option == "Citrus"):
    #     d.find_element_by_xpath(".//*[@class='pg-content']/div[2]/div[3]/div[5]/div/input").clear()
    #     d.find_element_by_xpath(".//*[@class='pg-content']/div[2]/div[3]/div[5]/div/input").send_keys("2")
    # else:
    #     d.find_element_by_xpath(".//*[@class='pg-content']/div[2]/div[3]/div[4]/div/input").clear()
    #     d.find_element_by_xpath(".//*[@class='pg-content']/div[2]/div[3]/div[4]/div/input").send_keys("2")
    d.find_element_by_xpath(path.payment_gateway_save_path).click()
    time.sleep(10)  

def adding_payment_policy(self):
    d = self.driver
    d.find_element_by_link_text("Booking Engine").click()
    d.find_element_by_link_text("Settings").click()
    time.sleep(2)   
    element = d.find_element_by_link_text("Payment-Policy")
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_link_text("Payment-Policy").click()
    time.sleep(2)
    satus = d.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div/span[1]/input").get_attribute("name")
    if (satus == "false"):
        d.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div/span[1]/input").click()
        time.sleep(2) 
    d.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div[1]/label/input").clear()
    d.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div[1]/label/input").send_keys(common_data.payment_policy_amount)               
    d.find_element_by_xpath(path.payment_gateway_save_path).click()
    time.sleep(5)

def adding_seasons(self, season_name, next_month):
    d = self.driver
    d.find_element_by_link_text("Rates & Inventory").click()
    time.sleep(2)
    d.find_element_by_link_text("Seasons").click()
    time.sleep(2)
    d.find_element_by_xpath(path.add_seasons_path).click()
    d.find_element_by_xpath(path.seasons_name_path).clear()
    d.find_element_by_xpath(path.seasons_name_path).send_keys(season_name)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    time.sleep(2)
    d.find_element_by_xpath(path.add_period_path).click()
    if (next_month == True):
        d.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[3]/div[1]/table/tr/div/div[2]/div/input").click()
        d.find_element_by_css_selector("th.next").click()
        d.find_element_by_xpath("/html/body/div[3]/div[1]/table/tbody/tr[3]/td[6]").click()
    d.find_element_by_xpath(path.seasons_save_path).click()
    time.sleep(5)

def adding_cancellation(self, penalty_name):
    d = self.driver
    d.find_element_by_link_text("Rates & Inventory").click()
    time.sleep(2)
    d.find_element_by_link_text("Cancellation Penalties").click()
    time.sleep(2)
    d.find_element_by_xpath(path.add_penalty_path).click()
    d.find_element_by_xpath(path.penalty_name_path).clear()
    d.find_element_by_xpath(path.penalty_name_path).send_keys(penalty_name)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    time.sleep(2)
    d.find_element_by_xpath(path.penalty_description_path).clear()
    d.find_element_by_xpath(path.penalty_description_path).send_keys("Penalty")
    d.find_element_by_xpath(path.penalty_save_path).click()
    time.sleep(2)
        
def adding_rates(self, rate_plane_name):
    d = self.driver
    d.find_element_by_link_text("Rates & Inventory").click()
    time.sleep(2)
    d.find_element_by_link_text("Rates").click()
    time.sleep(2)
    d.find_element_by_xpath(path.add_rate_plans_path).click()
    d.find_element_by_xpath(path.rate_plans_name_path).clear()
    d.find_element_by_xpath(path.rate_plans_name_path).send_keys(rate_plane_name)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    time.sleep(10)
    d.find_element_by_xpath(path.active_rate_plans_path).click()
    d.find_element_by_xpath(path.one_adult_value_path).clear()
    d.find_element_by_xpath(path.one_adult_value_path).send_keys(common_data.one_adult_price)
    d.find_element_by_xpath(path.two_adult_value_path).clear()
    d.find_element_by_xpath(path.two_adult_value_path).send_keys(common_data.two_adult_price)
    d.find_element_by_xpath(path.three_adult_value_path).clear()
    d.find_element_by_xpath(path.three_adult_value_path).send_keys(common_data.three_adult_price)
    d.find_element_by_xpath(path.extra_child_value_path).clear()
    d.find_element_by_xpath(path.extra_child_value_path).send_keys(common_data.extra_child_price)
    d.find_element_by_xpath(".//*[@class='form-group num-of-children row']/input").clear()
    d.find_element_by_xpath(".//*[@class='form-group num-of-children row']/input").send_keys("1")
    element = d.find_element_by_xpath(path.rate_short_description_path)
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_xpath(path.rate_short_description_path).clear()
    d.find_element_by_xpath(path.rate_short_description_path).send_keys(common_data.rate_plan_short_description)
    d.find_element_by_xpath(path.rate_detail_Description_path).clear()
    d.find_element_by_xpath(path.rate_detail_Description_path).send_keys(common_data.rate_plan_detail_description)
    d.find_element_by_xpath(path.rate_plans_save_path).click()
    time.sleep(2)

def adding_taxes_and_fees(self, tax_name):
    d = self.driver
    d.find_element_by_link_text("Rates & Inventory").click()
    d.find_element_by_link_text("Taxes and Fees").click()
    time.sleep(2)
    d.find_element_by_xpath(path.add_tax_path).click()
    d.find_element_by_xpath(path.tax_name_path).clear()
    d.find_element_by_xpath(path.tax_name_path).send_keys(tax_name)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    time.sleep(2)
    d.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/form/div[1]/div[2]/select/option[1]").click()
    time.sleep(1)
    d.find_element_by_xpath(path.tax_description_path).clear()
    d.find_element_by_xpath(path.tax_description_path).send_keys(common_data.tax_description)
    d.find_element_by_xpath(path.tax_save_path).click()
    time.sleep(2)

def select_different_season(self, season_name):
    d = self.driver
    d.find_element_by_link_text("Rates & Inventory").click()
    time.sleep(2)
    d.find_element_by_link_text("Rates").click()
    time.sleep(2)
    move_to_tax = d.find_element_by_xpath("//*[contains(text(), 'Extra Child')]")
    self.driver.execute_script("return arguments[0].scrollIntoView();", move_to_tax)
    select = Select( d.find_element_by_xpath("//div[label[contains(text(), 'Season')]]/select"))
    time.sleep(2)
    select.select_by_visible_text(season_name)
    d.find_element_by_xpath(path.rate_plans_save_path).click()

def adding_room(self):
    d = self.driver
    d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
    d.find_element_by_link_text(" Rooms").click()

   #-------------Creating New Room------------------------------------------
    d.find_element_by_link_text("Add New Room").click()
    d.find_element_by_id("pagename").send_keys(room_page_info_data.Room_Name)
    d.find_element_by_xpath(room_page_elements_path.Submit_Button_Path).click()
    time.sleep(10)

    #-------------Adding Hero Images-----------------------------------------
    d.find_element_by_xpath(room_page_elements_path.Add_Hero_Image_Path).click()
    d.find_element_by_xpath("/html/body/div[7]/div[2]/div/div[1]/div/div/div[1]/ul/li[1]/a").click()
    time.sleep(3)
    d.find_element_by_xpath(room_page_elements_path.Image_Path + "li[1]/div").click()
    d.find_element_by_xpath(room_page_elements_path.Image_Path + "li[2]/div").click()
    d.find_element_by_xpath(room_page_elements_path.Image_Path + "li[3]/div").click()
    d.find_element_by_xpath(room_page_elements_path.Image_Path + "li[4]/div").click()
    time.sleep(3)
    d.find_element_by_id("doneBtn").click()
    time.sleep(5)
    d.find_element_by_xpath(room_page_elements_path.Submit_Button_Path).click()
    time.sleep(10)
    d.refresh()
    time.sleep(15)

    #------------Adding Text to Short and Detailed Description of the Room-----------------
    d.switch_to_frame(d.find_element_by_xpath(room_page_elements_path.Short_Description_Iframe_Path))
    d.find_element_by_xpath("/html/body").send_keys(room_page_info_data.Room_Short_Description)
    d.switch_to_default_content()


    d.switch_to_frame(d.find_element_by_xpath(room_page_elements_path.Detailed_Description_Iframe_Path))
    d.find_element_by_xpath("/html/body").send_keys(room_page_info_data.Room_Detailed_Description)
    d.switch_to_default_content()
    
    #------------Adding User Content-----------------------
    #------------Adding H1 and H2-------------------------------
    element = d.find_element_by_xpath(room_page_elements_path.Add_Content_Path)
    d.execute_script("return arguments[0].scrollIntoView();", element)
    d.execute_script("window.scrollBy(0, -50);")
    d.find_element_by_xpath(room_page_elements_path.Add_Content_Path).click()
    d.find_element_by_link_text("Headers").click()
    d.find_element_by_xpath(room_page_elements_path.H1_Path).clear()
    d.find_element_by_xpath(room_page_elements_path.H1_Path).send_keys(room_page_info_data.H1)
    d.find_element_by_xpath(room_page_elements_path.H1_Done_Button_Path).click()

    #-------------Save------------------------------
    d.find_element_by_xpath("//button[@type='submit']").click()
    time.sleep(10)
    
    #-------------------Room Information & Amenities-------------------------------
    d.find_element_by_link_text("Room Information & Amenities").click()
    d.find_element_by_id("roomnumbers-1").clear()
    d.find_element_by_id("roomnumbers-1").send_keys("5")
    d.find_element_by_id("websitenumbers-1").clear()
    d.find_element_by_id("websitenumbers-1").send_keys("3")
    d.find_element_by_id("size-1").clear()
    d.find_element_by_id("size-1").send_keys("150")
    d.find_element_by_id("maxAdults-1").clear()
    d.find_element_by_id("maxAdults-1").send_keys("3")
    d.find_element_by_id("maxChild-1").clear()
    d.find_element_by_id("maxChild-1").send_keys("3")
    d.find_element_by_id("maxGuests-1").clear()
    d.find_element_by_id("maxGuests-1").send_keys("4")

    #-------------Save------------------------------
    d.find_element_by_xpath("//button[@type='submit']").click()
    time.sleep(20)

