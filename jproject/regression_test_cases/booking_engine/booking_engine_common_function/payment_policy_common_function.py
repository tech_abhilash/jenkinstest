# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
import be_common_elements_path
import datetime

path = be_common_elements_path


def login_to_admin(self, username, password):
    driver = self.driver
    driver.find_element_by_name("userid").clear()
    driver.find_element_by_name("userid").send_keys(username)
    driver.find_element_by_name("password").clear()
    driver.find_element_by_name("password").send_keys(password)
    driver.find_element_by_xpath("//button[@type='submit']").click()
    
def selecting_payment_gateway(self, admin_url, option):
    driver = self.driver
    driver.get(admin_url)
    driver.maximize_window()
    driver.find_element_by_link_text("Booking Engine").click()
    driver.find_element_by_link_text("Settings").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment Gateway").click()
    time.sleep(2)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[3]/div[1]/div/label[1]/input").click()
    select = Select(driver.find_element_by_xpath('/html/body/div[2]/div/section/div/div[2]/div/div[3]/div[2]/div[1]/div/div/select'))
    select.select_by_visible_text(option)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[4]/button[2]").click()
    time.sleep(5)

def unchecking_payment_policy(self, admin_url):
    driver = self.driver
    driver.get(admin_url)
    driver.maximize_window()
    driver.find_element_by_link_text("Booking Engine").click()
    driver.find_element_by_link_text("Settings").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment Gateway").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment-Policy").click()
    satus = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div/span[1]/input").get_attribute("name")
    if (satus == "true"):
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div/span[1]/input").click()
    
    satus = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[1]/div/label/input").get_attribute("name")
    if (satus == "true"):
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[1]/div/label/input").click()
    
    satus = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[1]/div/label/input").get_attribute("name")
    if (satus == "true"):
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[1]/div/label/input").click()
    driver.find_element_by_xpath(path.payment_gateway_save_path).click()
    time.sleep(5)

def selecting_collect_advance(self, admin_url, collect_advance):
    driver = self.driver
    driver.get(admin_url)
    driver.maximize_window()
    driver.find_element_by_link_text("Booking Engine").click()
    driver.find_element_by_link_text("Settings").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment Gateway").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment-Policy").click()
    time.sleep(2)
    satus = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div/span[1]/input").get_attribute("name")
    if (satus == "false"):
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div/span[1]/input").click()
        time.sleep(2) 
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div[1]/label/input").clear()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div[1]/label/input").send_keys(collect_advance)               
    driver.find_element_by_xpath(path.payment_gateway_save_path).click()
    time.sleep(5)

def selecting_override_policy_for_specific_days(self, admin_url, collect_advance):
    driver = self.driver
    driver.get(admin_url)
    driver.maximize_window()
    driver.find_element_by_link_text("Booking Engine").click()
    driver.find_element_by_link_text("Settings").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment Gateway").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment-Policy").click()
    time.sleep(2)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[1]/div/label/input").click()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[2]/div[1]/label/input").clear()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[2]/div[1]/label/input").send_keys(collect_advance)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[2]/div[2]/label/input[1]").click()
    n = 1
    for n in range(1,8):
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[2]/div[4]/input["+str(n)+"]").click()
        n = n + 1
    driver.find_element_by_xpath(path.payment_gateway_save_path).click()
    time.sleep(5)

def override_policy_for_last_minute_bookings(self, admin_url, collect_advance):
    driver = self.driver
    driver.get(admin_url)
    driver.maximize_window()
    driver.find_element_by_link_text("Booking Engine").click()
    driver.find_element_by_link_text("Settings").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment Gateway").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment-Policy").click()
    time.sleep(2)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[1]/div/label/input").click()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[2]/div[1]/label/input").clear()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[2]/div[1]/label/input").send_keys(collect_advance)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[2]/div[2]/label/input").clear()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[2]/div[2]/label/input").send_keys("20")
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[2]/div[3]/label/input[1]").click()
    driver.find_element_by_xpath(path.payment_gateway_save_path).click()
    time.sleep(5)
        
def checking_booking_engine_preview(self, previw_url, collect_advance):
    driver = self.driver
    driver.get(previw_url)
    driver.maximize_window()
    driver.find_element_by_xpath(be_common_elements_path.web_site_book_now_button_path).click()
    time.sleep(10)
    change_window = self.check_index_out_of_range(1)
    if(change_window == True):
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
    driver.maximize_window()
    time.sleep(5)

    driver.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
    time.sleep(2)
    
    #-----View rate---
    driver.find_element_by_xpath(be_common_elements_path.view_rate_path).click()
    time.sleep(2)
    
    driver.find_element_by_xpath(be_common_elements_path.select_room_tab_book_now_path).click()
    time.sleep(2)
    price = driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_grand_total_path).text.strip("INR ")
    print price
    pay_now1 = (float(price)*float(collect_advance))/100
    pay_now = driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_pay_now_path).text.strip("INR ")
    print pay_now
    print pay_now1
    pay_now1 = round(pay_now1,2)
    self.assertEqual(float(pay_now),float(pay_now1))
    change_window = self.check_index_out_of_range(1)
    if(change_window == True):
        driver.close()
        driver.switch_to_window(driver.window_handles[0])

def edit_inventry(self, admin_url, inventry_number):
    driver = self.driver
    driver.get(admin_url)
    driver.find_element_by_link_text("Rates & Inventory").click()
    time.sleep(2)
    driver.find_element_by_link_text("Inventory").click()
    day= datetime.date.today()
    today=str(day)
    driver.find_element_by_xpath(".//*[@id='emberApp']/div/section/div/div[1]/ul/li[2]/a").click()
    time.sleep(3)
    driver.find_element_by_xpath("//div[div[@class='month-heading'][i[@class='fa fa-chevron-left previous']]]//div[@data-date='" +today+ "']/span[@class='availability-holder']").click()
    driver.find_element_by_xpath(".//*[@class='inventory-action-container']/button[5]").click()
    driver.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[2]/form/div/div[1]/input").send_keys(inventry_number)
    driver.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[5]/button[2]").click()
    time.sleep(5)
    #         
    # n = 1
    # while 1:
    #     path = "/html/body/div[2]/div/section/div/div[2]/div/div[2]/div[1]/table/tbody/tr[1]/td["+str(n)+"]/div"
    #     x = self.check_exists_by_xpath(path)
    #     if (x == True):
    #         break
    #     else:
    #         n = n + 1
    # m = 1
    # while 1:
    #     classname = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/div[1]/table/tbody/tr["+str(m)+"]/td["+str(n)+"]/div").get_attribute("class")
    #     if (classname == "day-container   "):
    #         driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/div[1]/table/tbody/tr["+str(m)+"]/td["+str(n)+"]/div").click()
    #         driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[4]/button[4]").click()
    #         driver.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[2]/form/div/div[1]/input").send_keys(inventry_number)
    #         driver.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    #         driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[5]/button[2]").click()
    #         time.sleep(5)
    #         break
    #     elif ((n%7) == 0):
    #         m = m + 1
    #         n = 1    
    #     else:
    #         n = n + 1

