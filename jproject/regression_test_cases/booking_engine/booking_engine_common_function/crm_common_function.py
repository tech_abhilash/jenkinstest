# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
import booking_engine_page_elements_path
import datetime


def login_to_admin(self, username, password):
    driver = self.driver
    driver.find_element_by_name("userid").clear()
    driver.find_element_by_name("userid").send_keys(username)
    driver.find_element_by_name("password").clear()
    driver.find_element_by_name("password").send_keys(password)
    driver.find_element_by_xpath("//button[@type='submit']").click()
    
def unchecking_payment_policy(self, admin_url):
    driver = self.driver
    driver.get(admin_url)
    driver.maximize_window()
    driver.find_element_by_link_text("Booking Engine").click()
    driver.find_element_by_link_text("Settings").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment Gateway").click()
    time.sleep(2)
    driver.find_element_by_link_text("Payment-Policy").click()
    satus = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div/span[1]/input").get_attribute("name")
    if (satus == "true"):
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[1]/div[1]/div/span[1]/input").click()
    
    satus = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[1]/div/label/input").get_attribute("name")
    if (satus == "true"):
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[2]/div[1]/div/label/input").click()
    
    satus = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[1]/div/label/input").get_attribute("name")
    if (satus == "true"):
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[2]/form[3]/div[1]/div/label/input").click()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[3]/span[2]").click()
    time.sleep(5)

def booking_room(self, previw_url, booking_data):
    driver = self.driver
    driver.get(previw_url)
    driver.maximize_window()
    driver.find_element_by_xpath(booking_engine_page_elements_path.web_site_book_now_button_path).click()
    change_window = self.check_index_out_of_range(1)
    if(change_window == True):
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
    driver.maximize_window()
    time.sleep(5)

    driver.find_element_by_xpath(booking_engine_page_elements_path.check_availability_path).click()
    time.sleep(2)
    
    #-----View rate---
    driver.find_element_by_xpath(booking_engine_page_elements_path.view_rate_path).click()
    time.sleep(2)
    
    driver.find_element_by_xpath(booking_engine_page_elements_path.select_room_tab_book_now_path).click()
    time.sleep(2)
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_name_path).send_keys(booking_data[0])
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_email_path).send_keys(booking_data[1])
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_phone_path).send_keys(booking_data[2])
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_address_all_data_path).send_keys(booking_data[3])
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_city_path).send_keys(booking_data[4])
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_state_path).send_keys(booking_data[5])
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_pincode_path).send_keys(booking_data[6])
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_country_path).send_keys(booking_data[7])
    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_requests_path).send_keys(booking_data[8])

    driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_book_now_and_pay_button_with_all_data_path).click()
    driver.implicitly_wait(10)

    booking_id = driver.find_element_by_xpath(".//*[@id='collapseFour']/div/div/div[2]/div/div[2]/div/div/p").text
    time.sleep(10)
    change_window = self.check_index_out_of_range(1)
    if(change_window == True):
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
    return booking_id


def checking_sm_crm_transaction_details_table(self, cur, booking_id):
    cur.execute("use qa_db")
    cur.execute("SELECT * FROM sm_crm_transaction_details where booking_id=%s",(booking_id))
    data =  cur.fetchall()
    customer_id = data[0][3]
    return customer_id
    
def checking_sm_customer_details_table(self, cur, customer_id, booking_data):
    cur.execute("SELECT * FROM sm_customer_details where id=%s",(customer_id))
    data =  cur.fetchall()
    name = data[0][1]
    self.assertEqual(name,booking_data[0])
    email = data[0][2]
    self.assertEqual(email,booking_data[1])
    phone = data[0][3]
    self.assertEqual(phone,"+91"+booking_data[2])
    print name
    print email
    print phone
    

def checking_sm_customer_address_table(self, cur, customer_id, booking_data):
    cur.execute("SELECT * FROM sm_customer_address where customer_id=%s",(customer_id))
    data =  cur.fetchall()
    country = data[0][1]
    self.assertEqual(country,booking_data[7])
    state = data[0][2]
    self.assertEqual(state,booking_data[5])
    city = data[0][3]
    self.assertEqual(city,booking_data[4])
    pincode = data[0][4]
    self.assertEqual(pincode,booking_data[6])
    address = data[0][5]
    self.assertEqual(address,booking_data[3])
    print country
    print state
    print city
    print pincode
    print address

def checking_sm_customer_phone_table(self, cur, customer_id, booking_data):
    cur.execute("SELECT * FROM sm_customer_phone where customer_id=%s",(customer_id))
    data =  cur.fetchall()
    phone_no = data[0][1]
    self.assertEqual(phone_no,"+91"+booking_data[2])
    print phone_no

def checking_sm_customer_email_table(self, cur, customer_id, booking_data):
    cur.execute("SELECT * FROM sm_customer_phone where customer_id=%s",(customer_id))
    data =  cur.fetchall()
    email = data[0][1]
    self.assertEqual(email,booking_data[1])
    print email

def edit_inventry(self, admin_url, inventry_number):
    driver = self.driver
    driver.get(admin_url)
    driver.find_element_by_link_text("Booking Engine").click()
    time.sleep(2)
    driver.find_element_by_link_text("Inventory").click()
    day= datetime.date.today()
    today=str(day)
    driver.find_element_by_xpath("//div[div[@class='month-heading'][i[@class='fa fa-chevron-left previous']]]//div[@data-date='" +today+ "']/span[@class='availability-holder']").click()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[4]/button[4]").click()
    driver.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[2]/form/div/div[1]/input").send_keys(inventry_number)
    driver.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[5]/button[2]").click()
    time.sleep(5)