from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
from decimal import *
import unittest, time, re
from selenium.webdriver.support.ui import Select
import traceback
import sys
import os
import be_common_elements_path
import common_data



def checking_booking_form(self, group_name, hotel_name, booking_buttton_xpath):
    try:
        d = self.driver
        select = Select(d.find_element_by_id('booking-engine-groups'))
        select.select_by_visible_text(group_name)
        time.sleep(2)
        i = 1
        while 1:
            name = d.find_element_by_xpath(".//*[@id='booking-engine-groups']/option["+ str(i) +"]").text
            if (group_name == name):
                group_name_1 = d.find_element_by_xpath(".//*[@id='booking-engine-groups']/option["+ str(i) +"]").text
                print group_name_1
                break
            else:
                i = i + 1
        select = Select(d.find_element_by_id('booking-engine-hotels'))
        select.select_by_visible_text(hotel_name)
        time.sleep(2)
        i = 1
        while 1:
            name = d.find_element_by_xpath(".//*[@id='booking-engine-hotels']/option["+ str(i) +"]").text
            if (hotel_name == name):
                hotel_name_1 = d.find_element_by_xpath(".//*[@id='booking-engine-hotels']/option["+ str(i) +"]").text
                print hotel_name_1
                break
            else:
                i = i + 1

        d.find_element_by_xpath(booking_buttton_xpath).click()
        time.sleep(10)
        change_window = self.check_index_out_of_range(1)
        if(change_window == True):
            window_after = d.window_handles[1]
            d.switch_to_window(window_after)
        d.maximize_window()
        self.driver.implicitly_wait(30)

        print d.find_element_by_id('select2-chainLocation-container').text
        print d.find_element_by_id('select2-chainHotels-container').text
        self.assertEqual(d.find_element_by_id('select2-chainLocation-container').text,group_name_1)
        self.assertEqual(d.find_element_by_id('select2-chainHotels-container').text,hotel_name_1)
        change_window = self.check_index_out_of_range(1)
        if(change_window == True):
            d.close()
            d.switch_to_window(d.window_handles[0])
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())   
        sys.exit("Exception occurred while Checking Booking Form") 

def moving_to_child_hotels(self, group_name, hotel_name, booking_buttton_xpath):
    try:
        d = self.driver
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='mid-navbar-collapse']/li/a")).perform()
        i = 1
        flag = 0
        while 1:
            if (flag == 1):
                break
            path = ".//*[@id='mid-navbar-collapse']/li/ul/li["+ str(i) +"]/div/h4"
            x = self.check_exists_by_xpath(path)
            if ( x == True ):
                dropdown_group_name = d.find_element_by_xpath(".//*[@id='mid-navbar-collapse']/li/ul/li["+ str(i) +"]/div/h4").text
                if(dropdown_group_name == group_name):
                    k = 1
                    while 1:
                        path = ".//*[@id='mid-navbar-collapse']/li/ul/li["+ str(i) +"]/div/ol/li["+ str(k) +"]/a/span[2]"
                        x = self.check_exists_by_xpath(path)
                        if ( x == True):
                            dropdown_hotel_name = d.find_element_by_xpath(".//*[@id='mid-navbar-collapse']/li/ul/li["+ str(i) +"]/div/ol/li["+ str(k) +"]/a/span[2]").text
                            if(dropdown_hotel_name == hotel_name):
                                d.find_element_by_xpath(".//*[@id='mid-navbar-collapse']/li/ul/li["+ str(i) +"]/div/ol/li["+ str(k) +"]/a/span[2]").click()
                                self.driver.implicitly_wait(30)
                                select = Select(d.find_element_by_id('booking-engine-groups'))
                                option = select.first_selected_option
                                self.assertEqual(option.text,group_name)
                                select = Select(d.find_element_by_id('booking-engine-hotels'))
                                option = select.first_selected_option
                                self.assertEqual(option.text,hotel_name)
                                d.find_element_by_xpath(booking_buttton_xpath).click()
                                time.sleep(10)
                                change_window = self.check_index_out_of_range(1)
                                if(change_window == True):
                                    window_after = d.window_handles[1]
                                    d.switch_to_window(window_after)
                                d.maximize_window()
                                self.driver.implicitly_wait(30)
                                self.assertEqual(d.find_element_by_id('select2-chainLocation-container').text,group_name)
                                self.assertEqual(d.find_element_by_id('select2-chainHotels-container').text,hotel_name)
                                flag = 1
                                break
                            else:
                                k = k + 1
                        else:
                            print "hotel not found"
                            flag = 1
                            break

                else:
                    i = i + 1
            else:
                print "group not found"
                break   
    except Exception, err:
        print(traceback.format_exc())   
        sys.exit("Exception occurred while Checking Booking Form") 

def testing_max_adult(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.chain_check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.price_path).text, common_data.three_adult_price)
        d.find_element_by_xpath(be_common_elements_path.modify_button_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
        d.refresh()
        time.sleep(15)
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Max Adult")         

def testing_max_child(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.chain_check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        self.assertEqual(adult_and_child,"4 Guests")
        total = float(common_data.one_adult_price)+float(common_data.extra_child_price)+float(common_data.extra_child_price)
        value = d.find_element_by_xpath(be_common_elements_path.price_path).text
        self.assertEqual(Decimal(value).normalize(), Decimal(total).normalize())
        d.refresh()
        time.sleep(15)
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Max child")   

def testing_for_one_adult(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.chain_check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.price_path).text,common_data.one_adult_price)
        d.find_element_by_xpath(be_common_elements_path.modify_button_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for one Adult")

def testing_for_two_adult(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.chain_check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.price_path).text, common_data.two_adult_price)
        d.find_element_by_xpath(be_common_elements_path.modify_button_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for two Adult")

def testing_for_three_adult(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.chain_check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.price_path).text, common_data.three_adult_price)
        d.find_element_by_xpath(be_common_elements_path.modify_button_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for two Adult")

def testing_for_child_included_in_the_rate(self, room_name):
    try:
        d = self.driver
        d.refresh()
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.chain_check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.price_path).text,common_data.one_adult_price)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Extra 2 Child")


def testing_for_extra_child(self, room_name):
    try:
        d = self.driver
        d.refresh()
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.chain_check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        self.assertEqual(adult_and_child,"4 Guests")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        value = d.find_element_by_xpath(be_common_elements_path.price_path).text
        self.assertEqual(Decimal(value).normalize(), Decimal(total).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Extra Child")

def testing_for_extra_2child(self, room_name):
    try:
        d = self.driver
        d.refresh()
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.chain_check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        self.assertEqual(adult_and_child,"4 Guests")
        total = float(common_data.one_adult_price)+float(common_data.extra_child_price)+float(common_data.extra_child_price)
        value = d.find_element_by_xpath(be_common_elements_path.price_path).text
        self.assertEqual(Decimal(value).normalize(), Decimal(total).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Extra 2 Child")

def testing_rate_plane_and_photos(self, Room_Detailed_Description, wordList):
    try:    
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.view_rate_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.preview_rate_plans_name_path).text,common_data.rate_plane_name)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.preview_rate_plans_short_description).text,common_data.rate_plan_short_description)
        d.find_element_by_xpath(be_common_elements_path.preview_view_detail_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.preview_rate_plans_detail_description).text,common_data.rate_plan_detail_description)
        d.find_element_by_xpath(be_common_elements_path.preview_view_detail_close_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.detail_tab_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.detail_room_detail_description).text,Room_Detailed_Description)
        self.assertEqual(d.find_element_by_xpath(".//*[@class='room-details-container']/div[1]/div/div/div/div[2]/div[2]").text,"Amenities")
        d.find_element_by_xpath(be_common_elements_path.photos_tab_path).click()
        time.sleep(2)
        # i = 1
        # for i in range(1,4):
        #     print i
        #     image_name = d.find_element_by_xpath(be_common_elements_path.photo_tab_image_path + "div["+str(i)+"]/img").get_attribute("src").split("/")
        #     n = len(image_name) 
        #     image_name_1 = image_name[n-1]
        #     image_name = image_name_1.split("?")
        #     self.assertEqual(wordList[i-1], image_name[0])
        #     i = i + 1
        d.find_element_by_xpath(be_common_elements_path.photos_tab_view_rate_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.select_room_tab_book_now_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Rate Plane")


def testing_guest_information(self, check_in, check_out, room_name):
    try:
        d = self.driver
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_check_in_path).text,check_in)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_check_out_path).text,check_out)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_no_of_night_path).text,"1 Nights")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.guest_information_tab_adult_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 2 Children")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        price = d.find_element_by_xpath(be_common_elements_path.guest_information_tab_sub_total_path).text.strip("INR ")
        self.assertEqual(Decimal(price).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_tax_path).text.strip("INR "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath(be_common_elements_path.guest_information_tab_grand_total_path).text.strip("INR ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_with_all_data_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_error_msg_all_data_path).text,"Please provide your name for the reservation")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_name_path).send_keys("test name")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_email_path).send_keys("shabari@simplotel.com")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_phone_path).send_keys("8971412668")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_address_path).send_keys("simplotel")
        d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_with_all_data_path).click()
        time.sleep(20)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Guest Information Page")
        
def testing_booking_conformation_page(self,check_in, check_out, room_name):
    try:
        d = self.driver
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_path).text,"Thank You For Your Reservation")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_name_path).text,"test name")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_check_in_path).text,check_in)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_check_out_path).text,check_out)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_no_of_night_path).text,"1 Nights")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_adult_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 2 Children")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        grand_total_1 = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_rate_path).text.strip("INR ")
        self.assertEqual(Decimal(grand_total_1).normalize(),Decimal(total).normalize())
        grand_total_2 = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_sub_total_path).text.strip("INR ")
        self.assertEqual(Decimal(grand_total_2).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_tax_path).text.strip("INR "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_grand_total_path).text.strip("INR ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Booking Conformation Page")


def testing_cc_avenue_payment_page(self):
    try:
        d = self.driver
        title = d.title
        self.assertEqual(title,"CCAvenue: Billing Shipping")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.ccavenue_page_heading_path).text,"Simplotel")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)+float(common_data.tax_amount)
        pay_now = (float(total)*float(common_data.payment_policy_amount))/100
        finalTotal = d.find_element_by_id("finalTotal").text
        finalTotal = finalTotal.strip("INR ")
        self.assertEqual(Decimal(finalTotal).normalize(),Decimal(pay_now).normalize())
        d.find_element_by_xpath(be_common_elements_path.ccavenue_select_net_banking_path).click()
        select = Select(d.find_element_by_id("netBankingBank"))
        select.select_by_visible_text("AvenuesTest")
        time.sleep(5)
        d.find_element_by_xpath(".//*[@id='SubmitBillShip']").click()
        self.d.implicitly_wait(30)
        d.find_element_by_xpath(".//input[@value='Return To the Merchant Site']").click()
        self.d.implicitly_wait(30)
        d.switch_to.alert.accept()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing cc avenue payment page")

