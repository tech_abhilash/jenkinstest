# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
from decimal import *
import unittest, time, re
from selenium.webdriver.support.ui import Select
import traceback
import sys
import os
import be_common_elements_path
import common_data
import datetime
from time import gmtime, strftime


def moving_to_new_tab(self):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.web_site_book_now_button_path).click()
        time.sleep(10)
        change_window = self.check_index_out_of_range(1)
        print change_window
        if(change_window == True):
            window_after = d.window_handles[1]
            d.switch_to_window(window_after)
        d.maximize_window()
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())   
        sys.exit("Exception occurred while moving to new tab") 

def close_new_tab(self):
    try:
        d = self.driver
        change_window = self.check_index_out_of_range(1)
        print change_window
        if(change_window == True):
            window_after = d.window_handles[1]
            d.switch_to_window(window_after)
            d.close()
            window_after = d.window_handles[0]
            d.switch_to_window(window_after)
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())   
        sys.exit("Exception occurred while moving to new tab")

def testing_max_adult(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (3 Adults, 0 Children)")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        val = d.find_element_by_xpath(be_common_elements_path.price_path).text
        self.assertEqual(float(val), float(common_data.three_adult_price))
        d.find_element_by_xpath(be_common_elements_path.modify_button_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
        d.refresh()
        time.sleep(15)
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Max Adult")         

def testing_max_child(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (1 Adult, 3 Children)")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        total = float(common_data.one_adult_price)+float(common_data.extra_child_price)+float(common_data.extra_child_price)
        value = d.find_element_by_xpath(be_common_elements_path.price_path).text
        self.assertEqual(Decimal(value).normalize(), Decimal(total).normalize())
        d.refresh()
        time.sleep(15)
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Max child")   

def testing_for_one_adult(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (1 Adult, 0 Children)")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(float(d.find_element_by_xpath(be_common_elements_path.price_path).text), float(common_data.one_adult_price))
        d.find_element_by_xpath(be_common_elements_path.modify_button_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for one Adult")

def testing_for_two_adult(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (2 Adults, 0 Children)")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(float(d.find_element_by_xpath(be_common_elements_path.price_path).text), float(common_data.two_adult_price))
        d.find_element_by_xpath(be_common_elements_path.modify_button_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for two Adult")

def testing_for_three_adult(self, room_name):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (3 Adults, 0 Children)")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(float(d.find_element_by_xpath(be_common_elements_path.price_path).text), float(common_data.three_adult_price))
        d.find_element_by_xpath(be_common_elements_path.modify_button_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for three Adult")

def testing_for_child_included_in_the_rate(self, room_name):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (1 Adult, 1 Child)")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(float(d.find_element_by_xpath(be_common_elements_path.price_path).text), float(common_data.one_adult_price))
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Extra 2 Child")


def testing_for_extra_child(self, room_name):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (2 Adults, 2 Children)")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        value = d.find_element_by_xpath(be_common_elements_path.price_path).text
        self.assertEqual(Decimal(value).normalize(), Decimal(total).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Extra Child")

def testing_for_extra_2child(self, room_name):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (1 Adult, 3 Children)")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.room_name_path).text,room_name)
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        total = float(common_data.one_adult_price)+float(common_data.extra_child_price)+float(common_data.extra_child_price)
        value = d.find_element_by_xpath(be_common_elements_path.price_path).text
        self.assertEqual(Decimal(value).normalize(), Decimal(total).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Extra 2 Child")

def testing_rate_plane_and_photos(self, Room_Detailed_Description, wordList):
    try:    
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.view_rate_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.preview_rate_plans_name_path).text,common_data.rate_plane_name)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.preview_rate_plans_short_description).text,common_data.rate_plan_short_description)
        d.find_element_by_xpath(be_common_elements_path.preview_view_detail_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.preview_rate_plans_detail_description).text,common_data.rate_plan_detail_description)
        d.find_element_by_xpath(be_common_elements_path.preview_view_detail_close_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.detail_tab_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.detail_room_detail_description).text,Room_Detailed_Description)
        # self.assertEqual(d.find_element_by_xpath(".//*[@class='room-details-container']/div[1]/div/div/div/div[2]/div[2]").text,"Amenities")
        d.find_element_by_xpath(be_common_elements_path.photos_tab_path).click()
        time.sleep(2)
        # i = 1
        # for i in range(1,4):
        #     print i
        #     image_name = d.find_element_by_xpath(be_common_elements_path.photo_tab_image_path + "div["+str(i)+"]/img").get_attribute("src").split("/")
        #     n = len(image_name) 
        #     image_name_1 = image_name[n-1]
        #     image_name = image_name_1.split("?")
        #     self.assertEqual(wordList[i-1], image_name[0])
        #     i = i + 1
        d.find_element_by_xpath(be_common_elements_path.photos_tab_view_rate_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.select_room_tab_book_now_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Rate Plane")


def testing_guest_information(self, check_in, check_out, room_name, guset_data):
    try:
        d = self.driver
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_check_in_path).text,check_in)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_check_out_path).text,check_out)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_no_of_night_path).text,"1 Nights")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.guest_information_tab_adult_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 2 Children")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        price = d.find_element_by_xpath(be_common_elements_path.guest_information_tab_sub_total_path).text.strip("INR USD ")
        self.assertEqual(Decimal(price).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_tax_path).text.strip("INR USD "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath(be_common_elements_path.guest_information_tab_grand_total_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        pay_now = d.find_element_by_xpath("//div[div[div[contains(text(), 'Pay Now')]]]/div[2]/p").text.strip("INR USD ")
        d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_with_all_data_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_error_msg_all_data_path).text,"Please provide your name for the reservation")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_name_path).send_keys(guset_data[0])
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_email_path).send_keys(guset_data[1])
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_phone_path).send_keys(guset_data[2])
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_address_path).send_keys(guset_data[3])
        d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_with_all_data_path).click()
        time.sleep(20)
        return pay_now
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Guest Information Page")
        
def testing_booking_conformation_page(self,check_in, check_out, room_name, Booking_id, guest_name):
    try:
        d = self.driver
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_path).text,"Thank You For Your Reservation")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_name_path).text,guest_name)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_check_in_path).text,check_in)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_check_out_path).text,check_out)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_no_of_night_path).text,"1 Nights")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_adult_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 2 Children")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        grand_total_1 = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_rate_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total_1).normalize(),Decimal(total).normalize())
        grand_total_2 = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_sub_total_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total_2).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_tax_path).text.strip("INR USD "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_grand_total_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        self.assertEqual(d.find_element_by_xpath("//div[label[contains(text(), 'Reservation Id')]]/div/p").text, Booking_id)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Booking Conformation Page")

def testing_booking_conformation_page_with_out_payment(self,check_in, check_out, room_name, guest_name):
    try:
        d = self.driver
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_path).text,"Thank You For Your Reservation")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_name_path).text,guest_name)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_check_in_path).text,check_in)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_check_out_path).text,check_out)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_no_of_night_path).text,"1 Nights")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_name_path).text,room_name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_adult_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 2 Children")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        grand_total_1 = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_rate_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total_1).normalize(),Decimal(total).normalize())
        grand_total_2 = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_sub_total_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total_2).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_tax_path).text.strip("INR USD "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_grand_total_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        Booking_id = d.find_element_by_xpath("//div[label[contains(text(), 'Reservation Id')]]/div/p").text
        return Booking_id
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Booking Conformation Page")

def testing_cc_avenue_payment_page(self, pay_now):
    try:
        d = self.driver
        title = d.title
        self.assertEqual(title, "CCAvenue: Billing Shipping")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.ccavenue_page_heading_path).text,"Simplotel")
        finalTotal = d.find_element_by_id("finalTotal").text
        finalTotal = finalTotal.strip("INR USD ")
        self.assertEqual(Decimal(finalTotal).normalize(), Decimal(pay_now).normalize())
        booking_id_with_text = d.find_element_by_xpath(".//*[@id='ordertotal']/div[3]").text
        booking_id = re.sub(r"\bOrd\w+", "", booking_id_with_text)
        booking_id = booking_id.strip(" #:\n")
        d.find_element_by_xpath("//ul[@class='resp-tabs-list span3']/li[3]/div[1]/span[2]").click()
        time.sleep(3)   
        select = Select(d.find_element_by_id("netBankingBank"))
        select.select_by_visible_text("AvenuesTest")
        time.sleep(3)
        d.find_element_by_xpath(be_common_elements_path.make_payment).click()
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.return_to_marchant_site).click()
        try:
            alert = d.switch_to_alert()
            alert.accept()
            print "alert accepted"
        except:
            print "no alert"
        time.sleep(12)
        print booking_id
        return booking_id
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing CCAvenue Payment Gateway")

def gmail_verification_of_confirmation_mail(self, hotel_name, guset_data, booking_data):
    try:
        d = self.driver
        d.get("http://www.gmail.com")
        self.driver.implicitly_wait(30)
        d.find_element_by_xpath("//*[contains(text(),'Sign In')]").click()
        self.driver.implicitly_wait(30)
        d.find_element_by_id("identifierId").send_keys(guset_data[1])
        d.find_element_by_id("identifierNext").click()
        time.sleep(4)
        d.find_element_by_xpath(".//*[@id='password']/div[1]/div/div[1]/input").send_keys(guset_data[2])
        d.find_element_by_id("passwordNext").click()
        self.driver.implicitly_wait(30)
        time.sleep(10)
        d.find_element_by_xpath("//div[span[b[contains(text(), 'Booking Confirmation' )]]]/span[2][contains(text(), '"+ str(hotel_name) +"' )]").click()
        time.sleep(8)
        email_name_with_dear = d.find_element_by_xpath("//td[p[contains(text(),'Dear')]]/p[1]").text
        email_name = email_name_with_dear.strip("Dear ")
        email_name = email_name.strip(",")
        self.assertEqual(guset_data[0], email_name)
        email_text = d.find_element_by_xpath("//*[contains(text(),'  Thank you for choosing our ')]").text
        self.assertEqual(email_text, "Thank you for choosing our hotel for your next stay. Your reservation is confirmed and the details are below.")
        email_hotel_name = d.find_element_by_xpath("//tr[td[p[contains(text(),'Reservation ID')]]]/td[1]/h3/a").text
        self.assertEqual(email_hotel_name,hotel_name)
        email_booking_id_with_text = d.find_element_by_xpath("//p[contains(text(),'Reservation ID')]").text
        email_booking_id = email_booking_id_with_text.strip("Reservation ID ")
        email_booking_id = email_booking_id.strip(": ")
        self.assertEqual(booking_data[0],email_booking_id)
        x = False
        path = "//td[table[tbody[tr[td[p[contains(text(),'Reservation ID')]]]]]]/div/div[1]/div/div"
        x = self.check_exists_by_xpath(path)
        print x
        email_name_2 = d.find_element_by_xpath("//*[contains(text(),'Primary Guest')]/span").text
        self.assertEqual(guset_data[0],email_name_2)
        self.assertEqual(d.find_element_by_xpath("//tr[td[h3[contains(text(),'Primary Guest')]]]/td[2]/p").text,"1 Room(s), 1 Night(s)")
        email_room_name = d.find_element_by_xpath("//*[contains(text(),'Room 1')]/span").text.strip("- ")
        self.assertEqual(d.find_element_by_xpath("//*[contains(text(),'No of Guests')]").text,"No of Guests - 2 Adults, 2 Children")
        self.assertEqual(d.find_element_by_xpath("//td[p[contains(text(),'No of Guests')]]/p[2]").text,common_data.rate_plane_name)
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        grand_total_1 = d.find_element_by_xpath("//td[table[tbody[tr[td[p[contains(text(),'No of Guests')]]]]]]/table[2]/tbody/tr/td[2]/p").text.strip("Rs. USD ")
        print grand_total_1
        self.assertEqual(Decimal(grand_total_1).normalize(),Decimal(total).normalize())
        grand_total_2 = d.find_element_by_xpath("//tr[td[p[contains(text(),'Sub Total')]]]/td[2]/p[1]").text.strip("Rs. USD ")
        self.assertEqual(Decimal(grand_total_2).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath("//tr[td[p[contains(text(),'Sub Total')]]]/td[2]/p[2]").text.strip("Rs. USD "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath("//tr[td[p[contains(text(),'Grand Total')]]]/td[2]/p[1]").text.strip("Rs. USD ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        time.sleep(10)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification of Confirmation mail")

def getting_hotel_id(self):
    try:
        d = self.driver
        d.find_element_by_link_text("Ops").click()
        url = d.current_url
        url = url.split("/")
        property_id = url[4]
        return property_id
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Getting Hotel ID")


def checking_sm_booking_table(self, cur, booking_id, hotel_id):
    try:
        cur.execute("use simplotel")
        cur.execute("SELECT * FROM sm_booking where id=%s",[booking_id])
        data =  cur.fetchall()
        self.assertEqual(data[0][0],booking_id)
        self.assertEqual(data[0][1],datetime.date.today())
        self.assertEqual(data[0][2],datetime.date.today())
        self.assertEqual(data[0][3],datetime.date.today() + datetime.timedelta(days=1))
        self.assertEqual(data[0][13],"CONFIRMED")
        self.assertEqual(int(data[0][15]),int(hotel_id))
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)+ float(common_data.tax_amount)
        self.assertEqual(int(data[0][5]),int(total))    
    except Exception, err:
        print (traceback.format_exc())    
        sys.exit("Exception occurred while checking sm booking table")

def testing_override_taxes(self, num_adult, num_child, tax, price):
    try:
        d = self.driver
        if (num_adult == 2 and num_child == 0):
            d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        elif (num_adult == 1 and num_child == 1):
            d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        elif (num_adult == 2 and num_child == 2):
            d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
            d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
            d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.view_rate_path).click()
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.preview_view_detail_path).click()
        time.sleep(2)
        new_price = float(d.find_element_by_xpath("/html/body/section/div[2]/div/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div[2]/p").text.strip("INR "))
        new_tax = float(d.find_element_by_xpath("/html/body/section/div[2]/div/div/div/div[1]/div[2]/div/div/div[2]/div/div[3]/div[2]/p").text.strip("INR "))
        self.assertEqual(int(new_price),int(price))
        self.assertEqual(int(new_tax),int(tax))
        d.find_element_by_xpath("html/body/section/div[2]/div/div/div/div[2]/button").click()
        time.sleep(2)
        d.refresh()
        time.sleep(15)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for one Adult")

def testing_citrus_payment_page(self, pay_now):
    try:
        d = self.driver
        title = d.title
        self.assertEqual(title, "Citrus Checkout")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.ccavenue_page_heading_path).text,"Simplotel")
        finalTotal = d.find_element_by_id("finalTotal").text
        finalTotal = finalTotal.strip("INR USD ")
        self.assertEqual(Decimal(finalTotal).normalize(), Decimal(pay_now).normalize())
        booking_id_with_text = d.find_element_by_xpath(".//*[@id='ordertotal']/div[3]").text
        booking_id = re.sub(r"\bOrd\w+", "", booking_id_with_text)
        booking_id = booking_id.strip(" #:\n")
        d.find_element_by_xpath("//ul[@class='resp-tabs-list span3']/li[3]/div[1]/span[2]").click()
        time.sleep(3)   
        select = Select(d.find_element_by_id("netBankingBank"))
        select.select_by_visible_text("AvenuesTest")
        time.sleep(3)
        d.find_element_by_xpath(be_common_elements_path.make_payment).click()
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.return_to_marchant_site).click()
        try:
            alert = d.switch_to_alert()
            alert.accept()
            print "alert accepted"
        except:
            print "no alert"
        time.sleep(12)
        print booking_id
        return booking_id
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing CCAvenue Payment Gateway")