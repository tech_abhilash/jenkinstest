# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
import be_common_elements_path

path = be_common_elements_path

def login_to_admin(self, username, password):
    driver = self.driver
    driver.find_element_by_name("userid").clear()
    driver.find_element_by_name("userid").send_keys(username)
    driver.find_element_by_name("password").clear()
    driver.find_element_by_name("password").send_keys(password)
    driver.find_element_by_xpath("//button[@type='submit']").click()
    
def selecting_rounding_rule(self, option):
    driver = self.driver
    driver.find_element_by_link_text("Booking Engine").click()
    driver.find_element_by_link_text("Settings").click()
    time.sleep(2)
    driver.find_element_by_link_text("Rounding Rules").click()
    time.sleep(2)
    classname = driver.find_element_by_xpath("//div[div[@class='checkbox enable-box-button']]//div[2]").get_attribute("class")
    if (classname == "checkbox-container disabled"):
        driver.find_element_by_name("isRuleActive").click()
    select = Select(driver.find_element_by_id('selectRoundingRule'))
    select.select_by_visible_text(option)
    driver.find_element_by_xpath(path.payment_gateway_save_path).click()
    
def adding_rates(self, one_adult, two_adult, extra_child):
    driver = self.driver
    driver.find_element_by_link_text("Rates & Inventory").click()
    driver.find_element_by_link_text("Rates").click()
    time.sleep(2)
    driver.find_element_by_xpath(".//*[@class='override-action']/button").click()
    time.sleep(2)
    driver.find_element_by_link_text("Bulk Override of Rates").click()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div[2]/div/div[2]/div[1]/input").clear()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div[2]/div/div[2]/div[1]/input").send_keys(one_adult)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div[2]/div/div[2]/div[2]/input").clear()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div[2]/div/div[2]/div[2]/input").send_keys(two_adult)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div[2]/div/div[2]/div[4]/input").clear()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div[2]/div/div[2]/div[4]/input").send_keys(extra_child)
    driver.find_element_by_xpath(".//*[@id='bulkOverride']/div[2]/button[2]").click()
    time.sleep(3)
    driver.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    time.sleep(3)
    driver.find_element_by_xpath(".//*[@class='form-group num-of-children row']/input").clear()
    driver.find_element_by_xpath(".//*[@class='form-group num-of-children row']/input").send_keys("0")
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[6]/button[2]").click()
    time.sleep(5)

def checking_booking_engine_preview(self, previw_url, after_rounding, after_rounding_tax, val):
    driver = self.driver
    driver.get(previw_url)
    driver.maximize_window()
    driver.find_element_by_xpath(be_common_elements_path.web_site_book_now_button_path).click()
    time.sleep(10)
    change_window = self.check_index_out_of_range(1)
    if(change_window == True):
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
    driver.maximize_window()
    time.sleep(5)

    if (val == 2):
        driver.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
    elif(val == 3):
        driver.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()

    driver.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
    time.sleep(10)
   
    print driver.find_element_by_xpath(be_common_elements_path.price_path).text
    value = driver.find_element_by_xpath(be_common_elements_path.price_path).text.replace(",","")
    self.assertEqual(float(value),float(after_rounding.replace(",","")))
    
    #-----View rate---
    driver.find_element_by_xpath(be_common_elements_path.view_rate_path).click()
    time.sleep(2)
    driver.find_element_by_xpath(be_common_elements_path.preview_view_detail_path).click()
    time.sleep(2)
    value = driver.find_element_by_xpath("/html/body/section/div[2]/div/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div[2]/p").text.strip("INR ").replace(",","")
    self.assertEqual(float(value),float(after_rounding.replace(",","")))
    value = driver.find_element_by_xpath("/html/body/section/div[2]/div/div/div/div[1]/div[2]/div/div/div[2]/div/div[3]/div[2]/p").text.strip("INR ").replace(",","")
    self.assertEqual(float(value),float(after_rounding_tax.replace(",","")))
    driver.find_element_by_xpath("html/body/section/div[2]/div/div/div/div[2]/button").click()
    time.sleep(2)
    driver.find_element_by_xpath(be_common_elements_path.select_room_tab_book_now_path).click()
    time.sleep(2)

    print driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_room_rate_path).text
    self.assertEqual(driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_room_rate_path).text.strip("INR "),after_rounding)

    print driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_sub_total_path).text
    self.assertEqual(driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_sub_total_path).text.strip("INR "),after_rounding)

    print driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_tax_path).text
    self.assertEqual(driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_tax_path).text.strip("INR "),after_rounding_tax)
    
    
    if (val == 3):
        driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_name_path).send_keys("Test Name")
        driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_email_path).send_keys("shabari@simplotel.com")
        driver.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_phone_path).send_keys("8971412668")
        driver.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_with_all_data_path).click()
        driver.implicitly_wait(10)
        time.sleep(10)
        print driver.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_rate_path).text
        self.assertEqual(driver.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_rate_path).text.strip("INR "),after_rounding)

        print driver.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_sub_total_path).text
        self.assertEqual(driver.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_sub_total_path).text.strip("INR "),after_rounding)

        print driver.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_tax_path).text
        self.assertEqual(driver.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_tax_path).text.strip("INR "),after_rounding_tax)

        after_rounding = re.sub(',','',after_rounding)
        after_rounding_tax = re.sub(',','',after_rounding_tax)
        total = float(after_rounding)+float(after_rounding_tax) 
        print driver.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_grand_total_path).text
        total1 = driver.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_grand_total_path).text.strip("INR ")
        total1 = re.sub(',','',total1)
        self.assertEqual(float(total1),float(total))
        print driver.find_element_by_xpath(".//*[@id='collapseFour']/div/div/div[2]/div/div[2]/div/div/p").text
        return driver.find_element_by_xpath(".//*[@id='collapseFour']/div/div/div[2]/div/div[2]/div/div/p").text
        time.sleep(10)

    change_window = self.check_index_out_of_range(1)
    if(change_window == True):
        driver.close()
        driver.switch_to_window(driver.window_handles[0])


def checking_reservations_tab(self, admin_url, booking_id, after_rounding, after_rounding_tax):
    driver = self.driver
    driver.get(admin_url)
    driver.find_element_by_link_text("Reservations").click()
    time.sleep(2)
    n = 1
    while 1:    
        book_id = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div/table/tbody/tr["+str(n)+"]/td[7]").text
        if (book_id == booking_id):
            driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div/table/tbody/tr["+str(n)+"]/td[7]").click()
            time.sleep(5)
            n = n + 1
            self.assertEqual(driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div/table/tbody/tr["+str(n)+"]/td/div/div[2]/div[2]/div[2]/div/div[3]/div[2]/table/tbody/tr[1]/td[2]").text,after_rounding)
            self.assertEqual(driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div/table/tbody/tr["+str(n)+"]/td/div/div[2]/div[2]/div[2]/div/div[3]/div[2]/table/tbody/tr[1]/td[3]").text,after_rounding_tax)
            after_rounding = re.sub(',','',after_rounding)
            after_rounding_tax = re.sub(',','',after_rounding_tax)
            total = float(after_rounding)+float(after_rounding_tax) 
            print driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div/table/tbody/tr["+str(n)+"]/td/div/div[2]/div[2]/div[2]/div/div[3]/div[2]/table/tbody/tr[1]/td[4]").text
            total1 = driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div/table/tbody/tr["+str(n)+"]/td/div/div[2]/div[2]/div[2]/div/div[3]/div[2]/table/tbody/tr[1]/td[4]").text
            total1 = re.sub(',','',total1)
            self.assertEqual(float(total1),float(total))
            break
        else:
            n = n + 2
        
def selecting_tax(self, admin_url, option):
    driver = self.driver
    driver.get(admin_url)
    driver.find_element_by_link_text("Rates & Inventory").click()
    driver.find_element_by_link_text("Taxes and Fees").click()
    time.sleep(2)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/form/div[1]/div[1]/input").clear()
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/form/div[1]/div[1]/input").send_keys("10")
    driver.find_element_by_css_selector("button.btn.btn-primary").click()
    time.sleep(2)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/form/div[1]/div[2]/select/option["+str(option)+"]").click()
    time.sleep(1)
    driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[3]/button[2]").click()