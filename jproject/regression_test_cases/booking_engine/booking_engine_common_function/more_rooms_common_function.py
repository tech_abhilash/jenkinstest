# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
from decimal import *
import unittest, time, re
from selenium.webdriver.support.ui import Select
import traceback
import sys
import os
import be_common_elements_path
import common_data
        
def adding_new_room(self, i, no_of_adults, no_of_child):
    try:    
        d = self.driver
        d.find_element_by_xpath(".//*[@id='roomContainer-1']/div[2]/div/div[3]/button").click()
        time.sleep(1)
        n = 1
        for n in range(1,no_of_adults):
            d.find_element_by_xpath(".//*[@id='roomContainer-"+str(i)+"']/div[2]/div/div[1]/div/input[3]").click()
        for n in range(0,no_of_child):
            d.find_element_by_xpath(".//*[@id='roomContainer-"+str(i)+"']/div[2]/div/div[2]/div/input[3]").click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding relative rates")

def check_availability(self):
    try:    
        d = self.driver
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(5)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Rate Plan and Rates")

def testing_rate_plan_and_rates(self, rate_plane_name, price, discount_amount, extra_amount, n, i):
    try:
        d = self.driver
        old_price = d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div[1]/div/div/div/div[3]/div[2]/div[1]/s").text 
        print old_price
        self.assertEqual(Decimal(old_price).normalize(), Decimal(price).normalize())
        discount_amount = discount_amount.strip("-")
        discount = (float(price)*float(discount_amount))/100
        new_rate = float(price)-float(discount)
        relative_rate = d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div[1]/div/div/div/div[3]/div[2]/div[2]").text
        low_price = relative_rate
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())
        d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[2]/div/div/div/span/i[1]/span").click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[1]/div[1]/span[1]").text,rate_plane_name[1]) 
        old_price = d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[1]/div/div[2]/div/div[1]/div[1]/div[1]/div/s").text
        self.assertEqual(Decimal(old_price).normalize(), Decimal(price).normalize())
        discount_amount = discount_amount.strip("-")
        discount = (float(price)*float(discount_amount))/100
        new_rate = float(price)-float(discount)
        relative_rate = d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[1]/div/div[2]/div/div[1]/div[1]/div[2]/span").text
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())
        self.assertEqual(d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[1]/span[1]").text,rate_plane_name[0]) 
        old_price = d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div[2]/span").text
        self.assertEqual(Decimal(old_price).normalize(), Decimal(price).normalize())
        self.assertEqual(d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[3]/div[1]/span[1]").text,rate_plane_name[2]) 
        extra_price =  d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[3]/div/div[2]/div/div[1]/div[1]/div[2]/span").text
        extra_amount = extra_amount.strip("-")
        new_rate = float(price)+(float(extra_amount)*n)
        self.assertEqual(Decimal(extra_price).normalize(), Decimal(new_rate).normalize())
        d.find_element_by_xpath(".//*[@id='workflow-step-"+str(i)+"']/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[1]/div/div[2]/div/div[2]/button").click()
        time.sleep(10)
        return low_price
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Rate Plan and Rates")


def testing_guest_information(self, room_price):
    try:
        d = self.driver
        i = 1   
        n = 0
        for i in range(1,7):
            old_price = d.find_element_by_xpath(".//*[@id='workflow-step-7']/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[1]/div["+str(i)+"]/div[2]/p").text.strip("INR ")
            self.assertEqual(Decimal(old_price).normalize(),Decimal(room_price[n]).normalize())
            n = n + 1
        tax = d.find_element_by_xpath(".//*[@id='workflow-step-7']/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[3]/div[2]/p").text.strip("INR ")
        tax_amount_1 = float(common_data.tax_amount)*6
        self.assertEqual(Decimal(tax_amount_1).normalize(),Decimal(tax).normalize())
        total_amount = float(room_price[0])+float(room_price[1])+float(room_price[2])+float(room_price[3])+float(room_price[4])+float(room_price[5])
        total_amount = total_amount + tax_amount_1 
        print total_amount
        total = d.find_element_by_xpath(".//*[@id='workflow-step-7']/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div[4]/div/div/div[2]/p").text.strip("INR ")
        total = total.split(",")
        final_total = total[0] + total[1]
        print final_total
        self.assertEqual(Decimal(final_total).normalize(),Decimal(total_amount).normalize())

    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Guest Information")