# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
from decimal import *
import unittest, time, re
from selenium.webdriver.support.ui import Select
import traceback
import sys
import os
import be_common_elements_path
import common_data
        
def adding_relative_rates(self, rate_plane_name, amount, unit):
    try:    
        d = self.driver
        d.find_element_by_link_text("Rates & Inventory").click()
        time.sleep(2)
        d.find_element_by_link_text("Rates").click()
        time.sleep(2)
        i = 1
        while 1:
            r_name = d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li["+str(i)+"]/a").text
            if (r_name == rate_plane_name):
                d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li["+str(i)+"]/a").click()
                d.find_element_by_xpath(".//*[@class='col-xs-7 plan-type']/label[2]/input").click()
                select = Select( d.find_element_by_xpath(".//*[@class='room-rate']/div[1]/div[1]/div[1]/select"))
                select.select_by_visible_text(common_data.rate_plane_name)
                input_field = d.find_element_by_xpath(".//*[@class='room-rate']/div[1]/div[1]/div[2]/input")
                d.execute_script("arguments[0].value = ''", input_field)
                input_field.send_keys(amount)
                input_field.send_keys(Keys.RETURN)
                select = Select( d.find_element_by_xpath(".//*[@class='room-rate']/div[1]/div[1]/div[3]/select"))
                select.select_by_visible_text(unit)
                d.find_element_by_xpath(be_common_elements_path.rate_plans_save_path).click()
                time.sleep(2)
                break
            i = i + 1
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding relative rates")
    

def testing_one_adult_with_relative_rate(self, amount):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(10)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (1 Adult, 0 Children)")
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(float(d.find_element_by_xpath(be_common_elements_path.relative_normal_price_path).text),float(common_data.one_adult_price))
        amount = amount.strip("-")
        discount = (float(common_data.one_adult_price)*float(amount))/100
        new_rate = float(common_data.one_adult_price)-float(discount)
        print new_rate
        relative_rate = d.find_element_by_xpath(be_common_elements_path.relative_price_path).text
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for one Adult")

def testing_two_adult_with_relative_rate(self, amount):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (2 Adults, 0 Children)")
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(float(d.find_element_by_xpath(be_common_elements_path.relative_normal_price_path).text),float(common_data.two_adult_price))
        amount = amount.strip("-")
        discount = (float(common_data.two_adult_price)*float(amount))/100
        new_rate = float(common_data.two_adult_price)-float(discount)
        print new_rate
        relative_rate = d.find_element_by_xpath(be_common_elements_path.relative_price_path).text
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for two Adult")

def testing_three_adult_with_relative_rate(self, amount):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (3 Adults, 0 Children)")
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(float(d.find_element_by_xpath(be_common_elements_path.relative_normal_price_path).text),float(common_data.three_adult_price))
        amount = amount.strip("-")
        discount = (float(common_data.three_adult_price)*float(amount))/100
        new_rate = float(common_data.three_adult_price)-float(discount)
        print new_rate
        relative_rate = d.find_element_by_xpath(be_common_elements_path.relative_price_path).text
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for two Adult")

def testing_for_child_included_in_the_rate_with_relative_rate(self, amount):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (1 Adult, 1 Child)")
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        self.assertEqual(float(d.find_element_by_xpath(be_common_elements_path.relative_normal_price_path).text),float(common_data.one_adult_price))
        amount = amount.strip("-")
        discount = (float(common_data.one_adult_price)*float(amount))/100
        new_rate = float(common_data.one_adult_price)-float(discount)
        print new_rate
        relative_rate = d.find_element_by_xpath(be_common_elements_path.relative_price_path).text
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing for one Adult")

def testing_for_extra_2child_relative_rate(self, amount):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (1 Adult, 3 Children)")
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        total = float(common_data.one_adult_price)+float(common_data.extra_child_price)+float(common_data.extra_child_price)
        value = d.find_element_by_xpath(be_common_elements_path.relative_normal_price_path).text
        self.assertEqual(Decimal(value).normalize(), Decimal(total).normalize())
        amount = amount.strip("-")
        discount = (float(total)*float(amount))/100
        new_rate = float(total)-float(discount)
        print new_rate
        relative_rate = d.find_element_by_xpath(be_common_elements_path.relative_price_path).text
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())

    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing 2 Extra Child")

def testing_for_extra_child_relative_rate(self, amount):
    try:
        d = self.driver
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(20)
        d.find_element_by_xpath(be_common_elements_path.adult_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.child_plus_button_path).click()
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.select_room_title_path).text,"Select Room (2 Adults, 2 Children)")
        # adult_and_child = d.find_element_by_xpath(be_common_elements_path.max_occupancy_path).text
        # self.assertEqual(adult_and_child,"4 Guests")
        total = float(common_data.two_adult_price)+float(common_data.extra_child_price)
        value = d.find_element_by_xpath(be_common_elements_path.relative_normal_price_path).text
        self.assertEqual(Decimal(value).normalize(), Decimal(total).normalize())
        amount = amount.strip("-")
        discount = (float(total)*float(amount))/100
        new_rate = float(total)-float(discount)
        print new_rate
        relative_rate = d.find_element_by_xpath(be_common_elements_path.relative_price_path).text
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Extra Child")

def testing_rate_plan_and_rates(self, rate_plane_name, price, discount_amount, extra_amount, n):
    try:    
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.view_rate_path).click()
        time.sleep(5)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.preview_rate_plans_name_path).text,rate_plane_name[1]) 
        old_price = d.find_element_by_xpath(".//*[@class='tab-content']/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]").text
        self.assertEqual(Decimal(old_price).normalize(), Decimal(price).normalize())
        discount_amount = discount_amount.strip("-")
        discount = (float(price)*float(discount_amount))/100
        new_rate = float(price)-float(discount)
        print new_rate
        relative_rate = d.find_element_by_xpath(be_common_elements_path.relative_price_path).text
        self.assertEqual(Decimal(relative_rate).normalize(), Decimal(new_rate).normalize())
        self.assertEqual(d.find_element_by_xpath(".//*[@class='tab-content']/div[1]/div[2]/div[1]/span").text,rate_plane_name[0]) 
        old_price = d.find_element_by_xpath(".//*[@class='tab-content']/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/span").text
        self.assertEqual(Decimal(old_price).normalize(), Decimal(price).normalize())
        self.assertEqual(d.find_element_by_xpath(".//*[@class='tab-content']/div[1]/div[3]/div[1]/span").text,rate_plane_name[2]) 
        extra_price =  d.find_element_by_xpath(".//*[@class='tab-content']/div[1]/div[3]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/span").text
        print extra_price
        extra_amount = extra_amount.strip("-")
        print extra_amount
        new_rate = float(price)+(float(extra_amount)*n)
        print new_rate
        self.assertEqual(Decimal(extra_price).normalize(), Decimal(new_rate).normalize())
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Rate Plan and Rates")


def testing_guest_information(self, total, discount_amount):
    try:
        d = self.driver
        d.find_element_by_xpath(".//*[@class='tab-content']/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/button").click()
        time.sleep(2)
        discount_amount = discount_amount.strip("-")
        discount = (float(total)*float(discount_amount))/100
        new_rate = float(total)-float(discount)
        price = d.find_element_by_xpath(be_common_elements_path.guest_information_tab_sub_total_path).text.strip("INR ")
        self.assertEqual(Decimal(price).normalize(),Decimal(new_rate).normalize())
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_tab_tax_path).text.strip("INR "),common_data.tax_amount)
        final_total = float(new_rate)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath(be_common_elements_path.guest_information_tab_grand_total_path).text.strip("INR ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_with_all_data_path).click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_error_msg_all_data_path).text,"Please provide your name for the reservation")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_name_path).send_keys("test name")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_email_path).send_keys("shabari@simplotel.com")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_phone_path).send_keys("8971412668")
        d.find_element_by_xpath(be_common_elements_path.guest_information_tab_add_address_path).send_keys("simplotel")
        d.find_element_by_xpath(be_common_elements_path.guest_information_book_now_button_with_all_data_path).click()
        time.sleep(20)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Guest Information Page")

def adding_minimum_length_of_stay(self, days):
    try:
        d = self.driver
        d.find_element_by_link_text("Rates & Inventory").click()
        time.sleep(2)
        d.find_element_by_link_text("Rates").click()
        time.sleep(2)
        d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li[1]/a").click()
        time.sleep(2)
        element = d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[1]")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        x = self.check_visible_by_xpath("//div[label[text()='Length of Stay']]/div/input[2]", days)
        print x
        if (x == False):
            d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[1]").click()
            time.sleep(2)
        d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[2]").click()
        d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[2]").clear()
        d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[2]").send_keys(days)
        d.find_element_by_xpath(be_common_elements_path.rate_plans_save_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Minimum Length of stay")


def removing_minimum_advance_purchase(self, days):
    try:
        d = self.driver
        d.find_element_by_link_text("Rates & Inventory").click()
        time.sleep(2)
        d.find_element_by_link_text("Rates").click()
        time.sleep(2)
        d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li[1]/a").click()
        time.sleep(2)
        element = d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[1]")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        x = self.check_visible_by_xpath("//div[label[text()='Length of Stay']]/div/input[2]", days)
        print x
        if (x == True):
            d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[1]").click()
            time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.rate_plans_save_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Minimum Advance Purchase")


def adding_maximum_length_of_stay(self, days):
    try:
        d = self.driver
        d.find_element_by_link_text("Rates & Inventory").click()
        time.sleep(2)
        d.find_element_by_link_text("Rates").click()
        time.sleep(2)
        d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li[1]/a").click()
        time.sleep(2)
        element = d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[3]")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        x = self.check_visible_by_xpath("//div[label[text()='Length of Stay']]/div/input[4]", days)
        print x
        if (x == False):
            d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[3]").click()
            time.sleep(2)
        d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[4]").click()
        d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[4]").clear()
        d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[4]").send_keys(days)
        d.find_element_by_xpath(be_common_elements_path.rate_plans_save_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Maximum Length of stay")

def removing_maximum_advance_purchase(self, days):
    try:
        d = self.driver
        d.find_element_by_link_text("Rates & Inventory").click()
        time.sleep(2)
        d.find_element_by_link_text("Rates").click()
        time.sleep(2)
        d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li[1]/a").click()
        time.sleep(2)
        element = d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[3]")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        x = self.check_visible_by_xpath("//div[label[text()='Length of Stay']]/div/input[4]", days)
        print x
        if (x == True):
            d.find_element_by_xpath("//div[label[text()='Length of Stay']]/div/input[3]").click()
            time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.rate_plans_save_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Minimum Advance Purchase")

def adding_minimum_advance_purchase(self, days):
    try:
        d = self.driver
        d.find_element_by_link_text("Rates & Inventory").click()
        time.sleep(2)
        d.find_element_by_link_text("Rates").click()
        time.sleep(2)
        d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li[1]/a").click()
        time.sleep(2)
        element = d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[1]")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        x = self.check_visible_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[2]")
        print x
        if (x == False):
            d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[1]").click()
            time.sleep(2)
        d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[2]").click()
        d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[2]").clear()
        d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[2]").send_keys(days)
        d.find_element_by_xpath(be_common_elements_path.rate_plans_save_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Minimum Advance Purchase")


def adding_maximum_advance_purchase(self, days):
    try:
        d = self.driver
        d.find_element_by_link_text("Rates & Inventory").click()
        time.sleep(2)
        d.find_element_by_link_text("Rates").click()
        time.sleep(2)
        d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li[1]/a").click()
        time.sleep(2)
        element = d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[3]")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        x = self.check_visible_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[4]")
        print x
        if (x == False):
            d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[3]").click()
            time.sleep(2)
        d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[4]").click()
        d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[4]").clear()
        d.find_element_by_xpath("//div[label[text()='Advance Purchase']]/div/div/input[4]").send_keys(days)
        d.find_element_by_xpath(be_common_elements_path.rate_plans_save_path).click()
        time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Maximum Advance Purchase")

def testing_minimum_length_of_stay(self):
    try:
        d = self.driver
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        msg = d.find_element_by_xpath(be_common_elements_path.error_msg_path).text
        val = msg.split(",")
        self.assertEqual(val[0],"We're sorry")
        time.sleep(2)
        d.find_element_by_id("checkOutWrapper").click()
        i = d.find_element_by_xpath("//div[@id='left-calendar-container1']/div/div[1]/table/tbody/tr/td[@class='is-selected']").text
        j = i
        x = self.check_exists_by_xpath(".//*[@id='left-calendar-container1']/div/div[1]/table/tbody/tr/td[button[text()='" + str(int(i) + 1) + "']]")
        if (x == True):
            x = self.check_exists_by_xpath(".//*[@id='left-calendar-container1']/div/div[1]/table/tbody/tr/td[button[text()='" + str(int(j) + 2) + "']]")
            if (x == True):
                d.find_element_by_xpath(".//*[@id='left-calendar-container1']/div/div[1]/table/tbody/tr/td[button[text()='" + str(int(j) + 2) + "']]").click()
            else:
                d.find_element_by_xpath(".//*[@id='left-calendar-container1']/div/div[2]/table/tbody/tr/td[button[text()='1']]").click()
        else:
            d.find_element_by_xpath(".//*[@id='left-calendar-container1']/div/div[2]/table/tbody/tr/td[button[text()='2']]").click()
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(5)
        text = d.find_element_by_xpath("//span[contains(text(),'Modify')]").text
        self.assertEqual(text,"Modify")
        change_window = self.check_index_out_of_range(1)
        if(change_window == True):
            d.close()
            d.switch_to_window(d.window_handles[0])
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Minimum Length of stay")

def testing_maximum_length_of_stay(self):
    try:
        d = self.driver
        d.find_element_by_id("checkOutWrapper").click()
        i = d.find_element_by_xpath("//div[@id='left-calendar-container1']/div/div[1]/table/tbody/tr/td[@class='is-selected']").text
        j = i
        x = self.check_exists_by_xpath(".//*[@id='left-calendar-container1']/div/div[1]/table/tbody/tr/td[button[text()='" + str(int(i) + 1) + "']]")
        if (x == True):
            x = self.check_exists_by_xpath(".//*[@id='left-calendar-container1']/div/div[1]/table/tbody/tr/td[button[text()='" + str(int(j) + 2) + "']]")
            if (x == True):
                d.find_element_by_xpath(".//*[@id='left-calendar-container1']/div/div[1]/table/tbody/tr/td[button[text()='" + str(int(j) + 2) + "']]").click()
            else:
                d.find_element_by_xpath(".//*[@id='left-calendar-container1']/div/div[2]/table/tbody/tr/td[button[text()='1']]").click()
        else:
            d.find_element_by_xpath(".//*[@id='left-calendar-container1']/div/div[2]/table/tbody/tr/td[button[text()='2']]").click()
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(2)
        msg = d.find_element_by_xpath(be_common_elements_path.error_msg_path).text
        val = msg.split(",")
        self.assertEqual(val[0],"We're sorry")
        d.refresh()
        time.sleep(15)
        d.implicitly_wait(30)
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        time.sleep(5)
        text = d.find_element_by_xpath("//span[contains(text(),'Modify')]").text
        self.assertEqual(text,"Modify")
        change_window = self.check_index_out_of_range(1)
        if(change_window == True):
            d.close()
            d.switch_to_window(d.window_handles[0])
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Minimum Length of stay")

def adding_promo_code(self, rate_plane_name, promo_code_name):
    try:
        d = self.driver
        d.find_element_by_link_text("Rates & Inventory").click()
        time.sleep(2)
        d.find_element_by_link_text("Rates").click()
        time.sleep(10)
        i = 1
        for i in range(1,5):
            name = d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li["+str(i)+"]/a").text
            if(name == rate_plane_name):
                d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li["+str(i)+"]/a").click()
                time.sleep(2)
                element = d.find_element_by_xpath(".//*[contains(text(),'Promo Code')]")
                d.execute_script("return arguments[0].scrollIntoView();", element)
                d.execute_script("window.scrollBy(0, -50);")
                d.find_element_by_xpath(".//div[label[contains(text(),'Promo Code')]]/input").clear()
                d.find_element_by_xpath(".//div[label[contains(text(),'Promo Code')]]/input").send_keys(promo_code_name)
                time.sleep(2)
                d.find_element_by_xpath(be_common_elements_path.rate_plans_save_path).click()
                time.sleep(2)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Promo Code")
    
def checking_rate_plan_name_in_booking_engine(self, rate_plane_name_list, promo_code_name):
    try:
        d = self.driver
        time.sleep(5)
        print d.find_element_by_xpath(".//input[@id='promoCode']").get_attribute('value')
        self.assertEqual(d.find_element_by_xpath(".//input[@id='promoCode']").get_attribute('value'), promo_code_name)
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        self.driver.implicitly_wait(30)
        time.sleep(10)
        d.find_element_by_xpath(be_common_elements_path.view_rate_path).click()
        time.sleep(2)
        i = 1
        x = False
        while(1):
            x = self.check_exists_by_xpath(".//*[@class='tab-content']/div[1]/div["+str(i)+"]/div[1]/span[1]") 
            if x == True:
                name = d.find_element_by_xpath(".//*[@class='tab-content']/div[1]/div["+str(i)+"]/div[1]/span[1]").text
                if name in rate_plane_name_list:
                    i = i + 1
                else:
                    quit()
            else:
                if len(rate_plane_name_list) != i-1:
                    quit()
                break
                
        change_window = self.check_index_out_of_range(1)
        if(change_window == True):
            d.close()
            d.switch_to_window(d.window_handles[0])
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Promo Code")
    
def moving_to_new_tab_with_prmocode(self, promo_code_name):
    try:
        d = self.driver
        time.sleep(5)
        d.find_element_by_xpath(".//*[@id='bookingForm']/div[1]/div/a/span/i[2]").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='promo_code']").clear()
        d.find_element_by_xpath(".//*[@id='promo_code']").send_keys(promo_code_name)
        time.sleep(2)
        d.find_element_by_xpath(be_common_elements_path.web_site_book_now_button_path).click()
        time.sleep(10)
        change_window = self.check_index_out_of_range(1)
        print change_window
        if(change_window == True):
            window_after = d.window_handles[1]
            d.switch_to_window(window_after)
        d.maximize_window()
        self.driver.implicitly_wait(30)
    except Exception, err:
        print(traceback.format_exc())   
        sys.exit("Exception occurred while moving to new tab") 

def checking_wrong_promocode(self):
    try:
        d = self.driver
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.check_availability_path).click()
        print d.find_element_by_xpath(".//*[@class='error-content']").text
        self.assertEqual(d.find_element_by_xpath(".//*[@class='error-content']").text, "We're sorry, no valid rates are available for your stay dates.")
        change_window = self.check_index_out_of_range(1)
        if(change_window == True):
            d.close()
            d.switch_to_window(d.window_handles[0])
        self.driver.implicitly_wait(30)
        time.sleep(5)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Adding Promo Code")