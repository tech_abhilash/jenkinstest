# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from decimal import *
import time
import re
import sys
import traceback
import datetime
import os
import be_common_elements_path


def login_to_admin(self, username, password):
    d = self.driver
    d.find_element_by_name("userid").clear()
    d.find_element_by_name("userid").send_keys(username)
    d.find_element_by_name("password").clear()
    d.find_element_by_name("password").send_keys(password)
    d.find_element_by_xpath("//button[@type='submit']").click()


def enable_simplotel_instapay(self):
    try:
        d = self.driver
        d.find_element_by_link_text("Ops").click()
        element = d.find_element_by_link_text("Toggle Features")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Toggle Features").click()
        time.sleep(2)
        d.find_element_by_link_text("Instapay").click()
        time.sleep(2)
        d.find_element_by_xpath(".//*[@class='form-group']/div[1]/div[1]/label/input").click()
        d.find_element_by_xpath(".//*[@class='form-group']/div[1]/div[1]/button").click()
        time.sleep(5)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Enableing simplotel Instapay")


def creating_new_invoice(self, invoice_data):
    try:
        d = self.driver
        d.find_element_by_link_text("Inquiries & Payments").click()
        d.find_element_by_link_text("Invoices").click()
        time.sleep(2)
        d.refresh()
        time.sleep(10)
        amount = 5
        d.find_element_by_xpath(be_common_elements_path.instapay_create_invoice_button).click()
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.instapay_title_input).send_keys(invoice_data[0])
        d.find_element_by_xpath(be_common_elements_path.instapay_description_input).send_keys(invoice_data[1])
        d.find_element_by_xpath(be_common_elements_path.instapay_customer_name_input).send_keys(invoice_data[2])
        d.find_element_by_xpath(be_common_elements_path.instapay_email_input).send_keys(invoice_data[3])
        d.find_element_by_xpath(be_common_elements_path.instapay_phone_number_input).send_keys(invoice_data[4])
        d.find_element_by_xpath(be_common_elements_path.instapay_amount_input).send_keys(invoice_data[5])
        d.find_element_by_xpath(be_common_elements_path.instapay_expiry_time).send_keys(invoice_data[6])
        time.sleep(3)
        d.find_element_by_xpath(be_common_elements_path.instapay_save_button).click()
        time.sleep(5)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Creating new Invoice")


def creating_webpay_invoice(self, invoice_data, property_id):
    try:
        d = self.driver
        d.get("http://52.74.118.108/invoice/create?propertyId="+ str(property_id) + "")
        self.driver.implicitly_wait(30)
        d.find_element_by_id("name").clear()
        d.find_element_by_id("name").send_keys(invoice_data[0])
        d.find_element_by_id("email").clear()
        d.find_element_by_id("email").send_keys(invoice_data[1])
        d.find_element_by_id("phone_no").clear()
        d.find_element_by_id("phone_no").send_keys(invoice_data[2])
        d.find_element_by_id("amount").clear()
        d.find_element_by_id("amount").send_keys(invoice_data[3])
        d.find_element_by_id("description").clear()
        d.find_element_by_id("description").send_keys(invoice_data[4])
        time.sleep(3)
        d.find_element_by_xpath(".//*[@type='submit']").click()
        time.sleep(15)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Creating new Webpay Invoice")


def testing_invoice(self, invoice_data, status):
    try:
        d = self.driver
        d.find_element_by_link_text("Inquiries & Payments").click()
        d.find_element_by_link_text("Invoices").click()
        time.sleep(2)
        d.refresh()
        time.sleep(10)
        self.assertEqual(d.find_element_by_xpath("//table/tbody/tr[1]/td[2]").text, status)
        self.assertEqual(d.find_element_by_xpath("//table/tbody/tr[1]/td[8]").text, invoice_data[0])
        self.assertEqual(d.find_element_by_xpath("//table/tbody/tr[1]/td[9]").text, invoice_data[1])
        self.assertEqual(d.find_element_by_xpath("//table/tbody/tr[1]/td[10]").text, invoice_data[2])
        self.assertEqual(float(d.find_element_by_xpath("//table/tbody/tr[1]/td[11]").text), float(invoice_data[3]))
        return d.find_element_by_xpath("//table/tbody/tr[1]/td[6]").text
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Invoice")


def gmail_verification(self, hotel_name, email_subject, invoice_data):
    try:
        d = self.driver
        d.get("http://www.gmail.com")
        self.driver.implicitly_wait(30)
        d.find_element_by_id("identifierId").send_keys(invoice_data[1])
        d.find_element_by_id("identifierNext").click()
        time.sleep(4)
        d.find_element_by_xpath(".//*[@id='password']/div[1]/div/div[1]/input").send_keys(invoice_data[2])
        d.find_element_by_id("passwordNext").click()
        self.driver.implicitly_wait(30)
        time.sleep(10)
        d.find_element_by_xpath("//div[span[b[contains(text(), '"+ str(email_subject) +"' )]]]").click()
        time.sleep(8)
        text = d.find_element_by_xpath("//tr/td/p[contains(text(),'You have an invoice')]").text
        stopwords = ["you","have","an","invoice","raised","by","for","an","amount","of","rs.",".","please","see","below","for","details." ]
        querywords = text.split()
        resultwords  = [word for word in querywords if word.lower() not in stopwords]
        email_hotel_name_and_amount = ' '.join(resultwords)
        email_amount = email_hotel_name_and_amount.strip(hotel_name)
        email_hotel_name = email_hotel_name_and_amount.strip(email_amount)
        self.assertEqual(hotel_name, email_hotel_name.strip())
        # if (float(email_amount) == float(invoice_data[3])):
        #     print "amount same"
        # else:
        #     print "amount not same"
        #     sys.exit()
        var = d.find_element_by_xpath(be_common_elements_path.gmail_price_verify).text.strip("Rs. ")
        print var
        print invoice_data[3]
        if (float(invoice_data[3]) == float(var)):
            print "Invoice genereted SUCCESSFULLY"
        else:
            print "amount not same"
            sys.exit()
        title = d.find_element_by_xpath("//tbody[tr[td[contains(text(),'GRAND TOTAL')]]]/tr[1]/td[1]/p[1]").text
        self.assertEqual(title, invoice_data[0])
        description = d.find_element_by_xpath("//tbody[tr[td[contains(text(),'GRAND TOTAL')]]]/tr[1]/td[1]/p[2]").text
        self.assertEqual(description, invoice_data[4])
        d.find_element_by_xpath(be_common_elements_path.complete_payment).click()
        time.sleep(3)
        window_after = d.window_handles[1]
        d.switch_to_window(window_after)
        d.maximize_window()
        self.driver.implicitly_wait(30)
        time.sleep(10)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification")


def testing_ccavenue_payment_gateway(self, invoice_id, amount, customer_name):
    try:
        d = self.driver
        title = d.title
        self.assertEqual(title, "CCAvenue: Billing Shipping")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.ccavenue_page_heading_path).text,"Simplotel")
        payment_gateway_invoice_id = d.find_element_by_xpath(".//*[@id='ordertotal']/div[3]").text
        self.assertEqual(payment_gateway_invoice_id.strip("Order #:\n"), invoice_id)
        finalTotal = d.find_element_by_id("finalTotal").text
        finalTotal = finalTotal.strip("INR ")
        self.assertEqual(Decimal(finalTotal).normalize(), Decimal(amount).normalize())
        d.find_element_by_xpath("//ul[@class='resp-tabs-list span3']/li[3]/div[1]/span[2]").click()
        time.sleep(3)
        select = Select(d.find_element_by_id("netBankingBank"))
        select.select_by_visible_text("AvenuesTest")
        time.sleep(3)
        d.find_element_by_xpath(be_common_elements_path.make_payment).click()
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.return_to_marchant_site).click()
        try:
            alert = d.switch_to_alert()
            alert.accept()
            print "alert accepted"
        except:
            print "no alert"
        time.sleep(12)
        confirm_invoice_id = d.find_element_by_xpath(".//*[@id='collapseFour']/div/div/div[2]/div/div[2]/div/div/p").text
        self.assertEqual(confirm_invoice_id, invoice_id)
        confirm_name = d.find_element_by_xpath(".//*[@id='collapseFour']/div/div/div[2]/div/div[1]/div/div/p").text
        self.assertEqual(confirm_name, customer_name)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing CCAvenue Payment Gateway")

def testing_ccavenue_payment_gateway_for_wedpay(self, amount, customer_name):
    try:
        d = self.driver
        title = d.title
        self.assertEqual(title, "CCAvenue: Billing Shipping")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.ccavenue_page_heading_path).text,"Simplotel")
        payment_gateway_invoice_id = d.find_element_by_xpath(".//*[@id='ordertotal']/div[3]").text
        finalTotal = d.find_element_by_id("finalTotal").text
        finalTotal = finalTotal.strip("INR ")
        self.assertEqual(Decimal(finalTotal).normalize(), Decimal(amount).normalize())
        d.find_element_by_xpath("//ul[@class='resp-tabs-list span3']/li[3]/div[1]/span[2]").click()
        time.sleep(3)
        select = Select(d.find_element_by_id("netBankingBank"))
        select.select_by_visible_text("AvenuesTest")
        time.sleep(3)
        d.find_element_by_xpath(be_common_elements_path.make_payment).click()
        time.sleep(5)
        d.find_element_by_xpath(be_common_elements_path.return_to_marchant_site).click()
        try:
            alert = d.switch_to_alert()
            alert.accept()
            print "alert accepted"
        except:
            print "no alert"
        time.sleep(12)
        confirm_invoice_id = d.find_element_by_xpath(".//*[@id='collapseFour']/div/div/div[2]/div/div[2]/div/div/p").text
        self.assertEqual(payment_gateway_invoice_id.strip("Order #:\n"), confirm_invoice_id)
        confirm_name = d.find_element_by_xpath(".//*[@id='collapseFour']/div/div/div[2]/div/div[1]/div/div/p").text
        self.assertEqual(confirm_name, customer_name)
        return confirm_invoice_id   
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing CCAvenue Payment Gateway")

def gmail_verification_of_confirmation_mail(self, hotel_name, invoice_data, invoice_id):
    try:
        d = self.driver
        d.get("http://www.gmail.com")
        # d.find_element_by_id("Email").send_keys(invoice_data[2])
        # d.find_element_by_id("next").click()
        # time.sleep(4)
        # d.find_element_by_id("Passwd").send_keys(invoice_data[3])
        # d.find_element_by_id("signIn").click()
        self.driver.implicitly_wait(30)
        time.sleep(10)
        d.find_element_by_xpath("//div[span[b[contains(text(), 'Invoice Payment Confirmation' )]]]/span[2][contains(text(), '"+ str(hotel_name) +"' )]").click()
        time.sleep(8)
        email_name_with_dear = d.find_element_by_xpath("//td[p[contains(text(),'Dear')]]/p[1]").text
        email_name = email_name_with_dear.strip("Dear ")
        email_name = email_name.strip(",")
        self.assertEqual(invoice_data[1], email_name)
        text = d.find_element_by_xpath("//tbody[tr[td[p[contains(text(),'Invoice ID')]]]]/tr/td[2]/p").text
        stopwords = ["invoice", "id", ":", " "]
        querywords = text.split()
        resultwords  = [word for word in querywords if word.lower() not in stopwords]
        email_invoice_id = ' '.join(resultwords)
        self.assertEqual(email_invoice_id, invoice_id)
        email_titel = d.find_element_by_xpath("//tbody[tr[td[p[contains(text(),'Invoice ID')]]]]/tr/td[1]/h3/a").text
        self.assertEqual(invoice_data[0], email_titel)
        email_description = d.find_element_by_xpath("//tbody[tr[td[p[contains(text(),'Rs.')]]]]/tr/td/h3").text
        self.assertEqual(invoice_data[5], email_description)
        email_amount = d.find_element_by_xpath("//tbody[tr[td[p[contains(text(),'Rs.')]]]]/tr/td[2]/p").text
        email_amount = email_amount.strip("Rs. ")
        if (float(invoice_data[4]) == float(email_amount)):
            print "amount sam"
        else:
            print "amount not same"
            sys.exit()
        time.sleep(10)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification of Confirmation mail")