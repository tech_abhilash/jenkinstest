# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'simpadmin/room'))
import room_page_info_data
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '..', "booking_engine_common_function"))
import creating_booking_engine_data_common_function
import testing_booking_engine_data_common_function

class TestingSeason(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def test_season(self):
        driver = self.driver
        # driver.get(self.base_url + "/")
        # driver.maximize_window()
        # #-- login --
        # creating_booking_engine_data_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        # #-------- Adding Seasons ------------------------------------------    
        # season_name = "Seasons-2"
        # next_month = False      
        # creating_booking_engine_data_common_function.adding_seasons(self, season_name, next_month)
        
        # #-------- Selecting new Seasons -----------------------------------
        # creating_booking_engine_data_common_function.select_different_season(self, season_name)
        # time.sleep(5)
        driver.get(hotel_info.Preview_Url + "/")
        driver.maximize_window()
        self.driver.implicitly_wait(30)
        time.sleep(15)
        testing_booking_engine_data_common_function.moving_to_new_tab(self) 
        time.sleep(15)
        driver.find_element_by_xpath(".//*[@id='checkInWrapper']").click()
        driver.find_element_by_xpath(".//*[@id='left-calendar-container']/div/div[2]/table/tbody/tr[2]/td[1]/button").click()
        time.sleep(3)
        driver.find_element_by_xpath(".//*[contains(text(),'Check Availability')]").click()
        time.sleep(3)
        err_message = driver.find_element_by_xpath(".//*[@class='error-content']").text
        print err_message
        self.assertEqual(err_message, "We're sorry, no valid rates are available for your stay dates.")
        driver.refresh()
        testing_booking_engine_data_common_function.testing_max_adult(self, room_page_info_data.Room_Name)
        time.sleep(15)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
