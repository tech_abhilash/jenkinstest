# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import room_info_data
import booking_engine_page_elements_path

class SeasonsTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = room_info_data.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_seasons(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys(room_info_data.user_name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(room_info_data.Password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        driver.find_element_by_link_text("Hotel Regal Inn").click()
      

         #--------Adding Seasons------------------------------------------          
        driver.find_element_by_link_text("Booking Engine").click()
        driver.find_element_by_link_text("Seasons").click()
        time.sleep(2)
        driver.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[3]/table/tr/div/div[2]/div/input").click()
        driver.find_element_by_css_selector("th.next").click()
        driver.find_element_by_xpath("/html/body/div[3]/div[1]/table/tbody/tr[3]/td[3]").click()
        time.sleep(5)
        # driver.find_element_by_xpath(booking_engine_page_elements_path.add_seasons_path).click()
        # driver.find_element_by_xpath(booking_engine_page_elements_path.seasons_name_path).clear()
        # driver.find_element_by_xpath(booking_engine_page_elements_path.seasons_name_path).send_keys("Seasons")
        # driver.find_element_by_css_selector("button.btn.btn-primary").click()
        # time.sleep(2)
        # driver.find_element_by_xpath(booking_engine_page_elements_path.add_period_path).click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.seasons_save_path).click()
        time.sleep(10)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
