# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import info_data
import booking_engine_page_elements_path
import sys
import os
sys.path.append(os.path.join(os.path.dirname("booking_engine_auto"), '..', 'room'))
import room_page_info_data
sys.path.append(os.path.join(os.path.dirname("booking_engine_auto"), '..', ''))
import hotel_info


class Untitled2(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True
    
    def test_untitled2(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()

        driver.find_element_by_xpath(booking_engine_page_elements_path.web_site_book_now_button_path).click()
        change_window = self.check_index_out_of_range(1)
        if(change_window == True):
            window_after = driver.window_handles[1]
            driver.switch_to_window(window_after)
        driver.maximize_window()
        time.sleep(5)
        check_in = driver.find_element_by_xpath("/html/body/section/div[1]/div[2]/div/div[1]/div/div[2]/div/div/div[1]/div/div[1]/div/div/div[2]/div/div[2]/div/span[1]").text
        print check_in
        check_out = driver.find_element_by_xpath("/html/body/section/div[1]/div[2]/div/div[1]/div/div[2]/div/div/div[1]/div/div[1]/div/div/div[2]/div/div[5]/div/span[1]").text
        print check_out  
        print driver.find_element_by_xpath(booking_engine_page_elements_path.promotion_code_path).get_attribute("id")  
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.promotion_code_path).get_attribute("id"),"promoCode")
        driver.find_element_by_xpath(booking_engine_page_elements_path.adult_plus_button_path).click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.adult_plus_button_path).click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.check_availability_path).click()
        time.sleep(2)
        msg = driver.find_element_by_xpath(booking_engine_page_elements_path.error_msg_path).text
        val = msg.split(",")
        self.assertEqual(val[0],"We're sorry")
        time.sleep(2)
        driver.refresh()
        time.sleep(5) 
        driver.find_element_by_xpath(booking_engine_page_elements_path.child_plus_button_path).click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.child_plus_button_path).click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.child_plus_button_path).click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.check_availability_path).click()
        time.sleep(2)
        msg = driver.find_element_by_xpath(booking_engine_page_elements_path.error_msg_path).text
        val = msg.split(",")
        self.assertEqual(val[0],"We're sorry")
        driver.refresh()
        time.sleep(5) 
        driver.find_element_by_xpath(booking_engine_page_elements_path.check_availability_path).click()
        time.sleep(2)
        #------testing for one adult---------
        #----------Testing Heading-------
        print driver.find_element_by_xpath(booking_engine_page_elements_path.select_room_title_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.select_room_title_path).text,"Select Room")
        #----------Testing Room Name--------- 
        print driver.find_element_by_xpath(booking_engine_page_elements_path.room_name_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.room_name_path).text,room_page_info_data.Room_Name)
        #---------Testing Max Adult and Max Child-----------
        print driver.find_element_by_xpath(booking_engine_page_elements_path.max_adult_max_child_path).text
        adult_and_child = driver.find_element_by_xpath(booking_engine_page_elements_path.max_adult_max_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 1 Child")
        print driver.find_element_by_xpath(booking_engine_page_elements_path.price_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.price_path).text,info_data.one_adult_price)

        #-------Testing two adult--------------
        driver.find_element_by_xpath(booking_engine_page_elements_path.modify_button_path).click()
        time.sleep(2)
        driver.find_element_by_xpath(booking_engine_page_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
        driver.find_element_by_xpath(booking_engine_page_elements_path.adult_plus_button_path).click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.check_availability_path).click()
        time.sleep(2)
        #----------Testing Heading-------
        print driver.find_element_by_xpath(booking_engine_page_elements_path.select_room_title_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.select_room_title_path).text,"Select Room")
        #----------Testing Room Name--------- 
        print driver.find_element_by_xpath(booking_engine_page_elements_path.room_name_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.room_name_path).text,room_page_info_data.Room_Name)
        #---------Testing Max Adult and Max Child-----------
        print driver.find_element_by_xpath(booking_engine_page_elements_path.max_adult_max_child_path).text
        adult_and_child = driver.find_element_by_xpath(booking_engine_page_elements_path.max_adult_max_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 1 Child")
        print driver.find_element_by_xpath(booking_engine_page_elements_path.price_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.price_path).text,info_data.two_adult_price)
        
        #-----extra child-----------------------
        driver.find_element_by_xpath(booking_engine_page_elements_path.modify_button_path).click()
        time.sleep(2)
        driver.find_element_by_xpath(booking_engine_page_elements_path.modify_proceed_button_path).click()
        time.sleep(2)
        driver.find_element_by_xpath(booking_engine_page_elements_path.child_plus_button_path).click()
        driver.find_element_by_xpath(booking_engine_page_elements_path.check_availability_path).click()
        time.sleep(2)
        #----------Testing Heading-------
        print driver.find_element_by_xpath(booking_engine_page_elements_path.select_room_title_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.select_room_title_path).text,"Select Room")
        #----------Testing Room Name--------- 
        print driver.find_element_by_xpath(booking_engine_page_elements_path.room_name_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.room_name_path).text,room_page_info_data.Room_Name)
        #---------Testing Max Adult and Max Child-----------
        print driver.find_element_by_xpath(booking_engine_page_elements_path.max_adult_max_child_path).text
        adult_and_child = driver.find_element_by_xpath(booking_engine_page_elements_path.max_adult_max_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 1 Child")
        print driver.find_element_by_xpath(booking_engine_page_elements_path.price_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.price_path).text,info_data.child_plus_adult_price)
        

        #-----View rate---
        driver.find_element_by_xpath(booking_engine_page_elements_path.view_rate_path).click()
        time.sleep(2)
        print driver.find_element_by_xpath(booking_engine_page_elements_path.preview_rate_plans_name_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.preview_rate_plans_name_path).text,info_data.rate_plane_name)

        print driver.find_element_by_xpath(booking_engine_page_elements_path.preview_rate_plans_short_description).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.preview_rate_plans_short_description).text,info_data.rate_plan_short_description)
        

        driver.find_element_by_xpath(booking_engine_page_elements_path.preview_view_detail_path).click()
        time.sleep(2)

        print driver.find_element_by_xpath(booking_engine_page_elements_path.preview_rate_plans_detail_description).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.preview_rate_plans_detail_description).text,info_data.rate_plan_detail_description)

        driver.find_element_by_xpath(booking_engine_page_elements_path.preview_view_detail_close_path).click()
        time.sleep(2)
        driver.find_element_by_xpath(booking_engine_page_elements_path.detail_tab_path).click()
        time.sleep(2)
        print driver.find_element_by_xpath(booking_engine_page_elements_path.detail_room_detail_description).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.detail_room_detail_description).text,room_page_info_data.Room_Detailed_Description)
        print driver.find_element_by_xpath("/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[2]/div[2]").text
        self.assertEqual(driver.find_element_by_xpath("/html/body/section/div[1]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div/div/div[2]/div[2]").text,"Amenities")
        driver.find_element_by_xpath(booking_engine_page_elements_path.photos_tab_path).click()
        time.sleep(2)

        #----------------------------------------Getting Image Name----------------------------------
        with open("../room/image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        Image_Name_4 = wordList[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4



        # wordList = driver.find_element_by_xpath(booking_engine_page_elements_path.photo_tab_image_path + "div[1]/img").get_attribute("src").split("/")
        # n = len(wordList) 
        # Image_Name = wordList[n-1]
        # wordList = Image_Name.split("?")
        # Image_Name = wordList[0]
        # print Image_Name
        # self.assertEqual(Image_Name, Image_Name_1)

        # wordList = driver.find_element_by_xpath(booking_engine_page_elements_path.photo_tab_image_path + "div[2]/img").get_attribute("src").split("/")
        # n = len(wordList) 
        # Image_Name = wordList[n-1]
        # wordList = Image_Name.split("?")
        # Image_Name = wordList[0]
        # print Image_Name
        # self.assertEqual(Image_Name, Image_Name_2)

        # wordList = driver.find_element_by_xpath(booking_engine_page_elements_path.photo_tab_image_path + "div[3]/img").get_attribute("src").split("/")
        # n = len(wordList) 
        # Image_Name = wordList[n-1]
        # wordList = Image_Name.split("?")
        # Image_Name = wordList[0]
        # print Image_Name
        # self.assertEqual(Image_Name, Image_Name_3)

        # wordList = driver.find_element_by_xpath(booking_engine_page_elements_path.photo_tab_image_path + "div[4]/img").get_attribute("src").split("/")
        # n = len(wordList) 
        # Image_Name = wordList[n-1]
        # wordList = Image_Name.split("?")
        # Image_Name = wordList[0]
        # print Image_Name
        # self.assertEqual(Image_Name, Image_Name_4)

        driver.find_element_by_xpath(booking_engine_page_elements_path.photos_tab_view_rate_path).click()
        time.sleep(2)

        driver.find_element_by_xpath(booking_engine_page_elements_path.select_room_tab_book_now_path).click()
        time.sleep(2)

        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_title_path).click()
        
        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_check_in_path).text
        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_check_out_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_check_in_path).text,check_in)
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_check_out_path).text,check_out)
        
        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_no_of_night_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_no_of_night_path).text,"1 Nights")
        
        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_room_name_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_room_name_path).text,room_page_info_data.Room_Name)

        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_adult_child_path).text
        adult_and_child = driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_adult_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"2 Adults")
        self.assertEqual(val[1]," 1 Child")

        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_room_rate_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_room_rate_path).text," 400.00")

        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_sub_total_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_sub_total_path).text," 400.00")

        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_tax_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_tax_path).text," 10.00")

        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_grand_total_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_grand_total_path).text," 410.00")


        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_pay_now_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_pay_now_path).text," 46.95")

        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_balance_path).text
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_balance_path).text," 363.05")

        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_book_now_and_pay_button_with_all_data_path).click()
        time.sleep(2)
        print driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_book_now_and_pay_button_error_msg_all_data_path).click()
        self.assertEqual(driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_book_now_and_pay_button_error_msg_all_data_path).text,"Please provide your name for the reservation")

        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_name_path).send_keys("Shabari")
        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_email_path).send_keys("shabari@simplotel.com")
        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_phone_path).send_keys("8971412668")
        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_address_all_data_path).send_keys("Simplotel")
        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_city_path).send_keys("Test City")
        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_state_path).send_keys("Test State")
        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_pincode_path).send_keys("5777111")
        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_country_path).send_keys("Test Country")
        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_tab_add_requests_path).send_keys("Test Requests")


        driver.find_element_by_xpath(booking_engine_page_elements_path.guest_information_book_now_and_pay_button_with_all_data_path).click()
        time.sleep(5)
        title = driver.title
        print title
        self.assertEqual(title,"Citrus Payment")
        print driver.find_element_by_xpath(".//*[@id='summary_lable_div']/span").text
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='summary_lable_div']/span").text,"46.94 INR")
        print driver.find_element_by_xpath(".//*[@id='txt-email']").get_attribute("value")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='txt-email']").get_attribute("value"),"shabari@simplotel.com")
        driver.find_element_by_id("debitCard").click()
        time.sleep(5)
        # driver.find_element_by_id('dcCardType').click()
        # time.sleep(2)
        # driver.find_element_by_xpath(".//*[@id='ccCardType']/option[2]").click()
        select = Select(driver.find_element_by_id('dcCardType'))
        select.select_by_visible_text("Visa")
        time.sleep(2)
        driver.find_element_by_id("cardNumber").clear()
        driver.find_element_by_id("cardNumber").send_keys("4012888888881881")
        driver.find_element_by_id("cardName").clear()
        driver.find_element_by_id("cardName").send_keys("test name")
        select = Select(driver.find_element_by_id('expiryMonth'))
        select.select_by_visible_text("2")
        select = Select(driver.find_element_by_id('expiryYear'))
        select.select_by_visible_text("2027")
        driver.find_element_by_id("cvv").clear()
        driver.find_element_by_id("cvv").send_keys("123")
        driver.find_element_by_id("btn-makePayment").click()
        time.sleep(2)
        driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td/input[1]").click()
        time.sleep(10)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
