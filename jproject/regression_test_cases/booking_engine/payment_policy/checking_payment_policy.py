# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("booking_engine_without_payment"), '..', "booking_engine_common_function"))
import payment_policy_common_function
 
class Payment_policy(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_payment_policy(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        payment_policy_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        payment_policy_common_function.unchecking_payment_policy(self, hotel_info.Admin_Url)
        #Checking without payment gateway
        collect_advance = 0
        payment_policy_common_function.checking_booking_engine_preview(self, hotel_info.Preview_Url, collect_advance)
        #Checking with Collect Advance
        collect_advance = 11.45
        payment_policy_common_function.selecting_collect_advance(self, hotel_info.Admin_Url, collect_advance)
        payment_policy_common_function.checking_booking_engine_preview(self, hotel_info.Preview_Url, collect_advance)
        # checking Override Policy for specific days
        inventry_number = 22
        payment_policy_common_function.edit_inventry(self, hotel_info.Admin_Url, inventry_number)
        collect_advance = 2
        payment_policy_common_function.selecting_override_policy_for_specific_days(self, hotel_info.Admin_Url, collect_advance)
        payment_policy_common_function.checking_booking_engine_preview(self, hotel_info.Preview_Url, collect_advance)
        inventry_number = 8
        payment_policy_common_function.edit_inventry(self, hotel_info.Admin_Url, inventry_number)
        collect_advance = 11.45
        payment_policy_common_function.checking_booking_engine_preview(self, hotel_info.Preview_Url, collect_advance)
        collect_advance = 3
        payment_policy_common_function.override_policy_for_last_minute_bookings(self, hotel_info.Admin_Url, collect_advance)
        payment_policy_common_function.checking_booking_engine_preview(self, hotel_info.Preview_Url, collect_advance)
        inventry_number = 15
        payment_policy_common_function.edit_inventry(self, hotel_info.Admin_Url, inventry_number)
        collect_advance = 2
        payment_policy_common_function.checking_booking_engine_preview(self, hotel_info.Preview_Url, collect_advance)
        


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
