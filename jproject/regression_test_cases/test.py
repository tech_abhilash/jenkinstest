import unittest
# @unittest.skip("showing class skipping")
class TestStringMethods(unittest.TestCase):
    # @unittest.skip("demonstrating skipping")
    def test_upper(self):
        print "not skipped"
        self.assertEqual('foo'.upper(), 'Foo', "equal")
        # self.fail("failed")
    @unittest.expectedFailure
    def test_isupper(self):
        print "Expected Failure"
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

# if __name__ == '__main__':
#     unittest.main()

suite = unittest.TestLoader().loadTestsFromTestCase(TestStringMethods)
unittest.TextTestRunner(verbosity=2).run(suite)