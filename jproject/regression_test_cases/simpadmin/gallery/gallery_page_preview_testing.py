# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("gallery"), '../..', "config"))
import hotel_info
import gallery_page_info_data
import gallery_page_element_path



class GalleryPagePreviewTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_gallery_page_preview(self):
        d = self.driver
        d.get(self.base_url + "gallery.html")
        d.maximize_window()
        g = gallery_page_element_path
        #----------------------------------------Getting Image Name----------------------------------
        with open("image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        Image_Name_4 = wordList[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4

        #---------------------------Testing Group Name-----------------------------------
        print d.find_element_by_xpath(g.Preview_Nav_Group_Name_Path).text
        self.assertEqual(d.find_element_by_xpath(g.Preview_Nav_Group_Name_Path).text, gallery_page_info_data.Group_Name)
        d.find_element_by_xpath(g.Preview_Nav_Group_Name_Path).click()
        time.sleep(10)
       
        #--------------------------Testing Heading--------------------------------------------------
        print d.find_element_by_xpath(g.Preview_H1_Path).text
        self.assertEqual(d.find_element_by_xpath(g.Preview_H1_Path).text, gallery_page_info_data.Group_Heading)

       #---------------------------------------Tesing Hero Image---------------------
        wordList = d.find_element_by_xpath(g.Preview_Image_Path + "/div[2]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        wordList = d.find_element_by_xpath(g.Preview_Image_Path + "/div[3]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)

        wordList = d.find_element_by_xpath(g.Preview_Image_Path + "/div[4]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        wordList = d.find_element_by_xpath(g.Preview_Image_Path + "/div[5]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_4)
        
        #-------------------Testing SEO---------------------------------------------------------
        print d.title
        self.assertEqual(d.title, gallery_page_info_data.Page_Title)

        print d.find_element_by_xpath("/html/head/meta[1]").get_attribute("content")
        self.assertEqual(d.find_element_by_xpath("/html/head/meta[1]").get_attribute("content"), gallery_page_info_data.Page_Description)

        print d.find_element_by_xpath("/html/head/meta[8]").get_attribute("content")
        self.assertEqual(d.find_element_by_xpath("/html/head/meta[8]").get_attribute("content"), gallery_page_info_data.Page_Keywords)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
