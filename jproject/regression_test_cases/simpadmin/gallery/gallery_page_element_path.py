
Add_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[2]/a"
Image_Path = ".//*[@id='imageList']/"
Image_Selecter_Button_Path = ".//*[@id='doneBtn']"
Image_Cancel_Button_Path = "/html/body/div[4]/div[2]/div/div[2]/div[2]/button[1]"


Add_Content_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[2]/ul/li/a/span[1]"

#------------Path For Page Headers------------------------------
#------------H1-------------------------------------------------
Location_Name_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[5]/div/div/div[1]/div/div/input"

Group_Name_Save_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div/div/button"

                                
Short_Description_Image_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[3]/div/div/div/div[1]/label/div[2]/"
Short_Description_Iframe_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[3]/div/div/div/div[2]/label/div/div/div/iframe"
     

Latitude_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[5]/div/div/div[2]/div[1]/input"
Longitude_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[5]/div/div/div[2]/div[2]/input"                            
Address_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[5]/div/div/div[2]/div[3]/textarea"

#-----------------Submit Button Path------------------------------
Submit_Button_Path = "/html/body/div[1]/aside[2]/form/div/div/div[2]/button"


#------------------After Save----------------------------------
						 
After_Save_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[1]/div/div/div/div/ul/"   
Gallery_Group_Name_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[1]/input"
Gallery_Group_Heading_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/input"
               
                   
Page_Title_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/div/div[1]/label/input"
Page_Description_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/div/div[2]/label/textarea" 
Page_Keywords_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/div/div[3]/label/input"

        
        
      

#---------Preview Path-----------------------------------------

Preview_Heading_Path = "/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/div/h1"
Preview_Nav_Heading_Path = "/html/body/div[1]/div[1]/div/div[2]/div[1]/div/ul/li/a"

Preview_Location_Group_Name_Path = "/html/body/div[1]/div[1]/div/div[2]/div/div/div/div[2]/div[1]/p"

Preview_Nav_Group_Name_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div/div/ul/li[2]/a"
Preview_Location_Name_Path = "/html/body/div[1]/div[1]/div/div[1]/div/div/div[2]/div[2]/h3"
Preview_Location_Short_Description = "/html/body/div[1]/div[1]/div/div[1]/div/div/div[2]/div[2]/div/p"
Preview_Image_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div/div/div[1]/div[2]"
Preview_Detailed_Description_Path = "/html/body/div[1]/div[1]/div/div[1]/div/div/div[4]/div/p"
					
Preview_H1_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div/div/h1"
Preview_H2_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div[2]/div/h2"
Preview_Single_Text_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div[3]/div/p"
Preview_Two_Text = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div[4]/"
Preview_Three_Text = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div[5]/"


Preview_Only_Image_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/"
Preview_Image_Text_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/"
Preview_Image_Slider_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div[14]/div/div/div[1]/div[2]/div/"
Preview_Book_Button_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div[15]/div/div/form/input"
Preview_Other_button_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div[16]/div/a"
                        
Preview_Weather_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[2]/p/span"
Preview_Local_Time_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[3]/p/span"
Preview_TripAdvisor_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[5]/div/div/div/div[1]/dl/dt/a/img"
Preview_Social_Connect_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[6]/ul"
Preview_Location_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[7]/p/span"
Preview_Contact_Us_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[8]/p/span"
Preview_Newsletter_Signup_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[9]/p/span"
Preview_Promotion_Slider_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[10]/div/p/span"
Preview_Facebook_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div[4]/div/div/div/span/iframe"