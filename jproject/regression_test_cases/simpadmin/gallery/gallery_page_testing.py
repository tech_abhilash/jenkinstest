# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("gallery"), '../..', "config"))
import hotel_info
import gallery_page_info_data
import gallery_page_element_path
sys.path.append(os.path.join(os.path.dirname("gallery"), '..', "simpadmin_common_function"))
import create_data_common_function

class TestingGalleryPage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_gallery_page_testing(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        g = gallery_page_element_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text(" Photo Gallery").click()
        time.sleep(10)
        i = 1
        while 1:                                  
            p_name = d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[4]/ol[2]/li["+str(i)+"]/div/a").text
            if (p_name == gallery_page_info_data.Group_Name):
                d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[4]/ol[2]/li["+str(i)+"]/div/a").click()
                d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[4]/ol[2]/li["+str(i)+"]/div/div/a[1]/i").click()
                break
            i = i + 1 

        # #----------------------------------------Testing Gallery Group Name ----------------------------------------------------------------
    
        print d.find_element_by_xpath(g.Gallery_Group_Name_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(g.Gallery_Group_Name_Path).get_attribute("value"), gallery_page_info_data.Group_Name)

        print d.find_element_by_xpath(g.Gallery_Group_Heading_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(g.Gallery_Group_Heading_Path).get_attribute("value"), gallery_page_info_data.Group_Heading)

        # ----------------------Getting Image Name----------------------
        with open("image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        Image_Name_4 = wordList[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4

         #---------------------Testing Image Slider-----------------------------------------
        wordList = d.find_element_by_xpath(g.After_Save_Image_Path + "li[1]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        wordList = d.find_element_by_xpath(g.After_Save_Image_Path + "li[2]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)


        wordList = d.find_element_by_xpath(g.After_Save_Image_Path + "li[3]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        wordList = d.find_element_by_xpath(g.After_Save_Image_Path + "li[4]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_4)

       
        # # #----------------Testing Room Page SEO-------------------------------
        d.find_element_by_link_text("Photo Gallery Settings").click()
        print d.find_element_by_xpath(g.Page_Title_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(g.Page_Title_Path).get_attribute("value"), gallery_page_info_data.Page_Title)

        print d.find_element_by_xpath(g.Page_Description_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(g.Page_Description_Path).get_attribute("value"), gallery_page_info_data.Page_Description)

        print d.find_element_by_xpath(g.Page_Keywords_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(g.Page_Keywords_Path).get_attribute("value"), gallery_page_info_data.Page_Keywords)
        
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
