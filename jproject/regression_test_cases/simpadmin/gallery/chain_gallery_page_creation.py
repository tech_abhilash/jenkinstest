# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("gallery"), '../..', "config"))
import hotel_info
import gallery_page_info_data
import gallery_page_element_path
sys.path.append(os.path.join(os.path.dirname("gallery"), '..', "simpadmin_common_function"))
import create_data_common_function

class CreatingChainGalleryPage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_creating_gallery_page(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        g = gallery_page_element_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.chain_user_name, hotel_info.chain_password)

        d.find_element_by_xpath("(//button[@type='button'])[2]").click()
        d.find_element_by_link_text(hotel_info.chain_child_hotel_name_one).click()
        time.sleep(2)
        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text(" Photo Gallery").click()

        #-------------Creating New Gallery Page------------------------------------------
        d.find_element_by_link_text("Add Photo Group").click()
        d.find_element_by_id("name").send_keys(gallery_page_info_data.Group_Name)
        d.find_element_by_id("heading").send_keys(gallery_page_info_data.Group_Heading)
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)

        #----------------------------------------Adding Image Slider----------------------------------------------------------------
        d.find_element_by_xpath(g.Add_Image_Path).click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(3)
        d.find_element_by_xpath(g.Image_Path + "li[1]/div").click()
        d.find_element_by_xpath(g.Image_Path + "li[2]/div").click()
        d.find_element_by_xpath(g.Image_Path + "li[3]/div").click()
        d.find_element_by_xpath(g.Image_Path + "li[4]/div").click()
        time.sleep(3)
        wordList = d.find_element_by_xpath(g.Image_Path + "li[1]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]

        wordList = d.find_element_by_xpath(g.Image_Path + "li[2]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]

        wordList = d.find_element_by_xpath(g.Image_Path + "li[3]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]

        wordList = d.find_element_by_xpath(g.Image_Path + "li[4]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]

        #----------------Saving Image Name----------------------------------
        with open("image_names.txt",'w') as f:
            f.write(Image_Name_1 +" "+Image_Name_2+" "+Image_Name_3+" "+Image_Name_4)
            f.close()

        
        time.sleep(3)
        d.find_element_by_xpath(g.Image_Selecter_Button_Path).click()
        time.sleep(3)
        d.find_element_by_xpath("//button[@type='submit']").click()

        #------------------Adding SEO Content-------------------------------
        d.find_element_by_link_text("Photo Gallery Settings").click()
        time.sleep(5)
        d.find_element_by_id("meta-title").clear()
        d.find_element_by_id("meta-title").send_keys(gallery_page_info_data.Page_Title)
        d.find_element_by_id("meta-description").clear()
        d.find_element_by_id("meta-description").send_keys(gallery_page_info_data.Page_Description)
        d.find_element_by_id("meta-keywords").clear()
        d.find_element_by_id("meta-keywords").send_keys(gallery_page_info_data.Page_Keywords)
        
        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
