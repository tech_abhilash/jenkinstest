# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import sys
import os
import unittest, time, re
import home_page_info_data
import home_page_elements_path
sys.path.append(os.path.join(os.path.dirname("home_page"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("home_page"), '..', "simpadmin_common_function"))
import testing_data_common_function
import create_data_common_function

class TestingHomePage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_homepage(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        h = home_page_elements_path
        admin = create_data_common_function
        test_admin = testing_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        d.find_element_by_xpath("//button[@type='submit']").click()
        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text(" Home Page").click()
        time.sleep(10)

        #----------------------------------------Testing Heading  ----------------------------------------------------------------
        print d.find_element_by_xpath(home_page_elements_path.Heading_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(home_page_elements_path.Heading_Path).get_attribute("value"), home_page_info_data.Heading)
        
        #----------------------------------------Getting Image Name----------------------------------
        with open("image_names.txt",'r') as f:
            wordList_1 = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList_1[0]
        Image_Name_2 = wordList_1[1]
        Image_Name_3 = wordList_1[2]
        Image_Name_4 = wordList_1[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4

        #-------------------Testing Image ------------------------------------------
        wordList = d.find_element_by_xpath(home_page_elements_path.After_Save_Image_Path + "li[1]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        wordList = d.find_element_by_xpath(home_page_elements_path.After_Save_Image_Path + "li[2]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)


        wordList = d.find_element_by_xpath(home_page_elements_path.After_Save_Image_Path + "li[3]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        wordList = d.find_element_by_xpath(home_page_elements_path.After_Save_Image_Path + "li[4]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_4)

        # -----------------------Testing Add Content----------------------
        #------------------------H1 and H2 Testing------------------------
        test_admin.testing_h1(self, h.after_save_h1)
        test_admin.testing_h2(self, h.after_save_h2)
        
        #-----------------------Only Text Widget Testing-------------------
        test_admin.testing_single_text(self, h.after_save_single_text)
        test_admin.testing_two_text(self, h.after_save_two_text_left, h.after_save_two_text_right)
        test_admin.testing_three_text(self, h.after_save_three_text_left, h.after_save_three_text_middle, h.after_save_three_text_right)

        #---------------Testing Single Image---------------------
        test_admin.testing_single_image(self, h.after_save_single_image, wordList_1)
        test_admin.testing_two_image(self, h.after_save_two_image_left, h.after_save_two_image_right, wordList_1)
        test_admin.testing_three_image(self, h.after_save_three_image_left, h.after_save_three_image_middle, h.after_save_three_image_right, wordList_1)

        #----------------------------------------Testing Single Image and text---------------------------------------------------------------
        test_admin.testing_single_image_and_text_left(self, h.after_save_single_image_text_left_text, h.after_save_single_image_text_left_image, wordList_1)
        test_admin.testing_single_image_and_text_right(self, h.after_save_single_image_text_right_text, h.after_save_single_image_text_right_image, wordList_1)
        test_admin.testing_single_image_and_text_center(self, h.after_save_single_image_text_center_text, h.after_save_single_image_text_center_image, wordList_1)
        test_admin.testing_two_image_and_text(self, h.after_save_two_image_text_left_text, h.after_save_two_image_text_left_image, h.after_save_two_image_text_right_text, h.after_save_two_image_text_right_image, wordList_1)
        
        three_image_text_widget_text_path = [h.after_save_three_image_text_left_text, h.after_save_three_image_text_middle_text, h.after_save_three_image_text_right_text]
        three_image_text_widget_image_path = [h.after_save_three_image_text_left_image, h.after_save_three_image_text_middle_image, h.after_save_three_image_text_right_image]
        test_admin.testing_three_image_and_text(self, three_image_text_widget_text_path, three_image_text_widget_image_path, wordList_1)
        
        two_image_text_and_full_image_right_widget_text_path = [h.after_save_two_image_text_and_full_image_right_left_text, h.after_save_two_image_text_and_full_image_right_middle_text]
        two_image_text_and_full_image_right_widget_image_path = [h.after_save_two_image_text_and_full_image_right_left_image, h.after_save_two_image_text_and_full_image_right_middle_image, h.after_save_two_image_text_and_full_image_right_right_image]
        test_admin.testing_two_image_text_and_full_image_right(self, two_image_text_and_full_image_right_widget_text_path, two_image_text_and_full_image_right_widget_image_path, wordList_1)

        two_image_text_and_full_image_middle_widget_text_path = [h.after_save_two_image_text_and_full_image_middle_left_text, h.after_save_two_image_text_and_full_image_middlle_right_text]
        two_image_text_and_full_image_middle_widget_image_path = [h.after_save_two_image_text_and_full_image_middle_left_image, h.after_save_two_image_text_and_full_image_middle_middle_image, h.after_save_two_image_text_and_full_image_middlle_right_image]
        test_admin.testing_two_image_text_and_full_image_middle(self, two_image_text_and_full_image_middle_widget_text_path, two_image_text_and_full_image_middle_widget_image_path, wordList_1)

        single_image_text_and_full_image_right_widget_image_path = [h.after_save_single_image_text_and_fulll_image_right_left_image, h.after_save_single_image_text_and_fulll_image_right_right_image]
        test_admin.testing_single_image_text_and_full_image_right(self, h.after_save_single_image_text_and_fulll_image_right_left_text, single_image_text_and_full_image_right_widget_image_path, wordList_1)
        
        single_image_text_and_full_image_left_widget_image_path = [h.after_save_single_image_text_and_fulll_image_left_left_image, h.after_save_single_image_text_and_fulll_image_left_right_image]
        test_admin.testing_single_image_text_and_full_image_left(self, h.after_save_single_image_text_and_fulll_image_left_right_text, single_image_text_and_full_image_left_widget_image_path, wordList_1)
        
        testing_full_image_and_three_image_right_widget_image_path = [h.after_save_full_image_and_three_image_right_left_image, h.after_save_full_image_and_three_image_right_right_image1, h.after_save_full_image_and_three_image_right_right_image2, h.after_save_full_image_and_three_image_right_right_image3]
        test_admin.testing_full_image_and_three_image_right(self, testing_full_image_and_three_image_right_widget_image_path, wordList_1)
        
        #------------------Testing image slider------------------
        test_admin.testing_image_slider(self, wordList_1)



        #--------------------------Testing Booking Button------------------------------------------------
        test_admin.testing_book_now_button(self, h.Book_Button_Select_Path, h.Book_Button_Label_Path, home_page_info_data.Booking_Button_Name)
        test_admin.testing_other_button(self, h.Other_Button_Select_Path, h.Other_Button_Label_Path, home_page_info_data.Booking_Button_Name, h.Other_Button_Link_Path, home_page_info_data.Booking_Button_Link)


        #----------------------------------------Testing Predefined Content----------------------------------------------------------------
        test_admin.testing_predefined_content(self)
        
        #----------------Testing Room Page SEO-------------------------------
        print d.find_element_by_xpath(home_page_elements_path.Page_Title_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(home_page_elements_path.Page_Title_Path).get_attribute("value"), home_page_info_data.Page_Title)

        print d.find_element_by_xpath(home_page_elements_path.Page_Description_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(home_page_elements_path.Page_Description_Path).get_attribute("value"), home_page_info_data.Page_Description)

        print d.find_element_by_xpath(home_page_elements_path.Page_Keywords_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(home_page_elements_path.Page_Keywords_Path).get_attribute("value"), home_page_info_data.Page_Keywords)

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
