# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import sys
import os
import unittest, time, re
import home_page_info_data
import home_page_elements_path
sys.path.append(os.path.join(os.path.dirname("home_page"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("home_page"), '..', "simpadmin_common_function"))
import create_data_common_function

class ChainCreatingHomePage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_homepage(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        h = home_page_elements_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.chain_user_name, hotel_info.chain_password)

        d.find_element_by_xpath("(//button[@type='button'])[2]").click()
        d.find_element_by_link_text(hotel_info.chain_child_hotel_name_one).click()
        time.sleep(2)
        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text(" Home Page").click()
        
        #----------------Heading----------------
        d.find_element_by_id("homepage-tagline").clear()
        d.find_element_by_id("homepage-tagline").send_keys(home_page_info_data.Heading)
        d.find_element_by_xpath(h.Submit_Button_Path).click()
        time.sleep(10)


        #-------------Adding Hero Images-----------------------------------------

        d.find_element_by_xpath(h.Add_Hero_Image_Path).click()
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(3)
        d.find_element_by_xpath(h.Image_Path + "li[1]/div").click()
        d.find_element_by_xpath(h.Image_Path + "li[2]/div").click()
        d.find_element_by_xpath(h.Image_Path + "li[3]/div").click()
        d.find_element_by_xpath(h.Image_Path + "li[4]/div").click()
        time.sleep(3)

        #----------------------------------------Getting Image Name-----------------------------------------------------------------
    
        wordList = d.find_element_by_xpath(h.Image_Path + "li[1]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]

        wordList = d.find_element_by_xpath(h.Image_Path + "li[2]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]

        wordList = d.find_element_by_xpath(h.Image_Path + "li[3]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]

        wordList = d.find_element_by_xpath(h.Image_Path + "li[4]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]

        #----------------Saving Image Name----------------------------------
        with open("image_names.txt",'w') as f:
            f.write(Image_Name_1 +" "+Image_Name_2+" "+Image_Name_3+" "+Image_Name_4)
            f.close()

        
        time.sleep(3)
        d.find_element_by_xpath(h.Image_Selecter_Button_Path).click()
        time.sleep(10)
        d.find_element_by_xpath(h.Submit_Button_Path).click()
        time.sleep(10)
        d.refresh()
        time.sleep(15)

       
        #  ------------Adding User Content-----------------------
        # ------------Adding H1 and H2-------------------------------
        admin.adding_h1(self, h.H1_Path, h.H1_Done_Button_Path)
        admin.adding_h2(self, h.H2_Select_Button_Path, h.H2_Path, h.H2_Done_Button_Path)

        #-----------------------Only Text -------------------
        admin.adding_single_text(self, h.Single_Text_Iframe_Path, h.Single_Text_Done_Button_Path)
        admin.adding_two_text(self, h.Two_Text_Select_Button_Path, h.Two_Text_Iframe_Path_left, h.Two_Text_Iframe_Path_right, h.Two_Text_Done_Button_Path)
        admin.adding_three_text(self, h.Three_Text_Select_Button_Path, h.Three_Text_Iframe_Path_left, h.Three_Text_Iframe_Path_middle, h.Three_Text_Iframe_Path_right, h.Three_Text_Done_Button_Path)

        #----------------------------------------Adding only Image ----------------------------------------------------------------
        admin.adding_single_image(self, h.Image_Path, h.Image_Selecter_Button_Path, h.Single_Image_Done_Button_Path)
        admin.adding_two_image(self, h.Two_Image_Select_Button_Path, h.Image_Path, h.Image_Selecter_Button_Path, h.Two_Image_Done_Button_Path)
        admin.adding_three_image(self, h.Three_Image_Select_Button_Path, h.Image_Path, h.Image_Selecter_Button_Path, h.Three_Image_Done_Button_Path)

        #----------------------------------------Adding Image and Text----------------------------------------------------------------
        admin.adding_single_image_and_text_right(self, h.Single_Image_Text_Right_Iframe_Path, h.Image_Path, h.Image_Selecter_Button_Path, h.Single_Image_Text_Right_Done_Button_Path)
        admin.adding_single_image_and_text_left(self, h.Single_Image_Text_Left_Select_Button_Path, h.Single_Image_Text_Left_Iframe_Path, h.Image_Path, h.Image_Selecter_Button_Path, h.Single_Image_Text_Left_Done_Button_Path)
        admin.adding_single_image_and_text_center(self, h.Single_Image_Text_Center_Select_Button_Path, h.Single_Image_Text_Center_Iframe_Path, h.Image_Path, h.Image_Selecter_Button_Path, h.Single_Image_Text_Center_Done_Button_Path)
        iframe_paths = [h.Two_Image_Text_Iframe_Path_left,h.Two_Image_Text_Iframe_Path_right]
        admin.adding_two_image_and_text(self, h.Two_Image_Text_Select_Button_Path, iframe_paths, h.Image_Path, h.Image_Selecter_Button_Path, h.Two_Image_Text_Done_Button_Path)
        iframe_paths = [h.Three_Image_Text_Iframe_Path_left,h.Three_Image_Text_Iframe_Path_middle,h.Three_Image_Text_Iframe_Path_right]
        admin.adding_three_image_and_text(self, h.Three_Image_Text_Select_Button_Path, iframe_paths, h.Image_Path, h.Image_Selecter_Button_Path, h.Three_Image_Text_Done_Button_Path)
        iframe_paths = [h.Two_Image_text_and_Full_Image_Right_Iframe_Path_left,h.Two_Image_text_and_Full_Image_Right_Iframe_Path_middle]
        admin.adding_two_image_text_and_full_image_right(self, h.Two_Image_text_and_Full_Image_Right_Button_Path, iframe_paths, h.Image_Path, h.Image_Selecter_Button_Path, h.Two_Image_text_and_Full_Image_Right_Done_Button_Path)
        iframe_paths = [h.Two_Image_text_and_Full_Image_middle_Iframe_Path_left,h.Two_Image_text_and_Full_Image_middle_Iframe_Path_right]
        admin.adding_two_image_text_and_full_image_middle(self, h.Two_Image_text_and_Full_Image_middle_Button_Path, iframe_paths, h.Image_Path, h.Image_Selecter_Button_Path, h.Two_Image_text_and_Full_Image_middle_Done_Button_Path)
        admin.adding_single_image_text_and_full_image_right(self, h.Single_Image_Text_and_Full_Image_Right_Button_Path, h.Single_Image_Text_and_Full_Image_Right_Iframe_Path, h.Image_Path, h.Image_Selecter_Button_Path, h.Single_Image_Text_and_Full_Image_Right_Done_Button_Path)
        admin.adding_single_image_text_and_full_image_left(self, h.Single_Image_Text_and_Full_Image_left_Button_Path, h.Single_Image_Text_and_Full_Image_left_Iframe_Path, h.Image_Path, h.Image_Selecter_Button_Path, h.Single_Image_Text_and_Full_Image_left_Done_Button_Path)
        admin.adding_full_image_and_three_image_right(self, h.Full_Image_and_Three_Image_Right, h.Image_Path, h.Image_Selecter_Button_Path, h.Full_Image_and_Three_Image_Right_Done_Button_Path)
        # ----------------------------------------Adding Image Slider----------------------------------------------------------------
        admin.adding_image_slider(self, h.Image_Slider_Add_Image_Path, h.Image_Path, h.Image_Selecter_Button_Path)
        
        # ----------------------------------------Adding Book Button----------------------------------------------------------------
        admin.adding_book_now_button(self, home_page_info_data.Booking_Button_Name, h.Book_Button_Label_Path, h.Book_Button_Done_Path)
        admin.adding_other_button(self, home_page_info_data.Booking_Button_Name, home_page_info_data.Booking_Button_Link, h.Other_Button_Link_Path, h.Other_Button_Label_Path, h.Other_Button_Done_Path)

        
        #------------------Adding Predefined Content-------------------------------
        admin.adding_predefined_content(self, h.Predefined_Content_Path)
        
        #------------------Adding SEO Content-------------------------------
        d.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        d.find_element_by_id("meta-title").clear()
        d.find_element_by_id("meta-title").send_keys(home_page_info_data.Page_Title)
        d.find_element_by_id("meta-description").clear()
        d.find_element_by_id("meta-description").send_keys(home_page_info_data.Page_Description)
        d.find_element_by_id("meta-keywords").clear()
        d.find_element_by_id("meta-keywords").send_keys(home_page_info_data.Page_Keywords)
        
        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
