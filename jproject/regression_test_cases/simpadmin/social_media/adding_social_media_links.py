# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("social_media"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("social_media"), '..', "simpadmin_common_function"))
import create_data_common_function


class Addingsocialmedialink(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def adding_social_media_link(self, social_media):
        find_id = self.driver.find_element_by_id
        find_id("social-1").clear()
        find_id("social-1").send_keys(social_media["trip_advisor_link"])
        find_id("social-2").clear()
        find_id("social-2").send_keys(social_media["facebook_link"])
        find_id("social-3").clear()
        find_id("social-3").send_keys(social_media["google_plus_link"])
        find_id("social-4").clear()
        find_id("social-4").send_keys(social_media["twitter_link"])
        find_id("social-5").clear()
        find_id("social-5").send_keys(social_media["linkedIn_link"])
        find_id("social-6").clear()
        find_id("social-6").send_keys(social_media["pinterest_link"])
        find_id("social-7").clear()
        find_id("social-7").send_keys(social_media["instagram_link"])
        find_id("social-8").clear()
        find_id("social-8").send_keys(social_media["youtube_link"])

    def test_addingsocialmedialink(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        self.driver.implicitly_wait(30)
        d.find_element_by_link_text("Social Media").click()
        self.driver.implicitly_wait(30)
        #-------------Adding Social Media Link ------------------------------------------
        self.adding_social_media_link(hotel_info.social_media)
        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(5)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
