# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("social_media"), '../..', "config"))
import hotel_info
import social_media_info_data


class Testingsocialmedialink(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_testingsocialmedialink(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.maximize_window()

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[1]").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[1]").get_attribute("class"),"social-ta-svg")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[1]").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[1]").get_attribute("href"),social_media_info_data.trip_advisor_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[2]/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[2]/span").get_attribute("class"),"fa fa-facebook")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[2]").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[2]").get_attribute("href"),social_media_info_data.facebook_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[3]/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[3]/span").get_attribute("class"),"fa fa-google-plus")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[3]").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[3]").get_attribute("href"),social_media_info_data.google_plus_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[4]/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[4]/span").get_attribute("class"),"fa fa-twitter")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[4]").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[4]").get_attribute("href"),social_media_info_data.twitter_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[5]/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[5]/span").get_attribute("class"),"fa fa-linkedin")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[5]").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[5]").get_attribute("href"),social_media_info_data.linkedIn_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[6]/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[6]/span").get_attribute("class"),"fa fa-pinterest")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[6]").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[6]").get_attribute("href"),social_media_info_data.pinterest_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[7]/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[7]/span").get_attribute("class"),"fa fa-instagram")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[7]").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[7]").get_attribute("href"),social_media_info_data.instagram_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[8]/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[8]/span").get_attribute("class"),"fa fa-youtube")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[8]").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[3]/div/div/div/a[8]").get_attribute("href"),social_media_info_data.youtube_link)

        driver.get(self.base_url + "/m")
        driver.maximize_window()
        time.sleep(10)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[1]/a").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[1]/a").get_attribute("class"),"social-ta-svg")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[1]/a").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[1]/a").get_attribute("href"),social_media_info_data.trip_advisor_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[2]/a/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[2]/a/span").get_attribute("class"),"fa fa-facebook")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[2]/a").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[2]/a").get_attribute("href"),social_media_info_data.facebook_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[3]/a/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[3]/a/span").get_attribute("class"),"fa fa-google-plus")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[3]/a").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[3]/a").get_attribute("href"),social_media_info_data.google_plus_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[4]/a/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[4]/a/span").get_attribute("class"),"fa fa-twitter")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[4]/a").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[4]/a").get_attribute("href"),social_media_info_data.twitter_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[5]/a/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[5]/a/span").get_attribute("class"),"fa fa-linkedin")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[5]/a").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[5]/a").get_attribute("href"),social_media_info_data.linkedIn_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[6]/a/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[6]/a/span").get_attribute("class"),"fa fa-pinterest")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[6]/a").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[6]/a").get_attribute("href"),social_media_info_data.pinterest_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[7]/a/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[7]/a/span").get_attribute("class"),"fa fa-instagram")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[7]/a").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[7]/a").get_attribute("href"),social_media_info_data.instagram_link)

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[8]/a/span").get_attribute("class")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[8]/a/span").get_attribute("class"),"fa fa-youtube")

        print driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[8]/a").get_attribute("href")
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='footer']/div/div[4]/div/ul/li[8]/a").get_attribute("href"),social_media_info_data.youtube_link)

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
