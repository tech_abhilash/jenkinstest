# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("social_media"), '../..', "config"))
import hotel_info
import social_media_info_data  

class Addingsocialmedialink(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_addingsocialmedialink(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys(hotel_info.chain_user_name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(hotel_info.chain_password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        driver.find_element_by_link_text(hotel_info.chain_child_hotel_name_one).click()
        time.sleep(2)
        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Hotel Info").click()
        self.driver.implicitly_wait(30)
        driver.find_element_by_link_text("Social Media").click()
        self.driver.implicitly_wait(30)
        #-------------Creating New Room------------------------------------------
        driver.find_element_by_xpath(".//*[@id='social-1']").clear()
        driver.find_element_by_xpath(".//*[@id='social-1']").send_keys(social_media_info_data.trip_advisor_link)
        driver.find_element_by_xpath(".//*[@id='social-2']").clear()
        driver.find_element_by_xpath(".//*[@id='social-2']").send_keys(social_media_info_data.facebook_link)
        driver.find_element_by_xpath(".//*[@id='social-3']").clear()
        driver.find_element_by_xpath(".//*[@id='social-3']").send_keys(social_media_info_data.google_plus_link)
        driver.find_element_by_xpath(".//*[@id='social-4']").clear()
        driver.find_element_by_xpath(".//*[@id='social-4']").send_keys(social_media_info_data.twitter_link)
        driver.find_element_by_xpath(".//*[@id='social-5']").clear()
        driver.find_element_by_xpath(".//*[@id='social-5']").send_keys(social_media_info_data.linkedIn_link)
        driver.find_element_by_xpath(".//*[@id='social-6']").clear()
        driver.find_element_by_xpath(".//*[@id='social-6']").send_keys(social_media_info_data.pinterest_link)
        driver.find_element_by_xpath(".//*[@id='social-7']").clear()
        driver.find_element_by_xpath(".//*[@id='social-7']").send_keys(social_media_info_data.instagram_link)
        driver.find_element_by_xpath(".//*[@id='social-8']").clear()
        driver.find_element_by_xpath(".//*[@id='social-8']").send_keys(social_media_info_data.youtube_link)
        driver.find_element_by_xpath(".//*[@id='hotel-social-info']/div/div/div[2]/button").click()
        time.sleep(10)
        
        
        #-------------Save and Generation------------------------------
        # driver.find_element_by_xpath("//button[@type='submit']").click()
        # time.sleep(20)
        driver.find_element_by_link_text("Preview").click()
        driver.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
