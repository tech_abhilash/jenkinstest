# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("social_media"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("social_media"), '..', "simpadmin_common_function"))
import create_data_common_function

class Testingsocialmedialink(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def  checking_social_media_link(self, social_media):
        find_id = self.driver.find_element_by_id
        print find_id("social-1").get_attribute("value")
        self.assertEqual(find_id("social-1").get_attribute("value"),social_media['trip_advisor_link'])

        print find_id("social-2").get_attribute("value")
        self.assertEqual(find_id("social-2").get_attribute("value"),social_media['facebook_link'])

        print find_id("social-3").get_attribute("value")
        self.assertEqual(find_id("social-3").get_attribute("value"),social_media['google_plus_link'])

        print find_id("social-4").get_attribute("value")
        self.assertEqual(find_id("social-4").get_attribute("value"),social_media['twitter_link'])

        print find_id("social-5").get_attribute("value")
        self.assertEqual(find_id("social-5").get_attribute("value"),social_media['linkedIn_link'])

        print find_id("social-6").get_attribute("value")
        self.assertEqual(find_id("social-6").get_attribute("value"),social_media['pinterest_link'])

        print find_id("social-7").get_attribute("value")
        self.assertEqual(find_id("social-7").get_attribute("value"),social_media['instagram_link'])

        print find_id("social-8").get_attribute("value")
        self.assertEqual(find_id("social-8").get_attribute("value"),social_media['youtube_link'])

    def test_testingsocialmedialink(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        self.driver.implicitly_wait(30)
        driver.find_element_by_link_text("Social Media").click()
        self.driver.implicitly_wait(30)

        #---------- Testing Social Media -------------------
        self.checking_social_media_link(hotel_info.social_media)

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
