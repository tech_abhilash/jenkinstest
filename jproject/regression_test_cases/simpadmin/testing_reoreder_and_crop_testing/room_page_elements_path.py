After_Save_home_page_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/"
after_save_room_page_image_path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/"   
after_save_gallery_page_image_path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[1]/div/div/div/div/ul/"
image_remove_path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/"
gallery_page_image_remove_path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[1]/div/div/div/div/ul/"
Preview_Hero_Image_Path = "/html/body/div[1]/div[2]/div[1]/div[2]/div/"
room_page_preview_image_path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[2]/div/div/div[1]/div[2]/div/"
gallery_page_preview_image_path = "/html/body/div/div[1]/div/div/div[1]/div/div/div/div/div[1]/div[2]/"
home_page_image_edit_path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/"
room_page_image_edit_path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/"
gallery_page_image_edit_path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[1]/div/div/div/div/ul/"
Preview_Nav_Group_Name_Path = "/html/body/div[1]/div[1]/div/div/div[2]/div/div/ul/li[2]/a"
Add_Hero_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/a"
Image_Path = "/html/body/div[7]/div[2]/div/div[1]/div/div/div[2]/div[2]/ul/"
Image_Selecter_Button_Path = "/html/body/div[7]/div[2]/div/div[2]/div[2]/button[2]"
Image_Cancel_Button_Path = "/html/body/div[7]/div[2]/div/div[2]/div[2]/button[1]"
                         
					
Add_Content_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/ul/li/a/span[1]"

#------------Path For Page Headers------------------------------
#------------H1-------------------------------------------------
H1_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[1]/div[3]/div/input[8]"
H1_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[1]/div[3]/div/div/button"
#------------H2-------------------------------------------------
H2_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[2]/div[3]/div/input[8]"
H2_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[2]/div[1]/div/div[2]/div/div/button[2]"
H2_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[2]/div[3]/div/div/button"
#------------Short and Detailed Description Path---------------------------------------------------								
Short_Description_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div/div/div/div/label/div/div/div/iframe"
Detailed_Description_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div/div/div/div/label/div/div/div/iframe"

#----------------------Only Text---------------------------
Single_Text_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[3]/div[3]/div/div[1]/div/div/iframe"
Single_Text_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[3]/div[3]/div/div[2]/button"

Two_Text_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[4]/div[3]/div/div[1]/"
Two_Text_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[4]/div[1]/div/div[2]/div/div/button[2]"
Two_Text_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[4]/div[3]/div/div[2]/button"

Three_Text_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[5]/div[3]/div/div[1]/"
Three_Text_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[5]/div[1]/div/div[2]/div/div/button[3]"
Three_Text_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[5]/div[3]/div/div[2]/button"

#------------------Only Image--------------------------------------
Single_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[6]/div[3]/div/div[1]/div/label/div/"
Single_Image_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[6]/div[3]/div/div[2]/button"

Two_Image_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[7]/div[1]/div/div[2]/div[1]/div/button[2]"
Two_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[7]/div[3]/div/div[1]/"
Two_Image_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[7]/div[3]/div/div[2]/button"

Three_Image_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[8]/div[1]/div/div[2]/div[1]/div/button[3]"
Three_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[8]/div[3]/div/div[1]/"
Three_Image_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[8]/div[3]/div/div[2]/button"

#---------------------Image and Text------------------------------

Single_Image_Text_Right_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[9]/div[3]/div/div[1]/div[1]/div/div/div/iframe"
Single_Image_Text_Right_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[9]/div[3]/div/div[1]/div[2]/label/div/"
Single_Image_Text_Right_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[9]/div[3]/div/div[2]/button"
											
Single_Image_Text_Left_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[10]/div[1]/div/div[2]/div[1]/div/button[2]"
Single_Image_Text_Left_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[10]/div[3]/div/div[1]/div[2]/div/div/div/iframe"
Single_Image_Text_Left_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[10]/div[3]/div/div[1]/div[1]/label/div/"
Single_Image_Text_Left_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[10]/div[3]/div/div[2]/button"

Single_Image_Text_Center_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[11]/div[1]/div/div[2]/div[1]/div/button[3]"
Single_Image_Text_Center_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[11]/div[3]/div/div[1]/div[2]/div/div/div/iframe"
Single_Image_Text_Center_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[11]/div[3]/div/div[1]/div[1]/div/label/div/"
Single_Image_Text_Center_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[11]/div[3]/div/div[2]/button"

Two_Image_Text_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[12]/div[1]/div/div[2]/div[1]/div/button[4]"
Two_Image_Text_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[12]/div[3]/div/div[1]/div[2]/"
Two_Image_Text_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[12]/div[3]/div/div[1]/div[1]/"
Two_Image_Text_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[12]/div[3]/div/div[2]/button"

Three_Image_Text_Select_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[13]/div[1]/div/div[2]/div[1]/div/button[5]"
Three_Image_Text_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[13]/div[3]/div/div[1]/div[2]/"
Three_Image_Text_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[13]/div[3]/div/div[1]/div[1]/"
Three_Image_Text_Done_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[13]/div[3]/div/div[2]/button"

#-----------------Image Slider Path-------------------------------
Image_Slider_Add_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[14]/div[2]/div/div/div[2]/div/div"

#----------------------Book Button Path--------------------------                       
Book_Button_Label_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[15]/div[3]/div/input[8]"
Book_Button_Done_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[15]/div[3]/div/div[2]/button"
						
#---------------------Other Button Path--------------------------
Other_Button_Label_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[16]/div[3]/div/input[8]"
Other_Button_Link_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[16]/div[3]/div/input[9]"
Other_Button_Done_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[16]/div[3]/div/div[2]/button"

#-------------------Predefined Content Path--------------------------
Predefined_Content_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/ul/li/a/span[1]"
							
#-----------------Submit Button Path------------------------------
Submit_Button_Path = "/html/body/div[1]/aside[2]/form/div/div/div[2]/button"


#------------------After Save----------------------------------                    
After_Save_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/"   
Room_Name_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[1]/label/div/input"
After_Save_H1_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[1]/div[2]/div/div/h1"
After_Save_H2_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[2]/div[2]/div/div/h2"
Only_Text_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/"
Only_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/"
Image_Text_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/"
Image_Slider_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[14]/div[2]/div/div/div[1]/div/div/div/ul/"
Book_Button_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[15]/div[2]/div"
Other_Button_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div/div[16]/div[2]/div"

Weather_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[1]/div/div/div[3]/div/div/div/h4/b"
Local_Time_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[2]/div/div/div[3]/div/div/div/h4/b"
Facebook_Feed_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[3]/div/div/div/div[1]/div[3]/div/div/div/h4/b"
TripAdvisor_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[4]/div/div/div/div[1]/div[3]/div/div/div/h4/b"
Social_Connect_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[5]/div/div/div/div/div[3]/div/div/div/h4/b"
Home_Page_Promotions_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[6]/div/div/div[3]/div/div/div/h4/b"
Location_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[7]/div/div/div[3]/div/div/div/h4/b"
Contact_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[8]/div/div/div[3]/div/div/div/h4/b"
Newsletter_Signup_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[9]/div/div/div[3]/div/div/div/h4/b"
Promotions_Slider_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[10]/div/div/div[3]/div/div/div/h4/b"
Custom_Plugin_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div[11]/div/div/div/div[1]/div[3]/div/div/div/h4/b"

Page_Title_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[7]/div[2]/div/div[1]/label/input"
Page_Description_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[7]/div[2]/div/div[2]/label/textarea" 
Page_Keywords_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[7]/div[2]/div/div[3]/label/input"

#---------Preview Path-----------------------------------------
						
Preview_Heading_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[1]/div/h1/span"
Preview_Nav_Heading_Path = "/html/body/div/div[1]/div/div/div[2]/div[1]/div/ul/li/a"
					  
Preview_Image_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[18]/div/div/div[1]/div[2]/div/"
Preview_Detailed_Description_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[4]/div/p"
Preview_H1_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[5]/div/h1"
Preview_H2_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[6]/div/h2"
Preview_Single_Text_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[7]/div/p"
Preview_Two_Text = "/html/body/div/div[1]/div/div/div[1]/div/div/div[8]/"
Preview_Three_Text = "/html/body/div/div[1]/div/div/div[1]/div/div/div[9]/"
Preview_Only_Image_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/"
Preview_Image_Text_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/"
Preview_Image_Slider_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[18]/div/div/div[1]/div[2]/div/"
Preview_Book_Button_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[19]/div/div/form/input"
							
Preview_Other_button_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[20]/div/a"
								
Preview_Weather_Path = "/html/body/div/div[1]/div/div/div[2]/div[2]/p/span"
Preview_Local_Time_Path = "/html/body/div/div[1]/div/div/div[2]/div[3]/p/span"
Preview_TripAdvisor_Path = "/html/body/div/div[1]/div/div/div[2]/div[5]/div/div/div/div[1]/dl/dt/a/img"
Preview_Social_Connect_Path = "/html/body/div/div[1]/div/div/div[2]/div[6]/ul"
Preview_Location_Path = "/html/body/div/div[1]/div/div/div[2]/div[7]/p/span"
Preview_Contact_Us_Path = "/html/body/div/div[1]/div/div/div[2]/div[8]/p/span"
Preview_Newsletter_Signup_Path = "/html/body/div/div[1]/div/div/div[2]/div[9]/p/span"
Preview_Promotion_Slider_Path = "/html/body/div/div[1]/div/div/div[2]/div[10]/div/p/span"
Preview_Facebook_Path = "/html/body/div/div[1]/div/div/div[2]/div[4]/div/div/div/span/iframe"
