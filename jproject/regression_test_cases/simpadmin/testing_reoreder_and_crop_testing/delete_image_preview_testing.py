# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("testing_reoreder_and_crop_testing"), '../..', "config"))
import hotel_info
import info_data
import room_page_elements_path


class Testdeleteimage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_deleteimage(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.maximize_window()

        #----------------------------------------Getting Image Name----------------------------------
        with open("image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        Image_Name_4 = wordList[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4


        #---------------------------------------Tesing Hero Image---------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.Preview_Hero_Image_Path + "div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        wordList = driver.find_element_by_xpath(room_page_elements_path.Preview_Hero_Image_Path + "div[2]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        path = room_page_elements_path.Preview_Hero_Image_Path + "div[3]/img"
        x = self.check_exists_by_xpath(path)
        if (x == True):
            print "Home 3rd Image Present"
        else:        
            print "Home 3rd Image not Present"


        path = room_page_elements_path.Preview_Hero_Image_Path + "div[4]/img"
        x = self.check_exists_by_xpath(path)
        if (x == True):
            print "Home 4th Image Present"
        else:        
            print "Home 4th Image not Present"

        #---------------------Room page image test------------------------------------
        driver.get(self.base_url + "rooms/test-room.html")
        driver.maximize_window()


        #---------------------------------------Tesing Hero Image---------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.room_page_preview_image_path + "div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        wordList = driver.find_element_by_xpath(room_page_elements_path.room_page_preview_image_path + "div[2]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        path = room_page_elements_path.room_page_preview_image_path + "div[3]/img"
        x = self.check_exists_by_xpath(path)
        if (x == True):
            print "Room 3rd Image Present"
        else:        
            print "Room 3rd Image not Present"


        path = room_page_elements_path.room_page_preview_image_path + "div[4]/img"
        x = self.check_exists_by_xpath(path)
        if (x == True):
            print "Room 4th Image Present"
        else:        
            print "Room 4th Image not Present"

        #---------------------Gallery page image test------------------------------------
        driver.get(self.base_url + "gallery.html")
        driver.maximize_window()

        driver.find_element_by_xpath("/html/body/div/div[1]/div/div/div[2]/div/div/ul/li[2]/a").click()
        #---------------------------------------Tesing Hero Image---------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.gallery_page_preview_image_path + "div[2]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        wordList = driver.find_element_by_xpath(room_page_elements_path.gallery_page_preview_image_path + "div[3]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        path = room_page_elements_path.gallery_page_preview_image_path + "div[4]/div[1]/img"
        x = self.check_exists_by_xpath(path)
        if (x == True):
            print "Gallery 3rd Image Present"
        else:        
            print "Gallery 3rd Image not Present"


        path = room_page_elements_path.gallery_page_preview_image_path + "div[5]/div[1]/img"
        x = self.check_exists_by_xpath(path)
        if (x == True):
            print "Gallery 4th Image Present"
        else:        
            print "Gallery 4th Image not Present"

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
