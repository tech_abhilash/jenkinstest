from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Image Reorder Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("image_reorder_test")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Image Reorder Preview Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("image_reordering_preview_test")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Image Crop Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("image_crop_test")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Image Delete Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("image_delete_test")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Image Delete Preview Test"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("delete_image_preview_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)