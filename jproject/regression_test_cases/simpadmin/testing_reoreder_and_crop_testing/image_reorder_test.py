# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("testing_reoreder_and_crop_testing"), '../..', "config"))
import hotel_info
import room_page_elements_path

class Testimagereorder(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_imagereorder(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys(hotel_info.User_Name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(hotel_info.Password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Home Page").click()
        time.sleep(5)

        #-------------------------------Getting Image Name------------------------------------------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[1]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]
        print Image_Name_1

        wordList = driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[2]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]
        print Image_Name_2

        wordList = driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[3]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]
        print Image_Name_3

        wordList = driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[4]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]
        print Image_Name_4


        with open("image_names.txt",'w') as f:
            f.write(Image_Name_1+" "+Image_Name_2+" "+Image_Name_3+" "+Image_Name_4)
            f.close()
            
        #-------------------------Rerodering Image------------------------------------------------------------
        xoffset = 850
        yoffset = 328
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[1]/label/div/div/img")).perform()
        drag_source_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/li[1]/label/div/div/img")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/li[3]/label/div/div/img")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        time.sleep(5)
        actions.move_by_offset(xoffset, yoffset)
        time.sleep(5)
        actions.release(drag_target_path)
        actions.perform()
        time.sleep(5)
        xoffset = 1050
        yoffset = 328
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[1]/label/div/div/img")).perform()
        drag_source_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/li[2]/label/div/div/img")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/li[4]/label/div/div/img")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        time.sleep(5)
        actions.move_by_offset(xoffset, yoffset)
        time.sleep(5)
        actions.release(drag_target_path)
        actions.perform()
        time.sleep(5)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(15)
        
        #-------------------------------Testing Image Name------------------------------------------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[1]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_2)
        

        wordList = driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[2]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_3)

        wordList = driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[3]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_1)

        wordList = driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[4]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_4)

        #-------------------Room Page Image Reordering --------------------------------

        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Rooms").click()
        time.sleep(5)

        #-------------------------------Getting Room Image Name------------------------------------------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[1]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]
        print Image_Name_1

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[2]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]
        print Image_Name_2

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[3]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]
        print Image_Name_3

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[4]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]
        print Image_Name_4

        #-------------------------Rerodering Room Image------------------------------------------------------------
        xoffset = 850
        yoffset = 328
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[1]/label/div/div/img")).perform()
        drag_source_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/li[1]/label/div/div/img")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/li[3]/label/div/div/img")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        time.sleep(5)
        actions.move_by_offset(xoffset, yoffset)
        time.sleep(5)
        actions.release(drag_target_path)
        actions.perform()
        time.sleep(5)
        xoffset = 1050
        yoffset = 328
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[1]/label/div/div/img")).perform()
        drag_source_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/li[2]/label/div/div/img")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div/div/div/ul/li[4]/label/div/div/img")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        time.sleep(5)
        actions.move_by_offset(xoffset, yoffset)
        time.sleep(5)
        actions.release(drag_target_path)
        actions.perform()
        time.sleep(5)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(15)
        
        #-------------------------------Testing Room Image Name------------------------------------------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[1]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_2)

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[2]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_3)

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[3]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_1)

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[4]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_4)

        #-------------------Gallery Page Image Reordering --------------------------------

        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Photo Gallery").click()
        time.sleep(5)

        #-------------------------------Getting Gallery Image Name------------------------------------------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[1]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]
        print Image_Name_1

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[2]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]
        print Image_Name_2

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[3]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]
        print Image_Name_3

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[4]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]
        print Image_Name_4

        #-------------------------Rerodering Gallery Image------------------------------------------------------------
        xoffset = 850
        yoffset = 328
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[1]/label/div/div/img")).perform()
        drag_source_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[1]/div/div/div/div/ul/li[1]/label/div/div/img")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[1]/div/div/div/div/ul/li[3]/label/div/div/img")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        time.sleep(5)
        actions.move_by_offset(xoffset, yoffset)
        time.sleep(5)
        actions.release(drag_target_path)
        actions.perform()
        time.sleep(5)
        xoffset = 1050
        yoffset = 328
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[1]/label/div/div/img")).perform()
        drag_source_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[1]/div/div/div/div/ul/li[2]/label/div/div/img")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[1]/div/div/div/div/ul/li[4]/label/div/div/img")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        time.sleep(5)
        actions.move_by_offset(xoffset, yoffset)
        time.sleep(5)
        actions.release(drag_target_path)
        actions.perform()
        time.sleep(5)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)
        driver.find_element_by_link_text("Preview").click()
        driver.find_element_by_id("idGenerateSite").click()
        time.sleep(10)
        
        #-------------------------------Testing Gallery Image Name------------------------------------------------------
        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[1]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_2)

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[2]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_3)

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[3]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_1)

        wordList = driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[4]/label/div/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name,Image_Name_4)
        time.sleep(10)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
