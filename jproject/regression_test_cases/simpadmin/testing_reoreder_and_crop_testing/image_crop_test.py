# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("testing_reoreder_and_crop_testing"), '../..', "config"))
import hotel_info
# import info_data
import room_page_elements_path
# from PIL import Image

class Testcropimage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_cropimage(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys(hotel_info.User_Name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(hotel_info.Password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Home Page").click()
        
        
        #-------------------Testing Image ------------------------------------------
        element = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]")
        location = element.location
        size = element.size

        driver.save_screenshot('/home/akshata/Desktop/test_cases_admin_org/screec_shot/home_page_before_crop.png') 
        
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[1]/label/div/div/img")).perform()
        driver.find_element_by_xpath(room_page_elements_path.home_page_image_edit_path + "li[1]/label/div/a[2]/span").click()
        time.sleep(5)
        drag_source_path = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[10]")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[8]")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        actions.release(drag_target_path)
        actions.perform()
        driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[3]/div[2]/a").click()
        time.sleep(5)

        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.After_Save_home_page_Image_Path + "li[2]/label/div/div/img")).perform()
        driver.find_element_by_xpath(room_page_elements_path.home_page_image_edit_path + "li[2]/label/div/a[2]/span").click()
        time.sleep(5)
        drag_source_path = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[10]")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[8]")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        actions.release(drag_target_path)
        actions.perform()
        driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[3]/div[2]/a").click()
        time.sleep(5)


        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(5)

        driver.save_screenshot('/home/akshata/Desktop/test_cases_admin_org/screec_shot/home_page_after_crop.png')
        
        im = Image.open('/home/akshata/Desktop/test_cases_admin_org/screec_shot/home_page_before_crop.png')
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        im = im.crop((left, top, right, bottom))
        im.save('/home/akshata/Desktop/test_cases_admin_org/screec_shot/home_page_before_crop.png')

        im = Image.open('/home/akshata/Desktop/test_cases_admin_org/screec_shot/home_page_after_crop.png')
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        im = im.crop((left, top, right, bottom))
        im.save('/home/akshata/Desktop/test_cases_admin_org/screec_shot/home_page_after_crop.png')
        

       #----------------Room Page Croping testing------------------------------------------

        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Rooms").click()
        
        
        #-------------------Testing Image ------------------------------------------
        element = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]")
        location = element.location
        size = element.size

        driver.save_screenshot('/home/akshata/Desktop/test_cases_admin_org/screec_shot/room_page_before_crop.png') 
        
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[1]/label/div/div/img")).perform()
        driver.find_element_by_xpath(room_page_elements_path.room_page_image_edit_path + "li[1]/label/div/a[2]/span").click()
        time.sleep(5)
        drag_source_path = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[2]/div/div[1]/div/div[1]/div[2]/div[10]")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[2]/div/div[1]/div/div[1]/div[2]/div[8]")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        actions.release(drag_target_path)
        actions.perform()
        driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[3]/div[2]/a").click()
        time.sleep(5)

        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.after_save_room_page_image_path + "li[2]/label/div/div/img")).perform()
        driver.find_element_by_xpath(room_page_elements_path.room_page_image_edit_path + "li[2]/label/div/a[2]/span").click()
        time.sleep(5)
        drag_source_path = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[2]/div/div[1]/div/div[1]/div[2]/div[10]")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[2]/div/div[1]/div/div[1]/div[2]/div[8]")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        actions.release(drag_target_path)
        actions.perform()
        driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[3]/div[2]/a").click()
        time.sleep(5)


        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(5)

        driver.save_screenshot('/home/akshata/Desktop/test_cases_admin_org/screec_shot/room_page_after_crop.png')
        
        im = Image.open('/home/akshata/Desktop/test_cases_admin_org/screec_shot/room_page_before_crop.png')
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        im = im.crop((left, top, right, bottom))
        im.save('/home/akshata/Desktop/test_cases_admin_org/screec_shot/room_page_before_crop.png')

        im = Image.open('/home/akshata/Desktop/test_cases_admin_org/screec_shot/room_page_after_crop.png')
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        im = im.crop((left, top, right, bottom))
        im.save('/home/akshata/Desktop/test_cases_admin_org/screec_shot/room_page_after_crop.png')
        
        
        #----------------Gallery Page Croping testing------------------------------------------

        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Photo Gallery").click()
        
        
        #-------------------Testing Image ------------------------------------------
        element = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]")
        location = element.location
        size = element.size

        driver.save_screenshot('/home/akshata/Desktop/test_cases_admin_org/screec_shot/gallery_page_before_crop.png') 
        
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[1]/label/div/div/img")).perform()
        driver.find_element_by_xpath(room_page_elements_path.gallery_page_image_edit_path + "li[1]/label/div/a[2]/span").click()
        time.sleep(5)
        drag_source_path = driver.find_element_by_xpath("/html/body/div[3]/div[2]/div/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[10]")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[3]/div[2]/div/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[8]")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        actions.release(drag_target_path)
        actions.perform()
        driver.find_element_by_xpath("/html/body/div[3]/div[2]/div/div[3]/div[2]/a").click()
        time.sleep(5)

        ActionChains(driver).move_to_element(driver.find_element_by_xpath(room_page_elements_path.after_save_gallery_page_image_path + "li[2]/label/div/div/img")).perform()
        driver.find_element_by_xpath(room_page_elements_path.gallery_page_image_edit_path + "li[2]/label/div/a[2]/span").click()
        time.sleep(5)
        drag_source_path = driver.find_element_by_xpath("/html/body/div[3]/div[2]/div/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[10]")
        drag_target_path = driver.find_element_by_xpath("/html/body/div[3]/div[2]/div/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[8]")
        actions = ActionChains(driver)
        actions.click_and_hold(drag_source_path)
        actions.release(drag_target_path)
        actions.perform()
        driver.find_element_by_xpath("/html/body/div[3]/div[2]/div/div[3]/div[2]/a").click()
        time.sleep(5)


        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(5)

        driver.save_screenshot('/home/akshata/Desktop/test_cases_admin_org/screec_shot/gallery_page_after_crop.png')
        
        im = Image.open('/home/akshata/Desktop/test_cases_admin_org/screec_shot/gallery_page_before_crop.png')
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        im = im.crop((left, top, right, bottom))
        im.save('/home/akshata/Desktop/test_cases_admin_org/screec_shot/gallery_page_before_crop.png')

        im = Image.open('/home/akshata/Desktop/test_cases_admin_org/screec_shot/gallery_page_after_crop.png')
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        im = im.crop((left, top, right, bottom))
        im.save('/home/akshata/Desktop/test_cases_admin_org/screec_shot/gallery_page_after_crop.png')
        time.sleep(10)
        

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
