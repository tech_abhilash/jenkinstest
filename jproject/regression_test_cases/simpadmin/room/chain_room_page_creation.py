    # -*- coding: utf-8 -*-
# from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("room"), '../..', "config"))
import hotel_info
import room_page_info_data
import room_page_elements_path
sys.path.append(os.path.join(os.path.dirname("room"), '..', "simpadmin_common_function"))
import create_data_common_function

class CreatingChainRoomPage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_room_page(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        r = room_page_elements_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.chain_user_name, hotel_info.chain_password)

        d.find_element_by_xpath("(//button[@type='button'])[2]").click()
        d.find_element_by_link_text(hotel_info.chain_child_hotel_name_one).click()
        time.sleep(2)
        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text(" Rooms").click()

        #-------------Creating New Room------------------------------------------

        d.find_element_by_link_text("Add New Room").click()
        d.implicitly_wait(10)
        d.find_element_by_id("pagename").send_keys(room_page_info_data.Room_Name)
        d.find_element_by_xpath(r.Submit_Button_Path).click()
        time.sleep(10)

        #-------------Adding Hero Images-----------------------------------------

        d.find_element_by_xpath(r.Add_Hero_Image_Path).click()
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        # time.sleep(3)
        d.find_element_by_xpath(r.Image_Path + "li[1]/div").click()
        d.find_element_by_xpath(r.Image_Path + "li[2]/div").click()
        d.find_element_by_xpath(r.Image_Path + "li[3]/div").click()
        d.find_element_by_xpath(r.Image_Path + "li[4]/div").click()
        time.sleep(3)
        #---------------Saving Image Name-----------------
        wordList = d.find_element_by_xpath(r.Image_Path + "li[1]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]

        wordList = d.find_element_by_xpath(r.Image_Path + "li[2]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]

        wordList = d.find_element_by_xpath(r.Image_Path + "li[3]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]

        wordList = d.find_element_by_xpath(r.Image_Path + "li[4]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]

        #----------------Saving Image Name----------------------------------
        with open("image_names.txt",'w') as f:
            f.write(Image_Name_1+" "+Image_Name_2+" "+Image_Name_3+" "+Image_Name_4)
            f.close()

        
        time.sleep(3)
        d.find_element_by_id("doneBtn").click()
        time.sleep(5)
        d.find_element_by_xpath(r.Submit_Button_Path).click()
        d.implicitly_wait(10)
        d.refresh()
        d.implicitly_wait(15)
        #------------Adding Text to Short and Detailed Description of the Room-----------------
        
        d.switch_to_frame(d.find_element_by_xpath(r.Short_Description_Iframe_Path))
        d.find_element_by_xpath("/html/body").send_keys(room_page_info_data.Room_Short_Description)
        d.switch_to_default_content()


        d.switch_to_frame(d.find_element_by_xpath(r.Detailed_Description_Iframe_Path))
        d.find_element_by_xpath("/html/body").send_keys(room_page_info_data.Room_Detailed_Description)
        d.switch_to_default_content()

        #  ------------Adding User Content-----------------------
        # ------------Adding H1 and H2-------------------------------
        admin.adding_h1(self, r.H1_Path, r.H1_Done_Button_Path)
        admin.adding_h2(self, r.H2_Select_Button_Path, r.H2_Path, r.H2_Done_Button_Path)

        #-----------------------Only Text -------------------
        admin.adding_single_text(self, r.Single_Text_Iframe_Path, r.Single_Text_Done_Button_Path)
        admin.adding_two_text(self, r.Two_Text_Select_Button_Path, r.Two_Text_Iframe_Path_left, r.Two_Text_Iframe_Path_right, r.Two_Text_Done_Button_Path)
        admin.adding_three_text(self, r.Three_Text_Select_Button_Path, r.Three_Text_Iframe_Path_left, r.Three_Text_Iframe_Path_middle, r.Three_Text_Iframe_Path_right, r.Three_Text_Done_Button_Path)

        #----------------------------------------Adding only Image ----------------------------------------------------------------
        admin.adding_single_image(self, r.Image_Path, r.Image_Selecter_Button_Path, r.Single_Image_Done_Button_Path)
        admin.adding_two_image(self, r.Two_Image_Select_Button_Path, r.Image_Path, r.Image_Selecter_Button_Path, r.Two_Image_Done_Button_Path)
        admin.adding_three_image(self, r.Three_Image_Select_Button_Path, r.Image_Path, r.Image_Selecter_Button_Path, r.Three_Image_Done_Button_Path)

        #----------------------------------------Adding Image and Text----------------------------------------------------------------
        admin.adding_single_image_and_text_right(self, r.Single_Image_Text_Right_Iframe_Path, r.Image_Path, r.Image_Selecter_Button_Path, r.Single_Image_Text_Right_Done_Button_Path)
        admin.adding_single_image_and_text_left(self, r.Single_Image_Text_Left_Select_Button_Path, r.Single_Image_Text_Left_Iframe_Path, r.Image_Path, r.Image_Selecter_Button_Path, r.Single_Image_Text_Left_Done_Button_Path)
        admin.adding_single_image_and_text_center(self, r.Single_Image_Text_Center_Select_Button_Path, r.Single_Image_Text_Center_Iframe_Path, r.Image_Path, r.Image_Selecter_Button_Path, r.Single_Image_Text_Center_Done_Button_Path)
        iframe_paths = [r.Two_Image_Text_Iframe_Path_left,r.Two_Image_Text_Iframe_Path_right]
        admin.adding_two_image_and_text(self, r.Two_Image_Text_Select_Button_Path, iframe_paths, r.Image_Path, r.Image_Selecter_Button_Path, r.Two_Image_Text_Done_Button_Path)
        iframe_paths = [r.Three_Image_Text_Iframe_Path_left,r.Three_Image_Text_Iframe_Path_middle,r.Three_Image_Text_Iframe_Path_right]
        admin.adding_three_image_and_text(self, r.Three_Image_Text_Select_Button_Path, iframe_paths, r.Image_Path, r.Image_Selecter_Button_Path, r.Three_Image_Text_Done_Button_Path)
        iframe_paths = [r.Two_Image_text_and_Full_Image_Right_Iframe_Path_left,r.Two_Image_text_and_Full_Image_Right_Iframe_Path_middle]
        admin.adding_two_image_text_and_full_image_right(self, r.Two_Image_text_and_Full_Image_Right_Button_Path, iframe_paths, r.Image_Path, r.Image_Selecter_Button_Path, r.Two_Image_text_and_Full_Image_Right_Done_Button_Path)
        iframe_paths = [r.Two_Image_text_and_Full_Image_middle_Iframe_Path_left,r.Two_Image_text_and_Full_Image_middle_Iframe_Path_right]
        admin.adding_two_image_text_and_full_image_middle(self, r.Two_Image_text_and_Full_Image_middle_Button_Path, iframe_paths, r.Image_Path, r.Image_Selecter_Button_Path, r.Two_Image_text_and_Full_Image_middle_Done_Button_Path)
        admin.adding_single_image_text_and_full_image_right(self, r.Single_Image_Text_and_Full_Image_Right_Button_Path, r.Single_Image_Text_and_Full_Image_Right_Iframe_Path, r.Image_Path, r.Image_Selecter_Button_Path, r.Single_Image_Text_and_Full_Image_Right_Done_Button_Path)
        admin.adding_single_image_text_and_full_image_left(self, r.Single_Image_Text_and_Full_Image_left_Button_Path, r.Single_Image_Text_and_Full_Image_left_Iframe_Path, r.Image_Path, r.Image_Selecter_Button_Path, r.Single_Image_Text_and_Full_Image_left_Done_Button_Path)
        admin.adding_full_image_and_three_image_right(self, r.Full_Image_and_Three_Image_Right, r.Image_Path, r.Image_Selecter_Button_Path, r.Full_Image_and_Three_Image_Right_Done_Button_Path)
        # ----------------------------------------Adding Image Slider----------------------------------------------------------------
        admin.adding_image_slider(self, r.Image_Slider_Add_Image_Path, r.Image_Path, r.Image_Selecter_Button_Path)
        
        # ----------------------------------------Adding Book Button----------------------------------------------------------------
        admin.adding_book_now_button(self, room_page_info_data.Booking_Button_Name, r.Book_Button_Label_Path, r.Book_Button_Done_Path)
        admin.adding_other_button(self, room_page_info_data.Booking_Button_Name, room_page_info_data.Booking_Button_Link, r.Other_Button_Link_Path, r.Other_Button_Label_Path, r.Other_Button_Done_Path)

        
        #------------------Adding Predefined Content-------------------------------
        admin.adding_predefined_content(self, r.Predefined_Content_Path)


        #------------------Adding SEO Content-------------------------------
        d.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        d.find_element_by_id("meta-title").clear()
        d.find_element_by_id("meta-title").send_keys(room_page_info_data.Page_Title)
        d.find_element_by_id("meta-description").clear()
        d.find_element_by_id("meta-description").send_keys(room_page_info_data.Page_Description)
        d.find_element_by_id("meta-keywords").clear()
        d.find_element_by_id("meta-keywords").send_keys(room_page_info_data.Page_Keywords)

        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)
        
        #-------------------Room Information & Amenities-------------------------------
        element = d.find_element_by_link_text("Room Information & Amenities")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Room Information & Amenities").click()
        d.find_element_by_id("roomnumbers-1").clear()
        d.find_element_by_id("roomnumbers-1").send_keys("5")
        d.find_element_by_id("websitenumbers-1").clear()
        d.find_element_by_id("websitenumbers-1").send_keys("3")
        d.find_element_by_id("size-1").clear()
        d.find_element_by_id("size-1").send_keys("150")
        d.find_element_by_id("maxAdults-1").clear()
        d.find_element_by_id("maxAdults-1").send_keys("3")
        d.find_element_by_id("maxChild-1").clear()
        d.find_element_by_id("maxChild-1").send_keys("3")
        d.find_element_by_id("maxGuests-1").clear()
        d.find_element_by_id("maxGuests-1").send_keys("4")

        
        
        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        d.implicitly_wait(20)
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
        # driver.close()
        # self.display.stop()
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
