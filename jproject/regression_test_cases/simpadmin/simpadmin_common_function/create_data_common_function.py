# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
import traceback
import sys
import os

def login_to_admin(self, username = "", password = ""):
    try:
        d = self.driver
        d.find_element_by_name("userid").clear()
        d.find_element_by_name("userid").send_keys(username)
        d.find_element_by_name("password").clear()
        d.find_element_by_name("password").send_keys(password)
        d.find_element_by_xpath("//button[@type='submit']").click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Login")

def add_hotel_name(self, hotel_name):
    d = self.driver
    d.find_element_by_link_text("Create New Hotel").click()
    d.find_element_by_id("hotel-name").clear()
    d.find_element_by_id("hotel-name").send_keys(hotel_name)


def add_hote_initial_data(self, hote_initial_data):
    d = self.driver
    d.find_element_by_id("hotel-tag-line").clear()
    d.find_element_by_id("hotel-tag-line").send_keys(hote_initial_data["hotel_tag_line"])
    d.find_element_by_name("hotel.description").clear()
    d.find_element_by_name("hotel.description").send_keys(hote_initial_data["hotel_description"])
    d.find_element_by_id("pac-input").clear()
    d.find_element_by_id("pac-input").send_keys(hote_initial_data["location_name"])
    d.find_element_by_css_selector("span.pac-matched").click()
    d.find_element_by_id("hotel-url").clear()
    d.find_element_by_id("hotel-url").send_keys(hote_initial_data["hotel_url"])
    d.find_element_by_id("contact-email-").clear()
    d.find_element_by_id("contact-email-").send_keys(hote_initial_data["contact_email"])
    d.find_element_by_id("add-email").click()
    d.find_element_by_id("contact-email-2").clear()
    d.find_element_by_id("contact-email-2").send_keys(hote_initial_data["contact_email_2"])
    d.find_element_by_id("contact-phone-").clear()
    d.find_element_by_id("contact-phone-").send_keys(hote_initial_data["contact_phone"])
    d.find_element_by_id("contact-phone-attribute-").clear()
    d.find_element_by_id("contact-phone-attribute-").send_keys(hote_initial_data["contact_phone_label"])
    d.find_element_by_id("add-phone").click()
    d.find_element_by_id("contact-phone-2").clear()
    d.find_element_by_id("contact-phone-2").send_keys(hote_initial_data["contact_phone_2"])
    d.find_element_by_id("contact-phone-attribute-2").clear()
    d.find_element_by_id("contact-phone-attribute-2").send_keys(hote_initial_data["contact_phone_label_2"])

def add_hotel_time_zone(self, time_zone):
    d = self.driver
    d.find_element_by_xpath(".//*[@id='s2id_timezone']/a/span[2]/b").click()
    d.find_element_by_id("s2id_autogen1_search").clear()
    d.find_element_by_id("s2id_autogen1_search").send_keys(time_zone)
    time.sleep(2)
    i = 1
    while 1:                                
        print d.find_element_by_xpath("/html/body/div[6]/ul/li["+str(i)+"]/div/span").text
        if (d.find_element_by_xpath("/html/body/div[6]/ul/li["+str(i)+"]/div/span").text == time_zone):
            d.find_element_by_xpath("/html/body/div[6]/ul/li["+str(i)+"]/div/span").click()
            break
        i = i + 1

def add_hotel_currency(self, currency):
    d = self.driver
    d.find_element_by_xpath(".//*[@id='s2id_currency']/a/span[2]/b").click()
    d.find_element_by_id("s2id_autogen2_search").clear()
    d.find_element_by_id("s2id_autogen2_search").send_keys(currency)
    time.sleep(2)
    i = 1
    while 1:                               
        print d.find_element_by_xpath("/html/body/div[7]/ul/li["+str(i)+"]/div/span").text
        if (d.find_element_by_xpath("/html/body/div[7]/ul/li["+str(i)+"]/div/span").text == currency):
            d.find_element_by_xpath("/html/body/div[7]/ul/li["+str(i)+"]/div/span").click()
            break
        i = i + 1

def add_hotel_city(self, city):
    d = self.driver
    d.find_element_by_id("address-city").clear()
    d.find_element_by_id("address-city").send_keys(city)

def add_hotel_language(self, language):
    d = self.driver
    d.find_element_by_xpath(".//*[@id='s2id_language']/a/span[2]/b").click()
    d.find_element_by_id("s2id_autogen3_search").clear()
    d.find_element_by_id("s2id_autogen3_search").send_keys(language)
    time.sleep(2)
    i = 1
    while 1:                                
        print d.find_element_by_xpath("/html/body/div[8]/ul/li["+str(i)+"]/div/span").text
        if (d.find_element_by_xpath("/html/body/div[8]/ul/li["+str(i)+"]/div/span").text == language):
            d.find_element_by_xpath("/html/body/div[8]/ul/li["+str(i)+"]/div/span").click()
            break
        i = i + 1

def add_hotel_country(self, country):
    d = self.driver
    d.find_element_by_xpath(".//*[@id='s2id_address-country']/a/span[2]/b").click()
    d.find_element_by_id("s2id_autogen4_search").clear()
    d.find_element_by_id("s2id_autogen4_search").send_keys(country)
    time.sleep(2)
    i = 1
    while 1:                                
        print d.find_element_by_xpath("/html/body/div[9]/ul/li["+str(i)+"]/div/span").text
        if (d.find_element_by_xpath("/html/body/div[9]/ul/li["+str(i)+"]/div/span").text == country):
            d.find_element_by_xpath("/html/body/div[9]/ul/li["+str(i)+"]/div/span").click()
            break
        i = i + 1

def adding_h1(self, input_box_path, done_button_path):
    try:
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Headers").click()
        d.find_element_by_xpath(input_box_path).clear()
        d.find_element_by_xpath(input_box_path).send_keys("Page H1")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding H1")

def adding_h2(self, option_select_path, input_box_path, done_button_path):
    try:
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Headers").click()
        d.find_element_by_xpath(option_select_path).click()
        d.find_element_by_xpath(input_box_path).clear()
        d.find_element_by_xpath(input_box_path).send_keys("Page H2")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding H2")

def adding_single_text(self, iframe_path, done_button_path):
    try:
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Text").click()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path))
        d.find_element_by_xpath("/html/body").send_keys("Single Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding single Text")

def adding_two_text(self, option_select_path, iframe_path_left, iframe_path_right, done_button_path):
    try:
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Text").click()
        d.find_element_by_xpath(option_select_path).click()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path_left))
        d.find_element_by_xpath("/html/body").send_keys("Two Text")
        d.switch_to_default_content()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path_right))
        d.find_element_by_xpath("/html/body").send_keys("Two Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding Two Text")

def adding_three_text(self, option_select_path, iframe_path_left, iframe_path_middle, iframe_path_right, done_button_path):
    try:
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Text").click()
        d.find_element_by_xpath(option_select_path).click()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path_left))
        d.find_element_by_xpath("/html/body").send_keys("Three Text")
        d.switch_to_default_content()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path_middle))
        d.find_element_by_xpath("/html/body").send_keys("Three Text")
        d.switch_to_default_content()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path_right))
        d.find_element_by_xpath("/html/body").send_keys("Three Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding Three Text")

def adding_single_image(self, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images").click()
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-5-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-5-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding Single Image")

def adding_two_image(self, option_select_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-6-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-6-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-6-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-6-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding Two Image")

def adding_three_image(self, option_select_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-7-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-7-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[3]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-7-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-7-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-7-2']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-7-2']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding Three Image")

def adding_single_image_and_text_right(self, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-8-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-8-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path))
        d.find_element_by_xpath("html/body").send_keys("Single Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding single image and text right")

def adding_single_image_and_text_left(self, option_select_path, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-9-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-9-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path))
        d.find_element_by_xpath("html/body").send_keys("Single Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding single image and text left")

def adding_single_image_and_text_center(self, option_select_path, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-10-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-10-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[3]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path))
        d.find_element_by_xpath("html/body").send_keys("Single Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding single image and text center")

def adding_two_image_and_text(self, option_select_path, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-11-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-11-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-11-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-11-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[0]))
        d.find_element_by_xpath("html/body").send_keys("Two Text")
        d.switch_to_default_content()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[1]))
        d.find_element_by_xpath("html/body").send_keys("Two Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding two image and text")

def adding_three_image_and_text(self, option_select_path, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-12-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-12-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[3]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-12-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-12-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-12-2']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-12-2']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[0]))
        d.find_element_by_xpath("html/body").send_keys("Three Text")
        d.switch_to_default_content()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[1]))
        d.find_element_by_xpath("html/body").send_keys("Three Text")
        d.switch_to_default_content()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[2]))
        d.find_element_by_xpath("html/body").send_keys("Three Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding three image and text right")

def adding_two_image_text_and_full_image_right(self, option_select_path, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-13-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-13-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[3]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-13-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-13-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-13-2']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-13-2']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[0]))
        d.find_element_by_xpath("html/body").send_keys("Two Text")
        d.switch_to_default_content()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[1]))
        d.find_element_by_xpath("html/body").send_keys("Two Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding two image text and full image right")   

def adding_two_image_text_and_full_image_middle(self, option_select_path, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-14-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-14-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[3]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-14-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-14-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-14-2']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-14-2']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[0]))
        d.find_element_by_xpath("html/body").send_keys("Two Text")
        d.switch_to_default_content()
        d.switch_to_frame(d.find_element_by_xpath(iframe_path[1]))
        d.find_element_by_xpath("html/body").send_keys("Two Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding two image text and full image right")                   

def adding_single_image_text_and_full_image_right(self, option_select_path, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-15-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-15-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-15-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-15-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path))
        d.find_element_by_xpath("html/body").send_keys("Single Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding single image text and full image right")

def adding_single_image_text_and_full_image_left(self, option_select_path, iframe_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-16-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-16-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-16-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-16-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.switch_to_frame(d.find_element_by_xpath(iframe_path))
        d.find_element_by_xpath("html/body").send_keys("Single Text")
        d.switch_to_default_content()
        element = d.find_element_by_xpath(done_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding single image text and full image left")

def adding_full_image_and_three_image_right(self, option_select_path, image_path, image_selecter_button_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Images & Text").click()
        d.find_element_by_xpath(option_select_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-17-0']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-17-0']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[4]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-17-1']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-17-1']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[3]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-17-2']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-17-2']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        element = d.find_element_by_xpath(".//*[@id='pic-17-3']/img")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")    
        ActionChains(d).move_to_element(d.find_element_by_xpath(".//*[@id='pic-17-3']/img")).perform()
        d.find_element_by_xpath(".//*[@id='upload-17-3']/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding full image and three image right")

def adding_image_slider(self, add_image_button_path, image_path, image_selecter_button_path):
    try:
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Image Slider").click()
        time.sleep(3)
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(15)
        element = d.find_element_by_xpath(add_image_button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)  
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(add_image_button_path).click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()
        time.sleep(5)
        d.find_element_by_xpath(image_path + "li[1]/div").click()
        d.find_element_by_xpath(image_path + "li[2]/div").click()
        d.find_element_by_xpath(image_path + "li[3]/div").click()
        d.find_element_by_xpath(image_path + "li[4]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(image_selecter_button_path).click()
        time.sleep(3)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding slider")
    
def adding_book_now_button(self, button_name, button_name_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Book Button").click()
        d.find_element_by_xpath(button_name_path).clear()
        d.find_element_by_xpath(button_name_path).send_keys(button_name)
        # d.find_element_by_css_selector("ins.iCheck-helper").click()
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding book now button")

def adding_other_button(self, button_name, button_link, button_link_path, button_name_path, done_button_path):
    try:    
        d = self.driver
        element = d.find_element_by_link_text("Add Content ")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_link_text("Add Content ").click()
        d.find_element_by_link_text("Other Button").click()
        d.find_element_by_xpath(button_name_path).clear()
        d.find_element_by_xpath(button_name_path).send_keys(button_name)
        d.find_element_by_xpath(button_link_path).clear()
        d.find_element_by_xpath(button_link_path).send_keys(button_link)
        d.find_element_by_xpath(done_button_path).click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding other button")

def adding_predefined_content(self, predefined_content_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(predefined_content_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Weather").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Local Time").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Facebook Feed").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("TripAdvisor").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Social Connect").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Home Page Promotions").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Location").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Contact").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Newsletter Signup").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Promotion Slider").click()

        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(predefined_content_path).click()
        d.find_element_by_link_text("Custom Plugin").click()
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while adding predefined content")