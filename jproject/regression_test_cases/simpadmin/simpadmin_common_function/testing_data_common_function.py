# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
import traceback
import sys
import os

   
def testing_h1(self, input_box_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(input_box_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(input_box_path).text,"Page H1")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing H1")

def testing_h2(self, input_box_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(input_box_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(input_box_path).text,"Page H2")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing H2")

def testing_single_text(self, input_box_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(input_box_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(input_box_path).text,"Single Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing single text")

def testing_two_text(self, input_box_path_left, input_box_path_right):
    try:    
        d = self.driver
        element = d.find_element_by_xpath(input_box_path_left)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(input_box_path_left).text,"Two Text")
        self.assertEqual(d.find_element_by_xpath(input_box_path_right).text,"Two Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing two text")

def testing_three_text(self, input_box_path_left, input_box_path_middle, input_box_path_right):
    try:   
        d = self.driver
        element = d.find_element_by_xpath(input_box_path_left)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(input_box_path_left).text,"Three Text")
        self.assertEqual(d.find_element_by_xpath(input_box_path_middle).text,"Three Text")
        self.assertEqual(d.find_element_by_xpath(input_box_path_right).text,"Three Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing three text")

def testing_single_image(self, image_path, saved_image_path):
    try:    
        d = self.driver
        element = d.find_element_by_xpath(image_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing single image")

def testing_two_image(self, image_path_left, image_path_right, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path_left)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path_left).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[1])
        wordList = d.find_element_by_xpath(image_path_right).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing two image")

def testing_three_image(self, image_path_left, image_path_middle, image_path_right, saved_image_path):
    try:    
        d = self.driver
        element = d.find_element_by_xpath(image_path_left)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path_left).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[2])
        wordList = d.find_element_by_xpath(image_path_middle).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[1])
        wordList = d.find_element_by_xpath(image_path_right).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing three image")

def testing_single_image_and_text_left(self, input_box_path, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(input_box_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])
        self.assertEqual(d.find_element_by_xpath(input_box_path).text,"Single Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing single image and text left")

def testing_single_image_and_text_right(self, input_box_path, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(input_box_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[1])
        self.assertEqual(d.find_element_by_xpath(input_box_path).text,"Single Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing single image and text right")

def testing_single_image_and_text_center(self, input_box_path, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[2])
        self.assertEqual(d.find_element_by_xpath(input_box_path).text,"Single Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing single image and text center")

def testing_two_image_and_text(self, input_box_path_left, image_path_left, input_box_path_right, image_path_right, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path_left)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path_left).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[1])
        self.assertEqual(d.find_element_by_xpath(input_box_path_left).text,"Two Text")
        wordList = d.find_element_by_xpath(image_path_right).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])
        self.assertEqual(d.find_element_by_xpath(input_box_path_right).text,"Two Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing two image and text")

def testing_three_image_and_text(self, text_path, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path[0])
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path[0]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[2])
        self.assertEqual(d.find_element_by_xpath(text_path[0]).text,"Three Text")

        wordList = d.find_element_by_xpath(image_path[1]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[1])
        self.assertEqual(d.find_element_by_xpath(text_path[1]).text,"Three Text")

        wordList = d.find_element_by_xpath(image_path[2]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])
        self.assertEqual(d.find_element_by_xpath(text_path[2]).text,"Three Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing three image and text")

def testing_two_image_text_and_full_image_right(self, text_path, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path[0])
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path[0]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[2])
        self.assertEqual(d.find_element_by_xpath(text_path[0]).text,"Two Text")

        wordList = d.find_element_by_xpath(image_path[1]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[1])
        self.assertEqual(d.find_element_by_xpath(text_path[1]).text,"Two Text")

        wordList = d.find_element_by_xpath(image_path[2]).get_attribute("style").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name.strip('");'), saved_image_path[0])
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing three image and text")

def testing_two_image_text_and_full_image_middle(self, text_path, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path[0])
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path[0]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[2])
        self.assertEqual(d.find_element_by_xpath(text_path[0]).text,"Two Text")

        wordList = d.find_element_by_xpath(image_path[1]).get_attribute("style").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name.strip('");'), saved_image_path[1])

        wordList = d.find_element_by_xpath(image_path[2]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])
        self.assertEqual(d.find_element_by_xpath(text_path[1]).text,"Two Text")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing three image and text")

def testing_single_image_text_and_full_image_right(self, text_path, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path[0])
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path[0]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[1])
        self.assertEqual(d.find_element_by_xpath(text_path).text,"Single Text")

        wordList = d.find_element_by_xpath(image_path[1]).get_attribute("style").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name.strip('");'), saved_image_path[0])

    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing three image and text")

def testing_single_image_text_and_full_image_left(self, text_path, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path[0])
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path[0]).get_attribute("style").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name.strip('");'), saved_image_path[1])

        wordList = d.find_element_by_xpath(image_path[1]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])
        self.assertEqual(d.find_element_by_xpath(text_path).text,"Single Text")

    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing three image and text")

def testing_full_image_and_three_image_right(self, image_path, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(image_path[0])
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        wordList = d.find_element_by_xpath(image_path[0]).get_attribute("style").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name.strip('");'), saved_image_path[3])

        wordList = d.find_element_by_xpath(image_path[1]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[2])

        wordList = d.find_element_by_xpath(image_path[2]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[1])

        wordList = d.find_element_by_xpath(image_path[3]).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        self.assertEqual(Image_Name, saved_image_path[0])

    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing three image and text")

def testing_image_slider(self, saved_image_path):
    try:
        d = self.driver
        element = d.find_element_by_xpath(".//*[@id='pic-18-0']/img")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        i = 0
        for i in range (3):
            wordList = d.find_element_by_xpath(".//*[@id='pic-18-"+str(i)+"']/img").get_attribute("src").split("/")
            n = len(wordList) 
            Image_Name = wordList[n-1]
            self.assertEqual(Image_Name, saved_image_path[i])
            i = i +1
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing image slider")

def testing_book_now_button(self, button_path, button_name_path, button_name):
    try:
        d = self.driver
        element = d.find_element_by_xpath(button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(button_path).click()
        self.assertEqual(d.find_element_by_xpath(button_name_path).get_attribute("value"), button_name)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing book now button")

def testing_other_button(self, button_path, button_name_path, button_name, button_link_path, button_link):
    try:
        d = self.driver
        element = d.find_element_by_xpath(button_path)
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_xpath(button_path).click()
        self.assertEqual(d.find_element_by_xpath(button_name_path).get_attribute("value"), button_name)
        self.assertEqual(d.find_element_by_xpath(button_link_path).get_attribute("value"), button_link)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing other button")

def testing_predefined_content(self):
    try:
        d = self.driver
        element = d.find_element_by_xpath(".//*[@id='preblock0']/div/div/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock0']/div/div/div[3]/div/div/div/h4/b").text, "Weather")

        element = d.find_element_by_xpath(".//*[@id='preblock1']/div/div/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock1']/div/div/div[3]/div/div/div/h4/b").text, "Local Time")

        element = d.find_element_by_xpath(".//*[@id='preblock2']/div/div/div/div[1]/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock2']/div/div/div/div[1]/div[3]/div/div/div/h4/b").text, "Facebook Feed")

        element = d.find_element_by_xpath(".//*[@id='preblock3']/div/div/div/div[1]/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock3']/div/div/div/div[1]/div[3]/div/div/div/h4/b").text, "TripAdvisor")

        element = d.find_element_by_xpath(".//*[@id='preblock4']/div/div/div/div/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock4']/div/div/div/div/div[3]/div/div/div/h4/b").text, "Social Connect")

        element = d.find_element_by_xpath(".//*[@id='preblock5']/div/div/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock5']/div/div/div[3]/div/div/div/h4/b").text, "Home Page Promotions")

        element = d.find_element_by_xpath(".//*[@id='preblock6']/div/div/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock6']/div/div/div[3]/div/div/div/h4/b").text, "Location")

        element = d.find_element_by_xpath(".//*[@id='preblock7']/div/div/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock7']/div/div/div[3]/div/div/div/h4/b").text, "Contact")

        element = d.find_element_by_xpath(".//*[@id='preblock8']/div/div/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock8']/div/div/div[3]/div/div/div/h4/b").text, "Newsletter Widget")

        element = d.find_element_by_xpath(".//*[@id='preblock9']/div/div/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock9']/div/div/div[3]/div/div/div/h4/b").text, "Promotions Slider")

        element = d.find_element_by_xpath(".//*[@id='preblock10']/div/div/div/div[1]/div[3]/div/div/div/h4/b")
        d.execute_script("return arguments[0].scrollIntoView();", element)
        d.execute_script("window.scrollBy(0, -50);")
        self.assertEqual(d.find_element_by_xpath(".//*[@id='preblock10']/div/div/div/div[1]/div[3]/div/div/div/h4/b").text, "Custom Plugin")
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while testing predefined content")