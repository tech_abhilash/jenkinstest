from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Location Page Creation"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("location_page_creation")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing Location Page"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("location_page_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing Location Page Preview"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("location_page_preview_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)