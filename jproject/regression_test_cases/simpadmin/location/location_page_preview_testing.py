# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("location"), '../..', "config"))
import hotel_info
import location_page_info_data
import location_page_element_path


class Locationpage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_location_page_preview(self):
        d = self.driver
        d.get(self.base_url + "location.html")
        d.maximize_window()
        time.sleep(10)
        l = location_page_element_path
        
        #----------------------------------------Getting Image Name----------------------------------
        with open("image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        Image_Name_4 = wordList[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4

        #---------------------------Testing Location Group Name-----------------------------------
        print d.find_element_by_xpath(l.Preview_Location_Group_Name_Path).text
        self.assertEqual(d.find_element_by_xpath(l.Preview_Location_Group_Name_Path).text, location_page_info_data.Location_Group_Name)
        d.find_element_by_xpath(l.Preview_Location_Group_Name_Path).click()
        time.sleep(5)
        self.assertEqual(d.find_element_by_xpath(l.Preview_Nav_Location_Name_Path).text, location_page_info_data.Location_Name)
        d.find_element_by_xpath(l.Preview_Nav_Location_Name_Path).click()
        time.sleep(10)
       
        #--------------------------Testing Location Name--------------------------------------------------
        print d.find_element_by_xpath(l.Preview_Location_Name_Path).text
        self.assertEqual(d.find_element_by_xpath(l.Preview_Location_Name_Path).text, location_page_info_data.Location_Name)

        #--------------------------Testing Location Short Description Text--------------------------------------------------
        print d.find_element_by_xpath(l.Preview_Location_Short_Description).text
        self.assertEqual(d.find_element_by_xpath(l.Preview_Location_Short_Description).text, location_page_info_data.Location_Short_Description)

        
        #-------------------Testing SEO---------------------------------------------------------
        print d.title
        self.assertEqual(d.title, location_page_info_data.Page_Title)

        print d.find_element_by_xpath("/html/head/meta[1]").get_attribute("content")
        self.assertEqual(d.find_element_by_xpath("/html/head/meta[1]").get_attribute("content"), location_page_info_data.Page_Description)

        print d.find_element_by_xpath("/html/head/meta[6]").get_attribute("content")
        self.assertEqual(d.find_element_by_xpath("/html/head/meta[6]").get_attribute("content"), location_page_info_data.Page_Keywords)

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
