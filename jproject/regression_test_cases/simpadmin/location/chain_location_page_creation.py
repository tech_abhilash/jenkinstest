# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("location"), '../..', "config"))
import hotel_info
import location_page_info_data
import location_page_element_path
sys.path.append(os.path.join(os.path.dirname("location"), '..', "simpadmin_common_function"))
import create_data_common_function

class CreatingChainLocationPage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_location_page(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        l = location_page_element_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.chain_user_name, hotel_info.chain_password)

        d.find_element_by_xpath("(//button[@type='button'])[2]").click()
        d.find_element_by_link_text(hotel_info.chain_child_hotel_name_one).click()
        time.sleep(2)
        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text("   Locations").click()

        #-------------Creating New Promotions------------------------------------------

        d.find_element_by_link_text("Add Location Group").click()
        d.find_element_by_id("locationgroupname").send_keys(location_page_info_data.Location_Group_Name)
        d.find_element_by_xpath(l.Group_Name_Save_Button_Path).click()
        time.sleep(10)
        d.find_element_by_link_text("Add Location").click()
        d.find_element_by_id("name").send_keys(location_page_info_data.Location_Name)
       
        #------------Adding Text to Short and Image of the Promotions-----------------
        ActionChains(d).move_to_element(d.find_element_by_xpath(l.Short_Description_Image_Path + "div/img")).perform()
        d.find_element_by_xpath(l.Short_Description_Image_Path + "a[1]/span").click()
        time.sleep(3)
        d.find_element_by_xpath(".//*[@id='groupList']/li[1]/a").click()

        #----------------------------------------Getting Image Name-----------------------------------------------------------------
        
        wordList = d.find_element_by_xpath(l.Image_Path + "li[1]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]

        wordList = d.find_element_by_xpath(l.Image_Path + "li[2]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]

        wordList = d.find_element_by_xpath(l.Image_Path + "li[3]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]

        wordList = d.find_element_by_xpath(l.Image_Path + "li[4]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]

        #----------------Saving Image Name----------------------------------
        with open("image_names.txt",'w') as f:
            f.write(Image_Name_1 +" "+Image_Name_2+" "+Image_Name_3+" "+Image_Name_4)
            f.close()

        
        time.sleep(3)
    
        d.find_element_by_xpath(l.Image_Path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(l.Image_Selecter_Button_Path).click()
        
        d.switch_to_frame(d.find_element_by_xpath(l.Short_Description_Iframe_Path))
        d.find_element_by_xpath("/html/body").send_keys(location_page_info_data.Location_Short_Description)
        d.switch_to_default_content()

        #------------Adding Location-----------------------

        d.find_element_by_xpath(l.Location_Name_Path).clear()
        d.find_element_by_xpath(l.Location_Name_Path).send_keys(location_page_info_data.Location_Name)
        d.find_element_by_xpath(l.Latitude_Path).clear()
        d.find_element_by_xpath(l.Latitude_Path).send_keys(location_page_info_data.Latitude_No)
        d.find_element_by_xpath(l.Longitude_Path).clear()
        d.find_element_by_xpath(l.Longitude_Path).send_keys(location_page_info_data.Longitude_No)
        d.find_element_by_xpath(l.Address_Path).clear()
        d.find_element_by_xpath(l.Address_Path).send_keys(location_page_info_data.Address)
       
        d.find_element_by_xpath("//button[@type='submit']").click()
        
        #------------------Adding SEO Content-------------------------------
        time.sleep(5)
        d.find_element_by_link_text("Location Page Settings").click()
        d.find_element_by_id("meta-title").clear()
        d.find_element_by_id("meta-title").send_keys(location_page_info_data.Page_Title)
        d.find_element_by_id("meta-description").clear()
        d.find_element_by_id("meta-description").send_keys(location_page_info_data.Page_Description)
        d.find_element_by_id("meta-keywords").clear()
        d.find_element_by_id("meta-keywords").send_keys(location_page_info_data.Page_Keywords)
        
        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
