# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("location"), '../..', "config"))
import hotel_info
import location_page_info_data
import location_page_element_path
sys.path.append(os.path.join(os.path.dirname("location"), '..', "simpadmin_common_function"))
import create_data_common_function


class TestingLocationPage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_location_page_testing(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        l = location_page_element_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text("   Locations").click()
        time.sleep(10)
        i = 1
        while 1:                                  
            l_name = d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[4]/ol/li[2]/ol/li["+str(i)+"]/div/a").text
            if (l_name == location_page_info_data.Location_Name):
                d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[4]/ol/li[2]/ol/li["+str(i)+"]/div/a").click()
                d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[4]/ol/li[2]/ol/li["+str(i)+"]/div/div/a[1]/i").click()
                break
            i = i + 1 

        # #----------------------------------------Testing Promotion Name ----------------------------------------------------------------
    
        print d.find_element_by_xpath(l.Location_Name_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(l.Location_Name_Path).get_attribute("value"), location_page_info_data.Location_Name)

        
        # -----------------------Testing Add Content----------------------
        with open("image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        Image_Name_4 = wordList[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4

         #---------------------Testing Image Slider-----------------------------------------
        wordList = d.find_element_by_xpath(l.After_Save_Image_Path).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        d.switch_to_frame(d.find_element_by_xpath(l.Short_Description_Iframe_Path))
        print d.find_element_by_xpath("/html/body").text
        self.assertEqual(d.find_element_by_xpath('/html/body').text, location_page_info_data.Location_Short_Description)
        d.switch_to_default_content()

        print d.find_element_by_xpath(l.Latitude_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(l.Latitude_Path).get_attribute("value"), location_page_info_data.Latitude_No)

        print d.find_element_by_xpath(l.Longitude_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(l.Longitude_Path).get_attribute("value"), location_page_info_data.Longitude_No)

        print d.find_element_by_xpath(l.Address_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(l.Address_Path).get_attribute("value"), location_page_info_data.Address)

       
        # # # #----------------Testing Room Page SEO-------------------------------
        d.find_element_by_link_text("Location Page Settings").click()
        print d.find_element_by_xpath(l.Page_Title_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(l.Page_Title_Path).get_attribute("value"), location_page_info_data.Page_Title)

        print d.find_element_by_xpath(l.Page_Description_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(l.Page_Description_Path).get_attribute("value"), location_page_info_data.Page_Description)

        print d.find_element_by_xpath(l.Page_Keywords_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(l.Page_Keywords_Path).get_attribute("value"), location_page_info_data.Page_Keywords)
        
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
