Add_Hero_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/a"
Image_Path = ".//*[@id='imageList']/"
Image_Selecter_Button_Path = ".//*[@id='doneBtn']"
Image_Cancel_Button_Path = "/html/body/div[8]/div[2]/div/div[2]/div[2]/button[1]"


Add_Content_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[2]/ul/li/a/span[1]"

#------------Location Name-------------------------------------------------
Location_Name_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[5]/div/div/div[1]/div/div/input"
Group_Name_Save_Button_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div/div/button"
                                
Short_Description_Image_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[3]/div/div/div/div[1]/label/div[2]/"
Short_Description_Iframe_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[3]/div/div/div/div[2]/label/div/div/div/iframe"
     
Latitude_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[5]/div/div/div[2]/div[1]/input"
Longitude_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[5]/div/div/div[2]/div[2]/input"                            
Address_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[5]/div/div/div[2]/div[3]/textarea"

#-----------------Submit Button Path------------------------------
Submit_Button_Path = "/html/body/div[1]/aside[2]/form/div/div/div[2]/button"

#------------------After Save----------------------------------  

After_Save_Image_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[3]/div/div/div/div[1]/label/div[2]/div/img"   
After_Save_Image_select_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[3]/div/div/div/div[1]/label/div[2]/a[1]/span"
Location_Name_Path = "/html/body/div[1]/aside[2]/section[2]/div/div/div[2]/form/div/div[1]/input"

Page_Title_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/div/div[1]/label/input"
Page_Description_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/div/div[2]/label/textarea" 
Page_Keywords_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/div/div[3]/label/input"

#---------Preview Path-----------------------------------------
									
Preview_Location_Group_Name_Path = "/html/body/div[1]/div/div/div/div[2]/div/div/div/div[2]/div[1]"
Preview_Nav_Location_Name_Path = "/html/body/div[1]/div/div/div/div[2]/div/div/div/div[2]/div[2]/div/ul/li/a"
Preview_Location_Name_Path = "/html/body/div[1]/div/div/div/div[1]/div/div/div[2]/div[2]/h3"
Preview_Location_Short_Description = "/html/body/div[1]/div/div/div/div[1]/div/div/div[2]/div[2]/div/p"

Preview_Image_Path = "/html/body/div[1]/div/div/div/div[1]/div/div/div[18]/div/div/div[1]/div[2]/div/"
Preview_Detailed_Description_Path = "/html/body/div[1]/div/div/div/div[1]/div/div/div[4]/div/p"

