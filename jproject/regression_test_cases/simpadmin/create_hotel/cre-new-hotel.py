# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '..', "simpadmin_common_function"))
import create_data_common_function

class TestHotelCreation(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_create_hotel(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function 
        #-------Login to admin------
        admin.login_to_admin(self, hotel_info.admin_user_name, hotel_info.admin_password)
        
        #-------Creating  New Hotel ------
        admin.add_hotel_name(self, hotel_info.hotel_name)

        #--Hotel Initial data ------
        admin.add_hote_initial_data(self, hotel_info.hote_initial_data)

        #--Hotel Timezone ------
        admin.add_hotel_time_zone(self, hotel_info.hote_initial_data["hotel_time_zone"])

        #--Hotel Currency ------
        admin.add_hotel_currency(self, hotel_info.hote_initial_data["hotel_currency"])

        #--Hotel city ------
        admin.add_hotel_city(self, hotel_info.hote_initial_data["city_name"])

        #--Hotel language ------
        admin.add_hotel_language(self, hotel_info.hote_initial_data["hotel_language"])

        #--Hotel Country ------
        admin.add_hotel_country(self, hotel_info.hote_initial_data["hotel_country"])

        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)                                 
        self.assertEqual(d.find_element_by_xpath("//*[@class='info']/label[1]/h4").text, hotel_info.hotel_name)
        print "Hotel has been Created"

        time.sleep(10)
        d.find_element_by_xpath("(//button[@type='button'])[2]").click()
        d.find_element_by_link_text(hotel_info.hotel_name).click()
        time.sleep(10)
        #-----Creating new Admin User Name------ 
        d.find_element_by_link_text("Ops").click()
        d.find_element_by_link_text("Manage Users").click()
        d.find_element_by_id("firstname").clear()
        d.find_element_by_id("firstname").send_keys(hotel_info.hotel_name)
        d.find_element_by_id("lastname").clear()
        d.find_element_by_id("lastname").send_keys(hotel_info.hotel_name)
        d.find_element_by_id("email").clear()
        d.find_element_by_id("email").send_keys(hotel_info.hote_initial_data['contact_email'])
        d.find_element_by_id("username").clear()
        d.find_element_by_id("username").send_keys(hotel_info.user_name)
        d.find_element_by_id("password").clear()
        d.find_element_by_id("password").send_keys(hotel_info.password)
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)

        #-----Assigning User to Hotel------
        d.find_element_by_id("select2-chosen-1").click()
        time.sleep(2)
        d.find_element_by_id("s2id_autogen1_search").clear()
        d.find_element_by_id("s2id_autogen1_search").send_keys(hotel_info.hotel_name)
        time.sleep(2)
        i = 1
        while 1:   
            print d.find_element_by_xpath(".//*[@id='select2-results-1']/li["+str(i)+"]").text
            if (d.find_element_by_xpath(".//*[@id='select2-results-1']/li["+str(i)+"]").text == hotel_info.hotel_name):
                d.find_element_by_xpath(".//*[@id='select2-results-1']/li["+str(i)+"]").click()
                break
            i = i + 1 

        d.find_element_by_id("select2-chosen-2").click()
        time.sleep(2)
        d.find_element_by_id("s2id_autogen2_search").clear()
        d.find_element_by_id("s2id_autogen2_search").send_keys(hotel_info.user_name)
        time.sleep(2)
        i = 1
        while 1:                                   
            print d.find_element_by_xpath(".//*[@id='select2-results-2']/li["+str(i)+"]").text
            if (d.find_element_by_xpath(".//*[@id='select2-results-2']/li["+str(i)+"]").text == hotel_info.user_name):
                d.find_element_by_xpath(".//*[@id='select2-results-2']/li["+str(i)+"]").click()
                break
            i = i + 1 

        time.sleep(2)
        d.find_element_by_xpath(".//*[@id='idUserHotelManagement']/div/div[3]/button").click()
        time.sleep(10)

        #-----Adding hotel to opps group-------
        d.execute_script("return arguments[0].scrollIntoView();", d.find_element_by_id("select2-chosen-4"))
        d.execute_script("window.scrollBy(0, -50);")
        d.find_element_by_id("select2-chosen-4").click()
        time.sleep(2)
        d.find_element_by_id("s2id_autogen4_search").clear()
        d.find_element_by_id("s2id_autogen4_search").send_keys(hotel_info.user_name)
        time.sleep(2)
        i = 1
        while 1:   
            print d.find_element_by_xpath(".//*[@id='select2-results-4']/li["+str(i)+"]").text
            if (d.find_element_by_xpath(".//*[@id='select2-results-4']/li["+str(i)+"]").text == hotel_info.user_name):
                d.find_element_by_xpath(".//*[@id='select2-results-4']/li["+str(i)+"]").click()
                break
            i = i + 1 

        d.find_element_by_id("select2-chosen-3").click()
        time.sleep(2)
        d.find_element_by_id("s2id_autogen3_search").clear()
        d.find_element_by_id("s2id_autogen3_search").send_keys("OPS_USER")
        time.sleep(2)
        i = 1
        while 1:
            print d.find_element_by_xpath(".//*[@id='select2-results-3']/li["+str(i)+"]").text                          
            if (d.find_element_by_xpath(".//*[@id='select2-results-3']/li["+str(i)+"]").text == "OPS_USER"):
                d.find_element_by_xpath(".//*[@id='select2-results-3']/li["+str(i)+"]").click()
                break
            i = i + 1 
        time.sleep(2)
        d.find_element_by_xpath(".//*[@id='idUserGroupManagement']/div/div[3]/button").click()
        time.sleep(10)

        ##----design-------

        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text("  Design").click()
        Select(d.find_element_by_name("template_id")).select_by_visible_text("Evening")
        d.find_element_by_css_selector("option[value=\"100003\"]").click()
        d.find_element_by_xpath("/html/body/div/aside[2]/form/section/section/div/div[2]/div[2]/div/div/button").click()
        time.sleep(20)
        d.find_element_by_link_text("Theme Settings").click()
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(40)                                 


        #-----Saving booking engine------
        d.find_element_by_link_text("Booking Engine").click()
        d.find_element_by_link_text("Settings").click()
        time.sleep(2)
        d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
        time.sleep(5)

        #-----Checking new Hotel Login------
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        self.assertEqual(d.find_element_by_xpath(".//*[@id='navbar']/ul[2]/li[1]/a").text, "Logout")
        print "Normal User Login :- login successful"
        time.sleep(5)

    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
