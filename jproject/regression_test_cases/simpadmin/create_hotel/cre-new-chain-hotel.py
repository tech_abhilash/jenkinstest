# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "config"))
import hotel_info

class TestHotelCreation(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True

    def create_new_hotel(self, hotel_name):
        driver = self.driver
        driver.find_element_by_link_text("Create New Hotel").click()
        driver.find_element_by_id("hotel-name").clear()
        driver.find_element_by_id("hotel-name").send_keys(hotel_name)
        driver.find_element_by_id("hotel-tag-line").clear()
        driver.find_element_by_id("hotel-tag-line").send_keys(hotel_info.hote_initial_data["hotel_tag_line"])
        driver.find_element_by_name("hotel.description").clear()
        driver.find_element_by_name("hotel.description").send_keys(hotel_info.hote_initial_data["hotel_description"])
        driver.find_element_by_id("pac-input").clear()
        driver.find_element_by_id("pac-input").send_keys(hotel_info.hote_initial_data["location_name"])
        driver.find_element_by_css_selector("span.pac-matched").click()
        driver.find_element_by_id("hotel-url").clear()
        driver.find_element_by_id("hotel-url").send_keys(hotel_info.hote_initial_data["hotel_url"])
        driver.find_element_by_id("contact-email-").clear()
        driver.find_element_by_id("contact-email-").send_keys(hotel_info.hote_initial_data["contact_email"])
        driver.find_element_by_id("contact-phone-").clear()
        driver.find_element_by_id("contact-phone-").send_keys(hotel_info.hote_initial_data["contact_phone"])
        driver.find_element_by_id("address-city").clear()
        driver.find_element_by_id("address-city").send_keys(hotel_info.hote_initial_data["city_name"])
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)                                 
        self.assertEqual(driver.find_element_by_xpath('/html/body/div[1]/aside[1]/section/ul/div/div/label[1]').text, hotel_name)
        print "Hotel has been Created"
        time.sleep(10)
    
    def adding_child_to_group(self, group_name, i):
        driver = self.driver
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        driver.find_element_by_id("group-name").clear()
        driver.find_element_by_id("group-name").send_keys(group_name)
        driver.find_element_by_id("add-group-button").click()
        time.sleep(2)
        source_element = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section[2]/section/div/div[2]/div[3]/div/div[1]/div/ul/li[1]")
        dest_element = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section[2]/section/div/div[2]/div[3]/div/div[2]/div["+str(i)+"]/ul")
        ActionChains(driver).drag_and_drop(source_element, dest_element).perform()
        time.sleep(2)

    def adding_child_to_chain_user_id(self, hotel_name):
        driver = self.driver
        driver.find_element_by_xpath("/html/body/div[1]/aside[2]/section[2]/section/section[2]/div/div[2]/form/div/div[1]/div/div/a/span[2]").click()
        time.sleep(2)
        driver.find_element_by_id("s2id_autogen1_search").clear()
        driver.find_element_by_id("s2id_autogen1_search").send_keys(hotel_name)
        time.sleep(2)
        i = 1
        while 1:   
            print driver.find_element_by_xpath("/html/body/div[4]/ul/li["+str(i)+"]/div/span").text
            h_name = driver.find_element_by_xpath("/html/body/div[4]/ul/li["+str(i)+"]/div/span").text
            if (h_name == hotel_name):
                driver.find_element_by_xpath("/html/body/div[4]/ul/li["+str(i)+"]/div/span").click()
                break
            i = i + 1 
        driver.find_element_by_xpath("/html/body/div[1]/aside[2]/section[2]/section/section[2]/div/div[2]/form/div/div[2]/div/div/a/span[2]").click()
        time.sleep(2)
        driver.find_element_by_id("s2id_autogen2_search").clear()
        driver.find_element_by_id("s2id_autogen2_search").send_keys(hotel_info.chain_user_name)
        time.sleep(2)
        i = 1
        while 1:                                   
            user_name = driver.find_element_by_xpath("/html/body/div[5]/ul/li["+str(i)+"]/div/span").text
            if (user_name == hotel_info.chain_user_name):
                print driver.find_element_by_xpath("/html/body/div[5]/ul/li["+str(i)+"]/div/span").text
                driver.find_element_by_xpath("/html/body/div[5]/ul/li["+str(i)+"]/div/span").click()
                break
            i = i + 1 
        time.sleep(2)
        driver.find_element_by_xpath("/html/body/div[1]/aside[2]/section[2]/section/section[2]/div/div[2]/form/div/div[3]/button").click()
        time.sleep(10)

    def changing_child_hotel_url(self, hotel_name, hotel_url):
        driver = self.driver
        element = driver.find_element_by_xpath("(//button[@type='button'])[2]")
        driver.execute_script("return arguments[0].scrollIntoView();", element)
        driver.execute_script("window.scrollBy(0, -50);")
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        driver.find_element_by_link_text(hotel_name).click()
        time.sleep(5)
        driver.find_element_by_id("hotel-slugified-name").clear()
        driver.find_element_by_id("hotel-slugified-name").send_keys(hotel_url)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(5)

    def test_create_hotel(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys("simpadmin")
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys("simplehoteladmin!@#")
        driver.find_element_by_xpath("//button[@type='submit']").click()
        # driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        # driver.find_element_by_link_text(hotel_info.chain_hotel_name).click()

        # #-----------------Creating Chain Parent---------------------------
        driver.find_element_by_link_text("Create New Hotel Chain").click()
        driver.find_element_by_id("hotel-name").clear()
        driver.find_element_by_id("hotel-name").send_keys(hotel_info.chain_hotel_name)
        driver.find_element_by_id("hotel-tag-line").clear()
        driver.find_element_by_id("hotel-tag-line").send_keys(hotel_info.hote_initial_data["hotel_tag_line"])
        driver.find_element_by_name("hotel.description").clear()
        driver.find_element_by_name("hotel.description").send_keys(hotel_info.hote_initial_data["hotel_description"])
        driver.find_element_by_id("hotel-url").clear()
        driver.find_element_by_id("hotel-url").send_keys(hotel_info.hote_initial_data["hotel_url"])
        driver.find_element_by_id("contact-email-").clear()
        driver.find_element_by_id("contact-email-").send_keys(hotel_info.hote_initial_data["contact_email"])
        driver.find_element_by_id("contact-phone-").clear()
        driver.find_element_by_id("contact-phone-").send_keys(hotel_info.hote_initial_data["contact_phone"])
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)                                 
        self.assertEqual(driver.find_element_by_xpath('/html/body/div[1]/aside[1]/section/ul/div/div/label[1]').text, hotel_info.chain_hotel_name)
        print "Chain Hotel has been Created"
        time.sleep(10)

        #-----Creating Child hotels ---------------------------
        self.create_new_hotel(hotel_info.chain_child_hotel_name_one)
        self.create_new_hotel(hotel_info.chain_child_hotel_name_two)
        self.create_new_hotel(hotel_info.chain_child_hotel_name_three)
        self.create_new_hotel(hotel_info.chain_child_hotel_name_fourth)
       



        # #-------------Creating new Admin User Name -----------------
        driver.find_element_by_link_text("Ops").click()
        driver.find_element_by_link_text("Manage Users").click()
        driver.find_element_by_id("firstname").clear()
        driver.find_element_by_id("firstname").send_keys(hotel_info.chain_hotel_name)
        driver.find_element_by_id("lastname").clear() 
        driver.find_element_by_id("lastname").send_keys(hotel_info.chain_hotel_name)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys(hotel_info.hote_initial_data['contact_email'])
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys(hotel_info.chain_user_name)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys(hotel_info.chain_password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        
        #-------Adding to Admin User Name----------------------------
        self.adding_child_to_chain_user_id(hotel_info.chain_child_hotel_name_one)
        self.adding_child_to_chain_user_id(hotel_info.chain_child_hotel_name_two)
        self.adding_child_to_chain_user_id(hotel_info.chain_child_hotel_name_three)
        self.adding_child_to_chain_user_id(hotel_info.chain_child_hotel_name_fourth)
        self.adding_child_to_chain_user_id(hotel_info.chain_hotel_name)

        # ----------Adding Child hotel to parent -----------------------------------------
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        driver.find_element_by_link_text(hotel_info.chain_hotel_name).click()
        time.sleep(5)   
        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Hotel Info").click()
        html_list = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div[1]/ul")
        items = html_list.find_elements_by_tag_name("li")
        i = 1
        for item in items:
            text = item.text
            print text
            if (text == hotel_info.chain_child_hotel_name_one or text == hotel_info.chain_child_hotel_name_two or text == hotel_info.chain_child_hotel_name_three or text == hotel_info.chain_child_hotel_name_fourth):
                element = driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[1]/div/div[1]/ul/li["+str(i)+"]")
                driver.execute_script("return arguments[0].scrollIntoView();", element)
                driver.execute_script("window.scrollBy(0, -50);")
                item.click()
            i = i + 1
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)


        #------------Creating group and adding child hotel to group ---------------------------------
        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text("  Navigation").click()
        driver.find_element_by_id("pictureGroupId").clear()
        driver.find_element_by_id("pictureGroupId").send_keys(hotel_info.group_menu_name)
        #------------Adding child hotel to group----------------------
        self.adding_child_to_group(hotel_info.group_name_one, 2)
        self.adding_child_to_group(hotel_info.group_name_two, 3)
        self.adding_child_to_group(hotel_info.group_name_three, 4)
        self.adding_child_to_group(hotel_info.group_name_three, 4)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(5)   

        # -------------------Checking new Hotel Login------------------------
        time.sleep(5)
        driver.find_element_by_xpath(".//*[@id='navbar']/ul[2]/li[1]/a").click()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys(hotel_info.chain_user_name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(hotel_info.chain_password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        self.assertEqual(driver.find_element_by_xpath(".//*[@id='navbar']/ul[2]/li[1]/a").text, "Logout")
        print "Normal User Login :- login successful"
        time.sleep(10)

        
        
        #-------changing Child Hotel url-----------------------------
        self.changing_child_hotel_url(hotel_info.chain_child_hotel_name_one, hotel_info.child_hotel_one_url)
        self.changing_child_hotel_url(hotel_info.chain_child_hotel_name_two, hotel_info.child_hotel_two_url)
        self.changing_child_hotel_url(hotel_info.chain_child_hotel_name_three, hotel_info.child_hotel_three_url)
        self.changing_child_hotel_url(hotel_info.chain_child_hotel_name_fourth, hotel_info.child_hotel_fourth_url)


        #--Hotel Timezone
        driver.find_element_by_xpath(".//*[@id='s2id_timezone']/a/span[2]/b").click()
        driver.find_element_by_id("s2id_autogen1_search").clear()
        driver.find_element_by_id("s2id_autogen1_search").send_keys("America/Adak")
        time.sleep(2)
        i = 1
        while 1:                                
            print driver.find_element_by_xpath("/html/body/div[6]/ul/li["+str(i)+"]/div/span").text
            h_name = driver.find_element_by_xpath("/html/body/div[6]/ul/li["+str(i)+"]/div/span").text
            if (h_name == "America/Adak"):
                driver.find_element_by_xpath("/html/body/div[6]/ul/li["+str(i)+"]/div/span").click()
                break
            i = i + 1

        #--Hotel Currency
        driver.find_element_by_xpath(".//*[@id='s2id_currency']/a/span[2]/b").click()
        driver.find_element_by_id("s2id_autogen2_search").clear()
        driver.find_element_by_id("s2id_autogen2_search").send_keys("US Dollar")
        time.sleep(2)
        i = 1
        while 1:                               
            print driver.find_element_by_xpath("/html/body/div[7]/ul/li["+str(i)+"]/div/span").text
            h_name = driver.find_element_by_xpath("/html/body/div[7]/ul/li["+str(i)+"]/div/span").text
            if (h_name == "US Dollar"):
                driver.find_element_by_xpath("/html/body/div[7]/ul/li["+str(i)+"]/div/span").click()
                break
            i = i + 1

         #--Hotel language
        driver.find_element_by_xpath(".//*[@id='s2id_language']/a/span[2]/b").click()
        driver.find_element_by_id("s2id_autogen3_search").clear()
        driver.find_element_by_id("s2id_autogen3_search").send_keys("English")
        time.sleep(2)
        i = 1
        while 1:                                
            print driver.find_element_by_xpath("/html/body/div[8]/ul/li["+str(i)+"]/div/span").text
            h_name = driver.find_element_by_xpath("/html/body/div[8]/ul/li["+str(i)+"]/div/span").text
            if (h_name == "English"):
                driver.find_element_by_xpath("/html/body/div[8]/ul/li["+str(i)+"]/div/span").click()
                break
            i = i + 1

        #--Hotel Country
        driver.find_element_by_xpath(".//*[@id='s2id_address-country']/a/span[2]/b").click()
        driver.find_element_by_id("s2id_autogen4_search").clear()
        driver.find_element_by_id("s2id_autogen4_search").send_keys("United States")
        time.sleep(2)
        i = 1
        while 1:                                
            print driver.find_element_by_xpath("/html/body/div[9]/ul/li["+str(i)+"]/div/span").text
            h_name = driver.find_element_by_xpath("/html/body/div[9]/ul/li["+str(i)+"]/div/span").text
            if (h_name == "United States"):
                driver.find_element_by_xpath("/html/body/div[9]/ul/li["+str(i)+"]/div/span").click()
                break
            i = i + 1
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(5)
        element = driver.find_element_by_xpath("(//button[@type='button'])[2]")
        driver.execute_script("return arguments[0].scrollIntoView();", element)
        driver.execute_script("window.scrollBy(0, -50);")
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        driver.find_element_by_link_text(hotel_info.chain_hotel_name).click()
        time.sleep(5)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(5)
        driver.find_element_by_link_text("Preview").click()
        driver.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
   
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
