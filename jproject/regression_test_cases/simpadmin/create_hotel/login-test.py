# -*- coding: utf-8 -*-
# from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import unittest
import sys, os, time
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '..', "simpadmin_common_function"))
import create_data_common_function


class Logintest(unittest.TestCase):
    def setUp(self):
        # self.display = Display(visible=0, size=(1800, 900))
        # self.display.start()
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_adminlogin(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function 

        #-----Without UserName and Without Password---------
        admin.login_to_admin(self)

        self.assertEqual(d.find_element_by_xpath('/html/body/div/form/div[2]').text[:12], "Login failed")
        print "Without Username and Password :- Login Faild"

        #-----With UserName and Without Password---------
        admin.login_to_admin(self, username = "simpadmin")

        self.assertEqual(d.find_element_by_xpath('/html/body/div/form/div[2]').text[:12], "Login failed")
        print "With User nameand Without Password :- Login Faild"

        # Without UserName and With Password
        admin.login_to_admin(self, password = "simplehoteladmin!@#")

        self.assertEqual(d.find_element_by_xpath('/html/body/div/form/div[2]').text[:12], "Login failed")
        print "Without Username and With Password :- Login Faild"

        # With UserName and Wrong Password
        admin.login_to_admin(self, username = "simpadmin", password = "sdf")

        self.assertEqual(d.find_element_by_xpath('/html/body/div/form/div[2]').text[:12], "Login failed")
        print "With Correct Username and With Wrong Password :- Login Faild"

        # With Wrong UserName and With Password
        admin.login_to_admin(self, username = "sdf", password = "simplehoteladmin!@#")

        self.assertEqual(d.find_element_by_xpath('/html/body/div/form/div[2]').text[:12], "Login failed")
        print "With Wrong Username and With Correct Password :- Login Faild"

        # With UserName and With Password
        admin.login_to_admin(self, username = "simpadmin", password = "simplehoteladmin!@#")

        self.assertEqual(d.find_element_by_xpath(".//*[@id='navbar']/ul[2]/li[1]/a").text, "Logout")
        print "Admin User Login :- login successful"
        d.find_element_by_xpath(".//*[@id='navbar']/ul[2]/li[1]/a").click()

        # With Normal UserName and With Password
        admin.login_to_admin(self, username = hotel_info.user_name, password = hotel_info.password)

        self.assertEqual(d.find_element_by_xpath(".//*[@id='navbar']/ul[2]/li[1]/a").text, "Logout")
        print "Normal User Login :- login successful"
        # d.close()
        # self.display.stop()
    # @classmethod   
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
