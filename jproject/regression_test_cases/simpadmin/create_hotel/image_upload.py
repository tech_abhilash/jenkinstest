# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import sys
import os
import unittest, time
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '..', "simpadmin_common_function"))
import create_data_common_function

class UploadImages(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_uploadimages(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function 
        
        #-----Log in --------------------------
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        #-----Selecting Home page-----------------
        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text(" Home Page").click()

        #---- Uploading Image -----------------------------------------
        d.find_element_by_xpath(".//*[@id='addHero']").click()
        time.sleep(3)
        d.find_element_by_id("uploadphotos").click()
        time.sleep(3)
        element = d.find_element_by_xpath(".//*[@id='upload']/input")
        element.send_keys("/home/simplotel/work/qa-automation/regression_test_cases/image/image_1.jpg")
        time.sleep(3)
        element = d.find_element_by_xpath(".//*[@id='upload']/input")
        element.send_keys("/home/simplotel/work/qa-automation/regression_test_cases/image/image_2.jpg")
        time.sleep(3)
        element = d.find_element_by_xpath(".//*[@id='upload']/input")
        element.send_keys("/home/simplotel/work/qa-automation/regression_test_cases/image/image_3.jpg")
        time.sleep(3)
        element = d.find_element_by_xpath(".//*[@id='upload']/input")
        element.send_keys("/home/simplotel/work/qa-automation/regression_test_cases/image/image_4.jpg")
        time.sleep(10)
        d.find_element_by_xpath(".//*[@id='doneBtn']").click()
        time.sleep(5)

        #---------------------------------------- Checking Image Name ----------------------------------------
        wordList = d.find_element_by_xpath(".//*[@id='imageList']/li[1]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1_with_text = wordList[n-1].split("_")
        Image_Name_1 =  Image_Name_1_with_text[0]+"_"+Image_Name_1_with_text[1]
        print Image_Name_1
        self.assertEqual(Image_Name_1, "image_4")

        wordList = d.find_element_by_xpath(".//*[@id='imageList']/li[2]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2_with_text = wordList[n-1].split("_")
        Image_Name_2 =  Image_Name_2_with_text[0]+"_"+Image_Name_2_with_text[1]
        print Image_Name_2
        self.assertEqual(Image_Name_2, "image_3")

        wordList = d.find_element_by_xpath(".//*[@id='imageList']/li[3]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3_with_text = wordList[n-1].split("_")
        Image_Name_3 =  Image_Name_3_with_text[0]+"_"+Image_Name_3_with_text[1]
        print Image_Name_3
        self.assertEqual(Image_Name_3, "image_2")

        wordList = d.find_element_by_xpath(".//*[@id='imageList']/li[4]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4_with_text = wordList[n-1].split("_")
        Image_Name_4 =  Image_Name_4_with_text[0]+"_"+Image_Name_4_with_text[1]
        print Image_Name_4
        self.assertEqual(Image_Name_4, "image_1")
        time.sleep(10)    
        
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
