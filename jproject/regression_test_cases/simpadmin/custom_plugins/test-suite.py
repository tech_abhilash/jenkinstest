from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Custom Plugins Creation"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("custom_plugins_creation")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Custom Plugins Testing"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("custom_plugins_preview_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)