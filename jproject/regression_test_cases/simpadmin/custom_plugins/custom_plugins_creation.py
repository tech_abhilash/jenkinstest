# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("custom_plugins"), '../..', "config"))
import hotel_info
import custom_plugins_elements_path
sys.path.append(os.path.join(os.path.dirname("custom_plugins"), '..', "simpadmin_common_function"))
import create_data_common_function


class TestCustompluginsCreation(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_custom_plugins_creation(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        c = custom_plugins_elements_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text("  Custom Plugins").click()

        #-------------Creating Custom Plugins------------------------------------------

        driver.find_element_by_xpath(c.add_plugin_path).click()
        time.sleep(3)
        driver.find_element_by_xpath(c.plugin_name_path).clear()
        driver.find_element_by_xpath(c.plugin_name_path).send_keys(c.custom_plugin_name)
        driver.find_element_by_xpath(c.plugin_key_path).clear()
        driver.find_element_by_xpath(c.plugin_key_path).send_keys(c.custom_plugin_key)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(3)
        # driver.find_element_by_xpath(c.html_editor_path).click()
        # driver.find_element_by_xpath(c.html_editor_path).clear()
        driver.find_element_by_xpath(c.html_editor_path).send_keys(c.custom_plugin_text)
        driver.find_element_by_xpath(c.save_button_path).click()
        time.sleep(10)


        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text(" Home Page").click()
        time.sleep(2)
        driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/ul/li/a").click()
        driver.find_element_by_link_text("Custom Plugin").click()
        driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div[11]/div/div/div/div[2]/input[2]").send_keys(custom_plugins_elements_path.custom_plugin_key)
        # name = dri
        # i = 1
        # j = 1
        # while 1:
        #     path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div["+str(i)+"]/div/div/div/div[2]/input[2]"
        #     x = self.check_exists_by_xpath(path)
        #     if (x == True):
        #         i = i + 1
        #     else:
        #         i = i - 1
        #         driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div["+str(i)+"]/div/div/div/div[2]/input[2]").clear()
        #         driver.find_element_by_xpath("/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div["+str(i)+"]/div/div/div/div[2]/input[2]").send_keys(custom_plugins_elements_path.custom_plugin_key)
        #         break                         
        # #-------------Save and Generation------------------------------
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        driver.find_element_by_link_text("Preview").click()
        driver.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
