# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("booking_engine_room_test"), '..', ''))
import hotel_info
import custom_plugins_elements_path


class TestCustompluginsPreview(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_exists_by_id(self, elemenr_id):
        try:
            self.driver.find_element_by_id(elemenr_id)
        except NoSuchElementException:
            return False
        return True

    def test_custom_plugins_preview(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.maximize_window()
        time.sleep(5)
        elemenr_id = "widget-" + custom_plugins_elements_path.custom_plugin_key + ""
        x = self.check_exists_by_id(elemenr_id)
        if ( x == True):
            print driver.find_element_by_id(elemenr_id).text
            custom_plugins_elements_path.custom_plugin_text
            self.assertEqual(driver.find_element_by_id(elemenr_id).text, custom_plugins_elements_path.custom_plugin_text)
        else:
            print "Not found"

    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
