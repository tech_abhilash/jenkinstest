# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("navigation"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("navigation"), '..', "simpadmin_common_function"))
import create_data_common_function

class TestNavigation(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_navigation(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text("  Navigation").click()

        #-------------Creating New Room------------------------------------------
        i = 1
        while 1:
            path = ".//*[@id='sortable-pages']/li["+str(i)+"]/div/div[1]/div/div/div/ins"
            if (self.check_exists_by_xpath(path) == True):
                d.find_element_by_xpath(".//*[@id='sortable-pages']/li["+str(i)+"]/div/div[1]/div/div/div/ins").click()
                d.find_element_by_xpath(".//*[@id='sortable-mobile-pages']/li["+str(i)+"]/div/div[1]/div/div/div/ins").click()
                d.find_element_by_xpath(".//*[@id='sortable-footer-pages']/li["+str(i)+"]/div/div[1]/div/div/div/ins").click()
                i = i + 1
            else:
                break                          

        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(30)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
