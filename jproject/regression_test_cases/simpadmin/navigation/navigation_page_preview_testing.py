# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("navigation"), '../..', "config"))
import hotel_info
import navigation_page_elements_path


class TestNavigationPreview(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def test_navigation(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.maximize_window()
        time.sleep(5)
        n = navigation_page_elements_path
        if (self.is_element_present("xpath", n.header_path)):
            print "Header not working"
        else:
            if (self.is_element_present("xpath", n.footer_path)):
                print "Footer not working"
            else:
                print "Done"



        driver.get(self.base_url + "sitemap.html")
        time.sleep(5)
        if (self.is_element_present("xpath", n.site_map_heading_path)): 
            print "Done"
        else:
            print "Site Map Not workong"
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
