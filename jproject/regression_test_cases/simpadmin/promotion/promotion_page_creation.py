# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import sys
import os
sys.path.append(os.path.join(os.path.dirname("promotion"), '../..', "config"))
import hotel_info
import unittest, time, re
import promotion_info_data
import promotion_page_elements_path
sys.path.append(os.path.join(os.path.dirname("promotion"), '..', "simpadmin_common_function"))
import create_data_common_function


class PromotionPageCreation(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_create_page(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        p = promotion_page_elements_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text("  Promotions").click()

        #-------------Creating New Promotions------------------------------------------

        d.find_element_by_link_text("Add Promotion").click()
        d.find_element_by_id("pagename").send_keys(promotion_info_data.Promotion_Name)
        d.find_element_by_id("promooneline").send_keys(promotion_info_data.One_Line_Description)
       


        #------------Adding Text to Short and Image of the Promotions-----------------
        ActionChains(d).move_to_element(d.find_element_by_xpath(p.Short_Description_Image_Path + "div/img")).perform()
        d.find_element_by_xpath(p.Short_Description_Image_Path + "a[1]/span").click()

        #----------------------------------------Getting Image Name-----------------------------------------------------------------
        d.find_element_by_link_text("All Photos").click()
        time.sleep(3)
        
        wordList = d.find_element_by_xpath(p.Image_Path + "li[1]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]

        wordList = d.find_element_by_xpath(p.Image_Path + "li[2]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]

        wordList = d.find_element_by_xpath(p.Image_Path + "li[3]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]

        wordList = d.find_element_by_xpath(p.Image_Path + "li[4]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]

        #----------------Saving Image Name----------------------------------
        with open("image_names.txt",'w') as f:
            f.write(Image_Name_1 +" "+Image_Name_2+" "+Image_Name_3+" "+Image_Name_4)
            f.close()

        
        time.sleep(3)
        d.find_element_by_xpath(p.Image_Path + "li[1]/div").click()
        time.sleep(3)
        d.find_element_by_xpath(p.Image_Selecter_Button_Path).click()
        
    
     #------------Adding Text to Short and Detailed Description of the Room-----------------
        
        d.switch_to_frame(d.find_element_by_xpath(p.Short_Description_Iframe_Path))
        d.find_element_by_xpath("/html/body").send_keys(promotion_info_data.Promotion_Short_Description)
        d.switch_to_default_content()
        d.find_element_by_xpath(p.Submit_Button_Path).click()
        time.sleep(10)
        #  ------------Adding User Content-----------------------
        # ------------Adding H1 and H2-------------------------------
        admin.adding_h1(self, p.H1_Path, p.H1_Done_Button_Path)
        admin.adding_h2(self, p.H2_Select_Button_Path, p.H2_Path, p.H2_Done_Button_Path)

        #-----------------------Only Text -------------------
        admin.adding_single_text(self, p.Single_Text_Iframe_Path, p.Single_Text_Done_Button_Path)
        admin.adding_two_text(self, p.Two_Text_Select_Button_Path, p.Two_Text_Iframe_Path_left, p.Two_Text_Iframe_Path_right, p.Two_Text_Done_Button_Path)
        admin.adding_three_text(self, p.Three_Text_Select_Button_Path, p.Three_Text_Iframe_Path_left, p.Three_Text_Iframe_Path_middle, p.Three_Text_Iframe_Path_right, p.Three_Text_Done_Button_Path)

        #----------------------------------------Adding only Image ----------------------------------------------------------------
        admin.adding_single_image(self, p.Image_Path, p.Image_Selecter_Button_Path, p.Single_Image_Done_Button_Path)
        admin.adding_two_image(self, p.Two_Image_Select_Button_Path, p.Image_Path, p.Image_Selecter_Button_Path, p.Two_Image_Done_Button_Path)
        admin.adding_three_image(self, p.Three_Image_Select_Button_Path, p.Image_Path, p.Image_Selecter_Button_Path, p.Three_Image_Done_Button_Path)

        #----------------------------------------Adding Image and Text----------------------------------------------------------------
        admin.adding_single_image_and_text_right(self, p.Single_Image_Text_Right_Iframe_Path, p.Image_Path, p.Image_Selecter_Button_Path, p.Single_Image_Text_Right_Done_Button_Path)
        admin.adding_single_image_and_text_left(self, p.Single_Image_Text_Left_Select_Button_Path, p.Single_Image_Text_Left_Iframe_Path, p.Image_Path, p.Image_Selecter_Button_Path, p.Single_Image_Text_Left_Done_Button_Path)
        admin.adding_single_image_and_text_center(self, p.Single_Image_Text_Center_Select_Button_Path, p.Single_Image_Text_Center_Iframe_Path, p.Image_Path, p.Image_Selecter_Button_Path, p.Single_Image_Text_Center_Done_Button_Path)
        iframe_paths = [p.Two_Image_Text_Iframe_Path_left,p.Two_Image_Text_Iframe_Path_right]
        admin.adding_two_image_and_text(self, p.Two_Image_Text_Select_Button_Path, iframe_paths, p.Image_Path, p.Image_Selecter_Button_Path, p.Two_Image_Text_Done_Button_Path)
        iframe_paths = [p.Three_Image_Text_Iframe_Path_left,p.Three_Image_Text_Iframe_Path_middle,p.Three_Image_Text_Iframe_Path_right]
        admin.adding_three_image_and_text(self, p.Three_Image_Text_Select_Button_Path, iframe_paths, p.Image_Path, p.Image_Selecter_Button_Path, p.Three_Image_Text_Done_Button_Path)
        iframe_paths = [p.Two_Image_text_and_Full_Image_Right_Iframe_Path_left,p.Two_Image_text_and_Full_Image_Right_Iframe_Path_middle]
        admin.adding_two_image_text_and_full_image_right(self, p.Two_Image_text_and_Full_Image_Right_Button_Path, iframe_paths, p.Image_Path, p.Image_Selecter_Button_Path, p.Two_Image_text_and_Full_Image_Right_Done_Button_Path)
        iframe_paths = [p.Two_Image_text_and_Full_Image_middle_Iframe_Path_left,p.Two_Image_text_and_Full_Image_middle_Iframe_Path_right]
        admin.adding_two_image_text_and_full_image_middle(self, p.Two_Image_text_and_Full_Image_middle_Button_Path, iframe_paths, p.Image_Path, p.Image_Selecter_Button_Path, p.Two_Image_text_and_Full_Image_middle_Done_Button_Path)
        admin.adding_single_image_text_and_full_image_right(self, p.Single_Image_Text_and_Full_Image_Right_Button_Path, p.Single_Image_Text_and_Full_Image_Right_Iframe_Path, p.Image_Path, p.Image_Selecter_Button_Path, p.Single_Image_Text_and_Full_Image_Right_Done_Button_Path)
        admin.adding_single_image_text_and_full_image_left(self, p.Single_Image_Text_and_Full_Image_left_Button_Path, p.Single_Image_Text_and_Full_Image_left_Iframe_Path, p.Image_Path, p.Image_Selecter_Button_Path, p.Single_Image_Text_and_Full_Image_left_Done_Button_Path)
        admin.adding_full_image_and_three_image_right(self, p.Full_Image_and_Three_Image_Right, p.Image_Path, p.Image_Selecter_Button_Path, p.Full_Image_and_Three_Image_Right_Done_Button_Path)
        
        # ----------------------------------------Adding Image Slider----------------------------------------------------------------
        admin.adding_image_slider(self, p.Image_Slider_Add_Image_Path, p.Image_Path, p.Image_Selecter_Button_Path)
        
        # ----------------------------------------Adding Book Button----------------------------------------------------------------
        admin.adding_book_now_button(self, promotion_info_data.Booking_Button_Name, p.Book_Button_Label_Path, p.Book_Button_Done_Path)
        admin.adding_other_button(self, promotion_info_data.Booking_Button_Name, promotion_info_data.Booking_Button_Link, p.Other_Button_Link_Path, p.Other_Button_Label_Path, p.Other_Button_Done_Path)

        
        #------------------Adding Predefined Content-------------------------------
        admin.adding_predefined_content(self, p.Predefined_Content_Path)

        
        #------------------Adding SEO Content-------------------------------
        d.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        d.find_element_by_id("meta-title").clear()
        d.find_element_by_id("meta-title").send_keys(promotion_info_data.Page_Title)
        d.find_element_by_id("meta-description").clear()
        d.find_element_by_id("meta-description").send_keys(promotion_info_data.Page_Description)
        d.find_element_by_id("meta-keywords").clear()
        d.find_element_by_id("meta-keywords").send_keys(promotion_info_data.Page_Keywords)
        d.find_element_by_xpath("//button[@type='submit']").click()
        
        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
