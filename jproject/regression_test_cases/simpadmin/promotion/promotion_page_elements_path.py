Add_Hero_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/div[2]/a"
Image_Path = ".//*[@id='imageList']/"
Image_Selecter_Button_Path = ".//*[@id='doneBtn']"
Image_Cancel_Button_Path = "/html/body/div[7]/div[2]/div/div[2]/div[2]/button[1]"
	

Add_Content_Path = ".//*[@id='page-content']/section/section/div/div[2]/div[4]/div[2]/ul/li/a/span[1]"

#------------Path For Page Headers------------------------------
#------------H1-------------------------------------------------
		
H1_Path = ".//*[@id='widgetblock0']/div[3]/div/input[8]"
H1_Done_Button_Path = ".//*[@id='widgetblock0']/div[3]/div/div/button"
#------------H2-------------------------------------------------
H2_Path = ".//*[@id='widgetblock1']/div[3]/div/input[8]"
H2_Select_Button_Path = ".//*[@id='widgetblock1']/div[1]/div/div[2]/div/div/button[2]"
H2_Done_Button_Path = ".//*[@id='widgetblock1']/div[3]/div/div/button"
#---------------------------------------------------------------
  								
Short_Description_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div/div/div/div[1]/label/div[2]/"
Short_Description_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div/div/div/div[2]/label/div/div/div/iframe"


#----------------------Only Text---------------------------

Single_Text_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[3]/div[3]/div/div[1]/div/div/iframe"
Single_Text_Done_Button_Path = ".//*[@id='widgetblock2']/div[3]/div/div[2]/button"

Two_Text_Iframe_Path_left = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[4]/div[3]/div/div[1]/div[1]/div/div/div/iframe"
Two_Text_Iframe_Path_right = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[4]/div[3]/div/div[1]/div[2]/div/div/div/iframe"
Two_Text_Select_Button_Path = ".//*[@id='widgetblock3']/div[1]/div/div[2]/div/div/button[2]"
Two_Text_Done_Button_Path = ".//*[@id='widgetblock3']/div[3]/div/div[2]/button"

Three_Text_Iframe_Path_left = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[5]/div[3]/div/div[1]/div[1]/div/div/div/iframe"
Three_Text_Iframe_Path_middle = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[5]/div[3]/div/div[1]/div[2]/div/div/div/iframe"
Three_Text_Iframe_Path_right = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[5]/div[3]/div/div[1]/div[3]/div/div/div/iframe"
Three_Text_Select_Button_Path = ".//*[@id='widgetblock4']/div[1]/div/div[2]/div/div/button[3]"
Three_Text_Done_Button_Path = ".//*[@id='widgetblock4']/div[3]/div/div[2]/button"

#------------------Only Image--------------------------------------
Single_Image_Done_Button_Path = ".//*[@id='widgetblock5']/div[3]/div/div[2]/button"

Two_Image_Select_Button_Path = ".//*[@id='widgetblock6']/div[1]/div/div[2]/div[1]/div/button[2]"
Two_Image_Done_Button_Path = ".//*[@id='widgetblock6']/div[3]/div/div[2]/button"

Three_Image_Select_Button_Path = ".//*[@id='widgetblock7']/div[1]/div/div[2]/div[1]/div/button[3]"
Three_Image_Done_Button_Path = ".//*[@id='widgetblock7']/div[3]/div/div[2]/button"

#---------------------Image and Text------------------------------
Single_Image_Text_Right_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[9]/div[3]/div/div[1]/div[1]/div/div/div/iframe"
Single_Image_Text_Right_Done_Button_Path = ".//*[@id='widgetblock8']/div[3]/div[2]/div[2]/button"

Single_Image_Text_Left_Select_Button_Path = ".//*[@id='widgetblock9']/div[1]/div/div[2]/div[1]/div/button[2]"
Single_Image_Text_Left_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[10]/div[3]/div/div[1]/div[2]/div/div/div/iframe"
Single_Image_Text_Left_Done_Button_Path = ".//*[@id='widgetblock9']/div[3]/div[2]/div[2]/button"

Single_Image_Text_Center_Select_Button_Path = ".//*[@id='widgetblock10']/div[1]/div/div[2]/div[1]/div/button[3]"
Single_Image_Text_Center_Iframe_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[11]/div[3]/div/div[1]/div[2]/div/div/div/iframe"
Single_Image_Text_Center_Done_Button_Path = ".//*[@id='widgetblock10']/div[3]/div/div[2]/button"

Two_Image_Text_Select_Button_Path = ".//*[@id='widgetblock11']/div[1]/div/div[2]/div[1]/div/button[4]"
Two_Image_Text_Iframe_Path_left = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[12]/div[3]/div/div[1]/div[2]/div[1]/div/div/div/iframe"
Two_Image_Text_Iframe_Path_right = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[12]/div[3]/div/div[1]/div[2]/div[2]/div/div/div/iframe"
Two_Image_Text_Done_Button_Path = ".//*[@id='widgetblock11']/div[3]/div/div[2]/button"

Three_Image_Text_Select_Button_Path = ".//*[@id='widgetblock12']/div[1]/div/div[2]/div[1]/div/button[5]"
Three_Image_Text_Iframe_Path_left = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[13]/div[3]/div/div[1]/div[2]/div[1]/div/div/div/iframe"
Three_Image_Text_Iframe_Path_middle = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[13]/div[3]/div/div[1]/div[2]/div[2]/div/div/div/iframe"
Three_Image_Text_Iframe_Path_right = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[4]/div[2]/div/div[13]/div[3]/div/div[1]/div[2]/div[3]/div/div/div/iframe"
Three_Image_Text_Done_Button_Path = ".//*[@id='widgetblock12']/div[3]/div/div[2]/button"
												   
Two_Image_text_and_Full_Image_Right_Button_Path = ".//*[@id='widgetblock13']/div[1]/div/div[2]/div[1]/div/button[6]"
Two_Image_text_and_Full_Image_Right_Iframe_Path_left = ".//*[@id='widgetblock13']/div[3]/div/div[1]/div[1]/div[1]/div/div/div/div/iframe"
Two_Image_text_and_Full_Image_Right_Iframe_Path_middle = ".//*[@id='widgetblock13']/div[3]/div/div[1]/div[1]/div[2]/div/div/div/div/iframe"
Two_Image_text_and_Full_Image_Right_Done_Button_Path = ".//*[@id='widgetblock13']/div[3]/div/div[2]/button"
													
Two_Image_text_and_Full_Image_middle_Button_Path = ".//*[@id='widgetblock14']/div[1]/div/div[2]/div[1]/div/button[7]"
Two_Image_text_and_Full_Image_middle_Iframe_Path_left = ".//*[@id='widgetblock14']/div[3]/div/div[1]/div[1]/div[1]/div/div/div/div[1]/iframe"
Two_Image_text_and_Full_Image_middle_Iframe_Path_right = ".//*[@id='widgetblock14']/div[3]/div/div[1]/div[1]/div[3]/div/div/div/div[1]/iframe"
Two_Image_text_and_Full_Image_middle_Done_Button_Path = ".//*[@id='widgetblock14']/div[3]/div/div[2]/button"

Single_Image_Text_and_Full_Image_Right_Button_Path = ".//*[@id='widgetblock15']/div[1]/div/div[2]/div[1]/div/button[8]"
Single_Image_Text_and_Full_Image_Right_Iframe_Path = ".//*[@id='widgetblock15']/div[3]/div/div[1]/div[1]/div[1]/div/div/div/div[1]/iframe"
Single_Image_Text_and_Full_Image_Right_Done_Button_Path = ".//*[@id='widgetblock15']/div[3]/div/div[2]/button"

Single_Image_Text_and_Full_Image_left_Button_Path = ".//*[@id='widgetblock16']/div[1]/div/div[2]/div[1]/div/button[9]"
Single_Image_Text_and_Full_Image_left_Iframe_Path = ".//*[@id='widgetblock16']/div[3]/div/div[1]/div[1]/div[2]/div/div/div/div[1]/iframe"
Single_Image_Text_and_Full_Image_left_Done_Button_Path = ".//*[@id='widgetblock16']/div[3]/div/div[2]/button"
										
Full_Image_and_Three_Image_Right = ".//*[@id='widgetblock17']/div[1]/div/div[2]/div[1]/div/button[10]"
Full_Image_and_Three_Image_Right_Done_Button_Path = ".//*[@id='widgetblock17']/div[3]/div/div[2]/button"

#-----------------Image Slider Path-------------------------------
Image_Slider_Add_Image_Path = ".//*[@id='addCarouselImage-18']"



#----------------------Book Button Path--------------------------

Book_Button_Label_Path = ".//*[@id='widgetblock19']/div[3]/div/input[8]"
Book_Button_Done_Path = ".//*[@id='widgetblock19']/div[3]/div/div[2]/button"
						
#---------------------Other Button Path--------------------------
Other_Button_Label_Path = ".//*[@id='widgetblock20']/div[3]/div/input[8]"
Other_Button_Link_Path = ".//*[@id='widgetblock20']/div[3]/div/input[9]"
Other_Button_Done_Path = ".//*[@id='widgetblock20']/div[3]/div/div[2]/button"

#-------------------Predefined Content Path--------------------------
Predefined_Content_Path = ".//*[@id='addContent']/span[1]"




#-----------------Submit Button Path------------------------------
Submit_Button_Path = "/html/body/div[1]/aside[2]/form/div/div/div[2]/button"
#------------------After Save----------------------------------
                         
After_Save_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div/div/div/div[1]/label/div[2]/div "   
After_Save_Image_select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div/div/div/div[1]/label/div[2]/a[1]/span"
Promotion_Name_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[1]/input"
Promotion_One_Line_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[2]/input"
				 		
after_save_h1 = ".//*[@id='widgetblock0']/div[2]/div/div/h1"
after_save_h2 = ".//*[@id='widgetblock1']/div[2]/div/div/h2"
after_save_single_text = ".//*[@id='widgetblock2']/div[2]/div/p"
after_save_two_text_left = ".//*[@id='widgetblock3']/div[2]/div[1]/p"
after_save_two_text_right = ".//*[@id='widgetblock3']/div[2]/div[2]/p"
after_save_three_text_left = ".//*[@id='widgetblock4']/div[2]/div[1]/p"
after_save_three_text_middle = ".//*[@id='widgetblock4']/div[2]/div[2]/p"
after_save_three_text_right = ".//*[@id='widgetblock4']/div[2]/div[3]/p"
after_save_single_image = ".//*[@id='widgetblock5']/div[2]/div/div/img"
after_save_two_image_left = ".//*[@id='widgetblock6']/div[2]/div[1]/div/img"
after_save_two_image_right = ".//*[@id='widgetblock6']/div[2]/div[2]/div/img"
after_save_three_image_left = ".//*[@id='widgetblock7']/div[2]/div[1]/div/img"
after_save_three_image_middle = ".//*[@id='widgetblock7']/div[2]/div[2]/div/img"
after_save_three_image_right = ".//*[@id='widgetblock7']/div[2]/div[3]/div/img"
after_save_single_image_text_left_text = ".//*[@id='widgetblock8']/div[2]/div[1]/p"
after_save_single_image_text_left_image = ".//*[@id='widgetblock8']/div[2]/div[2]/div/img"
after_save_single_image_text_right_text = ".//*[@id='widgetblock9']/div[2]/div[2]/p"
after_save_single_image_text_right_image = ".//*[@id='widgetblock9']/div[2]/div[1]/div/img"
after_save_single_image_text_center_text = ".//*[@id='widgetblock10']/div[2]/div[2]/p"
after_save_single_image_text_center_image = ".//*[@id='widgetblock10']/div[2]/div[1]/div/div/img" 
after_save_two_image_text_left_text = ".//*[@id='widgetblock11']/div[2]/div[2]/div[1]/p"
after_save_two_image_text_left_image = ".//*[@id='widgetblock11']/div[2]/div[1]/div[1]/div/img"
after_save_two_image_text_right_text = ".//*[@id='widgetblock11']/div[2]/div[2]/div[2]/p"
after_save_two_image_text_right_image = ".//*[@id='widgetblock11']/div[2]/div[1]/div[2]/div/img"
after_save_three_image_text_left_text = ".//*[@id='widgetblock12']/div[2]/div[2]/div[1]/p"
after_save_three_image_text_left_image = ".//*[@id='widgetblock12']/div[2]/div[1]/div[1]/div/img"
after_save_three_image_text_middle_text = ".//*[@id='widgetblock12']/div[2]/div[2]/div[2]/p"
after_save_three_image_text_middle_image = ".//*[@id='widgetblock12']/div[2]/div[1]/div[2]/div/img"
after_save_three_image_text_right_text = ".//*[@id='widgetblock12']/div[2]/div[2]/div[3]/p"
after_save_three_image_text_right_image = ".//*[@id='widgetblock12']/div[2]/div[1]/div[3]/div/img"


after_save_two_image_text_and_full_image_right_left_text = ".//*[@id='widgetblock13']/div[2]/div[1]/div[2]/p"
after_save_two_image_text_and_full_image_right_left_image = ".//*[@id='widgetblock13']/div[2]/div[1]/div[1]/img"
after_save_two_image_text_and_full_image_right_middle_text = ".//*[@id='widgetblock13']/div[2]/div[2]/div[2]/p"
after_save_two_image_text_and_full_image_right_middle_image = ".//*[@id='widgetblock13']/div[2]/div[2]/div[1]/img"
after_save_two_image_text_and_full_image_right_right_image = ".//*[@id='widgetblock13']/div[2]/div[3]/div"

after_save_two_image_text_and_full_image_middle_left_text = ".//*[@id='widgetblock14']/div[2]/div[1]/div[2]/p"
after_save_two_image_text_and_full_image_middle_left_image = ".//*[@id='widgetblock14']/div[2]/div[1]/div[1]/img"
after_save_two_image_text_and_full_image_middle_middle_image = ".//*[@id='widgetblock14']/div[2]/div[2]/div"
after_save_two_image_text_and_full_image_middlle_right_image = ".//*[@id='widgetblock14']/div[2]/div[3]/div[1]/img"
after_save_two_image_text_and_full_image_middlle_right_text = ".//*[@id='widgetblock14']/div[2]/div[3]/div[2]/p"

after_save_single_image_text_and_fulll_image_right_left_image = ".//*[@id='widgetblock15']/div[2]/div[1]/div[1]/img"
after_save_single_image_text_and_fulll_image_right_left_text = ".//*[@id='widgetblock15']/div[2]/div[1]/div[2]/p"
after_save_single_image_text_and_fulll_image_right_right_image = ".//*[@id='widgetblock15']/div[2]/div[2]/div"


after_save_single_image_text_and_fulll_image_left_left_image = ".//*[@id='widgetblock16']/div[2]/div[1]/div"
after_save_single_image_text_and_fulll_image_left_right_text = ".//*[@id='widgetblock16']/div[2]/div[2]/div[2]/p"
after_save_single_image_text_and_fulll_image_left_right_image = ".//*[@id='widgetblock16']/div[2]/div[2]/div[1]/img"

after_save_full_image_and_three_image_right_left_image = ".//*[@id='widgetblock17']/div[2]/div[1]/div"
after_save_full_image_and_three_image_right_right_image1 = ".//*[@id='widgetblock17']/div[2]/div[2]/div/div[1]/div/img"
after_save_full_image_and_three_image_right_right_image2 = ".//*[@id='widgetblock17']/div[2]/div[2]/div/div[2]/div/img"
after_save_full_image_and_three_image_right_right_image3 = ".//*[@id='widgetblock17']/div[2]/div[2]/div/div[3]/div/img"

after_save_image_slider = ".//*[@id='pic-18-0']/img"


Only_Text_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[2]/div/"
Only_Image_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[2]/div/"
Image_Text_Select_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[2]/div/"
Image_Slider_Image_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[3]/div[2]/div/div[14]/div[2]/div/div/div[1]/div/div/div/ul/"
Book_Button_Select_Path = ".//*[@id='widgetblock14']/div[2]/div"
Other_Button_Select_Path = ".//*[@id='widgetblock15']/div[2]/div"
                
Weather_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[1]/div/div/div[3]/div/div/div/h4/b"
Local_Time_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[2]/div/div/div[3]/div/div/div/h4/b"
Facebook_Feed_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[3]/div/div/div/div[1]/div[3]/div/div/div/h4/b"
TripAdvisor_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[4]/div/div/div/div[1]/div[3]/div/div/div/h4/b"
Social_Connect_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[5]/div/div/div/div/div[3]/div/div/div/h4/b"
Home_Page_Promotions_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[6]/div/div/div[3]/div/div/div/h4/b"
Location_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[7]/div/div/div[3]/div/div/div/h4/b"
Contact_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[8]/div/div/div[3]/div/div/div/h4/b"
Newsletter_Signup_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[9]/div/div/div[3]/div/div/div/h4/b"
Promotions_Slider_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[10]/div/div/div[3]/div/div/div/h4/b"
Custom_Plugin_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[5]/div[2]/div[11]/div/div/div/div[1]/div[3]/div/div/div/h4/b"


Page_Title_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div/div[1]/label/input"
Page_Description_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div/div[2]/label/textarea" 
Page_Keywords_Path = "/html/body/div[1]/aside[2]/form/section/section/div/div[2]/div[6]/div[2]/div/div[3]/label/input"

        
        
#---------Preview Path-----------------------------------------

Preview_Heading_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[1]/div/h1"
Preview_Nav_Heading_Path = "/html/body/div/div[1]/div/div/div[2]/div[1]/div/ul/li/a"

Preview_Image_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[18]/div/div/div[1]/div[2]/div/"
Preview_Detailed_Description_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[4]/div/p"

Preview_H1_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[1]/div/h1"
Preview_H2_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[2]/div/h2"
Preview_Single_Text_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[3]/div/p"
Preview_Two_Text = "/html/body/div/div[1]/div/div/div[1]/div/div/div[4]/"
Preview_Three_Text = "/html/body/div/div[1]/div/div/div[1]/div/div/div[5]/"


Preview_Only_Image_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/"
Preview_Image_Text_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/"
Preview_Image_Slider_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[19]/div/div/div[1]/div[2]/div/"
Preview_Book_Button_Path = "/html/body/div[1]/div[1]/div/div/div[1]/div/div/div[20]/div/div/form/input[1]"
Preview_Other_button_Path = "/html/body/div/div[1]/div/div/div[1]/div/div/div[21]/div/a"
                        
Preview_Weather_Path = "/html/body/div/div[1]/div/div/div[2]/div[2]/p/span"
Preview_Local_Time_Path = "/html/body/div/div[1]/div/div/div[2]/div[3]/p/span"
Preview_TripAdvisor_Path = "/html/body/div/div[1]/div/div/div[2]/div[5]/div/div/div/div[1]/dl/dt/a/img"
Preview_Social_Connect_Path = "/html/body/div/div[1]/div/div/div[2]/div[6]/ul"
Preview_Location_Path = "/html/body/div/div[1]/div/div/div[2]/div[7]/p/span"
Preview_Contact_Us_Path = "/html/body/div/div[1]/div/div/div[2]/div[8]/p/span"
Preview_Newsletter_Signup_Path = "/html/body/div/div[1]/div/div/div[2]/div[9]/p/span"
Preview_Promotion_Slider_Path = "/html/body/div/div[1]/div/div/div[2]/div[10]/div/p/span"
Preview_Facebook_Path = "/html/body/div/div[1]/div/div/div[2]/div[4]/div/div/div/span/iframe"
