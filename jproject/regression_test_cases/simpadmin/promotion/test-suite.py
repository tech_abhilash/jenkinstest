from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Promotion Page Creation"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("promotion_page_creation")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing Promotion Page"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("promotion_page_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing Promotion Page Preview"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("promotion_page_preview_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)