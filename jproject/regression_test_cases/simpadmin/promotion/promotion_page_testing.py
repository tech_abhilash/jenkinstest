# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("promotion"), '../..', "config"))
import hotel_info
import promotion_info_data
import promotion_page_elements_path
sys.path.append(os.path.join(os.path.dirname("custom_page"), '..', "simpadmin_common_function"))
import testing_data_common_function
import create_data_common_function

class PromotionPageTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_promation_page(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        p = promotion_page_elements_path
        admin = create_data_common_function
        test_admin = testing_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text("  Promotions").click()
        time.sleep(10)
        i = 1
        while 1:
            p_name = d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[3]/ol/li/ol/li["+str(i)+"]").text
            if (p_name == promotion_info_data.Promotion_Name):
                d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[3]/ol/li/ol/li["+str(i)+"]").click()
                d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[3]/ol/li/ol/li["+str(i)+"]/div/div/a[1]/i").click()
                break
            i = i + 1 

        #----------------------------------------Testing Promotion Name ----------------------------------------------------------------
    
        print d.find_element_by_xpath(p.Promotion_Name_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(p.Promotion_Name_Path).get_attribute("value"), promotion_info_data.Promotion_Name)

        print d.find_element_by_xpath(p.Promotion_One_Line_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(p.Promotion_One_Line_Path).get_attribute("value"), promotion_info_data.One_Line_Description)

      
        # -----------------------Testing Add Content----------------------
        with open("image_names.txt",'r') as f:
            wordList_1 = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList_1[0]
        Image_Name_2 = wordList_1[1]
        Image_Name_3 = wordList_1[2]
        Image_Name_4 = wordList_1[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4

        #----------Testing short Description and Image------------------------
        wordList = d.find_element_by_xpath(".//*[@id='pic-thumb']/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        d.switch_to_frame(d.find_element_by_xpath(p.Short_Description_Iframe_Path))
        print d.find_element_by_xpath("/html/body").text
        self.assertEqual(d.find_element_by_xpath('/html/body').text, promotion_info_data.Promotion_Short_Description)
        d.switch_to_default_content()

        
          # -----------------------Testing Add Content----------------------
        #------------------------H1 and H2 Testing------------------------
        test_admin.testing_h1(self, p.after_save_h1)
        test_admin.testing_h2(self, p.after_save_h2)
        
        #-----------------------Only Text Widget Testing-------------------
        test_admin.testing_single_text(self, p.after_save_single_text)
        test_admin.testing_two_text(self, p.after_save_two_text_left, p.after_save_two_text_right)
        test_admin.testing_three_text(self, p.after_save_three_text_left, p.after_save_three_text_middle, p.after_save_three_text_right)

        #---------------Testing Single Image---------------------
        test_admin.testing_single_image(self, p.after_save_single_image, wordList_1)
        test_admin.testing_two_image(self, p.after_save_two_image_left, p.after_save_two_image_right, wordList_1)
        test_admin.testing_three_image(self, p.after_save_three_image_left, p.after_save_three_image_middle, p.after_save_three_image_right, wordList_1)

        #----------------------------------------Testing Single Image and text---------------------------------------------------------------
        test_admin.testing_single_image_and_text_left(self, p.after_save_single_image_text_left_text, p.after_save_single_image_text_left_image, wordList_1)
        test_admin.testing_single_image_and_text_right(self, p.after_save_single_image_text_right_text, p.after_save_single_image_text_right_image, wordList_1)
        test_admin.testing_single_image_and_text_center(self, p.after_save_single_image_text_center_text, p.after_save_single_image_text_center_image, wordList_1)
        test_admin.testing_two_image_and_text(self, p.after_save_two_image_text_left_text, p.after_save_two_image_text_left_image, p.after_save_two_image_text_right_text, p.after_save_two_image_text_right_image, wordList_1)
        
        three_image_text_widget_text_path = [p.after_save_three_image_text_left_text, p.after_save_three_image_text_middle_text, p.after_save_three_image_text_right_text]
        three_image_text_widget_image_path = [p.after_save_three_image_text_left_image, p.after_save_three_image_text_middle_image, p.after_save_three_image_text_right_image]
        test_admin.testing_three_image_and_text(self, three_image_text_widget_text_path, three_image_text_widget_image_path, wordList_1)


        two_image_text_and_full_image_right_widget_text_path = [p.after_save_two_image_text_and_full_image_right_left_text, p.after_save_two_image_text_and_full_image_right_middle_text]
        two_image_text_and_full_image_right_widget_image_path = [p.after_save_two_image_text_and_full_image_right_left_image, p.after_save_two_image_text_and_full_image_right_middle_image, p.after_save_two_image_text_and_full_image_right_right_image]
        test_admin.testing_two_image_text_and_full_image_right(self, two_image_text_and_full_image_right_widget_text_path, two_image_text_and_full_image_right_widget_image_path, wordList_1)

        two_image_text_and_full_image_middle_widget_text_path = [p.after_save_two_image_text_and_full_image_middle_left_text, p.after_save_two_image_text_and_full_image_middlle_right_text]
        two_image_text_and_full_image_middle_widget_image_path = [p.after_save_two_image_text_and_full_image_middle_left_image, p.after_save_two_image_text_and_full_image_middle_middle_image, p.after_save_two_image_text_and_full_image_middlle_right_image]
        test_admin.testing_two_image_text_and_full_image_middle(self, two_image_text_and_full_image_middle_widget_text_path, two_image_text_and_full_image_middle_widget_image_path, wordList_1)

        single_image_text_and_full_image_right_widget_image_path = [p.after_save_single_image_text_and_fulll_image_right_left_image, p.after_save_single_image_text_and_fulll_image_right_right_image]
        test_admin.testing_single_image_text_and_full_image_right(self, p.after_save_single_image_text_and_fulll_image_right_left_text, single_image_text_and_full_image_right_widget_image_path, wordList_1)
        
        single_image_text_and_full_image_left_widget_image_path = [p.after_save_single_image_text_and_fulll_image_left_left_image, p.after_save_single_image_text_and_fulll_image_left_right_image]
        test_admin.testing_single_image_text_and_full_image_left(self, p.after_save_single_image_text_and_fulll_image_left_right_text, single_image_text_and_full_image_left_widget_image_path, wordList_1)
        
        testing_full_image_and_three_image_right_widget_image_path = [p.after_save_full_image_and_three_image_right_left_image, p.after_save_full_image_and_three_image_right_right_image1, p.after_save_full_image_and_three_image_right_right_image2, p.after_save_full_image_and_three_image_right_right_image3]
        test_admin.testing_full_image_and_three_image_right(self, testing_full_image_and_three_image_right_widget_image_path, wordList_1)
        
        #------------------Testing image slider------------------
        test_admin.testing_image_slider(self, wordList_1)


        #--------------------------Testing Booking Button------------------------------------------------
        test_admin.testing_book_now_button(self, p.Book_Button_Select_Path, p.Book_Button_Label_Path, promotion_info_data.Booking_Button_Name)
        test_admin.testing_other_button(self, p.Other_Button_Select_Path, p.Other_Button_Label_Path, promotion_info_data.Booking_Button_Name, p.Other_Button_Link_Path, promotion_info_data.Booking_Button_Link)


        # #----------------------------------------Testing Predefined Content----------------------------------------------------------------
        test_admin.testing_predefined_content(self)

        # #----------------Testing Room Page SEO-------------------------------
        print d.find_element_by_xpath(p.Page_Title_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(p.Page_Title_Path).get_attribute("value"), promotion_info_data.Page_Title)

        print d.find_element_by_xpath(p.Page_Description_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(p.Page_Description_Path).get_attribute("value"), promotion_info_data.Page_Description)

        print d.find_element_by_xpath(p.Page_Keywords_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(p.Page_Keywords_Path).get_attribute("value"), promotion_info_data.Page_Keywords)       

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
