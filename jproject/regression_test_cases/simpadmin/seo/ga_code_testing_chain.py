# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("seo"), '../..', "config"))
import hotel_info
import seo_info_data
import seo_elements_path

class Testinggacodechain(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_testinggacodechain(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.chain_user_name, hotel_info.chain_password)
        d.find_element_by_link_text("Ops").click()
        d.find_element_by_link_text("Analytics Settings").click()
        print d.find_element_by_xpath("/html/body/div/aside[2]/form/section/section/div/div[2]/div[2]/label/div/input").get_attribute("value")
        self.assertEqual(d.find_element_by_xpath("/html/body/div/aside[2]/form/section/section/div/div[2]/div[2]/label/div/input").get_attribute("value"),seo_info_data.ga_code)
            
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
