from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Adding GA Code"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_ga_code")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing GA Code"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("ga_code_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing seo"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("seo_preview_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)