# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("seo"), '../..', "config"))
import hotel_info
import seo_info_data
sys.path.append(os.path.join(os.path.dirname("seo"), '..', "simpadmin_common_function"))
import create_data_common_function


class Addinggacodechain(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True
    
    def test_addinggacode(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.chain_user_name, hotel_info.chain_password)

        d.find_element_by_link_text("Ops").click()
        d.find_element_by_link_text("Analytics Settings").click()
        d.find_element_by_xpath("/html/body/div/aside[2]/form/section/section/div/div[2]/div[2]/label/div/input").clear()
        d.find_element_by_xpath("/html/body/div/aside[2]/form/section/section/div/div[2]/div[2]/label/div/input").send_keys(seo_info_data.ga_code)
        
        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        d.find_element_by_xpath(".//*[@id='navbar']/ul[1]/li[1]/a/span").click()
        time.sleep(5)
        d.find_element_by_link_text("Publish").click()
        d.find_element_by_xpath("/html/body/div[1]/aside[2]/section[2]/section[1]/form/div/div[2]/div/div[1]/div/div/ins").click()
        d.find_element_by_xpath("/html/body/div[1]/aside[2]/section[2]/section[1]/form/div/div[2]/div/div[2]/div[2]/input").clear()
        d.find_element_by_xpath("/html/body/div[1]/aside[2]/section[2]/section[1]/form/div/div[2]/div/div[2]/div[2]/input").send_keys("Adding GA Code")
        j = 3
        while 1:
            path = "/html/body/div[1]/aside[2]/section[2]/section[1]/form/div/div["+str(j)+"]/div/div/div[2]/input"
            x = self.check_exists_by_xpath(path)
            if (x == True):
                d.find_element_by_xpath(path).clear()
                d.find_element_by_xpath(path).send_keys("Adding GA Code")
                j = j + 1
            else:
                break
             
        d.find_element_by_id("publish-site").click()
        print " Published "
        time.sleep(120)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
