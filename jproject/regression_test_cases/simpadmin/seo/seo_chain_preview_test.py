# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import seo_info_data
import seo_elements_path
import sys
import os
sys.path.append(os.path.join(os.path.dirname("seo"), '../..', "config"))
import hotel_info


class Testinggacodechain(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://www.staging-static.simplotel.com/"#seo_info_data.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True
    def check_exists_by_link_text(self, xpath):
        try:
            self.driver.find_element_by_link_text(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_testinggacodechain(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()

        #------Testing GA Code present in Desktop 1st level---------------------
        print "--*** GA Code, Canonical Tag and Alternate Tag in Desktop 1st Level ***--"
        html_source = driver.page_source

        if "UA-1234567-8" in html_source:
            print "GA - Code Present Desktop 1st Level"
        else:
            print "GA - Code Not Present Desktop 1st Level"

        if '<link href="http://www.staging-static.simplotel.com/" rel="canonical" />' in html_source:
            print "Canonical Tag Present Desktop 1st Level"
        else:
            print "Canonical Tag Not Present Desktop 1st Level"

        if '<link href="http://www.staging-static.simplotel.com/m/" media="only screen and (max-width: 640px)" rel="alternate" />' in html_source:
            print "Alternate Tag Present Desktop 1st Level"
        else:
            print "Alternate Tag Not Present Desktop 1st Level"
        
        #------Testing GA Code present in Desktop 2nd level---------------------
        print "--*** GA Code, Canonical Tag and Alternate Tag in Desktop 2nd Level ***--"
        driver.get(self.base_url + "/test-menu/test-page.html")
        driver.maximize_window()
        html_source = driver.page_source

        if "UA-1234567-8" in html_source:
            print "GA - Code Present Desktop 2nd Level"
        else:
            print "GA - Code Not Present Desktop 2nd Level"

        if '<link href="http://www.staging-static.simplotel.com/rooms/test-room.html" rel="canonical" />' in html_source:
            print "Canonical Tag Present Desktop 2nd Level"
        else:
            print "Canonical Tag Not Present Desktop 2nd Level"

        if '<link href="http://www.staging-static.simplotel.com/m/rooms/test-room.html" media="only screen and (max-width: 640px)" rel="alternate" />' in html_source:
            print "Alternate Tag Present Desktop 2nd Level"
        else:
            print "Alternate Tag Not Present Desktop 2nd Level"

        #------Testing GA Code present in Mobile----------------------- 
        print "--*** GA Code, Canonical Tag and Alternate Tag in Mobile 1st Level ***--"    
        driver.get(self.base_url + "/m")
        driver.maximize_window()

        if "UA-1234567-8" in html_source:
            print "GA - Code Present Mobile 1st Level"
        else:
            print "GA - Code Not Present Mobile 1st Level"
            
        html_source = driver.page_source

        if '<link href="http://www.staging-static.simplotel.com/" rel="canonical" />' in html_source:
            print "Canonical Tag Present Mobile 1st Level"
        else:
            print "Canonical Tag Not Present Mobile 1st Level"

        if 'rel="alternate' in html_source:
            print "Alternate Tag Present Mobile 1st Level"
        else:
            print "Alternate Tag Not Present Mobile 1st Level"
        
        #------Testing GA Code present in Mobile 2nd level---------------------
        print "--*** GA Code, Canonical Tag and Alternate Tag in Mobile 2nd Level ***--"
        driver.get(self.base_url + "/m/test-menu/test-page.html")
        driver.maximize_window()
        html_source = driver.page_source

        if "UA-1234567-8" in html_source:
            print "GA - Code Present Mobile 2nd Level"
        else:
            print "GA - Code Not Present Mobile 2nd Level"

        if '<link href="http://www.staging-static.simplotel.com/rooms/test-room.html" rel="canonical" />' in html_source:
            print "Canonical Tag Present Mobile 2nd Level"
        else:
            print "Canonical Tag Not Present Mobile 2nd Level"

        if 'rel="alternate' in html_source:
            print "Alternate Tag Present Mobile 2nd Level"
        else:
            print "Alternate Tag Not Present Mobile 2nd Level"

        #-----------Testing Site Map HTML-------------------------------
        print "--*** Site Map HTML ***--"
        driver.get(self.base_url + "/sitemap.html")
        driver.maximize_window()

        time.sleep(3)
        print driver.find_element_by_xpath(seo_elements_path.site_map_h1_path).text
        self.assertEqual(driver.find_element_by_xpath(seo_elements_path.site_map_h1_path).text,"Sitemap - "+str(hotel_info.chain_hotel_name))

        x = self.check_exists_by_link_text(hotel_info.chain_child_hotel_name_one)
        if (x == True):
            print "1st Child Present"
        else:
            print "1st Child Not Prsent"

        x = self.check_exists_by_link_text(hotel_info.chain_child_hotel_name_two)
        if (x == True):
            print "2nd Child Present"
        else:
            print "2nd Child Not Prsent"

        

        #-----------Testing robots.txt-------------------------------

        print "--*** Robots-Txt ***--"
        driver.get(self.base_url + "/robots.txt")
        driver.maximize_window()
        time.sleep(2)
        print driver.find_element_by_xpath("/html/body").text   
        self.assertEqual(driver.find_element_by_xpath("/html/body").text,"User-agent: * \nDisallow: /") 

        #-----------Testing Site Map Xml-------------------------------
        driver.get(self.base_url + "/sitemap.xml")
        driver.maximize_window()
        time.sleep(2)
        html_source = driver.page_source

        print "--*** Site Map XML Canonical Tag ***--"
        if "<loc>http://www.staging-static.simplotel.com/</loc>" in html_source:
            print "Site Map Xml - Canonical Tag - Home Page Prsent"
        else:
            print "Site Map Xml - Canonical Tag - Home Page not Prsent"




        #--------------------------------------------------------------
        print "--*** Site Map XML Alternate Tag ***--"
        if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.staging-static.simplotel.com/m/" />' in html_source:
            print "Site Map Xml - Alternate Tag - Home Page Prsent"
        else:
            print "Site Map Xml - Alternate Tag - Home Page not Prsent"


        
        #---------------------------child hotel seo Testing----------------------------------------
        driver.get(self.base_url + "/sitemap.html")
        driver.maximize_window()

        x = self.check_exists_by_link_text(hotel_info.chain_child_hotel_name_one)
        if (x == True):
            child_link = driver.find_element_by_link_text(hotel_info.chain_child_hotel_name_one).get_attribute("href")
            print child_link
            if(child_link == "http://www.staging-static.simplotel.com/"+str(hotel_info.child_hotel_one_url)+"/"):
                driver.find_element_by_link_text(hotel_info.chain_child_hotel_name_one).click()
                time.sleep(2)
                print "--*** GA Code, Canonical Tag and Alternate Tag in Desktop child 1st Level ***--"
                html_source = driver.page_source

                if "UA-1234567-8" in html_source:
                    print "GA - Code Present Desktop child 1st Level"
                else:
                    print "GA - Code Not Present Desktoep child 1st Level"

                if '<link href="http://www.staging-static.simplotel.com/'+str(hotel_info.child_hotel_one_url)+'/" rel="canonical" />' in html_source:
                    print "Canonical Tag Present Desktop child 1st Level"
                else:
                    print "Canonical Tag Not Present Desktop child 1st Level"

                if '<link href="http://www.staging-static.simplotel.com/m/'+str(hotel_info.child_hotel_one_url)+'/" media="only screen and (max-width: 640px)" rel="alternate" />' in html_source:
                    print "Alternate Tag Present Desktop child 1st Level"
                else:
                    print "Alternate Tag Not Present Desktop child 1st Level"
                
                #------Testing GA Code present in Desktop child  2nd level---------------------
                print "--*** GA Code, Canonical Tag and Alternate Tag in Desktop child 2nd Level ***--"
                driver.get(self.base_url + "/"+str(hotel_info.child_hotel_one_url)+"/rooms/test-room.html")
                driver.maximize_window()
                html_source = driver.page_source

                if "UA-1234567-8" in html_source:
                    print "GA - Code Present Desktop child 2nd Level"
                else:
                    print "GA - Code Not Present Desktop child 2nd Level"

                if '<link href="http://www.staging-static.simplotel.com/'+str(hotel_info.child_hotel_one_url)+'/rooms/test-room.html" rel="canonical" />' in html_source:
                    print "Canonical Tag Present Desktop child 2nd Level"
                else:
                    print "Canonical Tag Not Present Desktop child 2nd Level"

                if '<link href="http://www.staging-static.simplotel.com/m/'+str(hotel_info.child_hotel_one_url)+'/rooms/test-room.html" media="only screen and (max-width: 640px)" rel="alternate" />' in html_source:
                    print "Alternate Tag Present Desktop child 2nd Level"
                else:
                    print "Alternate Tag Not Present Desktop child 2nd Level"

                #------Testing GA Code present in Mobile Child 1st level---------------------
                print "--*** GA Code, Canonical Tag and Alternate Tag in Mobile Child 1st Level ***--"
                driver.get(self.base_url + "/m/"+str(hotel_info.child_hotel_one_url)+"/")
                driver.maximize_window()
                html_source = driver.page_source

                if "UA-1234567-8" in html_source:
                    print "GA - Code Present Mobile Child 1st Level"
                else:
                    print "GA - Code Not Present Mobile Child 1st Level"

                if '<link href="http://www.staging-static.simplotel.com/'+str(hotel_info.child_hotel_one_url)+'/" rel="canonical" />' in html_source:
                    print "Canonical Tag Present Mobile Child 1st Level"
                else:
                    print "Canonical Tag Not Present Mobile Child 1st Level"

                if '<link href="http://www.staging-static.simplotel.com/m/'+str(hotel_info.child_hotel_one_url)+'/" media="only screen and (max-width: 640px)" rel="alternate" />' in html_source:
                    print "Alternate Tag Present Desktop Child 1st Level"
                else:
                    print "Alternate Tag Not Present Desktop Child 1st Level"

                #------Testing GA Code present in Mobile Child 2nd level---------------------
                print "--*** GA Code, Canonical Tag and Alternate Tag in Mobile Child 2nd Level ***--"
                driver.get(self.base_url + "/m/"+str(hotel_info.child_hotel_one_url)+"/rooms/test-room.html")
                driver.maximize_window()
                html_source = driver.page_source

                if "UA-1234567-8" in html_source:
                    print "GA - Code Present Mobile Child 2nd Level"
                else:
                    print "GA - Code Not Present Mobile Child 2nd Level"

                if '<link href="http://www.staging-static.simplotel.com/'+str(hotel_info.child_hotel_one_url)+'/rooms/test-room.html" rel="canonical" />' in html_source:
                    print "Canonical Tag Present Mobile Child 2nd Level"
                else:
                    print "Canonical Tag Not Present Mobile Child 2nd Level"

                if '<link href="http://www.staging-static.simplotel.com/m/'+str(hotel_info.child_hotel_one_url)+'/rooms/test-room.html" media="only screen and (max-width: 640px)" rel="alternate" />' in html_source:
                    print "Alternate Tag Present Desktop Child 2nd Level"
                else:
                    print "Alternate Tag Not Present Desktop Child 2nd Level"
            else:
                print "Wrong Url"
        else:
            print "1st Child Not Prsent"






    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
