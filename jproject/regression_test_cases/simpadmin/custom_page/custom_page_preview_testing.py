# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("custom_page"), '../..', "config"))
import hotel_info
import custom_page_info_data
import custom_page_elements_path


class TestingCustomPagePreview(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_custom_page(self):
        d = self.driver
        d.get(self.base_url + "custom-menu/custom-page.html")
        d.maximize_window()
        time.sleep(10)
        c = custom_page_elements_path
        #----------------------------------------Getting Image Name----------------------------------
        with open("image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        Image_Name_4 = wordList[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4


        #--------------------------Testing H1--------------------------------------------------
        print d.find_element_by_xpath(c.Preview_H1_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_H1_Path).text, custom_page_info_data.H1)

        #--------------------------Testing H2--------------------------------------------------
        print d.find_element_by_xpath(c.Preview_H2_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_H2_Path).text, custom_page_info_data.H2)

        #--------------------------Testing Single Text--------------------------------------------------
        print d.find_element_by_xpath(c.Preview_Single_Text_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Single_Text_Path).text, custom_page_info_data.Single_Text)

        #--------------------------Testing Two Text--------------------------------------------------
        print d.find_element_by_xpath(c.Preview_Two_Text + "div[1]/p").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Two_Text + "div[1]/p").text, custom_page_info_data.Two_Text)

        print d.find_element_by_xpath(c.Preview_Two_Text + "div[2]/p").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Two_Text + "div[2]/p").text, custom_page_info_data.Two_Text)

        #--------------------------Testing Two Text--------------------------------------------------
        print d.find_element_by_xpath(c.Preview_Three_Text + "div[1]/p").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Three_Text + "div[1]/p").text, custom_page_info_data.Three_Text)

        print d.find_element_by_xpath(c.Preview_Three_Text + "div[2]/p").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Three_Text + "div[2]/p").text, custom_page_info_data.Three_Text)

        print d.find_element_by_xpath(c.Preview_Three_Text + "div[3]/p").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Three_Text + "div[3]/p").text, custom_page_info_data.Three_Text)

        #-------------------------Testing Single Image--------------------------------------
     
        wordList = d.find_element_by_xpath(c.Preview_Only_Image_Path + "div[6]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)


        #-------------------------Testing Two Image--------------------------------------
     
        wordList = d.find_element_by_xpath(c.Preview_Only_Image_Path + "div[7]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)

        wordList = d.find_element_by_xpath(c.Preview_Only_Image_Path + "div[7]/div[2]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        #-------------------------Testing three Image--------------------------------------
     
        wordList = d.find_element_by_xpath(c.Preview_Only_Image_Path + "div[8]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        wordList = d.find_element_by_xpath(c.Preview_Only_Image_Path + "div[8]/div[2]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)

        wordList = d.find_element_by_xpath(c.Preview_Only_Image_Path + "div[8]/div[3]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        #----------------Single Image And Text Right-----------------------------------------------------------
        print d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[9]/div[1]/div/p").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[9]/div[1]/div/p").text, custom_page_info_data.Single_Text)

        wordList = d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[9]/div[2]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        #----------------Single Image And Text Left-----------------------------------------------------------
        
        wordList = d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[10]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)

        print d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[10]/div[2]/div/p").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[10]/div[2]/div/p").text, custom_page_info_data.Single_Text)


        #----------------Single Image And Text Center-----------------------------------------------------------
        
        wordList = d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[11]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        print d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[11]/div/p[2]").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[11]/div/p[2]").text, custom_page_info_data.Single_Text)

        #----------------Two Image And Text -----------------------------------------------------------
        
        wordList = d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[12]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)

        wordList = d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[12]/div[2]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        print d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[12]/div[1]/p[2]").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[12]/div[1]/p[2]").text, custom_page_info_data.Two_Text)

        print d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[12]/div[2]/p[2]").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[12]/div[2]/p[2]").text, custom_page_info_data.Two_Text)

        #----------------Three Image And Text -----------------------------------------------------------
        
        wordList = d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        wordList = d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[2]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)

        wordList = d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[3]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        print d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[1]/p[2]").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[1]/p[2]").text, custom_page_info_data.Three_Text)

        print d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[2]/p[2]").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[2]/p[2]").text, custom_page_info_data.Three_Text)

        print d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[3]/p[2]").text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Image_Text_Path + "div[13]/div[3]/p[2]").text, custom_page_info_data.Three_Text)

        #-------------------------Testing Image Slider--------------------------------------------------------
        wordList = d.find_element_by_xpath(c.Preview_Image_Slider_Path + "div[1]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        wordList = d.find_element_by_xpath(c.Preview_Image_Slider_Path + "div[2]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)

        wordList = d.find_element_by_xpath(c.Preview_Image_Slider_Path + "div[3]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)

        wordList = d.find_element_by_xpath(c.Preview_Image_Slider_Path + "div[4]/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_4)

        #---------------Testing Booking Button-----------------------------------------
                                            
        print d.find_element_by_xpath(c.Preview_Book_Button_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(c.Preview_Book_Button_Path).get_attribute("value"), custom_page_info_data.Booking_Button_Name)

        #--------------Testing Other Button-------------------------------------------------------------
        print d.find_element_by_xpath(c.Preview_Other_button_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Other_button_Path).text, custom_page_info_data.Booking_Button_Name)
        wordList = d.find_element_by_xpath(c.Preview_Other_button_Path).get_attribute("href").split("/")
        n = len(wordList) 
        Link_Name = wordList[n-2]+"/"+wordList[n-1]
        print Link_Name
        self.assertEqual(Link_Name, custom_page_info_data.Booking_Button_Link)

        #-------------------Testing Predefined Content--------------------------------------------------------
        print d.find_element_by_xpath(c.Preview_Weather_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Weather_Path).text, "WEATHER")

        print d.find_element_by_xpath(c.Preview_Local_Time_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Local_Time_Path).text, "LOCAL TIME")
        
        d.switch_to_frame(d.find_element_by_xpath(c.Preview_Facebook_Path))
        print d.find_element_by_xpath("/html").get_attribute("id")
        self.assertEqual(d.find_element_by_xpath("/html").get_attribute("id"), "facebook")
        d.switch_to_default_content()

        print d.find_element_by_xpath(c.Preview_TripAdvisor_Path).get_attribute("alt")
        self.assertEqual(d.find_element_by_xpath(c.Preview_TripAdvisor_Path).get_attribute("alt"), "TripAdvisor")
        
        print d.find_element_by_xpath(c.Preview_Social_Connect_Path).get_attribute("class")
        self.assertEqual(d.find_element_by_xpath(c.Preview_Social_Connect_Path).get_attribute("class"), "socialWidgetRow")

        print d.find_element_by_xpath(c.Preview_Location_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Location_Path).text, "LOCATION")
        
        print d.find_element_by_xpath(c.Preview_Contact_Us_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Contact_Us_Path).text, "CONTACT US")
        
        print d.find_element_by_xpath(c.Preview_Newsletter_Signup_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Newsletter_Signup_Path).text, "NEWSLETTER SIGNUP")
        
        print d.find_element_by_xpath(c.Preview_Promotion_Slider_Path).text
        self.assertEqual(d.find_element_by_xpath(c.Preview_Promotion_Slider_Path).text, "PROMOTIONS")

        #-------------------Testing SEO---------------------------------------------------------
        print d.title
        self.assertEqual(d.title, custom_page_info_data.Page_Title)

        print d.find_element_by_xpath("/html/head/meta[1]").get_attribute("content")
        self.assertEqual(d.find_element_by_xpath("/html/head/meta[1]").get_attribute("content"), custom_page_info_data.Page_Description)

        print d.find_element_by_xpath("/html/head/meta[8]").get_attribute("content")
        self.assertEqual(d.find_element_by_xpath("/html/head/meta[8]").get_attribute("content"), custom_page_info_data.Page_Keywords)


    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
