# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
import custom_page_info_data
import custom_page_elements_path
sys.path.append(os.path.join(os.path.dirname("custom_page"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("custom_page"), '..', "simpadmin_common_function"))
import testing_data_common_function
import create_data_common_function

class TestingCustomPage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_custom_page(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        c = custom_page_elements_path
        admin_test = testing_data_common_function
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text(" Custom Pages").click()
        time.sleep(10)
        i = 1
        while 1:
            p_name = d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[3]/ol/li[1]/ol/li["+str(i)+"]/div/a").text
            if (p_name == custom_page_info_data.Custom_Page_Name):
                d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[3]/ol/li[1]/ol/li["+str(i)+"]/div/a").click()
                d.find_element_by_xpath("/html/body/div[1]/aside[1]/section/ul/li[3]/ol/li[1]/ol/li["+str(i)+"]/div/div/a[1]/i").click()
                break
            i = i + 1 

        # #----------------------------------------Testing Promotion Name ----------------------------------------------------------------
    
        print d.find_element_by_xpath(c.Custom_Page_Name_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(c.Custom_Page_Name_Path).get_attribute("value"), custom_page_info_data.Custom_Page_Name)

        
        # # -----------------------Testing Add Content----------------------
        with open("image_names.txt",'r') as f:
            wordList_1 = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList_1[0]
        Image_Name_2 = wordList_1[1]
        Image_Name_3 = wordList_1[2]
        Image_Name_4 = wordList_1[3]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3
        print Image_Name_4

        # #----------Testing short Description and Image------------------------
        wordList = d.find_element_by_xpath(c.Short_Description_Image_Path + "/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        d.switch_to_frame(d.find_element_by_xpath(c.Short_Description_Iframe_Path))
        print d.find_element_by_xpath("/html/body").text
        self.assertEqual(d.find_element_by_xpath('/html/body').text, custom_page_info_data.Custom_Page_Short_Description)
        d.switch_to_default_content()

        
         # -----------------------Testing Add Content----------------------
        #------------------------H1 and H2 Testing------------------------
        admin_test.testing_h1(self, c.after_save_h1)
        admin_test.testing_h2(self, c.after_save_h2)
        
        #-----------------------Only Text Widget Testing-------------------
        admin_test.testing_single_text(self, c.after_save_single_text)
        admin_test.testing_two_text(self, c.after_save_two_text_left, c.after_save_two_text_right)
        admin_test.testing_three_text(self, c.after_save_three_text_left, c.after_save_three_text_middle, c.after_save_three_text_right)

        #---------------Testing Single Image---------------------
        admin_test.testing_single_image(self, c.after_save_single_image, wordList_1)
        admin_test.testing_two_image(self, c.after_save_two_image_left, c.after_save_two_image_right, wordList_1)
        admin_test.testing_three_image(self, c.after_save_three_image_left, c.after_save_three_image_middle, c.after_save_three_image_right, wordList_1)

        #----------------------------------------Testing Single Image and text---------------------------------------------------------------
        admin_test.testing_single_image_and_text_left(self, c.after_save_single_image_text_left_text, c.after_save_single_image_text_left_image, wordList_1)
        admin_test.testing_single_image_and_text_right(self, c.after_save_single_image_text_right_text, c.after_save_single_image_text_right_image, wordList_1)
        admin_test.testing_single_image_and_text_center(self, c.after_save_single_image_text_center_text, c.after_save_single_image_text_center_image, wordList_1)
        admin_test.testing_two_image_and_text(self, c.after_save_two_image_text_left_text, c.after_save_two_image_text_left_image, c.after_save_two_image_text_right_text, c.after_save_two_image_text_right_image, wordList_1)
        
        three_image_text_widget_text_path = [c.after_save_three_image_text_left_text, c.after_save_three_image_text_middle_text, c.after_save_three_image_text_right_text]
        three_image_text_widget_image_path = [c.after_save_three_image_text_left_image, c.after_save_three_image_text_middle_image, c.after_save_three_image_text_right_image]
        admin_test.testing_three_image_and_text(self, three_image_text_widget_text_path, three_image_text_widget_image_path, wordList_1)

        two_image_text_and_full_image_right_widget_text_path = [c.after_save_two_image_text_and_full_image_right_left_text, c.after_save_two_image_text_and_full_image_right_middle_text]
        two_image_text_and_full_image_right_widget_image_path = [c.after_save_two_image_text_and_full_image_right_left_image, c.after_save_two_image_text_and_full_image_right_middle_image, c.after_save_two_image_text_and_full_image_right_right_image]
        admin_test.testing_two_image_text_and_full_image_right(self, two_image_text_and_full_image_right_widget_text_path, two_image_text_and_full_image_right_widget_image_path, wordList_1)

        two_image_text_and_full_image_middle_widget_text_path = [c.after_save_two_image_text_and_full_image_middle_left_text, c.after_save_two_image_text_and_full_image_middlle_right_text]
        two_image_text_and_full_image_middle_widget_image_path = [c.after_save_two_image_text_and_full_image_middle_left_image, c.after_save_two_image_text_and_full_image_middle_middle_image, c.after_save_two_image_text_and_full_image_middlle_right_image]
        admin_test.testing_two_image_text_and_full_image_middle(self, two_image_text_and_full_image_middle_widget_text_path, two_image_text_and_full_image_middle_widget_image_path, wordList_1)

        single_image_text_and_full_image_right_widget_image_path = [c.after_save_single_image_text_and_fulll_image_right_left_image, c.after_save_single_image_text_and_fulll_image_right_right_image]
        admin_test.testing_single_image_text_and_full_image_right(self, c.after_save_single_image_text_and_fulll_image_right_left_text, single_image_text_and_full_image_right_widget_image_path, wordList_1)
        
        single_image_text_and_full_image_left_widget_image_path = [c.after_save_single_image_text_and_fulll_image_left_left_image, c.after_save_single_image_text_and_fulll_image_left_right_image]
        admin_test.testing_single_image_text_and_full_image_left(self, c.after_save_single_image_text_and_fulll_image_left_right_text, single_image_text_and_full_image_left_widget_image_path, wordList_1)
        
        testing_full_image_and_three_image_right_widget_image_path = [c.after_save_full_image_and_three_image_right_left_image, c.after_save_full_image_and_three_image_right_right_image1, c.after_save_full_image_and_three_image_right_right_image2, c.after_save_full_image_and_three_image_right_right_image3]
        admin_test.testing_full_image_and_three_image_right(self, testing_full_image_and_three_image_right_widget_image_path, wordList_1)
        
        #------------------Testing image slider------------------
        admin_test.testing_image_slider(self, wordList_1)



        #--------------------------Testing Booking Button------------------------------------------------
        admin_test.testing_book_now_button(self, c.Book_Button_Select_Path, c.Book_Button_Label_Path, custom_page_info_data.Booking_Button_Name)
        admin_test.testing_other_button(self, c.Other_Button_Select_Path, c.Other_Button_Label_Path, custom_page_info_data.Booking_Button_Name, c.Other_Button_Link_Path, custom_page_info_data.Booking_Button_Link)


        # #----------------------------------------Testing Predefined Content----------------------------------------------------------------
        admin_test.testing_predefined_content(self)

        # #----------------Testing Room Page SEO-------------------------------
        print d.find_element_by_xpath(c.Page_Title_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(c.Page_Title_Path).get_attribute("value"), custom_page_info_data.Page_Title)

        print d.find_element_by_xpath(c.Page_Description_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(c.Page_Description_Path).get_attribute("value"), custom_page_info_data.Page_Description)

        print d.find_element_by_xpath(c.Page_Keywords_Path).get_attribute("value")
        self.assertEqual(d.find_element_by_xpath(c.Page_Keywords_Path).get_attribute("value"), custom_page_info_data.Page_Keywords)
        

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
