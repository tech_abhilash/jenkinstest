# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("custom_page"), '../..', "config"))
import hotel_info
import custom_page_info_data
import custom_page_elements_path
sys.path.append(os.path.join(os.path.dirname("custom_page"), '..', "simpadmin_common_function"))
import create_data_common_function

class CreatingCustomPage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_custom_page(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        c = custom_page_elements_path
        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        d.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        d.find_element_by_link_text(" Custom Pages").click()

        #-------------Creating New custom page------------------------------------------

        d.find_element_by_link_text("Add Menu Item").click()
        d.find_element_by_id("pagegroupname").send_keys(custom_page_info_data.Custom_Menu_Name)
        d.find_element_by_xpath(c.Submit_Button_Path).click()
        time.sleep(10)
        d.find_element_by_link_text("Add Page").click()
        d.find_element_by_id("pagename").send_keys(custom_page_info_data.Custom_Page_Name)
        d.find_element_by_xpath(c.Submit_Button_Path).click()
        time.sleep(10)


        #------------Adding Text to Short and Image to the custom page-----------------
        ActionChains(d).move_to_element(d.find_element_by_xpath(c.Short_Description_Image_Path + "div/img")).perform()
        d.find_element_by_xpath(c.Short_Description_Image_Path + "a[1]/span").click()
        d.find_element_by_link_text("All Photos").click()
        time.sleep(3)
         #----------------------------------------Getting Image Name-----------------------------------------------------------------
        
        wordList = d.find_element_by_xpath(c.Image_Path + "li[1]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]

        wordList = d.find_element_by_xpath(c.Image_Path + "li[2]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]

        wordList = d.find_element_by_xpath(c.Image_Path + "li[3]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]

        wordList = d.find_element_by_xpath(c.Image_Path + "li[4]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_4 = wordList[n-1]

        #----------------Saving Image Name----------------------------------
        with open("image_names.txt",'w') as f:
            f.write(Image_Name_1 +" "+Image_Name_2+" "+Image_Name_3+" "+Image_Name_4)
            f.close()


        d.find_element_by_xpath(c.Image_Path + "li[1]/div").click()
        #print d.find_element_by_xpath("/html/body/div[8]/div[2]/div/div[1]/div/div/div[2]/div[2]/ul/li[1]/div/img").get_attribute("src")
        time.sleep(3)
        d.find_element_by_xpath(c.Image_Selecter_Button_Path).click()
        
        d.switch_to_frame(d.find_element_by_xpath(c.Short_Description_Iframe_Path))
        d.find_element_by_xpath("/html/body").send_keys(custom_page_info_data.Custom_Page_Short_Description)
        d.switch_to_default_content()

#  ------------Adding User Content-----------------------
        # ------------Adding H1 and H2-------------------------------
        create_data_common_function.adding_h1(self, c.H1_Path, c.H1_Done_Button_Path)
        create_data_common_function.adding_h2(self, c.H2_Select_Button_Path, c.H2_Path, c.H2_Done_Button_Path)

        #-----------------------Only Text -------------------
        create_data_common_function.adding_single_text(self, c.Single_Text_Iframe_Path, c.Single_Text_Done_Button_Path)
        create_data_common_function.adding_two_text(self, c.Two_Text_Select_Button_Path, c.Two_Text_Iframe_Path_left, c.Two_Text_Iframe_Path_right, c.Two_Text_Done_Button_Path)
        create_data_common_function.adding_three_text(self, c.Three_Text_Select_Button_Path, c.Three_Text_Iframe_Path_left, c.Three_Text_Iframe_Path_middle, c.Three_Text_Iframe_Path_right, c.Three_Text_Done_Button_Path)

        #----------------------------------------Adding only Image ----------------------------------------------------------------
        create_data_common_function.adding_single_image(self, c.Image_Path, c.Image_Selecter_Button_Path, c.Single_Image_Done_Button_Path)
        create_data_common_function.adding_two_image(self, c.Two_Image_Select_Button_Path, c.Image_Path, c.Image_Selecter_Button_Path, c.Two_Image_Done_Button_Path)
        create_data_common_function.adding_three_image(self, c.Three_Image_Select_Button_Path, c.Image_Path, c.Image_Selecter_Button_Path, c.Three_Image_Done_Button_Path)

        #----------------------------------------Adding Image and Text----------------------------------------------------------------
        create_data_common_function.adding_single_image_and_text_right(self, c.Single_Image_Text_Right_Iframe_Path, c.Image_Path, c.Image_Selecter_Button_Path, c.Single_Image_Text_Right_Done_Button_Path)
        create_data_common_function.adding_single_image_and_text_left(self, c.Single_Image_Text_Left_Select_Button_Path, c.Single_Image_Text_Left_Iframe_Path, c.Image_Path, c.Image_Selecter_Button_Path, c.Single_Image_Text_Left_Done_Button_Path)
        create_data_common_function.adding_single_image_and_text_center(self, c.Single_Image_Text_Center_Select_Button_Path, c.Single_Image_Text_Center_Iframe_Path, c.Image_Path, c.Image_Selecter_Button_Path, c.Single_Image_Text_Center_Done_Button_Path)
        iframe_paths = [c.Two_Image_Text_Iframe_Path_left,c.Two_Image_Text_Iframe_Path_right]
        create_data_common_function.adding_two_image_and_text(self, c.Two_Image_Text_Select_Button_Path, iframe_paths, c.Image_Path, c.Image_Selecter_Button_Path, c.Two_Image_Text_Done_Button_Path)
        iframe_paths = [c.Three_Image_Text_Iframe_Path_left,c.Three_Image_Text_Iframe_Path_middle,c.Three_Image_Text_Iframe_Path_right]
        create_data_common_function.adding_three_image_and_text(self, c.Three_Image_Text_Select_Button_Path, iframe_paths, c.Image_Path, c.Image_Selecter_Button_Path, c.Three_Image_Text_Done_Button_Path)
        iframe_paths = [c.Two_Image_text_and_Full_Image_Right_Iframe_Path_left,c.Two_Image_text_and_Full_Image_Right_Iframe_Path_middle]
        create_data_common_function.adding_two_image_text_and_full_image_right(self, c.Two_Image_text_and_Full_Image_Right_Button_Path, iframe_paths, c.Image_Path, c.Image_Selecter_Button_Path, c.Two_Image_text_and_Full_Image_Right_Done_Button_Path)
        iframe_paths = [c.Two_Image_text_and_Full_Image_middle_Iframe_Path_left,c.Two_Image_text_and_Full_Image_middle_Iframe_Path_right]
        create_data_common_function.adding_two_image_text_and_full_image_middle(self, c.Two_Image_text_and_Full_Image_middle_Button_Path, iframe_paths, c.Image_Path, c.Image_Selecter_Button_Path, c.Two_Image_text_and_Full_Image_middle_Done_Button_Path)
        create_data_common_function.adding_single_image_text_and_full_image_right(self, c.Single_Image_Text_and_Full_Image_Right_Button_Path, c.Single_Image_Text_and_Full_Image_Right_Iframe_Path, c.Image_Path, c.Image_Selecter_Button_Path, c.Single_Image_Text_and_Full_Image_Right_Done_Button_Path)
        create_data_common_function.adding_single_image_text_and_full_image_left(self, c.Single_Image_Text_and_Full_Image_left_Button_Path, c.Single_Image_Text_and_Full_Image_left_Iframe_Path, c.Image_Path, c.Image_Selecter_Button_Path, c.Single_Image_Text_and_Full_Image_left_Done_Button_Path)
        create_data_common_function.adding_full_image_and_three_image_right(self, c.Full_Image_and_Three_Image_Right, c.Image_Path, c.Image_Selecter_Button_Path, c.Full_Image_and_Three_Image_Right_Done_Button_Path)
        
        # ----------------------------------------Adding Image Slider----------------------------------------------------------------
        create_data_common_function.adding_image_slider(self, c.Image_Slider_Add_Image_Path, c.Image_Path, c.Image_Selecter_Button_Path)
        
        # ----------------------------------------Adding Book Button----------------------------------------------------------------
        create_data_common_function.adding_book_now_button(self, custom_page_info_data.Booking_Button_Name, c.Book_Button_Label_Path, c.Book_Button_Done_Path)
        create_data_common_function.adding_other_button(self, custom_page_info_data.Booking_Button_Name, custom_page_info_data.Booking_Button_Link, c.Other_Button_Link_Path, c.Other_Button_Label_Path, c.Other_Button_Done_Path)

        
        #------------------Adding Predefined Content-------------------------------
        create_data_common_function.adding_predefined_content(self, c.Predefined_Content_Path)


        #------------------Adding SEO Content-------------------------------
        d.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        d.find_element_by_id("meta-title").clear()
        d.find_element_by_id("meta-title").send_keys(custom_page_info_data.Page_Title)
        d.find_element_by_id("meta-description").clear()
        d.find_element_by_id("meta-description").send_keys(custom_page_info_data.Page_Description)
        d.find_element_by_id("meta-keywords").clear()
        d.find_element_by_id("meta-keywords").send_keys(custom_page_info_data.Page_Keywords)

        #-------------Save and Generation------------------------------
        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
