# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import sys
import os
import unittest, time, re
import logo_page_elements_path
sys.path.append(os.path.join(os.path.dirname("theme"), '..', ''))
import hotel_info
import theme_info_data

class Adding_new_values(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
        
    
    def test_with_new_values(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()   
        driver.find_element_by_name("userid").send_keys(hotel_info.User_Name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(hotel_info.Password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text("  Design").click()
        driver.find_element_by_link_text("Theme Settings").click()
        #-------------Adding Hero Images-----------------------------------------
        time.sleep(5)
        Select(driver.find_element_by_name("template_id")).select_by_visible_text("Dawn")
        driver.find_element_by_css_selector("option[value=\"100002\"]").click()
        time.sleep(10)

        driver.find_element_by_id("reset-settings").click()
        time.sleep(5)
        driver.find_element_by_xpath("html/body/div[7]/div[7]/button[2]").click()
        time.sleep(20)
        driver.find_element_by_link_text("Advanced").click()
        time.sleep(2)
        #---Background--#
        default_body_bg = driver.find_element_by_id("variable-body-bg").get_attribute("value")
       
        # --Top Menu--#
        default_navbar_bg = driver.find_element_by_id("variable-navbar-bg").get_attribute("value")
        default_navbar_bg_transparency = driver.find_element_by_id("variable-navbar-bg-transparency").get_attribute("value")
        default_navbar_item_text_color = driver.find_element_by_id("variable-navbar-item-text-color").get_attribute("value") 
        default_navbar_item_hover_text_color = driver.find_element_by_id("variable-navbar-item-hover-text-color").get_attribute("value") 
        default_navbar_item_hover_border_bottom_color = driver.find_element_by_id("variable-navbar-item-hover-border-bottom-color").get_attribute("value") 
        #--Panel--#
        default_widget_bg = driver.find_element_by_id("variable-widget-bg").get_attribute("value") 
        default_widget_bg_transparency = driver.find_element_by_id("variable-widget-bg-transparency").get_attribute("value")
        default_sidenavheader_text_color = driver.find_element_by_id("variable-sidenavheader-text-color").get_attribute("value") 
         #--Footer--#
        default_footer_text_color = driver.find_element_by_id("variable-footer-text-color").get_attribute("value") 
        default_footer_bg_color = driver.find_element_by_id("variable-footer-bg-color").get_attribute("value") 
        default_footer_border_color = driver.find_element_by_id("variable-footer-border-color").get_attribute("value") 
        #--Homepage Caption--#
        default_homepage_carousel_caption_bg_color = driver.find_element_by_id("variable-homepage-carousel-caption-bg-color").get_attribute("value") 
        default_Homepage_carousel_caption_color = driver.find_element_by_id("variable-homepage-carousel-caption-color").get_attribute("value") 
        #--Fonts--#
        default_font_size_base = driver.find_element_by_id("variable-font-size-base").get_attribute("value").replace('px','')
        #--Mobile Menu--#
        default_mobile_accordion_bg_color = driver.find_element_by_id("variable-mobile-accordion-bg-color").get_attribute("value") 
        default_mobile_accordion_text_color = driver.find_element_by_id("variable-mobile-accordion-text-color").get_attribute("value") 
        print default_body_bg
        print default_navbar_bg
        print default_navbar_bg_transparency
        print default_navbar_item_text_color
        print default_navbar_item_hover_text_color
        print default_navbar_item_hover_border_bottom_color
        print default_widget_bg
        print default_widget_bg_transparency
        print default_sidenavheader_text_color
        print default_footer_text_color
        print default_footer_bg_color
        print default_footer_border_color
        print default_homepage_carousel_caption_bg_color
        print default_Homepage_carousel_caption_color
        print default_font_size_base
        print default_mobile_accordion_bg_color
        print default_mobile_accordion_text_color

        #----------------Saving Default Values----------------------------------
        with open("default_valus.py",'w') as f:
            f.write("default_body_bg = '"+default_body_bg+"'\ndefault_navbar_bg = '"+default_navbar_bg+"'\ndefault_navbar_bg_transparency = '"+default_navbar_bg_transparency+"'\ndefault_navbar_item_text_color = '"+default_navbar_item_text_color+"'\ndefault_navbar_item_hover_text_color = '"+default_navbar_item_hover_text_color+"'\ndefault_navbar_item_hover_border_bottom_color = '"+default_navbar_item_hover_border_bottom_color+"'\ndefault_widget_bg = '"+default_widget_bg+"'\ndefault_widget_bg_transparency = '"+default_widget_bg_transparency+"'\ndefault_sidenavheader_text_color = '"+default_sidenavheader_text_color+"'\ndefault_footer_text_color = '"+default_footer_text_color+"'\ndefault_footer_bg_color = '"+default_footer_bg_color+"'\ndefault_footer_border_color = '"+default_footer_border_color+"'\ndefault_homepage_carousel_caption_bg_color = '"+default_homepage_carousel_caption_bg_color+"'\ndefault_Homepage_carousel_caption_color = '"+default_Homepage_carousel_caption_color+"'\ndefault_font_size_base = '"+default_font_size_base+"'\ndefault_mobile_accordion_bg_color = '"+default_mobile_accordion_bg_color+"'\ndefault_mobile_accordion_text_color = '"+default_mobile_accordion_text_color+"'")
            f.close()
        #--adding new values--#
        #---Background--#
        driver.find_element_by_id("variable-body-bg").clear()
        driver.find_element_by_id("variable-body-bg").send_keys(theme_info_data.new_main_bg_color)
        #--Top Menu--#
        driver.find_element_by_id("variable-navbar-bg").clear()
        driver.find_element_by_id("variable-navbar-bg").send_keys(theme_info_data.new_bg_color)
        driver.find_element_by_id("variable-navbar-bg-transparency").clear()
        driver.find_element_by_id("variable-navbar-bg-transparency").send_keys(theme_info_data.new_transparency)
        driver.find_element_by_id("variable-navbar-item-text-color").clear()
        driver.find_element_by_id("variable-navbar-item-text-color").send_keys(theme_info_data.new_text_color)
        driver.find_element_by_id("variable-navbar-item-hover-text-color").clear()
        driver.find_element_by_id("variable-navbar-item-hover-text-color").send_keys(theme_info_data.new_hover_color)
        driver.find_element_by_id("variable-navbar-item-hover-border-bottom-color").clear()
        driver.find_element_by_id("variable-navbar-item-hover-border-bottom-color").send_keys(theme_info_data.new_hover_color)
        #--Panel--#
        driver.find_element_by_id("variable-widget-bg").clear()
        driver.find_element_by_id("variable-widget-bg").send_keys(theme_info_data.new_bg_color)
        driver.find_element_by_id("variable-widget-bg-transparency").clear()
        driver.find_element_by_id("variable-widget-bg-transparency").send_keys(theme_info_data.new_transparency)
        driver.find_element_by_id("variable-sidenavheader-text-color").clear()
        driver.find_element_by_id("variable-sidenavheader-text-color").send_keys(theme_info_data.new_text_color)
        driver.find_element_by_id("variable-sidenavheader-bg").clear()
        driver.find_element_by_id("variable-sidenavheader-bg").send_keys(theme_info_data.new_bg_color)
         #--Footer--#
        driver.find_element_by_id("variable-footer-text-color").clear()
        driver.find_element_by_id("variable-footer-text-color").send_keys(theme_info_data.new_text_color)
        driver.find_element_by_id("variable-footer-bg-color").clear()
        driver.find_element_by_id("variable-footer-bg-color").send_keys(theme_info_data.new_bg_color)
        driver.find_element_by_id("variable-footer-border-color").clear()
        driver.find_element_by_id("variable-footer-border-color").send_keys(theme_info_data.new_bg_color)
        #--Homepage Caption--#
        driver.find_element_by_id("variable-homepage-carousel-caption-bg-color").clear()
        driver.find_element_by_id("variable-homepage-carousel-caption-bg-color").send_keys(theme_info_data.new_bg_color)
        driver.find_element_by_id("variable-homepage-carousel-caption-color").clear()
        driver.find_element_by_id("variable-homepage-carousel-caption-color").send_keys(theme_info_data.new_text_color)
        #--Fonts--#
        driver.find_element_by_id("variable-font-size-base").clear()
        driver.find_element_by_id("variable-font-size-base").send_keys(theme_info_data.new_font_size_base)
        #--Mobile Menu--#
        driver.find_element_by_id("variable-mobile-accordion-bg-color").clear()
        driver.find_element_by_id("variable-mobile-accordion-bg-color").send_keys(theme_info_data.new_bg_color)
        driver.find_element_by_id("variable-mobile-accordion-text-color").clear()
        driver.find_element_by_id("variable-mobile-accordion-text-color").send_keys(theme_info_data.new_text_color)
        time.sleep(5)
        driver.find_element_by_xpath("/html/body/div[1]/aside[2]/div[2]/div/div[2]/button").click()
        time.sleep(30)

        #--testing saved valuess--
        driver.find_element_by_link_text("Advanced").click()
        time.sleep(5)
        #---Background--#
        self.assertEqual(driver.find_element_by_id("variable-body-bg").get_attribute("value"),theme_info_data.new_main_bg_color)
        #--Top Menu--#
        self.assertEqual(driver.find_element_by_id("variable-navbar-bg").get_attribute("value"),theme_info_data.new_bg_color)
        self.assertEqual(driver.find_element_by_id("variable-navbar-bg-transparency").get_attribute("value"),theme_info_data.new_transparency)
        self.assertEqual(driver.find_element_by_id("variable-navbar-item-text-color").get_attribute("value"),theme_info_data.new_text_color)
        self.assertEqual(driver.find_element_by_id("variable-navbar-item-hover-text-color").get_attribute("value"),theme_info_data.new_hover_color)
        self.assertEqual(driver.find_element_by_id("variable-navbar-item-hover-border-bottom-color").get_attribute("value"),theme_info_data.new_hover_color)
        #--Panel--#
        self.assertEqual(driver.find_element_by_id("variable-widget-bg").get_attribute("value"),theme_info_data.new_bg_color)
        self.assertEqual(driver.find_element_by_id("variable-widget-bg-transparency").get_attribute("value"),theme_info_data.new_transparency)
        self.assertEqual(driver.find_element_by_id("variable-sidenavheader-text-color").get_attribute("value"),theme_info_data.new_text_color)
         #--Footer--#
        self.assertEqual(driver.find_element_by_id("variable-footer-text-color").get_attribute("value"),theme_info_data.new_text_color)
        self.assertEqual(driver.find_element_by_id("variable-footer-bg-color").get_attribute("value"),theme_info_data.new_bg_color)
        self.assertEqual(driver.find_element_by_id("variable-footer-border-color").get_attribute("value"),theme_info_data.new_bg_color)
        #--Homepage Caption--#
        self.assertEqual(driver.find_element_by_id("variable-homepage-carousel-caption-bg-color").get_attribute("value"),theme_info_data.new_bg_color)
        self.assertEqual(driver.find_element_by_id("variable-homepage-carousel-caption-color").get_attribute("value"),theme_info_data.new_text_color)
        #--Fonts--#
        self.assertEqual(driver.find_element_by_id("variable-font-size-base").get_attribute("value"),theme_info_data.new_font_size_base)
        #--Mobile Menu--#
        self.assertEqual(driver.find_element_by_id("variable-mobile-accordion-bg-color").get_attribute("value"),theme_info_data.new_bg_color)
        self.assertEqual(driver.find_element_by_id("variable-mobile-accordion-text-color").get_attribute("value"),theme_info_data.new_text_color)
        







    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

    
    # print testx

if __name__ == "__main__":
    unittest.main()