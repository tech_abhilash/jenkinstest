from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Adding New values to Advance and Superadvance Theme Setting"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_new_values_to_advance_and_superadvance")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Checking New Values in Preview"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("testing_preview_with_new_values")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Resetting default values to Advance and Superadvance Theme Setting "
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("reseting_to_default_advance_and_super_advance_values")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Checking Default Values in Preview"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("testing_preview_with_default_values")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)