# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
from selenium.common.exceptions import TimeoutException
from timeit import Timer
import unittest, time, re
import theme_info_data


class Theme(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = theme_info_data.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except StaleElementReferenceException:
            time.sleep(5)
            return True
        except NoSuchElementException:
            return False
        return True
    
    def test_theme_setting(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        driver.find_element_by_name("userid").clear()
        driver.find_element_by_name("userid").send_keys(theme_info_data.User_Name)
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys(theme_info_data.Password)
        driver.find_element_by_xpath("//button[@type='submit']").click()
        
        #-------------testing theme------------------------------------------
        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text("  Design").click()
        driver.find_element_by_link_text("Theme Settings").click()
        Select(driver.find_element_by_name("template_id")).select_by_visible_text("Dawn")
        driver.find_element_by_css_selector("option[value=\"100002\"]").click()
        print driver.find_element_by_xpath("/html/body").get_attribute("class")
        print driver.find_element_by_xpath("/html/body/div[7]/p").text
        self.assertEqual(driver.find_element_by_xpath("/html/body/div[7]/p").text, "Changing Template")
        time.sleep(30)
        driver.find_element_by_xpath("/html/body/div[1]/aside[2]/div[2]/div/div[2]/button").click()
        name = driver.find_element_by_xpath("/html/body").get_attribute("class")
        print driver.find_element_by_xpath("/html/body").get_attribute("class")
        print driver.find_element_by_xpath("/html/body/div[7]/p").text
        self.assertEqual(driver.find_element_by_xpath("/html/body/div[7]/p").text, "Saving theme data")
        t0 = 0
        path = "/html/body"
        while 1:
            t0 = time.clock()
            x = self.check_exists_by_xpath(path)
            if (x == True):
                if (name != driver.find_element_by_xpath("/html/body").get_attribute("class")):
                    time.sleep(5)
                    print driver.find_element_by_xpath("/html/body").get_attribute("class")
                    break
                elif (t0 >= 15):
                    print t0
                    print "error"
                    break
        print "Done"
        time.sleep(10)

    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

