# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import sys
import os
import unittest, time, re
import ast
import logo_page_elements_path
sys.path.append(os.path.join(os.path.dirname("theme"), '..', ''))
import hotel_info
import theme_info_data

class Preview_test_with_new_values(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
        
    
    def test_new_values_in_preview(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()

        print "----Background----"
        rgba = driver.find_element_by_class_name("for-homepage").value_of_css_property('background-color') 
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        main_background_color = '#%02x%02x%02x' % (r, g, b)
        print "Main Background Color = " +main_background_color
        print alpha
        self.assertEqual(main_background_color,theme_info_data.new_main_bg_color)        

        print "----Top Menu------"
        rgba = driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/nav').value_of_css_property('background-color')
        r, g, b, nav_bar_transparency = ast.literal_eval(rgba.strip("rgba"))
        nav_bar_background = '#%02x%02x%02x' % (r, g, b)
        print "Nav bar background = " +nav_bar_background
        print "Nav bar transparency = " +str(nav_bar_transparency)
        #--- checking nav bar bg ---
        self.assertEqual(nav_bar_background,theme_info_data.new_bg_color)
        #--- checking nav bar transparency ---
        self.assertEqual(nav_bar_transparency,float(theme_info_data.new_transparency))

        rgba = driver.find_element_by_xpath(".//*[@id='top-navbar-collapse']/li[1]/a").value_of_css_property('color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        nav_bar_text_colour = '#%02x%02x%02x' % (r, g, b)
        print "Nav bar text colour = " +nav_bar_text_colour
        print alpha
        #--- checking nav bar text color
        self.assertEqual(nav_bar_text_colour,theme_info_data.new_text_color)
                                 
        ActionChains(driver).move_to_element(driver.find_element_by_xpath(".//*[@id='top-navbar-collapse']/li[1]/a")).perform()
        time.sleep(3)
        rgba = driver.find_element_by_xpath(".//*[@id='top-navbar-collapse']/li[1]/a").value_of_css_property('color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        nav_bar_text_colour_hover = '#%02x%02x%02x' % (r, g, b)
        print "Nav bar text colour hover = " +nav_bar_text_colour_hover
        print alpha
        #---checking nav bar text on hover---
        self.assertEqual(nav_bar_text_colour_hover,theme_info_data.new_hover_color)

        ActionChains(driver).move_to_element(driver.find_element_by_xpath(".//*[@id='top-navbar-collapse']/li[1]/a")).perform()
        rgba = driver.find_element_by_xpath(".//*[@id='top-navbar-collapse']/li[1]/a").value_of_css_property('border-bottom-color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        nav_bar_border_colour_hover = '#%02x%02x%02x' % (r, g, b)
        print "Nav bar border colour hover = " +nav_bar_text_colour_hover
        print alpha
        #---checking nav bar border on hover---
        self.assertEqual(nav_bar_border_colour_hover,theme_info_data.new_hover_color)


        print "----footer----"
        rgba = driver.find_element_by_class_name('bottom').value_of_css_property('background-color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        footer_backgroud_colour = '#%02x%02x%02x' % (r, g, b)
        print footer_backgroud_colour
        print alpha
        #--- cheking footer background----
        self.assertEqual(footer_backgroud_colour,theme_info_data.new_bg_color)

        rgba = driver.find_element_by_class_name('footer-detail').value_of_css_property('color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        footer_text_colour = '#%02x%02x%02x' % (r, g, b)
        print footer_text_colour
        print alpha
        #---Checking footer text color
        self.assertEqual(footer_text_colour,theme_info_data.new_text_color)
        rgba = driver.find_element_by_xpath("/html/body/div[1]/footer/div/div[1]").value_of_css_property('border-bottom-color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        footer_border_colour = '#%02x%02x%02x' % (r, g, b)
        print footer_border_colour
        print alpha
        

        print "---Font---"
        font = driver.find_element_by_class_name('for-homepage').value_of_css_property('font-size')
        print "Font = " +font
        #--testing font
        self.assertEqual(font,theme_info_data.new_font_size_base)
        
        
        print "----Panel-----"
        rgba = driver.find_element_by_class_name('custom-left-widget-wrapper').value_of_css_property('background-color')
        r, g, b, widget_background_transparency = ast.literal_eval(rgba.strip("rgba"))
        widget_background_colour = '#%02x%02x%02x' % (r, g, b)
        print widget_background_colour
        print widget_background_transparency
        
        self.assertEqual(widget_background_colour,theme_info_data.new_bg_color)
        self.assertEqual(widget_background_transparency,float(theme_info_data.new_transparency))
        
        driver.get(self.base_url + "/rooms.html") 
        rgba = driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div/div[2]/div/div/div/p").value_of_css_property('background-color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        side_nav_header_Bg = '#%02x%02x%02x' % (r, g, b)
        print side_nav_header_Bg
        self.assertEqual(side_nav_header_Bg,theme_info_data.new_bg_color)        
        rgba = driver.find_element_by_xpath(".//*[@id='wrapper']/div[1]/div/div/div[2]/div/div/div/p").value_of_css_property('color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        side_nav_header_text_colour = '#%02x%02x%02x' % (r, g, b)
        print side_nav_header_text_colour
        self.assertEqual(side_nav_header_text_colour,theme_info_data.new_text_color)
        print alpha

        print "---mobile---"
        driver.get(self.base_url + "/m")
        rgba = driver.find_element_by_xpath(".//*[@id='accordion']/div[1]/a/div").value_of_css_property('background-color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        mobile_accordion_bg_colour = '#%02x%02x%02x' % (r, g, b)
        print mobile_accordion_bg_colour
        self.assertEqual(mobile_accordion_bg_colour,theme_info_data.new_bg_color)

        rgba = driver.find_element_by_xpath(".//*[@id='accordion']/div[1]/a/div").value_of_css_property('color')
        r, g, b, alpha = ast.literal_eval(rgba.strip("rgba"))
        mobile_accordion_text_colour = '#%02x%02x%02x' % (r, g, b)
        print mobile_accordion_text_colour
        self.assertEqual(mobile_accordion_text_colour,theme_info_data.new_text_color)



    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

    
    # print testx

if __name__ == "__main__":
    unittest.main()