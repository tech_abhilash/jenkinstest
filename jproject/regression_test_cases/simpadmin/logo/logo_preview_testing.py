# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import sys
import os
import unittest, time, re
import logo_page_elements_path
sys.path.append(os.path.join(os.path.dirname("logo"), '../..', "config"))
import hotel_info


class Logopreview(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Preview_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_logo_preview(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.maximize_window()

         #----------------------------------------Getting Image Name----------------------------------
        with open("image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3

        #---------------------------------------Tesing Desktop Logo Image---------------------
        wordList = driver.find_element_by_xpath("html/body/div[1]/header/nav/div/div/div[1]/a/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)



        driver.get(self.base_url + "/m")
        time.sleep(10)
        #---------------------------------------Tesing Mobile Logo Image---------------------
        wordList = driver.find_element_by_xpath("html/body/div/div/div[1]/a/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        wordList = Image_Name.split("?")
        Image_Name = wordList[0]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)



    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
