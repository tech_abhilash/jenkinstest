# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import sys
import os
import unittest, time, re
import logo_page_elements_path
sys.path.append(os.path.join(os.path.dirname("logo"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("logo"), '..', "simpadmin_common_function"))
import create_data_common_function

class Logoadding(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_logo_adding(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        admin = create_data_common_function 
        
        #-----Log in --------------------------
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text("  Design").click()
        driver.find_element_by_link_text("Logo Settings").click()
        #-------------Adding Hero Images-----------------------------------------

        driver.find_element_by_xpath(logo_page_elements_path.logo_selecter_path).click()
        time.sleep(3)
        driver.find_element_by_link_text("All Photos").click()
        time.sleep(3)
        driver.find_element_by_xpath(logo_page_elements_path.select_logo_image_path).click()
        time.sleep(3)

        wordList = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[1]/div/div/div[2]/div[2]/ul/li[1]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_1 = wordList[n-1]

        wordList = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[1]/div/div/div[2]/div[2]/ul/li[2]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_2 = wordList[n-1]

        wordList = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[1]/div/div/div[2]/div[2]/ul/li[3]/div/img").get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name_3 = wordList[n-1]

        #----------------Saving Image Name----------------------------------
        with open("image_names.txt",'w') as f:
            f.write(Image_Name_1+" "+Image_Name_2+" "+Image_Name_3)
            f.close()
        time.sleep(3)
        
        driver.find_element_by_id("doneBtn").click()
        time.sleep(5)

        driver.find_element_by_xpath(logo_page_elements_path.fav_ico_selecter_path).click()
        time.sleep(3)
        driver.find_element_by_link_text("All Photos").click()
        time.sleep(3)
        driver.find_element_by_xpath(logo_page_elements_path.select_fav_ico_image_path).click()
        time.sleep(3)
        driver.find_element_by_id("doneBtn").click()
        time.sleep(5)


        driver.find_element_by_xpath(logo_page_elements_path.mobile_logo_selecter_path).click()
        time.sleep(3)
        driver.find_element_by_link_text("All Photos").click()
        time.sleep(3)
        driver.find_element_by_xpath(logo_page_elements_path.select_mobile_logo_image_path).click()
        time.sleep(3)
        driver.find_element_by_id("doneBtn").click()
        time.sleep(5)


        #-------------Save and Generation------------------------------
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(20)
        driver.find_element_by_link_text("Preview").click()
        driver.find_element_by_id("idGenerateSite").click()
        time.sleep(20)
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
