# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import sys
import os
import unittest, time, re
import logo_page_elements_path
sys.path.append(os.path.join(os.path.dirname("logo"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("logo"), '..', "simpadmin_common_function"))
import create_data_common_function

class Logotest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_logo(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        admin = create_data_common_function 
        
        #-----Log in --------------------------
        admin.login_to_admin(self, hotel_info.user_name, hotel_info.password)

        driver.find_element_by_css_selector("a.dropdown-toggle.active > span").click()
        driver.find_element_by_link_text("  Design").click()
        driver.find_element_by_link_text("Logo Settings").click()
        time.sleep(10)
        
        #----------------------------------------Getting Image Name----------------------------------
        with open("image_names.txt",'r') as f:
            wordList = f.read().split(' ')
            f.close()
        Image_Name_1 = wordList[0]
        Image_Name_2 = wordList[1]
        Image_Name_3 = wordList[2]
        print Image_Name_1
        print Image_Name_2
        print Image_Name_3

        #-------------------Testing Image ------------------------------------------

        wordList = driver.find_element_by_xpath(logo_page_elements_path.after_save_logo_image_path).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_1)

        wordList = driver.find_element_by_xpath(logo_page_elements_path.after_save_fav_ico_path).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_2)

        wordList = driver.find_element_by_xpath(logo_page_elements_path.after_save_mobile_logo_path).get_attribute("src").split("/")
        n = len(wordList) 
        Image_Name = wordList[n-1]
        print Image_Name
        self.assertEqual(Image_Name, Image_Name_3)
   
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
