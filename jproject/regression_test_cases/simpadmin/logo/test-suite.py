from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Adding Logo"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("logo_adding")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing Logo"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("logo_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Testing Logo Preview"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("logo_preview_testing")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)