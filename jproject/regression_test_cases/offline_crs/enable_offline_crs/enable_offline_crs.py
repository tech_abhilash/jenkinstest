# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
import datetime
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("enable_offline_crs"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("enable_offline_crs"), '..', "offline_crs_common_function"))
import offline_crs_common_function
sys.path.append(os.path.join(os.path.dirname("enable_offline_crs"), '../..', "booking_engine/booking_engine_common_function"))
import common_data

class EnableOfflineCrs(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
     
    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).click()
        except NoSuchElementException:
            return False
        return True
        
    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True
    
    def test_enable_offline_crs(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        #------------- login -------------
        offline_crs_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        # ------------- Enable Simplotel Offline_Crs -------------
        offline_crs_common_function.enable_offline_crs(self)
        #------------- Enable Offline_Crs_in_Rateplan -------------
        offline_crs_common_function.enable_offline_crs_in_rateplan(self, common_data.tax_name)
        #------------- Inventry Edit -------------     
        inventry_number = 8       
        offline_crs_common_function.edit_inventry(self, inventry_number)
        time.sleep(5)      

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
