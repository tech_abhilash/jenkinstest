# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
import sys
import os
import datetime
from selenium.webdriver.support.ui import Select
sys.path.append(os.path.join(os.path.dirname("enable_offline_crs"), '../..', 'config'))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("enable_offline_crs"), '..', "offline_crs_common_function"))
import offline_crs_common_function
sys.path.append(os.path.join(os.path.dirname("enable_offline_crs"), '../..', "booking_engine/booking_engine_common_function"))
import common_data

class OffLineCrsTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url 
        self.verificationErrors = []
        self.accept_next_alert = True
     
    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).click()
        except NoSuchElementException:
            return False
        return True
        
    def check_index_out_of_range(self, val):
        try:
            self.driver.window_handles[val]
        except IndexError:
            return False
        return True
    
    def test_offlinecrstest(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        #-- login --
        offline_crs_common_function.login_to_admin(self, hotel_info.user_name, hotel_info.password)
        guest_name = "Test Name"
        guest_phone_no = "8971412668"
        guest_email_id = "test.simplotel@gmail.com"
        email_password = "120tc002"
        check_in = str(datetime.date.today())
        check_out = str(datetime.date.today() + datetime.timedelta(days=1))
       
        # --------creating  Quote------------------------------------------    
        offline_crs_common_function.creating_quote(self)
        # --------Checking  Quote Email with out guest count--------------------
        option = "Days"
        offline_crs_common_function.test_quote_email_with_out_guest_count(self, option)
        status = "PENDING"
        offline_crs_common_function.test_quote_tab(self, hotel_info.Hotel_Name, guest_name, status)
        quote_data = [guest_email_id, email_password, guest_name]
        offline_crs_common_function.gmail_verification_quote_email(self, hotel_info.Hotel_Name, quote_data)
        driver.get(self.base_url + "/")
        option = "Minutes"
        option = "Days"

        # # ----Testing Checking Send invoice------------
        offline_crs_common_function.creating_quote(self)
        offline_crs_common_function.checking_send_invoice(self, common_data.one_adult_price)
        status = "PENDING"
        booking_id = offline_crs_common_function.test_quote_tab_send_invoice(self, hotel_info.Hotel_Name, guest_name, status)
        print booking_id
        guset_data = [guest_name, guest_email_id, email_password, guest_phone_no]
        booking_data = [check_in, check_out]
        hotel_currency = "INR"
        offline_crs_common_function.gmail_verification_of_send_invoice(self, hotel_info.Hotel_Name, guset_data, hotel_currency)
        offline_crs_common_function.testing_cc_avenue_payment_page(self)
        offline_crs_common_function.testing_booking_conformation_page(self, guest_name)
        driver.get(self.base_url + "/")
# gmail_verification_of_confirmation_mail
# test_quote_tab_for_make_booking
        # ----Testing Checking Makke Booking------------
        offline_crs_common_function.creating_quote(self)
        offline_crs_common_function.checking_make_booking(self, common_data.one_adult_price)
        status = "CONFIRMED"
        booking_id = offline_crs_common_function.test_quote_tab_for_make_booking(self, hotel_info.Hotel_Name, guest_name, status)
        print booking_id
        guset_data = [guest_name, guest_email_id, email_password, guest_phone_no]
        booking_data = [check_in, check_out]
        offline_crs_common_function.gmail_verification_of_confirmation_mail(self, hotel_info.Hotel_Name, guset_data)
        driver.get(self.base_url + "/")
        # ----Testing Checking Makke Booking------------
        offline_crs_common_function.creating_quote(self)
        hotel_currency = offline_crs_common_function.checking_send_confirmation_mail(self, common_data.one_adult_price)
        status = "PENDING"
        booking_id = offline_crs_common_function.test_quote_tab_for_send_confirmation_mail(self, hotel_info.Hotel_Name, guest_name, status)
        hotel_currency = "INR"
        offline_crs_common_function.gmail_verification_of_send_confirmation_mail(self, hotel_info.Hotel_Name, guset_data, hotel_currency)
        Booking_id = offline_crs_common_function.testing_booking_conformation_page(self, guest_name)
        offline_crs_common_function.gmail_verification_of_confirmation_mail(self, hotel_info.Hotel_Name, guset_data)

        time.sleep(20)      
        # testing_booking_engine_data_common_function.moving_to_new_tab(self)                           


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
