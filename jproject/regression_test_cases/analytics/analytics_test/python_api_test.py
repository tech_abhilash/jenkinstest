import requests
import json
import MySQLdb
import unittest

class Hotel_info_api_test(unittest.TestCase):
	def fetching_hotel_name(self, cur, property_id):
		cur.execute("SELECT hotel_name FROM sm_hotel_info where hotel_id = %s",[property_id])
		data =  cur.fetchall()
		return data[0][0]
	
	def fetching_hotel_website(self, cur, property_id):
		cur.execute("SELECT attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id = '11'",[property_id])	
		data = cur.fetchall()
		return data[0][0]

	def fetching_hotel_email(self, cur, property_id):
		cur.execute("SELECT hotel_email from sm_hotel_email where hotel_id = %s",[property_id])
		data = cur.fetchall()
		return data

	def fetching_room_data(self, cur, property_id):
		cur.execute("SELECT room_type_id,room_type_name,long_description,max_adults,max_children,max_guests,size,room_size_attribute_name from sm_hotel_room where hotel_id = %s and version_no = '0'",[property_id])
		data = cur.fetchall()
		return data

	def fetching_phone_number(self,cur,property_id):
		cur.execute("SELECT hotel_phone,additional_attribute from sm_hotel_phone where hotel_id = %s",[property_id])
		data = cur.fetchall()
		return data

	def fetching_room_amenities(self,cur,room_id):
		cur.execute("SELECT attribute_name from sm_hotel_room_attributes where hotel_room_attribute_id in (SELECT attribute_id from sm_room_attributes where room_type_id = %s)",[room_id])
		data = cur.fetchall()
		return data

	def test_api(self):
		property_id = 925
		url = "http://stagingbe.simplotel.com/content/property/"+str(property_id)+"/info"
		r = requests.get(url) 
		print r.status_code
		print r.reason

#------- python dictionary---------------------
		resp_dict = json.loads(r.text)

#--------Data Base Connection-------------------
		db = MySQLdb.connect(host='simplotel-db.c2zzwgclqpx6.ap-southeast-1.rds.amazonaws.com', user='simploteladmin', passwd='simplotel123',  db='qa_db')
		cur = db.cursor()
		cur.execute("use simplotel")

#------- Checking Hotel Name ------------------
		database_hotel_name = self.fetching_hotel_name(cur, property_id)
		print database_hotel_name
		print database_hotel_name == resp_dict[''+str(property_id)+'']['en-US']['name']

#------- Checking Hotel Website Name ------------------		
		database_hotel_website_name = self.fetching_hotel_website(cur, property_id)
		print database_hotel_website_name
		print database_hotel_website_name == resp_dict[''+str(property_id)+'']['en-US']['website']

#------- Checking Hotel Email ------------------		
		database_hotel_email = self.fetching_hotel_email(cur, property_id)
		length = len(database_hotel_email)
		i = 0
		for i in range(length):
			print database_hotel_email[i][0]
			print database_hotel_email[i][0] == resp_dict[''+str(property_id)+'']['en-US']['emails'][i]	
		
#------- Checking Room Details ------------------
		database_room_detail = self.fetching_room_data(cur, property_id)
		length = len(database_room_detail)
		i = 0
		for i in range(length):
			j = 0
			print "Room Id :- %s " %database_room_detail[i][j]
			print "Room Name :- %s " %database_room_detail[i][j+1]
			print "Room Detailed Description :- %s " %database_room_detail[i][j+2]
			print "Room Max Adults :- %s " %database_room_detail[i][j+3]
			print "Room Max Children :- %s " %database_room_detail[i][j+4]
			print "Room Max Guest :- %s " %database_room_detail[i][j+5]
			print "Room Max Size :- %s " %database_room_detail[i][j+6]
			print "Room Max Unit :- %s " %database_room_detail[i][j+7]

			print database_room_detail[i][j] == resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['id']
			print database_room_detail[i][j+1] == resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['name']
			print database_room_detail[i][j+2] == resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['description']
			print database_room_detail[i][j+3] == resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['adults']
			print database_room_detail[i][j+4] == resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['children']
			print database_room_detail[i][j+5] == resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['guests']
			print database_room_detail[i][j+6] == resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['size']['area']
			print database_room_detail[i][j+7] == resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['size']['unit']

			database_room_aminities = self.fetching_room_amenities(cur, database_room_detail[i][j])
			room_aminities_length = len(database_room_aminities)
			k = 0
			room_aminities_array = []
			for k in range(room_aminities_length):
				room_aminities_array.append(database_room_aminities[k][0])

#------- Checking Room aminities ------------------
			for k in range(room_aminities_length):
				print resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['amenities'][k]
		 		if resp_dict[''+str(property_id)+'']['en-US']['rooms'][i]['amenities'][k] in room_aminities_array:
		 			print True
		 		else:
		 			print False 

#------- Checking hotel Phone Number ------------------
		database_hotel_phone_number = self.fetching_phone_number(cur, property_id)
		length = len(database_hotel_phone_number)
		i = 0
		for i in range(length):
			j = 0
			print database_hotel_phone_number[i][j]
			print database_hotel_phone_number[i][j+1]
			print database_hotel_phone_number[i][j] == resp_dict[''+str(property_id)+'']['en-US']['phone_numbers'][i]['number']
			print database_hotel_phone_number[i][j+1] == resp_dict[''+str(property_id)+'']['en-US']['phone_numbers'][i]['label']
		


		cur.close()
		db.close()


if __name__ == "__main__":
    unittest.main()

	# # print ['propertyId']name
	# url = "http://stagingbe.simplotel.com/availability"
	# payload = {"propertyId":925,"checkIn":"2017-07-16","checkOut":"2017-07-17","rooms":[{"id":1,"adults":1,"children":0}],"isSterling":False,"promocode":"","name":"None","email":"None","phone":"None","step":"None","roomId":"None","ratePlanId":"None"}
	# r = requests.post(url, data=json.dumps(payload))
	# print r.status_code
	# print r.reason
	# print r.text
	# print r["925"]["en-US"].propertyId	
 