from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re
from selenium.webdriver.support.ui import Select
import sys
import os
import datetime
from time import gmtime, strftime
from decimal import *
import traceback
from dateutil import parser
sys.path.append(os.path.join(os.path.dirname("offline_crs_common_function"), '../..', "booking_engine/booking_engine_common_function"))
import common_data
import be_common_elements_path
sys.path.append(os.path.join(os.path.dirname("offline_crs_common_function"), '../..', 'simpadmin/room'))
import room_page_info_data


def login_to_admin(self, username, password):
    d = self.driver
    d.find_element_by_name("userid").clear()
    d.find_element_by_name("userid").send_keys(username)
    d.find_element_by_name("password").clear()
    d.find_element_by_name("password").send_keys(password)
    d.find_element_by_xpath("//button[@type='submit']").click()

def enable_offline_crs(self):
    d = self.driver
    d.find_element_by_link_text("Ops").click()
    d.find_element_by_link_text("Toggle Features").click()
    d.find_element_by_link_text("Offline CRS").click()
    d.find_element_by_xpath("//*[@value='enabled']").click()
    d.find_element_by_xpath("//*[@type='submit']").click()
    time.sleep(5)

def enable_offline_crs_in_rateplan(self, tax_name):
    d = self.driver
    d.find_element_by_link_text("Rates & Inventory").click()
    d.find_element_by_link_text("Rates").click()
    time.sleep(2)
    #---- Deleting Old Rate Plan ----
    while 1:
        d.refresh()
        try:
            d.find_element_by_xpath("//div[button[@class='btn btn-success']]/ul/li[2]/a").click()
            d.find_element_by_xpath(".//*[@class='delete-icon']/i").click()
            time.sleep(2)
            d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
        except NoSuchElementException, e:
            break
    
    d.find_element_by_xpath(".//*[@class='content-sidebar']/ul/li/a").click()
    time.sleep(2)
    move_to_rack_rate = d.find_element_by_xpath("//label[contains(text(),'Apply Tax on Rack Rate')]")
    self.driver.execute_script("return arguments[0].scrollIntoView();", move_to_rack_rate)
    n = 1
    while  1:
        time.sleep(3)
        try:
            present = d.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr["+str(n)+"]/td[2]/input").is_selected()
            if(present == True):
                d.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr["+str(n)+"]/td[2]/input").click()
                n = n + 1
            else:
                n = n + 1
        except NoSuchElementException, e:
            d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
            time.sleep(8)
            break
     #--- Deleting Tax ---
    d.find_element_by_link_text("Rates & Inventory").click()
    d.find_element_by_link_text("Taxes and Fees").click()
    time.sleep(2)
    while 1:
        try:
            d.find_element_by_xpath("//div[button[@class='btn btn-success']]/ul/li[1]/a").click()
            time.sleep(3)
            d.find_element_by_xpath("//i[@class='fa fa-trash fa-lg']").click()
            time.sleep(2)
            d.find_element_by_xpath("//button[contains(text(),'Ok')]").click()
            time.sleep(4)
        except NoSuchElementException, e:
            break
     # #---- Creating New Tax with out Override ----      
    d.find_element_by_xpath("//button[@class='btn btn-success']").click()
    d.find_element_by_xpath("//div[label[contains(text(),'Name of the Tax')]]/input").send_keys(tax_name)
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(5)
    select = Select(d.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/form/div[1]/div[2]/select"))
    select.select_by_visible_text("Fixed Amount")
    time.sleep(2)
    d.find_element_by_xpath("//textarea[@class='ember-view ember-text-area']").send_keys("First Tax")
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(5)
    #--- Selecting 1st Tax ---
    d.find_element_by_link_text("Rates & Inventory").click()
    d.find_element_by_link_text("Rates").click()
    time.sleep(5)
    move_to_tax1 = d.find_element_by_xpath("//label[contains(text(),'Show Tax Slab')]")
    self.driver.execute_script("return arguments[0].scrollIntoView();", move_to_tax1)
    time.sleep(2)
    d.find_element_by_xpath("//table[thead[tr[th[contains(text(),'TAX NAME')]]]]/tbody/tr[1]/td[2]/input").click()
    time.sleep(3)
    present = d.find_element_by_xpath("//div[label[contains(text(),'Reservation Desk')]]/input").is_selected()
    print present
    if (present == False):
        d.find_element_by_xpath("//div[label[contains(text(),'Reservation Desk')]]/input").click()
        time.sleep(2)
    d.find_element_by_xpath("//button[contains(text(),'Save')]").click()
    time.sleep(5)

def edit_inventry(self, inventry_number):
    d = self.driver
    d.find_element_by_link_text("Rates & Inventory").click()
    time.sleep(2)
    d.find_element_by_link_text("Inventory").click()
    day= datetime.date.today()
    today=str(day)
    d.find_element_by_xpath(".//*[@id='emberApp']/div/section/div/div[1]/ul/li[2]/a").click()
    time.sleep(3)
    d.find_element_by_xpath("//div[div[@class='month-heading'][i[@class='fa fa-chevron-left previous']]]//div[@data-date='" +today+ "']/span[@class='availability-holder']").click()
    d.find_element_by_xpath(".//*[@class='inventory-action-container']/button[5]").click()
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[2]/form/div/div[1]/input").send_keys(inventry_number)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    d.find_element_by_xpath("/html/body/div[2]/div/section/div/div[2]/div/div[5]/button[2]").click()
    time.sleep(5)

def creating_quote(self):
    d = self.driver
    d.find_element_by_link_text("Inquiries & Payments").click()
    time.sleep(2)
    d.find_element_by_link_text("Create New Quote").click()
    time.sleep(2)
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Name')]]/input").clear()
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Name')]]/input").send_keys("Test Name")
    time.sleep(2)
    d.find_element_by_xpath(".//*[contains(text(),'Get Tariff')]").click()
    time.sleep(10)

def checking_price_and_rateplan(self, rate_plan_data, penalty_name, price_data):
    d = self.driver
    print d.find_element_by_xpath(".//*[@class='rateplan-desc']/div[1]/span").text
    self.assertEqual(d.find_element_by_xpath(".//*[@class='rateplan-desc']/div[1]/span").text, rate_plan_data[0])
    print d.find_element_by_xpath(".//*[@class='rateplan-desc']/div[2]").text
    self.assertEqual(d.find_element_by_xpath(".//*[@class='rateplan-desc']/div[2]").text, rate_plan_data[1])
    d.find_element_by_xpath(".//*[@class='rateplan-desc']/div[1]/span").click()
    time.sleep(2)
    print d.find_element_by_xpath(".//*[@id='offlineRateDetailsModal']/div[2]/div/div[2]/div[1]").text
    self.assertEqual(d.find_element_by_xpath(".//*[@id='offlineRateDetailsModal']/div[2]/div/div[2]/div[1]").text, rate_plan_data[2])
    print d.find_element_by_xpath(".//*[@id='offlineRateDetailsModal']/div[2]/div/div[2]/div[3]").text
    self.assertEqual(d.find_element_by_xpath(".//*[@id='offlineRateDetailsModal']/div[2]/div/div[2]/div[3]").text, penalty_name)
    d.find_element_by_xpath(".//*[contains(text(),'Close')]").click()
    time.sleep(2)
    print d.find_element_by_xpath(".//td[2][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text
    self.assertEqual(d.find_element_by_xpath(".//td[2][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text, price_data[0])
    print d.find_element_by_xpath(".//td[3][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text
    self.assertEqual(d.find_element_by_xpath(".//td[3][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text, price_data[1])
    print d.find_element_by_xpath(".//td[4][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text
    self.assertEqual(d.find_element_by_xpath(".//td[4][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text, price_data[2])
    print d.find_element_by_xpath(".//td[5][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text
    self.assertEqual(d.find_element_by_xpath(".//td[5][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text, price_data[3])

def testing_max_adult(self):
    d = self.driver
    d.find_element_by_xpath(".//*[@id='availability-rooms']/div/div[1]/div/div[2]/div/div[1]").click()
    for i in range(0,3):
        d.find_element_by_xpath(".//*[@id='roomContainer-0']/div/div/div[2]/div/input[3]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    print d.find_element_by_xpath(".//*[@class='c-notification__content']").text
    self.assertEqual("We're sorry, available rooms at the hotel cannot accommodate 4 adults. Please add more rooms to your search to accommodate 4 adults.", d.find_element_by_xpath(".//*[@class='c-notification__content']").text)
    time.sleep(5)
    for i in range(0,3):
        d.find_element_by_xpath(".//*[@id='roomContainer-0']/div/div/div[2]/div/input[1]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[1]/div/div[2]/button").click()
    time.sleep(5)

def testing_max_child(self):
    d = self.driver
    d.find_element_by_xpath(".//*[@id='availability-rooms']/div/div[1]/div/div[2]/div/div[1]").click()
    for i in range(0,4):
        d.find_element_by_xpath(".//*[@id='roomContainer-0']/div/div/div[3]/div/input[3]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    print d.find_element_by_xpath(".//*[@class='c-notification__content']").text
    self.assertEqual("We're sorry, available rooms at the hotel cannot accommodate 4 children.", d.find_element_by_xpath(".//*[@class='c-notification__content']").text)
    time.sleep(5)
    for i in range(0,4):
        d.find_element_by_xpath(".//*[@id='roomContainer-0']/div/div/div[3]/div/input[1]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[1]/div/div[2]/button").click()

def testing_max_rooms(self):
    d = self.driver
    d.find_element_by_xpath(".//*[@id='availability-rooms']/div/div[1]/div/div[2]/div/div[1]").click()
    for i in range(0,8):
        d.find_element_by_xpath(".//*[@id='roomContainer-0']/div/div/div[1]/div/input[3]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    print d.find_element_by_xpath(".//*[@class='c-notification__content']").text
    self.assertEqual("We're sorry, there are only 8 rooms available at the hotel.", d.find_element_by_xpath(".//*[@class='c-notification__content']").text)
    time.sleep(5)
    for i in range(0,8):
        d.find_element_by_xpath(".//*[@id='roomContainer-0']/div/div/div[1]/div/input[1]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[1]/div/div[2]/button").click()

def testing_specify_guest_count_with_all_option(self, price_data):
    d = self.driver
    d.find_element_by_xpath(".//*[@id='availability-rooms']/div/div[1]/div/div[2]/div/div[1]").click()
    for i in range(0,6):
        d.find_element_by_xpath(".//*[@id='roomContainer-0']/div/div/div[4]/button").click()
    time.sleep(2)
    #--on1 adult 0 child--
    #--2 adult 0 child--    
    d.find_element_by_xpath(".//*[@id='roomContainer-1']/div/div/div[2]/div/input[3]").click()
    #--3 adult 0 child--    
    d.find_element_by_xpath(".//*[@id='roomContainer-2']/div/div/div[2]/div/input[3]").click()
    d.find_element_by_xpath(".//*[@id='roomContainer-2']/div/div/div[2]/div/input[3]").click()
    #--1 adult 1 child--    
    d.find_element_by_xpath(".//*[@id='roomContainer-3']/div/div/div[3]/div/input[3]").click()
    #--1 adult 2 child--    
    d.find_element_by_xpath(".//*[@id='roomContainer-4']/div/div/div[3]/div/input[3]").click()
    d.find_element_by_xpath(".//*[@id='roomContainer-4']/div/div/div[3]/div/input[3]").click()
    #--1 adult 3 child--    
    d.find_element_by_xpath(".//*[@id='roomContainer-5']/div/div/div[3]/div/input[3]").click()
    d.find_element_by_xpath(".//*[@id='roomContainer-5']/div/div/div[3]/div/input[3]").click()
    d.find_element_by_xpath(".//*[@id='roomContainer-5']/div/div/div[3]/div/input[3]").click()
    #--2 adult 2 child
    d.find_element_by_xpath(".//*[@id='roomContainer-6']/div/div/div[2]/div/input[3]").click()
    d.find_element_by_xpath(".//*[@id='roomContainer-6']/div/div/div[3]/div/input[3]").click()
    d.find_element_by_xpath(".//*[@id='roomContainer-6']/div/div/div[3]/div/input[3]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    time.sleep(5)
    final_total = 0
    for i in range(1,8):
        print i
        total = d.find_element_by_xpath(".//td["+str(i+1)+"][div[div[@class='average-price-value']]]/div/div[1]/span[2]").text
        print Decimal(total)
        self.assertEqual(Decimal(total), Decimal(price_data[i-1]))
        print d.find_element_by_xpath(".//td["+str(i+1)+"][div[div[@class='average-price-value']]]/div/div[2]").text.strip("INR USD for 1 nights.")
        rate_and_tax = d.find_element_by_xpath(".//td["+str(i+1)+"][div[div[@class='average-price-value']]]/div/div[3]").text
        rate_and_tax = rate_and_tax.split(' ')
        rate = rate_and_tax[1]
        tax = rate_and_tax[5].strip(")")
        print tax
        print rate
        # print x[5]
        final_total = final_total + float(total)
        print final_total
    for i in range(2,9):
        select = Select(d.find_element_by_xpath(".//td["+str(i)+"][div[div[@class='average-price-value']]]/div[2]/select"))
        select.select_by_visible_text("1")
        time.sleep(2)
        d.find_element_by_xpath(".//td["+str(i)+"][div[div[@class='average-price-value']]]/div[2]/button").click()
    print d.find_element_by_xpath("//div[span[contains(text(),'Grand Total:')]]/p").text.strip("USD INR")
    print d.find_element_by_xpath(".//div[div[div[contains(text(),'Sub Total')]]]/div[2]/p").text.strip("USD INR")
    print d.find_element_by_xpath(".//div[div[div[contains(text(),'Taxes And Fees')]]]/div[2]/p").text.strip("USD INR")    
    d.find_element_by_xpath(".//div[div[div[span[contains(text(),'Search')]]]]/div[3]/div/button").click()
    time.sleep(2)        

def test_quote_email_with_out_guest_count(self, option):
    d = self.driver
    d.find_element_by_xpath(".//*[contains(text(),'Send Quote Email')]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Phone*')]]/input").clear()
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Phone*')]]/input").send_keys("8050896077")
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Email*')]]/input").clear()
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Email*')]]/input").send_keys("test.simplotel@gmail.com")
    d.find_element_by_xpath(".//div[div[label[contains(text(),'Time period till the Quote is Valid')]]]/div[2]/div[1]/input").clear()
    d.find_element_by_xpath(".//div[div[label[contains(text(),'Time period till the Quote is Valid')]]]/div[2]/div[1]/input").send_keys("1")
    select = Select(d.find_element_by_xpath(".//div[div[label[contains(text(),'Time period till the Quote is Valid')]]]/div[2]/div[2]/select"))
    time.sleep(2)
    select.select_by_visible_text(option)
    time.sleep(2)
    d.find_element_by_xpath(".//button[contains(text(),'Ok')]").click()
    time.sleep(10)
        
def gmail_verification_quote_email(self, hotel_name, quote_data):
    try:
        d = self.driver
        d.get("http://www.gmail.com")
        d.find_element_by_id("Email").send_keys(quote_data[0])
        d.find_element_by_id("next").click()
        time.sleep(4)
        d.find_element_by_id("Passwd").send_keys(quote_data[1])
        d.find_element_by_id("signIn").click()
        self.driver.implicitly_wait(30)
        time.sleep(10)
        d.find_element_by_xpath("//div[span[b[contains(text(), 'Quote for your next stay in Bangalore' )]]]/span[2][contains(text(), '"+ str(hotel_name) +"' )]").click()
        time.sleep(8)
        email_name_with_dear = d.find_element_by_xpath("//p[contains(text(),'Dear')]").text
        email_name = email_name_with_dear.strip("Dear ")
        email_name = email_name.strip(",")
        self.assertEqual(quote_data[2], email_name)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification of Confirmation mail")

def test_quote_tab(self, Hotel_Name, guest_name, status):
    try:
        d = self.driver
        d.find_element_by_link_text("Inquiries & Payments").click()
        d.find_element_by_link_text("All Quotes").click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[1]").text, Hotel_Name)
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[2]").text, status)
        quote_created_time = d.find_element_by_xpath(".//tbody/tr[1]/td[3]").text.split(",")
        quote_expiry_time = d.find_element_by_xpath(".//tbody/tr[1]/td[4]").text.split(",")
        quote_created_time = str(parser.parse(quote_created_time[0]))        
        quote_expiry_time = str(parser.parse(quote_expiry_time[0]))
        quote_created_time = quote_created_time.split(" ") 
        quote_expiry_time =  quote_expiry_time.split(" ")
        self.assertEqual(quote_created_time[0],str(datetime.date.today()))
        self.assertEqual(quote_expiry_time[0],str(datetime.date.today() + datetime.timedelta(days=1)))
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[8]").text, "QUOTE")
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[9]").text, guest_name)
        check_in = d.find_element_by_xpath(".//tbody/tr[1]/td[10]").text
        check_out = d.find_element_by_xpath(".//tbody/tr[1]/td[11]").text
        check_in = datetime.datetime.strptime(check_in, '%d/%m/%Y').strftime('%Y-%m-%d')
        check_out = datetime.datetime.strptime(check_out, '%d/%m/%Y').strftime('%Y-%m-%d')
        self.assertEqual(check_in,str(datetime.date.today()))
        self.assertEqual(check_out,str(datetime.date.today() + datetime.timedelta(days=1)))
        time.sleep(5)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification of Confirmation mail")

def test_quote_tab_for_make_booking(self, Hotel_Name, guest_name, status):
    try:
        d = self.driver
        d.find_element_by_link_text("Inquiries & Payments").click()
        d.find_element_by_link_text("All Quotes").click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[1]").text, Hotel_Name)
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[2]").text, status)
        quote_confirmed_time = d.find_element_by_xpath(".//tbody/tr[1]/td[5]").text.split(",")
        quote_confirmed_time = str(parser.parse(quote_confirmed_time[0]))        
        quote_confirmed_time = quote_confirmed_time.split(" ") 
        self.assertEqual(quote_confirmed_time[0],str(datetime.date.today()))
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[8]").text, "RESERVATION")
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[9]").text, guest_name)
        check_in = d.find_element_by_xpath(".//tbody/tr[1]/td[10]").text
        check_out = d.find_element_by_xpath(".//tbody/tr[1]/td[11]").text
        check_in = datetime.datetime.strptime(check_in, '%d/%m/%Y').strftime('%Y-%m-%d')
        check_out = datetime.datetime.strptime(check_out, '%d/%m/%Y').strftime('%Y-%m-%d')
        self.assertEqual(check_in,str(datetime.date.today()))
        self.assertEqual(check_out,str(datetime.date.today() + datetime.timedelta(days=1)))
        total_booking_amount = float(d.find_element_by_xpath(".//tbody/tr[1]/td[12]").text)
        total = float(common_data.one_adult_price) + float(common_data.tax_amount)
        self.assertEqual(total_booking_amount, total) 
        time.sleep(5)   
        return d.find_element_by_xpath(".//tbody/tr[1]/td[7]").text
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification of Confirmation mail")

def checking_make_booking(self, price):
    d = self.driver
    print price
    d.find_element_by_xpath(".//*[contains(text(),'Specify Guest Count')]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    total_amount = float(price) + float(common_data.tax_amount) 
    select = Select(d.find_element_by_xpath(".//td[2][div[div[@class='average-price-value']]]/div[2]/Select"))
    select.select_by_visible_text("1")
    time.sleep(2)
    d.find_element_by_xpath(".//td[2][div[div[@class='average-price-value']]]/div[2]/button").click()
    time.sleep(2)
    self.assertEqual(float(total_amount), float(d.find_element_by_xpath(".//div[span[contains(text(),'Grand Total:')]]/p").text.strip("USD INR")))
    d.find_element_by_xpath(".//div[div[contains(text(),'Book Now')]]/div[1]/div[1]").click()
    time.sleep(2)
    self.assertEqual(float(total_amount), float(d.find_element_by_xpath(".//*[@id='booking-payment']/div/div[2]/div/div/div/div/div/div[1]/div[2]").text.strip("INR USD")))
    d.find_element_by_xpath(".//*[@id='booking-payment']/div/div[2]/div/div/div/div/div/div[7]/div/button[1]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Phone*')]]/input").clear()
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Phone*')]]/input").send_keys("+918050896077")
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Email*')]]/input").clear()
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Email*')]]/input").send_keys("test.simplotel@gmail.com")
    d.find_element_by_xpath(".//button[contains(text(),'Ok')]").click()
    time.sleep(10)

def checking_send_confirmation_mail(self, price):
    d = self.driver
    print price
    d.find_element_by_xpath(".//*[contains(text(),'Specify Guest Count')]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//*[@id='emberModal']/div[2]/div/div[3]/button[2]").click()
    total_amount = float(price) + float(common_data.tax_amount) 
    select = Select(d.find_element_by_xpath(".//td[2][div[div[@class='average-price-value']]]/div[2]/Select"))
    select.select_by_visible_text("1")
    time.sleep(2)
    d.find_element_by_xpath(".//td[2][div[div[@class='average-price-value']]]/div[2]/button").click()
    time.sleep(2)
    self.assertEqual(float(total_amount), float(d.find_element_by_xpath(".//div[span[contains(text(),'Grand Total:')]]/p").text.strip("USD INR")))
    d.find_element_by_xpath(".//div[div[contains(text(),'Book Now')]]/div[1]/div[1]").click()
    time.sleep(2)
    self.assertEqual(float(total_amount), float(d.find_element_by_xpath(".//*[@id='booking-payment']/div/div[2]/div/div/div/div/div/div[1]/div[2]").text.strip("INR USD")))
    hotel_currency = d.find_element_by_xpath(".//div[div[button[contains(text(),'Override Prices')]]]/div[2]/i").text
    d.find_element_by_xpath(".//*[@id='booking-payment']/div/div[2]/div/div/div/div/div/div[7]/div/button[2]").click()
    time.sleep(2)
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Phone*')]]/input").clear()
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Phone*')]]/input").send_keys("+918050896077")
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Email*')]]/input").clear()
    d.find_element_by_xpath(".//div[label[contains(text(),'Guest Email*')]]/input").send_keys("test.simplotel@gmail.com")
    d.find_element_by_xpath(".//button[contains(text(),'Ok')]").click()
    time.sleep(10)
    return hotel_currency

def test_quote_tab_for_send_confirmation_mail(self, Hotel_Name, guest_name, status):
    try:
        d = self.driver
        d.find_element_by_link_text("Inquiries & Payments").click()
        d.find_element_by_link_text("All Quotes").click()
        time.sleep(2)
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[1]").text, Hotel_Name)
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[2]").text, status)
        quote_created_time = d.find_element_by_xpath(".//tbody/tr[1]/td[3]").text.split(",")
        quote_expiry_time = d.find_element_by_xpath(".//tbody/tr[1]/td[4]").text.split(",")
        quote_created_time = str(parser.parse(quote_created_time[0]))        
        quote_expiry_time = str(parser.parse(quote_expiry_time[0]))
        quote_created_time = quote_created_time.split(" ") 
        quote_expiry_time =  quote_expiry_time.split(" ")
        self.assertEqual(quote_created_time[0],str(datetime.date.today()))
        self.assertEqual(quote_expiry_time[0],str(datetime.date.today() + datetime.timedelta(days=7)))
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[8]").text, "RESERVATION")
        self.assertEqual(d.find_element_by_xpath(".//tbody/tr[1]/td[9]").text, guest_name)
        check_in = d.find_element_by_xpath(".//tbody/tr[1]/td[10]").text
        check_out = d.find_element_by_xpath(".//tbody/tr[1]/td[11]").text
        check_in = datetime.datetime.strptime(check_in, '%d/%m/%Y').strftime('%Y-%m-%d')
        check_out = datetime.datetime.strptime(check_out, '%d/%m/%Y').strftime('%Y-%m-%d')
        self.assertEqual(check_in,str(datetime.date.today()))
        self.assertEqual(check_out,str(datetime.date.today() + datetime.timedelta(days=1)))
        total_booking_amount = float(d.find_element_by_xpath(".//tbody/tr[1]/td[12]").text)
        total = float(common_data.one_adult_price) + float(common_data.tax_amount)
        self.assertEqual(total_booking_amount, total) 
        time.sleep(5)
        return d.find_element_by_xpath(".//tbody/tr[1]/td[7]").text
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification of Confirmation mail")

def gmail_verification_of_confirmation_mail(self, hotel_name, guset_data):
    try:
        d = self.driver
        d.get("http://www.gmail.com")
        # self.driver.implicitly_wait(30)
        # d.find_element_by_xpath("//*[contains(text(),'Sign In')]").click()
        # self.driver.implicitly_wait(30)
        # d.find_element_by_id("Email").send_keys(guset_data[1])
        # d.find_element_by_id("next").click()
        # time.sleep(4)
        # d.find_element_by_id("Passwd").send_keys(guset_data[2])
        # d.find_element_by_id("signIn").click()
        # self.driver.implicitly_wait(30)
        time.sleep(10)
        d.find_element_by_xpath("//div[span[b[contains(text(), 'Booking Confirmation' )]]]/span[2][contains(text(), '"+ str(hotel_name) +"' )]").click()
        time.sleep(8)
        email_name_with_dear = d.find_element_by_xpath("//td[p[contains(text(),'Dear')]]/p[1]").text
        email_name = email_name_with_dear.strip("Dear ")
        email_name = email_name.strip(",")
        self.assertEqual(guset_data[0], email_name)
        email_text = d.find_element_by_xpath("//*[contains(text(),'  Thank you for choosing our ')]").text
        self.assertEqual(email_text, "Thank you for choosing our hotel for your next stay. Your reservation is confirmed and the details are below.")
        email_hotel_name = d.find_element_by_xpath("//tr[td[p[contains(text(),'Reservation ID')]]]/td[1]/h3/a").text
        self.assertEqual(email_hotel_name,hotel_name)
        x = False
        path = "//td[table[tbody[tr[td[p[contains(text(),'Reservation ID')]]]]]]/div/div[1]/div/div"
        x = self.check_exists_by_xpath(path)
        print x
        email_name_2 = d.find_element_by_xpath("//*[contains(text(),'Primary Guest')]/span").text
        self.assertEqual(guset_data[0],email_name_2)
        self.assertEqual(d.find_element_by_xpath("//tr[td[h3[contains(text(),'Primary Guest')]]]/td[2]/p").text,"1 Room(s), 1 Night(s)")
        email_room_name = d.find_element_by_xpath("//*[contains(text(),'Room 1')]/span").text.strip("- ")
        self.assertEqual(email_room_name, room_page_info_data.Room_Name)
        self.assertEqual(d.find_element_by_xpath("//*[contains(text(),'No of Guests')]").text,"No of Guests - 1 Adults, 0 Children")
        self.assertEqual(d.find_element_by_xpath("//td[p[contains(text(),'No of Guests')]]/p[2]").text,common_data.rate_plane_name)
        total = float(common_data.one_adult_price)
        grand_total_1 = d.find_element_by_xpath("//td[table[tbody[tr[td[p[contains(text(),'No of Guests')]]]]]]/table[2]/tbody/tr/td[2]/p").text.strip("Rs. USD ")
        print grand_total_1
        self.assertEqual(Decimal(grand_total_1).normalize(),Decimal(total).normalize())
        grand_total_2 = d.find_element_by_xpath("//tr[td[p[contains(text(),'Sub Total')]]]/td[2]/p[1]").text.strip("Rs. USD ")
        self.assertEqual(Decimal(grand_total_2).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath("//tr[td[p[contains(text(),'Sub Total')]]]/td[2]/p[2]").text.strip("Rs. USD "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath("//tr[td[p[contains(text(),'Grand Total')]]]/td[2]/p[1]").text.strip("Rs. USD ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        time.sleep(10)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification of Confirmation mail")


def gmail_verification_of_send_confirmation_mail(self, hotel_name, guset_data, hotel_currency):
    try:
        d = self.driver
        d.get("http://www.gmail.com")
        self.driver.implicitly_wait(30)
        # d.find_element_by_xpath("//*[contains(text(),'Sign In')]").click()
        # self.driver.implicitly_wait(30)
        d.find_element_by_id("Email").send_keys(guset_data[1])
        d.find_element_by_id("next").click()
        time.sleep(4)
        d.find_element_by_id("Passwd").send_keys(guset_data[2])
        d.find_element_by_id("signIn").click()
        self.driver.implicitly_wait(30)
        time.sleep(10)
        d.find_element_by_xpath("//div[span[b[contains(text(), 'Please Confirm your Stay with us in' )]]]/span[2][contains(text(), '"+ str(hotel_name) +"' )]").click()
        time.sleep(8)
        email_name_with_dear = d.find_element_by_xpath("//td[p[contains(text(),'Dear')]]/p[1]").text
        email_name = email_name_with_dear.strip("Dear ")
        email_name = email_name.strip(",")
        self.assertEqual(guset_data[0], email_name)
        total = float(common_data.one_adult_price) + float(common_data.tax_amount)
        email_text = d.find_element_by_xpath("//*[contains(text(),'  We are excited that you are considering ')]").text
        self.assertEqual(email_text, "We are excited that you are considering "+ str(hotel_name) +" for your next stay. No deposit is required at this time. You will pay "+ str(hotel_currency) +" "+ '{0:.2f}'.format(float(total)) +" during your stay at the hotel.")
        email_name_2 = d.find_element_by_xpath("//*[contains(text(),'Primary Guest')]/span").text
        self.assertEqual(guset_data[0],email_name_2)
        self.assertEqual(d.find_element_by_xpath("//tr[td[h3[contains(text(),'Primary Guest')]]]/td[2]/p").text,"1 Room(s), 1 Night(s)")
        email_room_name = d.find_element_by_xpath("//*[contains(text(),'Room 1')]/span").text.strip("- ")
        self.assertEqual(email_room_name, room_page_info_data.Room_Name)
        self.assertEqual(d.find_element_by_xpath("//*[contains(text(),'No of Guests')]").text,"No of Guests - 1 Adults, 0 Children")
        self.assertEqual(d.find_element_by_xpath("//td[p[contains(text(),'No of Guests')]]/p[2]").text,common_data.rate_plane_name)
        total = float(common_data.one_adult_price)
        grand_total_1 = d.find_element_by_xpath("//td[table[tbody[tr[td[p[contains(text(),'No of Guests')]]]]]]/table[2]/tbody/tr/td[2]/p").text.strip("INR. USD ")
        print grand_total_1
        self.assertEqual(Decimal(grand_total_1).normalize(),Decimal(total).normalize())
        grand_total_2 = d.find_element_by_xpath("//tr[td[p[contains(text(),'Sub Total')]]]/td[2]/p[1]").text.strip("INR. USD ")
        self.assertEqual(Decimal(grand_total_2).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath("//tr[td[p[contains(text(),'Sub Total')]]]/td[2]/p[2]").text.strip("INR. USD "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath("//tr[td[p[contains(text(),'Grand Total')]]]/td[2]/p[1]").text.strip("INR. USD ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        d.find_element_by_xpath(".//a[contains(text(),'Confirm Booking')]").click()
        time.sleep(10)
        change_window = self.check_index_out_of_range(1)
        print change_window
        if(change_window == True):
            window_after = d.window_handles[1]
            d.switch_to_window(window_after)
        d.maximize_window()
        self.driver.implicitly_wait(30)
        time.sleep(10)
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Gmail Verification of Confirmation mail")

        
def testing_booking_conformation_page(self, guest_name):
    try:
        d = self.driver
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_path).text,"Thank You For Your Reservation")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_name_path).text,guest_name)
        # self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_check_in_path).text,check_in)
        # self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_check_out_path).text,check_out)
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_no_of_night_path).text,"1 Nights")
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_name_path).text,room_page_info_data.Room_Name)
        adult_and_child = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_adult_child_path).text
        val = adult_and_child.split(",")
        self.assertEqual(val[0],"1 Adult")
        self.assertEqual(val[1]," 0 Children")
        total = float(common_data.one_adult_price)
        grand_total_1 = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_room_rate_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total_1).normalize(),Decimal(total).normalize())
        grand_total_2 = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_sub_total_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total_2).normalize(),Decimal(total).normalize())
        self.assertEqual(d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_tab_tax_path).text.strip("INR USD "),common_data.tax_amount)
        final_total = float(total)+ float(common_data.tax_amount)
        print final_total
        grand_total = d.find_element_by_xpath(be_common_elements_path.thank_you_for_your_reservation_grand_total_path).text.strip("INR USD ")
        self.assertEqual(Decimal(grand_total).normalize(),Decimal(final_total).normalize())
        Booking_id = d.find_element_by_xpath("//div[label[contains(text(), 'Reservation Id')]]/div/p").text
        return Booking_id
    except Exception, err:
        print(traceback.format_exc())
        sys.exit("Exception occurred while Testing Booking Conformation Page")