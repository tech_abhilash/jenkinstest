from element import BasePageElement
from locators import MainPageLocators

class SearchTextElement(BasePageElement):
    """This class gets the search text from the specified locator"""

    #The locator for search box where search string is entered
    print "in locater class"
    locator = 'q'


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""
    print "Base Page class"
    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):
    """Home page action methods come here. I.e. Python.org"""

    #Declares a variable that will contain the retrieved text
    print "in Main page"
    search_text_element = SearchTextElement()

    def is_title_matches(self):
        """Verifies that the hardcoded text "Python" appears in page title"""
        print "in title match function"
        return "Python" in self.driver.title

    def click_go_button(self):
        """Triggers the search"""
        print "in button click function"
        element = self.driver.find_element(*MainPageLocators.GO_BUTTON)
        element.click()


class SearchResultsPage(BasePage):
    """Search results page action methods come here"""
    print "in search result page class"
    def is_results_found(self):
        # Probably should search for this text in the specific page
        # element, but as for now it works fine
        print "in search result page FUNCTION"
        return "No results found." not in self.driver.page_source