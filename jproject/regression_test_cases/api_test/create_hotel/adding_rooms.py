# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "simpadmin/simpadmin_common_function"))
import create_data_common_function
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "simpadmin/room"))
import room_page_elements_path



class TestRoomCreation(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def creating_room(self, room_name):
        #-------------Creating New Room------------------------------------------
        driver = self.driver
        path = room_page_elements_path
        driver.find_element_by_link_text("Add New Room").click()
        driver.implicitly_wait(10)
        driver.find_element_by_id("pagename").send_keys(room_name)

        #------------Adding Text to Short and Detailed Description of the Room-----------------
        driver.switch_to_frame(driver.find_element_by_xpath(path.Short_Description_Iframe_Path))
        driver.find_element_by_xpath("/html/body").send_keys("Room Short Description")
        driver.switch_to_default_content()

        driver.switch_to_frame(driver.find_element_by_xpath(path.Detailed_Description_Iframe_Path))
        driver.find_element_by_xpath("/html/body").send_keys("Room Detailed Description")
        driver.switch_to_default_content()

        # ------------Adding H1-------------------------------
        create_data_common_function.adding_h1(self, path.H1_Path, path.H1_Done_Button_Path)
        driver.find_element_by_xpath(path.Submit_Button_Path).click()
        time.sleep(10)
 
    def add_room_information(self, room_data):
        by_id = self.driver.find_element_by_id
        i = room_data['room_serial_no']
        by_id("roomnumbers-"+str(i)).clear()
        by_id("roomnumbers-"+str(i)).send_keys(room_data['room_numbers'])
        by_id("websitenumbers-"+str(i)).clear()
        by_id("websitenumbers-"+str(i)).send_keys(room_data['website_numbers'])
        by_id("size-"+str(i)).clear()
        by_id("size-"+str(i)).send_keys(room_data['room_size'])
        by_id("maxAdults-"+str(i)).clear()
        by_id("maxAdults-"+str(i)).send_keys(room_data['max_adults'])
        by_id("maxChild-"+str(i)).clear()
        by_id("maxChild-"+str(i)).send_keys(room_data['max_child'])
        by_id("maxGuests-"+str(i)).clear()
        by_id("maxGuests-"+str(i)).send_keys(room_data['max_guests'])


    def test_create_room(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        #-------Login to admin------

        admin = create_data_common_function
        admin.login_to_admin(self, hotel_info.admin_user_name, hotel_info.admin_password)
        time.sleep(10)

        #-------Creating  New Hotel ------
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        driver.find_element_by_link_text(hotel_info.api_test_hotel_name).click()
        time.sleep(10)
        driver.find_element_by_xpath(".//*[@id='navbar']/ul[1]/li[1]/a").click()
        driver.find_element_by_link_text(" Rooms").click()

        room_one_data = {'room_name' : "Test Room_1",
                            'room_serial_no' : 1,
                            'room_numbers' : 10,
                            'website_numbers' : 5,
                            'room_size' : 150,
                            'max_adults' : 4,
                            'max_child' : 2,
                            'max_guests' : 4}

        room_two_data = {'room_name' : "Test Room_2",
                            'room_serial_no' : 2,
                            'room_numbers' : 15,
                            'website_numbers' : 7,
                            'room_size' : 200,
                            'max_adults' : 3,
                            'max_child' : 2,
                            'max_guests' : 3}

        #----------Create Room-------------------
        self.creating_room(room_one_data['room_name'])
        self.creating_room(room_two_data['room_name'])

        #-------------------Room Information & Amenities-------------------------------
        driver.find_element_by_link_text("Room Information & Amenities").click()
        self.add_room_information(room_one_data)
        self.add_room_information(room_two_data)


        driver.find_element_by_id("minChildAge").clear()
        driver.find_element_by_id("minChildAge").send_keys("2")
        driver.find_element_by_id("maxChildAge").clear()
        driver.find_element_by_id("maxChildAge").send_keys("6")
        
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)


    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
