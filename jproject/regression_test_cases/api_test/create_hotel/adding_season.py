import requests
import json
import MySQLdb
import unittest
import sys, os
from datetime import datetime
import calendar
from dateutil import relativedelta

class AddingSeason(unittest.TestCase):
    def add_season(self, season_name, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/seasons"
        payload = {"season":{"name":season_name,"periods":[],"enabled":True}}
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        print "Add season Response status code:", response.status_code
        resp_dict = json.loads(response.text)
        return  resp_dict['season']['id']

    def update_season(self, season_name, season_id, from_date, to_date, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/seasons/"+str(season_id)
        payload = {"season":{"name":season_name,"periods":[{"id":season_id,"start":from_date,"end":to_date}],"enabled":True}}
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.put(url, data=json.dumps(payload), headers=headers)
        print "Update season Response status code:", response.status_code
        

    def test_season(self):
        with open("booking_engine_detail.json",'r') as f:
            booking_engine_detail = json.loads(f.read())
            f.close()

        season_name_1 = "Season_1"
        season_id_1 = self.add_season(season_name_1, booking_engine_detail['property_id'])
        print season_id_1
        days_in_month = calendar.monthrange(datetime.now().year,datetime.now().month)
        from_date = str(datetime.now().year)+"-"+str(datetime.now().month)+"-01"
        print from_date
        to_date = str(datetime.now().year)+"-"+str(datetime.now().month)+"-"+str(days_in_month[1])
        print to_date
        self.update_season(season_name_1, season_id_1, from_date, to_date, booking_engine_detail['property_id'])

        season_name_2 = "Season_2"
        season_id_2 = self.add_season(season_name_2, booking_engine_detail['property_id'])
        print season_id_2
        next_month = datetime.now().replace(day=1) + relativedelta.relativedelta(months=1)
        nextmonth = str(next_month).split("-")
        days_in_month = calendar.monthrange(int(nextmonth[0]),int(nextmonth[1]))
        from_date = str(nextmonth[0])+"-"+str(nextmonth[1])+"-01"
        print from_date
        to_date = str(nextmonth[0])+"-"+str(nextmonth[1])+"-"+str(days_in_month[1])
        print to_date
        self.update_season(season_name_2, season_id_2, from_date, to_date, booking_engine_detail['property_id'])

        season_name_3 = "Season_3"
        season_id_3 = self.add_season(season_name_3, booking_engine_detail['property_id'])
        print season_id_3
        next_year = str(datetime(year=next_month.year+1, month=1, day=1)).split("-")
        days_in_month = calendar.monthrange(int(next_year[0]),int(next_year[1]))
        from_date = str(next_year[0])+"-"+str(next_year[1])+"-01"
        print from_date
        to_date = str(next_year[0])+"-"+str(next_year[1])+"-"+str(days_in_month[1])
        print to_date
        self.update_season(season_name_3, season_id_3, from_date, to_date, booking_engine_detail['property_id'])

        season_name_4 = "Season_4"
        season_id_4 = self.add_season(season_name_4, booking_engine_detail['property_id'])
        print season_id_4
        next_year = str(datetime(year=next_month.year+1, month=2, day=1)).split("-")
        days_in_month = calendar.monthrange(int(next_year[0]),int(next_year[1]))
        from_date = str(next_year[0])+"-"+str(next_year[1])+"-01"
        print from_date
        to_date = str(next_year[0])+"-"+str(next_year[1])+"-"+str(days_in_month[1])
        print to_date
        self.update_season(season_name_4, season_id_4, from_date, to_date, booking_engine_detail['property_id'])


        booking_engine_detail['season_name_1'] = season_name_1
        booking_engine_detail['season_id_1'] = season_id_1
        booking_engine_detail['season_name_2'] = season_name_2
        booking_engine_detail['season_id_2'] = season_id_2
        booking_engine_detail['season_name_3'] = season_name_3
        booking_engine_detail['season_id_3'] = season_id_3
        booking_engine_detail['season_name_4'] = season_name_4
        booking_engine_detail['season_id_4'] = season_id_4
        print booking_engine_detail
        with open('booking_engine_detail.json', 'w') as outfile:
            json.dump(booking_engine_detail, outfile)   

if __name__ == "__main__":
    unittest.main()


        # client = requests.session()

        # crf = client.get(url).cookies['csrftken']
        # print crf 
        # payload = {"csrfmiddlewaretoken":crf, "userid":"test_hotel_27_april_2017", "password":"test2"}
        # response = client.post(url, data=payload)
        # print "Add season Response status code:", response.status_code