import requests
import json
import MySQLdb
import unittest
import sys, os, time
import random
from datetime import datetime
import calendar
from dateutil import relativedelta

class AddingRatePlans(unittest.TestCase):
    def setUp(self):
        self.db = MySQLdb.connect(host="simplotel-db.c2zzwgclqpx6.ap-southeast-1.rds.amazonaws.com", user="simploteladmin", passwd="simplotel123", db="simplotel")

    def add_rate_plan(self, rate_plan_name, season_id, penalty_id, data, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/rateplans"
        payload = {"rateplan":{"name":rate_plan_name,"active":False,"applicableDays":{},"modify_relative_value":False,"description":"","short_description":None,
        "baseplan":None,"season":season_id,"promocode":"","penalty":penalty_id,"enabled":True,"relative_value":None,
        "relative_unit":None,"constraints":[{"name":"minStayDays","value":0,"unit":"DAYS","enabled":False},
        {"name":"maxStayDays","value":0,"unit":"DAYS","enabled":False},{"name":"minAdvanceDays","value":0,
        "unit":"DAYS","enabled":False},{"name":"maxAdvanceDays","value":0,"unit":"DAYS","enabled":False},
        {"name":"autoCreatePromotions","value":0,"unit":"DAYS","enabled":False},{"name":"rackRateTaxes",
        "value":0,"unit":"DAYS","enabled":False}],"rates":[{"room_type":str(data[0][0]),"room_name":str(data[0][1]),
        "no_of_children":2,"enabled":True,"day_rates":[{"day":0,"rates":[{"no_of_adults":1,"rate":0},{"no_of_adults":2,
        "rate":0},{"no_of_adults":3,"rate":0},{"no_of_adults":4,"rate":0},{"no_of_adults":0,"rate":0}]}]},
        {"room_type":str(data[1][0]),"room_name":str(data[1][1]),"no_of_children":2,"enabled":True,
        "day_rates":[{"day":0,"rates":[{"no_of_adults":1,"rate":0},{"no_of_adults":2,"rate":0},{"no_of_adults":3,"rate":0},
        {"no_of_adults":0,"rate":0}]}]}],"taxes":[],"bookingModes":[],"inclusions":{},"addons":{}}}
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        print "Add Rate Plan Response status code:", response.status_code
        resp_dict = json.loads(response.text)
        return  resp_dict


    def activate_rate_plan(self, rate_plan_id, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/rateplans/"+str(rate_plan_id)+"/activate"
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.put(url, headers=headers)
        print "Activate Rate Plan Response status code:", response.status_code

    def update_rate_plan(self, rate_plan_id, payload, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/rateplans/"+str(rate_plan_id)
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.put(url, data=json.dumps(payload), headers=headers)
        print "Update Rate Plan Response status code:", response.status_code

    def add_room_price(self, response):
        room_1_prince = ['800','1000','2000','3000','4000']
        room_2_prince = ['800','1000','2000','3000']
        guest_in_room = response["rateplan"]["rates"][0]["day_rates"][0]["rates"]
        for guest in range(len(guest_in_room)):
            guest_in_room[guest]["rate"] = room_1_prince[guest]

        guest_in_room = response["rateplan"]["rates"][1]["day_rates"][0]["rates"]
        for guest in range(len(guest_in_room)):
            guest_in_room[guest]["rate"] = room_2_prince[guest]
        return response

    def test_adding_rate_plan(self):
        with open("booking_engine_detail.json",'r') as f:
            booking_engine_detail = json.loads(f.read())
            f.close()

        cur = self.db.cursor()
        cur.execute("SELECT room_type_id,room_type_name from sm_hotel_room where hotel_id = %s and enabled = '1'", [booking_engine_detail['property_id']])
        data = cur.fetchall()
        rate_plan_1_name = "DEFAULT_RATE_PLAN_1"
        response = self.add_rate_plan(rate_plan_1_name, booking_engine_detail["season_id_1"], booking_engine_detail["penalty_id_1"], data, booking_engine_detail['property_id'])
        rate_plan_1_id = response["rateplan"].pop("id")
        time.sleep(5)
        self.activate_rate_plan(rate_plan_1_id, booking_engine_detail['property_id'])
        response["rateplan"]['taxes'][1]['enabled'] = False
        response["rateplan"]["active"] = True 
        response["rateplan"]['inclusions']['inclusions'] = [{"iconClass":"fa-cutlery","name":"Breakfast included","type":"defaultInclusion","icon":"cutlery"},{"iconClass":"fa-cutlery","name":"Breakfast and lunch included","type":"defaultInclusion","icon":"cutlery"},{"iconClass":"fa-cutlery","name":"Breakfast and dinner included","type":"defaultInclusion","icon":"cutlery"}]
        response["rateplan"]['constraints'][1]['enabled'] = True
        response["rateplan"]['constraints'][1]['value'] = 3
        response["rateplan"]['addons']['enabled_addons'] = response["rateplan"]['addons']['default_addons']
        for add_on in response["rateplan"]['addons']['enabled_addons']:
            add_on['enabled'] = True

        updated_response = self.add_room_price(response)
        self.update_rate_plan(rate_plan_1_id, updated_response, booking_engine_detail['property_id'])

        rate_plan_2_name = "DEFAULT_RATE_PLAN_2"
        response = self.add_rate_plan(rate_plan_2_name, booking_engine_detail["season_id_1"], booking_engine_detail["penalty_id_1"], data, booking_engine_detail['property_id'])
        rate_plan_2_id = response["rateplan"].pop("id")
        time.sleep(5)
        self.activate_rate_plan(rate_plan_2_id, booking_engine_detail['property_id'])
        response["rateplan"]["applicableDays"] = {"Wed" :{'status':True, 'relative_value':0},"Sun" :{'status':True, 'relative_value':0},"Fri":{'status':True, 'relative_value':0},"Tue":{'status':True, 'relative_value':0},"Mon":{'status':True, 'relative_value':0},"Thu":{'status':True, 'relative_value':0},"Sat":{'status':True, 'relative_value':0}}
        response["rateplan"]["baseplan"] = rate_plan_1_id
        response["rateplan"]["relative_value"] = -100
        response["rateplan"]["relative_unit"] = "FIXEDAMOUNT"
        response["rateplan"]["active"] = True 
        response["rateplan"]['constraints'][1]['enabled'] = True
        response["rateplan"]['constraints'][1]['value'] = 6
        response["rateplan"]['inclusions']['inclusions'] = [{"iconClass":"fa-wifi","name":"Free Wi-Fi","type":"defaultInclusion","icon":"wifi"}]
        self.update_rate_plan(rate_plan_2_id, response, booking_engine_detail['property_id'])

        rate_plan_3_name = "NEXT_SEASON_RATE_PLAN_1"
        response = self.add_rate_plan(rate_plan_3_name, booking_engine_detail["season_id_2"], booking_engine_detail["penalty_id_1"],  data, booking_engine_detail['property_id'])
        rate_plan_3_id = response["rateplan"].pop("id")
        time.sleep(5)
        self.activate_rate_plan(rate_plan_3_id, booking_engine_detail['property_id'])
        response["rateplan"]['inclusions']['inclusions'] = [{"iconClass":"fa-wifi","name":"Free Wi-Fi","type":"defaultInclusion","icon":"wifi"},{"iconClass":"fa-cutlery","name":"Breakfast included","type":"defaultInclusion","icon":"cutlery"}]
        response["rateplan"]["active"] = True 
        updated_response = self.add_room_price(response)
        self.update_rate_plan(rate_plan_3_id, updated_response, booking_engine_detail['property_id'])

        rate_plan_4_name = "NEXT_SEASON_RATE_PLAN_2"
        response = self.add_rate_plan(rate_plan_4_name, booking_engine_detail["season_id_2"], booking_engine_detail["penalty_id_1"], data, booking_engine_detail['property_id'])
        rate_plan_4_id = response["rateplan"].pop("id")
        time.sleep(5)
        self.activate_rate_plan(rate_plan_4_id, booking_engine_detail['property_id'])
        response["rateplan"]["applicableDays"] = {"Wed" :{'status':True, 'relative_value':0},"Sun" :{'status':True, 'relative_value':0},"Fri":{'status':True, 'relative_value':0},"Tue":{'status':True, 'relative_value':0},"Mon":{'status':True, 'relative_value':0},"Thu":{'status':True, 'relative_value':0},"Sat":{'status':True, 'relative_value':0}}
        response["rateplan"]["baseplan"] = rate_plan_3_id
        response["rateplan"]["relative_value"] = -10
        response["rateplan"]["relative_unit"] = "PERCENTAGE"
        response["rateplan"]["active"] = True 
        self.update_rate_plan(rate_plan_4_id, response, booking_engine_detail['property_id'])

        rate_plan_5_name = "PROMO_CODE_CHECK_RATE_PLAN"
        response = self.add_rate_plan(rate_plan_5_name, booking_engine_detail["season_id_1"], booking_engine_detail["penalty_id_1"], data, booking_engine_detail['property_id'])
        rate_plan_5_id = response["rateplan"].pop("id")
        time.sleep(5)
        self.activate_rate_plan(rate_plan_5_id, booking_engine_detail['property_id'])
        response["rateplan"]["active"] = True 
        response["rateplan"]["promocode"] = "TEST"
        updated_response = self.add_room_price(response)
        self.update_rate_plan(rate_plan_5_id, updated_response, booking_engine_detail['property_id'])

        rate_plan_6_name = "rate_plan_106"
        response = self.add_rate_plan(rate_plan_6_name, booking_engine_detail["season_id_3"], booking_engine_detail["penalty_id_1"], data, booking_engine_detail['property_id'])
        rate_plan_6_id = response["rateplan"].pop("id")
        time.sleep(5)
        self.activate_rate_plan(rate_plan_6_id, booking_engine_detail['property_id'])
        response["rateplan"]["active"] = True 
        response["rateplan"]['constraints'][3]['enabled'] = True
        response["rateplan"]['constraints'][3]['value'] = 3
        updated_response = self.add_room_price(response)
        self.update_rate_plan(rate_plan_6_id, updated_response, booking_engine_detail['property_id'])

        rate_plan_7_name = "rate_plan_107"
        response = self.add_rate_plan(rate_plan_7_name, booking_engine_detail["season_id_4"], booking_engine_detail["penalty_id_1"], data, booking_engine_detail['property_id'])
        rate_plan_7_id = response["rateplan"].pop("id")
        time.sleep(5)
        self.activate_rate_plan(rate_plan_7_id, booking_engine_detail['property_id'])
        response["rateplan"]["active"] = True 
        response["rateplan"]["promocode"] = "TEST"
        updated_response = self.add_room_price(response)
        self.update_rate_plan(rate_plan_7_id, updated_response, booking_engine_detail['property_id'])


    def tearDown(self):
        self.db.close()


if __name__ == "__main__":
    unittest.main()

