import requests
import json
import MySQLdb
import unittest
import sys, os
from datetime import datetime
import calendar
from dateutil import relativedelta

class AddingCancellationPenalties(unittest.TestCase):
    def add_penalty(self, penalty_name, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/penalties"
        payload = {"penalty":{"name":penalty_name,"description":"","tax_included":False,"enabled":True,"rules":[]}}
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        print "Add season Response status code:", response.status_code
        resp_dict = json.loads(response.text)
        return  resp_dict['penalty']['id']

    def test_cancellation_penalties(self):
        with open("booking_engine_detail.json",'r') as f:
            booking_engine_detail = json.loads(f.read())
            f.close()

        penalty_name_1 = "Penalty"
        penalty_id_1 = self.add_penalty(penalty_name_1, booking_engine_detail['property_id'])
        print penalty_id_1
                
        with open("booking_engine_detail.json",'r') as f:
            booking_engine_detail = json.loads(f.read())
            f.close()
        
        booking_engine_detail['penalty_name_1'] = penalty_name_1
        booking_engine_detail['penalty_id_1'] = penalty_id_1
        print booking_engine_detail
        with open('booking_engine_detail.json', 'w') as outfile:
            json.dump(booking_engine_detail, outfile)   


if __name__ == "__main__":
    unittest.main()
