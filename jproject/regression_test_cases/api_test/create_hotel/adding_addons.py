import requests
import json
import unittest

class AddingAddons(unittest.TestCase):
    def add_addons(self, addon_name, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/addons"
        payload = {"addon":{"name":addon_name,"active":False,"description":None,"base_add_on":None,"enabled":True,
                    "relative_value":None,"relative_unit":None,"taxes":[],"applicable_to":1,"applicable_for":1,
                    "prices":[{"no_of_adults":0,"price":"0"},{"no_of_adults":1,"price":"0"}],"icon":"fa-asterisk",
                    "addon_type":1,"default_icons_list":{},"split_price":True}}
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        print "Add Rate Plan Response status code:", response.status_code
        resp_dict = json.loads(response.text)
        return  resp_dict


    def activate_addons(self, rate_plan_id, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/addons/"+str(rate_plan_id)+"/activate"
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.put(url, headers=headers)
        print "Activate Rate Plan Response status code:", response.status_code

    def update_addons(self, rate_plan_id, payload, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/addons/"+str(rate_plan_id)
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.put(url, data=json.dumps(payload), headers=headers)
        print "Update Rate Plan Response status code:", response.status_code

    
    def test_adding_addons(self):
        with open("booking_engine_detail.json",'r') as f:
            booking_engine_detail = json.loads(f.read())
            f.close()

        addon_name_1 = "addons_1"
        response = self.add_addons(addon_name_1, booking_engine_detail['property_id'])
        addon_id_1 = response["addon"].pop("id")
        self.activate_addons(addon_id_1, booking_engine_detail['property_id'])
        response["addon"]["active"] = True
        response["addon"]['description'] = "test Description"
        response["addon"]['addon_type'] = 1
        response["addon"]['applicable_for'] = 1
        response["addon"]['applicable_to'] = 1
        response["addon"]['split_price'] = True
        response["addon"]["prices"][0]['price'] = 1000
        response["addon"]["prices"][1]['price'] = 500
        self.update_addons(addon_id_1, response, booking_engine_detail['property_id'])

        addon_name_2 = "addons_2"
        response = self.add_addons(addon_name_2, booking_engine_detail['property_id'])
        addon_id_2 = response["addon"].pop("id")
        self.activate_addons(addon_id_2, booking_engine_detail['property_id'])
        response["addon"]["active"] = True
        response["addon"]['description'] = "test Description"
        response["addon"]['addon_type'] = 1
        response["addon"]['applicable_for'] = 2
        response["addon"]['applicable_to'] = 1
        response["addon"]['split_price'] = True
        response["addon"]["prices"][0]['price'] = 1000
        response["addon"]["prices"][1]['price'] = 500
        self.update_addons(addon_id_2, response, booking_engine_detail['property_id'])

        addon_name_3 = "addons_3"
        response = self.add_addons(addon_name_3, booking_engine_detail['property_id'])
        addon_id_3 = response["addon"].pop("id")
        self.activate_addons(addon_id_3, booking_engine_detail['property_id'])
        response["addon"]["active"] = True
        response["addon"]['description'] = "test Description"
        response["addon"]['addon_type'] = 1
        response["addon"]['applicable_for'] = 1
        response["addon"]['applicable_to'] = 2
        response["addon"]['split_price'] = False
        response["addon"]["prices"] = [{'no_of_adults': 1,'price' : 100}]
        self.update_addons(addon_id_3, response, booking_engine_detail['property_id'])

        addon_name_4 = "addons_4"
        response = self.add_addons(addon_name_4, booking_engine_detail['property_id'])
        addon_id_4 = response["addon"].pop("id")
        self.activate_addons(addon_id_4, booking_engine_detail['property_id'])
        response["addon"]["active"] = True
        response["addon"]['description'] = "test Description"
        response["addon"]['addon_type'] = 1
        response["addon"]['applicable_for'] = 2
        response["addon"]['applicable_to'] = 2
        response["addon"]['split_price'] = False
        response["addon"]["prices"] = [{'no_of_adults': 1,'price' : 100}]
        self.update_addons(addon_id_4, response, booking_engine_detail['property_id'])


        booking_engine_detail['addon_name_1'] = addon_name_1
        booking_engine_detail['addon_id_1'] = addon_id_1
        booking_engine_detail['addon_name_2'] = addon_name_2
        booking_engine_detail['addon_id_2'] = addon_id_2
        booking_engine_detail['addon_name_3'] = addon_name_3
        booking_engine_detail['addon_id_3'] = addon_id_3
        booking_engine_detail['addon_name_4'] = addon_name_4
        booking_engine_detail['addon_id_4'] = addon_id_4
        print booking_engine_detail
        with open('booking_engine_detail.json', 'w') as outfile:
            json.dump(booking_engine_detail, outfile) 

if __name__ == "__main__":
    unittest.main()

