from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.action_chains import ActionChains 
import unittest, time, re
print "------------------------------------------------------------------------"
print "Create New Hotel"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("create_new_hotel")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Addiing Rooms"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_rooms")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Adding Season"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_season")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Adding Cancellation Penalties"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_cancellation_penalties")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Adding Taxes and Fees"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_taxes_and_fees")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Adding Addons"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_addons")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
print "------------------------------------------------------------------------"
print "Adding Rate Plans"
print "------------------------------------------------------------------------"
suites = [unittest.defaultTestLoader.loadTestsFromName("adding_rate_plans")]
testSuite = unittest.TestSuite(suites)
text_runner = unittest.TextTestRunner().run(testSuite)
