import requests
import json
import MySQLdb
import unittest
import sys, os
from datetime import datetime
import calendar
from dateutil import relativedelta

class AddingTaxesAndFees(unittest.TestCase):
    def add_taxes(self, taxe_name, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/taxes"
        payload = {"tax":{"name":taxe_name,"value":10,"unit_id":"PERCENTAGE","description":"","compounded":False,"enabled":True,"override_tax_enabled":False,"overrideTax":[]}}
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        print "Add taxe Response status code:", response.status_code
        resp_dict = json.loads(response.text)
        return  resp_dict['tax']['id']

    def update_taxes(self, taxe_name, taxe_id, unit_id, max_value, min_value, tax_value, property_id):
        url = "http://staging.simplotel.com/api/v1/hotel/"+str(property_id)+"/taxes/"+str(taxe_id)
        payload = {"tax":{"name":taxe_name,"value":10,"unit_id":unit_id,"description":"Tax","compounded":False,"enabled":True,"override_tax_enabled":True,"overrideTax":[{"max_value":max_value,"min_value":min_value,"tax_value":tax_value,"tax_id":taxe_id}]}}
        headers = {'Authorization':'Basic c2ltcGFkbWluOnNpbXBsZWhvdGVsYWRtaW4hQCM=','Content-Type': 'application/json'}
        response = requests.put(url, data=json.dumps(payload), headers=headers)
        print "Update taxe Response status code:", response.status_code
    
    def test_adding_tax(self):
        with open("booking_engine_detail.json",'r') as f:
            booking_engine_detail = json.loads(f.read())
            f.close()

        taxe_name_1 = "tax_1"
        taxe_id_1 = self.add_taxes(taxe_name_1, booking_engine_detail['property_id'])
        print taxe_id_1
        self.update_taxes(taxe_name_1, taxe_id_1, "FIXEDAMOUNT", 9999, 0, 10, booking_engine_detail['property_id'])
        taxe_name_2 = "tax_2"
        taxe_id_2 = self.add_taxes(taxe_name_2, booking_engine_detail['property_id'])
        print taxe_id_2
        self.update_taxes(taxe_name_2, taxe_id_2, "PERCENTAGE", 500, 5, 15, booking_engine_detail['property_id'])

        with open("booking_engine_detail.json",'r') as f:
            booking_engine_detail = json.loads(f.read())
            f.close()
        
        booking_engine_detail['taxe_name_1'] = taxe_name_1
        booking_engine_detail['taxe_id_1'] = taxe_id_1
        booking_engine_detail['taxe_name_2'] = taxe_name_2
        booking_engine_detail['taxe_id_2'] = taxe_id_2
        print booking_engine_detail
        with open('booking_engine_detail.json', 'w') as outfile:
            json.dump(booking_engine_detail, outfile)   


if __name__ == "__main__":
    unittest.main()
