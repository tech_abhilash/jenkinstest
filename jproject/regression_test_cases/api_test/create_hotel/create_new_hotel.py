# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
import os
import json
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "config"))
import hotel_info
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "simpadmin/simpadmin_common_function"))
import create_data_common_function
sys.path.append(os.path.join(os.path.dirname("create_hotel"), '../..', "booking_engine/booking_engine_common_function"))
import creating_booking_engine_data_common_function


class TestHotelCreation(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = hotel_info.Admin_Url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def save_theme_and_logo(self):
        d = self.driver
        d.find_element_by_xpath(".//*[@id='navbar']/ul[1]/li[1]/a").click()
        d.find_element_by_link_text("  Design").click()
        time.sleep(5)
        d.find_element_by_link_text("Logo Settings").click()
        time.sleep(5)
        d.find_element_by_xpath("//*[@type='submit']").click()

    def save_property_id(self):
        d = self.driver
        d.find_element_by_link_text("Ops").click()
        url = d.current_url.split("/")
        dict_save = {}
        dict_save['property_id'] = url[4]
        print dict_save
        with open('booking_engine_detail.json', 'w') as outfile:
            json.dump(dict_save, outfile)

    def test_create_hotel(self):
        d = self.driver
        d.get(self.base_url + "/")
        d.maximize_window()
        admin = create_data_common_function 
        booking_engine = creating_booking_engine_data_common_function
        
        #-------Login to admin------
        admin.login_to_admin(self, hotel_info.admin_user_name, hotel_info.admin_password)
        
        #-------Creating  New Hotel ------
        admin.add_hotel_name(self, hotel_info.api_test_hotel_name)

        #--Hotel Initial data ------
        admin.add_hote_initial_data(self, hotel_info.hote_initial_data)

        #--Hotel Timezone ------
        admin.add_hotel_time_zone(self, hotel_info.hote_initial_data["hotel_time_zone"])

        #--Hotel Currency ------
        admin.add_hotel_currency(self, hotel_info.hote_initial_data["hotel_currency"])

        #--Hotel city ------
        admin.add_hotel_city(self, hotel_info.hote_initial_data["city_name"])

        #--Hotel language ------
        admin.add_hotel_language(self, hotel_info.hote_initial_data["hotel_language"])

        #--Hotel Country ------
        admin.add_hotel_country(self, hotel_info.hote_initial_data["hotel_country"])

        d.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(10)                                 
        self.assertEqual(d.find_element_by_xpath("//*[@class='info']/label[1]/h4").text, hotel_info.api_test_hotel_name)
        print "Hotel has been Created"
        
        #----design-------
        self.save_theme_and_logo()

        # ------- Enable Simplotel BE-----
        booking_engine.enable_simplotel_be(self)

        # --------Selecting Simplotel BE-----------
        booking_engine.selecting_simplotel_be(self)

        # --------Save Payment Gateway-----------------------
        booking_engine.save_payment_gateway(self)

        # --------Save Booking Engine Messages---------------
        booking_engine.save_booking_engine_messages(self)

        # ------- Enable Simplotel BE Email Slider----
        booking_engine.enable_simplotel_be_email_slider(self)

        # --------Selecting Simplotel BE Advertisement-------
        booking_engine.enable_simplotel_be_advertisement(self)

        # --------Selecting Simplotel BE Pay now-------------
        booking_engine.enable_simplotel_pay_now(self)

        # --------Selecting Simplotel BE Pay now-------------
        booking_engine.enable_otp_for_pay_at_hotel(self)

        #---------Save Hotel Property Id----------------------
        self.save_property_id()

        d.find_element_by_link_text("Preview").click()
        d.find_element_by_id("idGenerateSite").click()
        time.sleep(2)


    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
