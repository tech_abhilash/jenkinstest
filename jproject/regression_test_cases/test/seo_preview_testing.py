# -*- coding: utf-8 -*-
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
# import seo_info_data
import seo_elements_path
import sys
import os
# sys.path.append(os.path.join(os.path.dirname("seo"), '..', ''))
# import hotel_info


class Untitled2(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1024, 768))
        self.display.start()
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://www.goodhoteldemo.info/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath).text
        except NoSuchElementException:
            return False
        return True
    def check_exists_by_link_text(self, xpath):
        try:
            self.driver.find_element_by_link_text(xpath).text
        except NoSuchElementException:
            return False
        return True

    def test_untitled2(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.maximize_window()

        #------Testing GA Code present in Desktop 1st level---------------------
        print "-------------------GA Code, Canonical Tag and Alternate Tag in Desktop 1st Level------------------------------"
        html_source = driver.page_source

        if "UA-1234567-8" in html_source:
            print "GA - Code Present Desktop 1st Level"
        else:
            print "GA - Code Not Present Desktop 1st Level"

        if '<link href="http://www.goodhoteldemo.info/" rel="canonical" />' in html_source:
            print "Canonical Tag Present Desktop 1st Level"
        else:
            print "Canonical Tag Not Present Desktop 1st Level"

        if '<link href="http://www.goodhoteldemo.info/m/" media="only screen and (max-width: 640px)" rel="alternate" />' in html_source:
            print "Alternate Tag Present Desktop 1st Level"
        else:
            print "Alternate Tag Not Present Desktop 1st Level"
        
        #------Testing GA Code present in Desktop 2nd level---------------------
        print "-------------------GA Code, Canonical Tag and Alternate Tag in Desktop 2nd Level------------------------------"
        driver.get(self.base_url + "/rooms/test-room.html")
        driver.maximize_window()
        html_source = driver.page_source

        if "UA-1234567-8" in html_source:
            print "GA - Code Present Desktop 2nd Level"
        else:
            print "GA - Code Not Present Desktop 2nd Level"

        if '<link href="http://www.goodhoteldemo.info/rooms/test-room.html" rel="canonical" />' in html_source:
            print "Canonical Tag Present Desktop 2nd Level"
        else:
            print "Canonical Tag Not Present Desktop 2nd Level"

        if '<link href="http://www.goodhoteldemo.info/m/rooms/test-room.html" media="only screen and (max-width: 640px)" rel="alternate" />' in html_source:
            print "Alternate Tag Present Desktop 2nd Level"
        else:
            print "Alternate Tag Not Present Desktop 2nd Level"

        #------Testing GA Code present in Mobile----------------------- 
        print "-------------------GA Code, Canonical Tag and Alternate Tag in Mobile 1st Level------------------------------"    
        driver.get(self.base_url + "/m")
        driver.maximize_window()

        if "UA-1234567-8" in html_source:
            print "GA - Code Present Mobile 1st Level"
        else:
            print "GA - Code Not Present Mobile 1st Level"
            
        html_source = driver.page_source

        if '<link href="http://www.goodhoteldemo.info/" rel="canonical" />' in html_source:
            print "Canonical Tag Present Mobile 1st Level"
        else:
            print "Canonical Tag Not Present Mobile 1st Level"

        if 'rel="alternate' in html_source:
            print "Alternate Tag Present Mobile 1st Level"
        else:
            print "Alternate Tag Not Present Mobile 1st Level"
        
        #------Testing GA Code present in Mobile 2nd level---------------------
        print "-------------------GA Code, Canonical Tag and Alternate Tag in Mobile 2nd Level------------------------------"
        driver.get(self.base_url + "/m/rooms/test-room.html")
        driver.maximize_window()
        html_source = driver.page_source

        if "UA-1234567-8" in html_source:
            print "GA - Code Present Mobile 2nd Level"
        else:
            print "GA - Code Not Present Mobile 2nd Level"

        if '<link href="http://www.goodhoteldemo.info/rooms/test-room.html" rel="canonical" />' in html_source:
            print "Canonical Tag Present Mobile 2nd Level"
        else:
            print "Canonical Tag Not Present Mobile 2nd Level"

        if 'rel="alternate' in html_source:
            print "Alternate Tag Present Mobile 2nd Level"
        else:
            print "Alternate Tag Not Present Mobile 2nd Level"

        driver.close()
        self.display.stop()

        # #-----------Testing Site Map HTML-------------------------------
        # print "-------------------Site Map HTML------------------------------"
        # driver.get(self.base_url + "/sitemap.html")
        # driver.maximize_window()

        # time.sleep(3)
        # print driver.find_element_by_xpath(seo_elements_path.site_map_h1_path).text
        # self.assertEqual(driver.find_element_by_xpath(seo_elements_path.site_map_h1_path).text,"Sitemap")

        # path = seo_elements_path.site_map_hotel_name_path
        # x = self.check_exists_by_xpath(path)
        # if (x == True):
        #     self.assertEqual(hotel_info.Hotel_Name,driver.find_element_by_xpath(seo_elements_path.site_map_hotel_name_path).text)
        #     print "Hotel Name Prsent"
        # else:
        #     print "Hotel Name Not Prsent"

        
        # x = self.check_exists_by_link_text("Home")
        # if (x == True):
        #     print "Home tab Prsent"
        # else:
        #     print "Home tab Not Prsent"

        # x = self.check_exists_by_link_text("Rooms")
        # if (x == True):
        #     print "Rooms tab Prsent"
        # else:
        #     print "Rooms tab Not Prsent"

        # x = self.check_exists_by_link_text("Test Room")
        # if (x == True):
        #     print "Test Room tab Prsent"
        # else:
        #     print "Test Room tab Not Prsent"

        # x = self.check_exists_by_link_text("Gallery")
        # if (x == True):
        #     print "Gallery tab Prsent"
        # else:
        #     print "Gallery tab Not Prsent"

        # x = self.check_exists_by_link_text("Location")
        # if (x == True):
        #     print "Location tab Prsent"
        # else:
        #     print "Location tab Not Prsent"
       
        # x = self.check_exists_by_link_text("Promotions")
        # if (x == True):
        #     print "Promotions tab Prsent"
        # else:
        #     print "Promotions tab Not Prsent"

        # x = self.check_exists_by_link_text("Test Promotion")
        # if (x == True):
        #     print "Test Promotion tab Prsent"
        # else:
        #     print "Test Promotion tab Not Prsent"
            
        # x = self.check_exists_by_link_text("Custom Menu")
        # if (x == True):
        #     print "Custom Menu tab Prsent"
        # else:
        #     print "Custom Menu tab Not Prsent"

        # x = self.check_exists_by_link_text("Custom Page")
        # if (x == True):
        #     print "Custom Page tab Prsent"
        # else:
        #     print "Custom Page tab Not Prsent"

        # #-----------Testing robots.txt-------------------------------

        # print "-------------------Robots-Txt-----------------------------"
        # driver.get(self.base_url + "/robots.txt")
        # driver.maximize_window()
        # time.sleep(2)
        # print driver.find_element_by_xpath("/html/body").text   
        # self.assertEqual(driver.find_element_by_xpath("/html/body").text,"User-agent: * \nDisallow: /") 

        # #-----------Testing Site Map Xml-------------------------------
        # driver.get(self.base_url + "/sitemap.xml")
        # driver.maximize_window()
        # time.sleep(2)
        # html_source = driver.page_source

        # print "-------------------Site Map XML Canonical Tag------------------------------"
        # if "<loc>http://www.goodhoteldemo.info/</loc>" in html_source:
        #     print "Site Map Xml - Canonical Tag - Home Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - Home Page not Prsent"

        # if "http://www.goodhoteldemo.info/custom-menu.html" in html_source:
        #     print "Site Map Xml - Canonical Tag - custom summary Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - custom summary Page not Prsent"

        # if "http://www.goodhoteldemo.info/custom-menu/custom-page.html" in html_source:
        #     print "Site Map Xml - Canonical Tag - custom detail Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - custom detail Page not Prsent"

        # if "http://www.goodhoteldemo.info/promotions.html" in html_source:
        #     print "Site Map Xml - Canonical Tag - promotion summary Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - promotion summary Page not Prsent"

        # if "http://www.goodhoteldemo.info/promotions/test-promotion.html" in html_source:
        #     print "Site Map Xml - Canonical Tag - promotion detail Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - promotion detail Page not Prsent"

        # if "http://www.goodhoteldemo.info/location.html" in html_source:
        #     print "Site Map Xml - Canonical Tag - location Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - location Page not Prsent"

        # if "http://www.goodhoteldemo.info/gallery.html" in html_source:
        #     print "Site Map Xml - Canonical Tag - gallery Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - gallery Page not Prsent"

        # if "http://www.goodhoteldemo.info/rooms.html" in html_source:
        #     print "Site Map Xml - Canonical Tag - rooms summary Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - rooms summary Page not Prsent" 

        # if "http://www.goodhoteldemo.info/rooms/test-room.html" in html_source:
        #     print "Site Map Xml - Canonical Tag - rooms detail Page Prsent"
        # else:
        #     print "Site Map Xml - Canonical Tag - rooms detail Page not Prsent"

        # #--------------------------------------------------------------
        # print "-------------------Site Map XML Alternate Tag------------------------------"
        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - Home Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - Home Page not Prsent"

        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/custom-menu.html" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - custom summary Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - custom summary Page not Prsent"

        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/custom-menu/custom-page.html" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - custom detail Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - custom detail Page not Prsent"

        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/promotions.html" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - promotion summary Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - promotion summary Page not Prsent"

        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/promotions/test-promotion.html" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - promotion detail Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - promotion detail Page not Prsent"

        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/location.html" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - location Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - location Page not Prsent"

        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/gallery.html" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - gallery Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - gallery Page not Prsent"

        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/rooms.html" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - rooms summary Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - rooms summary Page not Prsent" 

        # if '<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.goodhoteldemo.info/m/rooms/test-room.html" />' in html_source:
        #     print "Site Map Xml - Alternate Tag - rooms detail Page Prsent"
        # else:
        #     print "Site Map Xml - Alternate Tag - rooms detail Page not Prsent"



    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
