Admin_Url = "http://staging.simplotel.com" #"http://qaadmin.simplotel.com" #"http://staging.simplotel.com"
Preview_Url = "http://previewsite.simplotel.com/A19B0AD1E664FA38/"
admin_user_name = "simpadmin"
admin_password = "simplehoteladmin!@#"
hotel_name = "test hotel 3rd Oct-2017"
api_test_hotel_name = "Api Test Hotel 9th Oct-2017"
user_name = "test_hotel_3_oct_2017"
password = "test2"

hote_initial_data = {"hotel_tag_line" : "booking auto",
						"hotel_description" : "simp-hotel",
						"location_name" : "Simplotel",
						"hotel_url" : "staging-static.simplotel.com",
						"contact_email" : "shabari@simplotel.com",
						"contact_email_2" : "abhilash@simplotel.com",
						"contact_phone" : "8971412668",
						"contact_phone_label" : "Phone Number 1",
						"contact_phone_2" : "8123043923",
						"contact_phone_label_2" : "Phone Number 2",
						"hotel_time_zone" : "Asia/Kolkata",
						"hotel_currency" : "Indian Rupee",
						"hotel_language" : "English",
						"hotel_country" : "India",
						"city_name" : "Bangalore"}

social_media = {"trip_advisor_link" : "http://www.tripadvisor.in/Hotel_Review-g304551-d304415-Reviews-The_Grand_New_Delhi-New_Delhi_National_Capital_Territory_of_Delhi.html",
					"facebook_link" : "https://www.facebook.com/pages/The-Orchid-HotelMumbai/185561521498209" ,
					"google_plus_link" : "https://plus.google.com/+Simplotel/posts",
					"twitter_link" : "https://twitter.com/Simplotel",
					"linkedIn_link" : "https://www.linkedin.com/company/simplotel",
					"pinterest_link" : "https://www.pinterest.com/Simploteltech/",
					"instagram_link" : "https://www.instagram/",
					"youtube_link" : "https://www.youtube link/"}


#-------------------------------Chain-------------------------------
chain_hotel_name = "Chain test hotel 9th Oct-2017"
chain_user_name = "chain_test_hotel_9_Oct_2017"
chain_password = "test2"
chain_child_hotel_name_one = "Child 1st test hotel 9th Oct-2017"
chain_child_hotel_name_two = "Child 2nd test hotel 9th Oct-2017"
chain_child_hotel_name_three = "Child 3rd test hotel 9th Oct-2017"
chain_child_hotel_name_fourth = "Child 4th test hotel 9th Oct-2017"

child_hotel_one_url = "child-one_9_Oct_2017"
child_hotel_two_url = "child-two_9_Oct_2017"
child_hotel_three_url = "child-three_9_Oct_2017"
child_hotel_fourth_url = "child-fourth_9_Oct_2017"
chain_Preview_Url = "http://previewsite.simplotel.com/8FCDF7928C2A195F/"
#----------------------Navigation Page-------------------------------
group_menu_name = "our Hotel"
group_name_one = "Goa"
group_name_two = "bangalore"
group_name_three = "delhi"